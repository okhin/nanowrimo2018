# Chapitre 4.

> [ rumeurs.net/noc ]
> __ > Je suis le seul à avoir des problèmes de déco ?
> * __ has quit (ping timeout)
> * H+ has quit (ping timeout)
> * FleshOverMetal has quit (ping timeout)
> * u+1f595 has quit (ping timeout)
> * __ joined noc
> __ > Bon, si je trouve le connard qui joue avec la connexion ici, ça va
mal se mettre 

> Mel > __ on t'a déjà dit que ce bloc est en jachère, jack. Et
d'ailleurs, ce ne sera pas stable avant un bail. 

> __ > En jachère ? Mais depuis quand ?

> Mel > Depuis que t'as cogné la dernière équipe qu'on a envoyé essayer de
régler les déconnexions.

> * __ has quit (kicked by Mel: Bye bye) 

***

La nef du Hanged Billionaire, installé dans l'ancienne cathédrale de
Kónigsberg, commence à se remplir. Les vitraux, reprenant les codes du
réalisme socialiste, projettent leurs motifs lumineux sur les arrivants,
les baignant de tons chauds et rouge. Les images des saints et des figures
du socialisme qui ornaient autrefois la cathédrale ont été depuis
remplacées par les créations de différents artistes du Complexe. Chaque
vitrail représente des éléments de la société de consommation en reprenant
les codes des cartes à jouer, opposant tête bêche les vertus de la
consommation et leur impact sur les individus et le monde. Chaque figure
est ornée de créatures baroques qui semble les attaquer, les mettre à mal.

En dépit de la diversité des artistes ayant participé à la création de ces
œuvres monumentales, il y a une cohérence dans le choix des couleurs, dans
les postures reprenant les codes des propagandistes du vingtième siècle
qui permet de mettre en évidence les spécificités de chaque artiste, de
chaque scènette, représentée tantôt avec une richesse de détails, une
finesse de trait rendue possible uniquement par les techniques de forge
moderne, et tantôt abstraite à un nombre de traits, de teintes, réduits,
chaque courbe minutieusement choisie pour rendre la scène lisible,
transmettre de l'énergie, du mouvement, à une représentation statique.

Ces vitraux, encadrant la nef et le chœur, convergent vers les rosaces du
transept, rosaces qui représentent des scènes de foules dansantes et
d'orgies, les corps formant une spirale s'éloignant du centre dont la
tonalité passe d'un orange chaud jusqu'à un mauve froid. Les deux rosaces
sont riches des techniques et des variations stylistiques du travail
collectif de tous les autres artisans et artistes qui ont travaillé sur
les vitraux précédents.

L'ex-voto kaléidoscopique projette le souvenir des fêtes et des
rassemblements qui ont été tenus ici sur la peau des danseurs et des
danseuses, non sans rappeler, par écho mémétique, les créatures qui
habitent les vitraux.

L'orgue monumental qui surplombe le vestibule, est maintenant encadré de
gigantesques enceintes, propulsant vers le chœur les ondes sonores créées
par les DJ et musiciens qui agitent les individus situés en contrebas. Une
scène, suspendue à mi-hauteur entre l'orgue et le sol par une toile de
chaîne, de câbles électriques et d'échelles métallique, sert de podium
pour les danseurs et danseuses qui le veulent. Enny est déjà en place, nue
comme un ver. Les flashs stroboscopiques qui l'éclairent donne
l'impression que ses mouvements sont saccadés, la lumière noire faisant
ressortie les motifs phosphorescents qu'elle a peints sur son corps,
soulignant les courbes. L'ensemble donne l'impression que des lignes de
lumière phosphorescente l'enroule comme un serpent, et semblent bouger de
manière propre, les mouvements de la danseuse semblant être à contretemps
de ceux des lignes sinueuses qui la couvre des pieds à la tête.

Les chapelles qui encadrent la nef, dont certaines sont maintenant
ouvertes vers l'extérieur, sont devenus des lieux d'exposition et
d'intimité. Au fur et à mesure, des extensions y ont été ajoutées, se
développant tels des nids d'insectes sur le tronc du bâtiment, fournissant
aux fêtards et fêtardes des nids dans lesquels elles peuvent s'installer
discuter, boire ou manger, et s'envoyer en l'air. Ces extensions, se
développant régulièrement, sont majoritairement créées à partir des
restes des camions et véhicules qui ont été abandonnées lors de
l'évacuation de la ville.

Le tout forme un syncrétisme culturel particulier, mêlant les flèches
gothiques de la cathédrale aux cadavres de rouille forme une étrange
sépulture aux monstres 
routiers ayant dévastés une bonne partie de l'écosystème local. Les
nombreux jeux de lumière projettent des ombres colorées sur les arrètes
saillantes des engins, maintenus ensemble par un réseau micellaire
permettant de répartir courant électrique et fibre optique sur la
plupart de ces annexes.

Les chapelles et ces annexes, forment une sorte de labyrinthe faiblement
éclairé et diffusant, via divers haut-parleurs plus ou moins dissimulés,
la musique jouée dans la nef. Les habitacles et alcôves, décorées et
aménagée sans concertations, peuvent être fermées par une tenture ou une
porte de voiture, fournissant un cocon de métal de et de faux cuir aux
personnes à l'intérieur et cherchant un peu de calme et d'intimité.

Saina est restée quelques instants stupéfaite par le mélange de
l'architecture gothique avec le style postindustriel / cybernétique de la
décoration réalisées par l'accumulation des infrastructures nécessaires au
fonctionnement du lieu, de patchs appliqués pour protéger certains
éléments sous tension et d'engins à la croisée de la robotique et de l'art
qui ne peut être que le résultat d'un travail collectif non concerté,
l'expression artistique d'une conscience de groupe.

Il fait chaud pour une soirée de septembre, même pour le deuxième automne
de l'année et il y a déjà beaucoup de monde qui danse alors que le soleil
n'est pas encore complètement couché, dans divers stades d'ébriété et de
nudité, comme si certains sont là depuis plusieurs jours. Saina réalise
que c'est probablement le cas, au moins pour certaines, à en juger par
l'état des corps mélangés, vibrant au rythme des pulsations d'afro-beat
balancés à pleine puissance et résonnant dans l'acoustique monacale de
la cathédrale.

Cependant, il n'y a aucune trace de Valkyrie pour le moment. Personne ne
semble y prêter attention, les DJ enchaînant les sets les uns après les
autres, personne ne ressent le besoin d'écouter quelque chose de
spécifique, dansant, riant, pleurant aussi parfois. Les personnes
présentes ne sont pas là pour un concert particulier, juste pour se sentir
bien. L'ancien autel religieux trônant au centre du chœur a laissé la
place à un bar fournissant à qui demande tout ce dont on peut avoir besoin
pour passer un bon moment, mais également des kits d'urgence en cas
d'overdose ou de blessure.

Considérer ce comptoir comme un bar est inadéquat. Un bar impliquerait la
présence de barmen et barmaids, alors que dans ce cas, la gestion est
laissée au public. Il y a bien des Amazons qui semblent surveiller tout
ça. Portant crânement le bouson orange et bleu du syndicat, et parfois
guère plus, les Amazons sont très présentes dans le lieu. Les blousons
orange sont visibles aussi bien dans la foule de plus en plus compacte,
que sur le dos d'une danseuse qui a rejoint Enny, ou bien à faire
régulièrement le tour de la cathédrale pour s'assurer que tout le monde va
bien, et, parfois, ramasser les kits d'injections usagés et cannettes
vides oubliées ou réapprovisionner le comptoir en nourriture et boissons.

Saina se sent décalée par rapport à l'ambiance du moment. À contretemps.
Le signal ici se résume à un bruit blanc, un léger bourdonnement qu'elle
perçoit essentiellement par les vibrations sur ses tempes. Le signal est
vide d'information, en dépit du nombre de terminaux connectés, elle ne
sent que les ping-pongs des appareils maintenant une connexion. Aucune
donnée n'est échangée dans le lieu ou, du moins, pas suffisamment pour que
cela soit perceptible.

Ce murmure perpétuel, léger mais bien présent, la déstabilise. Pour la
première fois depuis qu'elle est arrivée ici, elle est complètement
perdue, déconnectée. Elle ne voit le monde que par ce bruit blanc
monotone, et perd sa capacité d'empathie, sa faculté humaine, sociale, de
pouvoir s'accorder et comprendre les humeurs des personnes autour d'elle.
Les gens dansent, se touchent, se caressent, se jettent les uns sur les
autres ou restent seulement accoudés au bord, un verre à la main,
à s'imprégner de l'ambiance. Mais elle, elle est comme inexistante,
invisible. Branchée sur un canal de bruit blanc, sans sens ni contexte,
elle commence à se replier sur elle-même.

« … tu ? » entend-elle à peine. « Hey, ça va ? » répète la voix doucement.
Elle remarque maintenant la main sur son épaule. La main a les ongles
courts et les doigts abîmés de quelqu'un qui passe ses journées à s'en
servir. Elle est connectée par un poignet au reste d'une personne qui
cherche son regard. Les yeux violets caractéristiques des utilisateurs de
Purple Haze, les cheveux crépus et tressés, un blouson portant un logo
qu'elle a déjà vu avant mais dont elle n'arrive pas à retrouver la
provenance. Tous ces éléments disparates ne se connectent que
difficilement pour Saina.

« Tu as besoin d'aide ? articule doucement et à son oreille la voix des
yeux violets.

— Je crois oui, » répond Saina. La main des yeux attrape la sienne et la
tire doucement vers le transept. Elle la suit dans la pénombre des alcôves
avant qu'elle ne se sente assise sur une banquette en velours, elle sait
que les yeux lui parle, mais elle a perdu le sens, sa capacité de
comprendre et d'assembler les syllabes en vecteurs sémantique. La
dissonance entre ce que capte sa peau ordinateur et ses propres caméras
biologiques l'empêche de faire ce lien, inondant son cerveau de stress, le
forçant à se préserver et à préserver sa conscience.

Elle la laissa s'échapper. L'outil complexe permettant habituellement de
regrouper les taches de couleurs perçues par ses caméras biologiques, en
objet sémantique et culturels porteur de sens et d'expériences a cessé de
fonctionner. Pour préserver sa conscience, son cerveau a désactivé sa
capacité à ressentir et à comprendre le monde. Et tout se vide, comme si
sa conscience se rétractait vers son estomac, comme si ses pensées
étaient, d'un coup, devenues étrangères.

Il lui faut un peu de temps pour faire le point. Inconsciente et
pourtant lucide. Ici, au calme, elle retrouve lentement le sens des
signaux. Les yeux la fixe toujours, la main tient la sienne, le bruit
blanc se détache et le système de sauvegarde qui a cloisonné son esprit
relâche progressivement son étreinte, permettant à la conscience de Saina
de pouvoir enregistré et vivre de nouvelles expériences, comme si elle
sortait d'un rêve.

À côté d'elle, suffisamment proche pour pouvoir lui parler doucement mais
suffisamment éloignée pour ne pas la mettre mal à l'aise ou la forcer dans
une intimité qui pourrait aggraver les choses, Tonya, puisque c'est le nom
de la personne possédant les yeux et a main, la regarde revenir à elle.

« Tu vas mieux ? lui demande-t-elle.

— Je crois. C'est rien de grave, juste un petit vertige.

— Bien. » Puis, après un silence, elle ajoute « Tu veux quelque chose ?
Que je te laisse seule ou que j'aille chercher quelqu'un ?

— Non, c'est bon. Je boirais bien quelque chose cela dit.

— Je reviens.

Elle se lève, sort de l'alcôve puis, repassant la tête à travers l'arche
qui la sépare de la grappe de couloirs et d'alcôve similaire, demande

« J'y pense, tu n'as pas d'allergies alimentaire ? »

Saina répond en secouant doucement la tête. Pas d'allergies alimentaires
à sa connaissance. Elle profite du départ de Tonya pour déchiffrer
l'endroit où elle se trouve. Il s'agit probablement d'une cabine d'un de
ces vieux trains roulants trans-sibérien, trains routiers qu'elle connait
bien pour les avoir pas mal pratiqués dans son voyage jusqu'ici. Les
cabines sont des secondes maisons pour ces chauffeurs qui passent une
grande partie de leur temps seuls au milieu de nulle part, connectant les
villes et villages devenues accessibles depuis la fonte des glaces au
reste du monde.

Celui-là est un mélange entre l'une de ces cabines et un boudoir rococo.
Un paravent orné d'un triptyque d'hommes en train de baiser remplace une
des portières et sert de porte à la chapelle. Le fauteuil / banquette
/ lit est recouvert d'un velours sombre et est orientée dos au tableau de
bord, Le pare-brise a été remplacé par un écran et les commandes sur le
volant servent à parcourir une petite bibliothèque de clips de soft porn
mixés et arrangés par un algorithme local, donnant une vie parfois hyper
réaliste aux formes et corps dépeint dans ces clips.

À l'autre bout de la cabine, la portière a été transformé en une toilette
dont la console est jonchée de papiers gras, d'inhalateurs divers et de
cannettes vide. Le miroir de la toilette est juste un de ces vieux écrans
à cristaux liquide permettant de diffuser, avec un léger retard dû à la
vétusté de l'installation, les formes déformeés par morphing algorithmique
de tout ce qui est en face de lui. Les cristaux liquides cramés et
déformés transforme encore ces silhouettes pour les rendre presque
malaisantes, étrange, leurs caractéristiques évoquant aussi bien l'homme et
la machine et force les esprits à résoudre cet étrange paradoxe.

Saina va indubitablement mieux lorsque Tonya revient, poussant le paravent
de ses fesses et tenant deux grandes tasses en céramiques aux motifs usés,
dont s'échappe une forte odeur de cannelle. Sa légère
désorientation se dissipe au fur et à mesure qu'elle parcourt les objets
de décorations et l'agencement des choses ici. Elle est perdue, mais elle
sait que c'est une sensation qui doit être normale dans ces lieux.

Tonya s'installe sur la banquette à cîté de Saina, un peu plus près d'elle
que tout à l'heure et se présente avant de caler les tasses dans le porte
gobelets de l'accoudoire. Puis, ne laissant pas le silence entre elles
s'installer, Tonya démarre la conversation demandant à Saina pourquoi elle
est venue ici.

Saina hésite un peu avant de répondre. Elle n'est pas habituée à parler
d'elle ni à confronter sa vision du monde avec une étrangère. Là d'oú elle
vient, son chamanisme est toléré au mieux, et uniquement parce qu'elle
fournit des services aux communautés locales. L'essentiel de sa vie s'est
déroulé en isolation du monde, n'y prenant part qu'à la demande des autres
et essayant, le reste du temps, de ne pas l'altérer plus que sa survie
nécssite.

Elle baisse les yeux et se concentre sur sa tasse avant de prendre une
profonde inspiration et d'essayer d'expliquer à Tonya la raison pour
laquelle elle est venue ici. Plus exactement, elle essaye de ne pas trop
entrer dans le détail.

Après quelques phrases, elle parvient à se détendre un peu, la personne
qui est assise juste à côté d'elle, proche au point qu'elles peuvent
presque se toucher, la mets en confiance et semble montrer un intérêt pour
ce qu'elle dit.

Elle explique son rôle de guide dans le signal. Elle explique comment elle
utilise des états proches de la transe pour interpréter le signal émis par
toute machine. Elle explique le rôle fondamental et cependant
presque oublié, des langages machines, de leurs limitations, de leurs
forces. Elle explique que ses runes et ses mantras sont équivalents aux
lignes de code et widgets utilisés par les codeurs et hackers.

D'après elle, l'informatique est en équilibre entre math et linguistique,
et les chamanes, en tant qu'observateur en bordure de la société, priorise
l'approche linguistique et donc culturelle à la méthode purement
mathématique de l'industrie.

Tonya la laisse parler la plupart du temps, l'interrompant de questions
concises quand elle ne comprend pas, mais elle la laisse parler, écoutant
attentivement tout ce que la chamane a envie ou besoin de dire.

Elles restent à discuter, oubliant le temps qui passe, Tonya questionnant
Saina qui, en retour, explique les parallèles entre théorie des langages
et animisme, explique comment les cultures sont génératrices de language et
de pensée.

Puis Tonya lui pose des questions sur ses tatouages. Sur ses antennes sous
cutanée insérées autour de ses biceps. Sur les minuscules portes logiques
tatouées sur ses épaules. Sur les transistors à effet de champs qui
descendent le long de son épine dorsale. Tonya fait glisser ses doigts sur
la peau ordinateur de Saina, provoquant en elle une sensation qu'elle
a presque oubliée, celle de sentir la peau d'une autre personne sur la
sienne.

Déstabilisée, Saina inverse les rôles et c'est Tonya qui lui explique
comment c'est de grandir au Complexe, qu'elle y est née et que les choses
n'arretent pas de changer, tout en étant cependant un endroit stable.

Elle parle des jardins, des fermes sur les toits, des projets titanesques
du programme spatial ESP, des rencontres avec des gens de cultures très
variées, de comment la structure du Complexe permet, grâce à ses
habitations tournées vers elle-même, une cohabitation de personnes très
diverses, cohabitation qui ne pourrait pas fonctionner ailleurs.

Tonya n'as pas bougé sa main de la cuisse de Saina, là oú un groupe de
transistor à effet de champs, arrangé pour former une étrange créature qui
grimpe vers son abdomen, disparaissant sous son short. Les vibrations du
signal se fondent sous les doigts de Tonya, envoyant des messages
différents à ses centres nerveux, messages dont elle redécouvre
l'existence.

Elles s'arrêtent de parler, n'ayant plus envie de se raconter à l'autre.
Saina pose sa tête sur l'épaule de Tonya qui la serre contre elle. Elles
écoutent la musique étouffée qui parvient jusqu'à elles, réduite à une
pulsation de basse, légèrement décalée par rapport à leurs rythme
cardiaques.

Saina essaye d'embrasser Tonya qui l'interromps, lui mettant un doigt sur
les lèvres. Elle lui explique qu'il y a plusieurs personnes dans sa vie,
des amies, des amoureuses, des amantes. Elle lui explique aussi qu'elle
a besoin que Saina le sache. Et qu'elle a envide de danser un peu d'abord,
d'aller secouer leurs corps au rythme de la musique, l'une contre l'autre.
Et que Saina peut rester chez elle aussi, elle ne dérangera personne, il
y a de la place pour une chambre de plus et ça sera forcément mieux que la
chambre collective qu'elle doit occuper en ce moment. Et, dans la foulée,
elle embrasse rapidement Saina, avant de sauter sur ses pieds, de
l'attraper par la main et d'aller danser. 

De retour dans la nef, Saina découvre une ambiance différente. À moins que
ce ne soit elle qui soit dans un état d'esprit différent plus compatible
avec l'invitation à sauter et danser avec les autres. Elle a perdu de vue
Enny, qui doit pourtant être quelque part, ou le fait que les Valkyries ne
sont jamais venues jouer finalement. 

Les DJ, MC, groupes ou jams se succèdent, les artistes se croisent,
partagent l'espace scénique et sonore, créant quelque chose d'unique. Une
expérience qui ne peut être décrite à une autre personne, quelque chose
qu'il faut vivre pour la comprendre. Une expérience que des
enregistrements ne saurait restituer pleinement, quelque chose qui ne peut
être compris, intégré, qu'en étant ici.

Le soleil commence à poindre et projette ses rayons sur l'assemblée via
les rosaces de l'ancienne cathédrale. Saina commence à fatiguer et fait
signe à Tonya qu'elle a besoin de prendre l'air deux minutes.

Elles sortent, protégées de la fine pluie qui tombe sur le Complexe par
les auvents de toile déployées autour de l'aggrégat architectural.
D'autres sont également là, à discuter tranquillement, sans avoir besoin
de couvrir la musique, ou à prendre un peu l'air, essayant de décider
s'ils veulent rentrer ou s'ils veulent rester encore un peu.

Tonya sort de la poche ventrale de sa salopette une paire de sucettes
d'after six, un léger relaxant musculaire qui permet généralement de
calmer les courbatures le lendemain. Elle en prend une et en tend une
à Saina. Puis, après avoir posé son blouson à l'un des poteaux métallique
supportant l'auvent, elle va sous la pluie de moins en moins fine et
commence à tourner sur elle-même.

La pluie la tremple complètement, son T-shirt dégoutte d'eau, sa coiffure
s'est effacée sous le poids de l'eau, et son maquillage coule sur son
visage formant des larmes oranges et mauves qui descendent le long de son
cou. Saina l'observe, adossée au mur, considérant encore l'after six que
lui a filé Tonya.

Elle est préoccupée. Depuis tout à l'heure, elle a envie que Tonya la
prenne dans ses bras, elle a envie de la sentir contre sa peau. Mais elle
ne sait pas comment tout cela fonctionne ici, si Tonya n'est qu'une
rencontre pour la soirée, ou si elle est réellement intéressée par elle.
Et elle n'a pas envie d'être juste ça, un coup d'un soir.

Sans qu'elle ne le remarque, Tonya est en train de la pousser sous la
pluie maintenant battante. Elle ne répondra aux questions de Saina que si
celle-ci est au moins aussi trempée qu'elle lui dit-elle. Son insouciance
est contagieuse et Saina est rapidement trempée elle aussi, elles dansent
sous la pluie toutes les deux, la musique filtrant à travers les pierres
millénaires.

Saina embrasse Tonya, qui la serre contre elle, sa main glissant sur la
peau de Saina, passant sous ses vêtements, remontant vers sa poitrine.
Soudainement, elle enlève la main de Tonya de sa peau et se décolle
d'elle. C'est trop pour elle, pour le moment. Elle veut bien des calins,
danser, être contre Tonya, mais rien de plus pour le moment. L'ascenseur
émotionnel l'épuise et elle sent les larmes monter. Et elle fait lamême
chose que lorsqu'elle est à bout de nerf, elle parle, elle verbalise les
questions réponse qu'elle se fait, n'imagineant que les pires scénarios.

Tonya la prend dans ses bras et la laisse pleurer sous la pluie. Elle
attend que Saina dise ce qu'elle semble avoir besoin de dire. Puis elle
attend encore un peu. Enfin, elle renouvelle son offre d'une chambre pour
Saina, et que c'est OK aussi pour elle de faire des câlins.

Tonya se relève et aide Saina à en faire autant puis, la pluie ne semblant
pas faiblir, elles traversent encourant un des septs ponts de Konigsberg
en direction du cœur du Complexe et de la médina oú vit Tonya. Une fois
arrivée au sec, Tonya fait faire une visite éclair du lieu où elle vit
avant de pousser Saina dans sa chambre en lui demandant de virer ses
fringues trempées pour ne pas dormir dans des draps mouillés.

Après s'etre déshabillée et séchée dans une serviette que lui à passé
Tonya, et alors que celle-ci finit d'essorer ses fringues, Saina se glisse
sous l'épaisse couette de son hôtesse avant de lentement sombrer dans le
sommeil. Le stress accumulé depuis son arrivée se dissipant enfin,
laissant place à une fatigue intense, qui achève les derniers instants de
lucidité de Saina qui sent à peine Tonya se glisser contre elle sous la
couette et lui souhaiter une bonne nuit.
