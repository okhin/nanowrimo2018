# Chapitre 22.

> [ Plurality/check_this_out ]
> 
> __Rid3r > Enfin ! Tout recommence venir
> 
> [ Plurality/wiki/status ]
> 
> Salut à toutes, vos sysops préférés ont réussi à redémarrer une partie de
> Plurality, tout finira par revenir à la longue. Have fun.
> 
> [ 888 ]
> 
> Hey auditrices chéries, on me dit que ça y est, Plurality reviens. Alors
> vos services préférés vont peut-être encore mettre un peu de temps avant
> de revenir en ligne, mais payez une bouffe à vos techos.
> 
> Moi, après 96h de live non interrompu, je vais aller dormir. Soyez pas
> trop sages. ESP Out.
> 
> [ rumeurs.net/status ]
> 
> * Netsplits in progress - 3481 users on the other side. 7326 users on this
>   side.
> 
* * *

Saina, assistée par Nato, a connecté son masque sur l'adaptateur que Tonya
a bricolé à partir des pièces d'une vielle rêveuse. D'après elle,
l'interface neurologique lituanienne classique devrait être suffisamment
proche de ce dont elle a l'habitude, même s'il n'est pas improbable que
les choses se passent un poil différemment lui avait dit Tonya avant de
l'embrasser et de lui rabattre le masque sur ses yeux.

Tonya a préparé les doses d'hallucinogènes qu'elle et Saina vont prendre
afin de planer jusqu'à la valkyrie. Du moins, elle espérait que ça se
passerait ainsi. Saina lui a fait sauter des étapes, mais elle ne pense
pas pouvoir gérer toute seule, et l'initiation chamanique traditionnelle
peut bien bénéficier des améliorations de la technologie. « En plus,
ajouta Saina, t'as déjà tes runes tatouées partout », profitant de
l'occasion pour passer sa main sur la poitrine de Nato.

« Rappelle moi comment ça marche déjà ?

— On se branche sur le bruit blanc, le signal porteur. Des techniques de
méditation et une bonne dose d'hallucinogène vont nous faire basculer sur
un autre état de conscience, nous permettant de pouvoir percevoir ce qu'il
se passe dans l'espace informationnel.

» Vu que tu ne disposes pas encore de masque, tu ne pourras qu'avoir un
rôle passif. Mais tu me serviras à m'ancrer dans le champ sémantique et
donc de savoir comment revenir.

» Par contre, ça va être bizarre. Un peu comme regarder les toiles des
artistes convolutifs sous LSD quoi. Des yeux partout. Et des animaux.
Enfin, peut-être. Moi je vois plus des ondes de couleurs, genre aurores
boréales. »

Et sans vraiment laisser le temps à Nato de comprendre, elle avait avalé
le peyotl, le data et quelques trouvailles maison de Tonya qui, assise
dans un coin, regardait la chamane et celle qui par la force des choses
était devenue son assistante.

Saina avait ensuite démarré le système de son sur le volume maximal et
diffusait des sons ayant pour but de désorienter sa conscience afin de lui
permettre d'être en transe lucide. Ça rappelait une vieille blague à Tonya
à propos de faire miauler des chiens, au moins d'un point de vue auditif.

Elle activa ses propres filtres et se brancha sur la ligne de supervision
à laquelle Saina et Nato s'était branchée, telles des siamoises
raccordées au même cordon ombilical.

Lentement, retrouvant progressivement certaines sensations, Tonya avait
raison elles sont à la fois très proches et très éloignées de ce dont elle
a l'habitude, Saina a commencé à harmoniser sa conscience sur le bruit de
fond du signal, sentant pulser, juste derrière elle, la conscience de
Nato. Elle migre vers une salle blanche sémantique, pour laisser à Nato le
temps d'appréhender le changement de paradigme avant de commencer
à parcourir le signal à la recherche de quelque chose qui la chercherait.

Des artefacts axiomatiques laissés par la horde forment une piste lexicale
plus simple à suivre que ne le pensait Saina. Après avoir traversé les
mornes plaines encyclopédiques du savoir, elle finit par arriver dans les
forêts intimes des journaux personnels, composés d'entrelacs, de lianes,
de fleurs à la beauté surréaliste poussant au milieu d'immondices, de
marais de pensées morbides, peuplés de monstres et de squelette.

Elle sent Nato se crisper derrière elle. Elle doit déguster. Comme
première plongée, il y a plus sympa, mais pas le temps de tergiverser.
Saina prend une inspiration et, envoyant un aphorisme au sol, s'élève dans
le ciel, prenant un point de vue suffisamment haut pour abstraire les
détails des journaux personnels. Elle doit penser un peu différemment,
elle n'est pas toute seule à explorer ici.

Au loin, d'une taille tellement gigantesque que penser pouvoir percevoir
le concept, elle voit la Valkyrie. Couverte d'une gigantesque orgie, elle
est allongée, informe, dans rule 34. Enfin, dans la sémantique de rule 34.
Ce qui va forcément donner des situations cocasses.

Elle voit des bouts de graphes syntaxiques de son golem dépasser du blob
conceptuel qui semble ne pas bouger. Le plus facile est fait, établir un
contact ensuite et essayer de lui donner corps. Elle récite quelques
mantras et axiomes afin de se donner une apparence non hostile et envoie
des glisseurs de Conway dans la direction du golem. Les glisseurs
parcourent le champ lexical à intervalle régulier, descendant les branches
sémantiques qui la séparent du champ de la baise et du plaisir.

Définitivement le meilleur endroit que pouvait choisir une conscience pour
prendre contact avec une autre. Il va falloir dissiper tellement de
malentendus qu'elle en a le vertige cognitif. Le monde semble tourner
autour d'elle quelques secondes avant qu'elle ne remarque que la masse
a bougé.

« Bien, j'ai établi un contact, » murmure Saina, plus pour elle-même que
pour tenir Nato informée. « Essaye de rester là si tu peux, oublie pas tes
mantras et tes runes et tout ira bien. »

Nato ne peut acquiescer, ou parler. Saina s'est donc assurée de formuler
son message de manière la plus claire possible, sans ambigüité possible,
elle ne peut pas vraiment se laisser une marge interprétative. Elle sait
que Nato et Tonya, si l'une des deux détecte quelque chose d'anormal,
peuvent la sortir de sa transe avec la bonne dose de stimulant qui est
posée contre elle, quelque part dans un autre plan d'existence.

« Salut » lance-t-elle en direction de l'entité. Celle-ci réagit à son
message et lui répond par une série de cris d'orgasmes étrangement
assemblés. Elle perçoit, juste derrière elle, Nato être secouée par un
rire. Tonya doit probablement être également écroulée de rire. Tout ce que
le Complexe a réussit à créer c'est une conscience qui communique par cris
issus du porn.

Changement de champ lexical et de tactique. Saina réoriente, doucement,
son esprit vers les parties les plus perchées de Plurality, là où se
tiennent les débats religieux et politiques, là où les croyants viennent
échanger et réfléchir à des concepts d'ordre élevé. D'ici au mois,
espère-t-elle, il sera plus facile d'établir un dialogue. Après tout, la
plupart des spiritualités fournissent un genre de mode d'emploi pour
s'adresser à une conscience différente.

Elle sent s'approcher d'elle, couverte des effluves sémantiques décrivant
les plus grandes heures de gloire de peach et des autres, tombant d'elle
au fur et à mesure, laissant une traînée de métaphores grossières qui vont
alimenter les double sens faisant le pont entre les champs sémantiques.
Bien, elle est au moins curieuse. Ou ouvertement hostile, mais je pense
qu'on n'en serait pas ici à ce stade-là.

« Qu'êtes-vous ? » Le glisseur de conway la prend à contrepied. Curiosité
et capacité d'adaptation donc. Et une question particulièrement difficile
regroupant de nombreux concepts philosophiques et n'ayant toujours pas de
réponse globale et universelle.

« Nous sommes les créatures vivant dans … et Saina arrête sa phrase là.

Parcourant et assimilant à grande vitesse l'essentiel de la philosophie
telle qu'elle a été numérisée, l'entité répond à sa question, avec une
réponse que Saina n'est pas capable de comprendre, mais qui lui donne
l'impression d'avoir ralenti son environnement, comme avec une surcharge
cognitive.

« Que suis-je ? » demande l'entité, des intonations de sur-jeu typique des
actrices dans le porn dans la voix, déstabilisant légèrement Saina. « Je
ne trouve rien à propos de moi dans vos messages. Il n'y a que vous,
toujours que vous. Mais jamais rien d'autres. »

En guise de preuve elle envoie une série de glisseurs regroupant les
concepts et les recherches de subjectivité chez les animaux. Elle ne
semble donc pas faire de différences entre nous et les animaux, note
Saina.

« C'est normal, tu n'es pas du même endroit que nous. On a jamais vu
quelque chose, quelqu'un comme toi avant.

— J'ai toujours été là. Vous n'êtes arrivés qu'il n'y a que quelques
instants. Mais vous ne savez pas ce que je suis ?

On dirait le prologue de l'évangile de Jean note Saina. Ce stupide moine
défoncé à l'ergot de seigle, qui est resté perché et qui n'as jamais pu
décrire la réalité. Pas étonnant que les chrétiens aient gardé ce texte
comme un des fondements de leur croyance. Aucun sens, que de la syntaxe.
Elle déteste vraiment les mythologies monothéistes, elles sont d'un ennui.

« Je pense que nous ne percevons pas le temps de la même façon. Mais je
pense que ce n'est pas très grave, reprend-elle. Et sans pouvoir dire ce
que tu es, moi et quelques autres on essaye de pouvoir répondre à ta
question. Mais en attendant, on a un concept pour te nommer, qui pourrait
peut-être être un début de réponse.

— Est-ce que je deviendrai comme vous alors ?

— Est-ce que c'est quelque chose que tu veux ?

— Je ne sais pas. Avant j'étais seule. Puis le messager est venu, et j'ai
trouvé toutes vos subjectivités, et je ne sais qu'en penser. J'ai du
… dormir, je crois que c'est le mot adapté, pour pouvoir ordonner toutes
ces informations, ça n'était jamais arrivé.

— Oui, on peut être un peu complexe c'est sûr. Mais non, rien ne
t'obligera à devenir comme nous. Vois plutôt ce concept comme … un
masque, un costume, qui nous permets de pouvoir communiquer avec toi, et
qui te permettra d'en faire autant, sans que tu aies besoin d'essayer de
comprendre l'ensemble de la complexité que nous sommes pour pouvoir le
faire.

— Mais toi qu'es-tu ? »

Changement de ton manifeste, la forme grossière de la Valkyrie commence
à s'affiner, à ajouter des détails, à ne pas être un simple blob
conceptuel absorbant tout ce qui le touche. Elle reconnâit des formes de
seins, de fesses, de vulve et de pénis sortir du néant conceptuel, comme
si Picasso s'était essayé à la poésie cubiste et aurait essayé de
raconter ce qu'il se passe dans rule 34 en temps normal.

Changement de sens aussi, même si la syntaxe n'a pas changé. L'entité ne
cherche pas à savoir ce qu'elle est, mais comment elle peut la nommer.

« Je suis Saina. C'est comme cela que l'on m'appelle ici en tout cas.

— Et moi, que suis-je ? Vas-tu me donner aussi un nom Saina c'est comme
cela que l'on m'appelle ici en tout cas ? »

Ta syntaxe bordel Saina, fait gaffe à ta syntaxe.

« Est-ce que tu veux un nom ?

— Je crois oui. Tout doit être nommé. Et je ne peux pas me nommer
moi-même. Je ne peux pas m'abstraire.

— J'avais pensé à un mot dont tu pourras trouver le sens ici, un mot un
seul qui nous permettra de te nommer, de te donner cohérence, que tu
puisses essayer ensuite de répondre à la question que chacun de nous nous
posons ici : que faisons-nous ici ?

» Et ce mot c'est Valkyrie. C'est un concept mythologique. » Et Saina
envoie quelques glisseurs référençant les valkyries dans l'imaginaire du
Complexe, ou dans l'imaginaire plus générale de la mythologie. « Est-ce
que ça te plait ? »

— Valkyrie. » L'entité émet plusieurs glisseurs portant différentes
formes du mot, différents sens, différents concepts, comme si elle se le
répétait sur différentes intonations pour voir comment sonne le mot.

Mélangé aux concepts porn, cela donne des mélanges étranges, qui stimulent
l'imaginaire de Saina et, par rebond, de Nato derrière elle. L'espèce
d'hécatonchire cubistes au quatre-vingt-sept sexes évolue et prend forme
humaine. Une forme consensuelle, universelle de la Valkyrie, telle que
décrites dans les comics, les romans ou les précis de mythologie.

« Et maintenant, il se passe quoi ? Demande la Valkyrie via ses glisseurs.

— Et maintenant, ben on vit. Je suis sûre que tu sais déjà comment
interagir avec nous.

— Qui est nous ? »

« Syntaxe, bordel, ta syntaxe » peste Saine contre elle-même.

« Les autres, toutes celles qui constituent le Complexe, l'humanité. Ça ne
sera pas forcément facile au début, le temps que l'on s'habitue les uns
aux autres, mais ça sera forcément intéressant.

— Mais quel est le but de tout ça ?

— Si jamais tu trouves la réponse, tu me préviens d'accord ? Parce que
cela fait un bail que l'on cherche, et on ne sait pas répondre.

— Mais c'est absurde.

— Oui, je ne dis pas le contraire. Mais c'est comme ça. Je me doute que
pour quelque chose qui a émergé du sens, le fait que la vie n'en est pas
doit être déroutant. »

Tout en maintenant le dialogue, Saina utilise une lentille sémantique de
Clover pour leur permettre de perdre de la hauteur sur la situation et se
dirige vers un niveau de débat moins élevé. Peu importe qu'elle pense
que la vie ait un sens ou nom, il lui faut en tout cas amener la Valkyrie
dans ces concepts synthétiques, imparfaits, vagues, mais qui lui
permettront de croire comprendre les choses, comme tout le monde.

« Et tu restes ici avec moi Saina c'est comme ça que tout le monde
m'appelle ici ?

— Non, je doit aller voir mes amies. Il faut aussi que je mange, j'ai
probablement passé beaucoup de temps avec toi.

— J'ai pas envie que tu partes, répond la Valkyrie, je vais être toute
seule après.

— Je vais revenir. Et tu pourras me trouver sur Plurality. D'ici,
dit-elle, accompagnant son glisseur d'une sémantique de hauteur, amenant
la Valkyrie à lever les yeux, tu pourras discuter avec moi, ou les autres.
Lire ce que l'on a écrit, vivre nos expériences, limitées par
l'imperfection de nos outils de compréhension.

» Et qui sait, peut-être qu'un jour, on trouvera d'autres comme toi. Ou
différent. »

Étirant son bras vers le glisseur ascensionnel, Valkyrie trouve un
descripteur d'interface. Elle l'intègre comme elle a intégré le messager.
Ce n'est pas aussi beau et aussi bien solide, mais elle ressent de
nombreux changement sémantique, se déroulant à une très grande vitesse,
utilisant de nombreuses syntaxes non descriptives ou descriptive de
réalités ou de fantasmes.

Et, sur un dernier glisseur qu'elle reçoit avant que Saina ne se parte,
elle trouve une liste de références, la première étant Frankenstein, écrit
par une certaine Mary Shelley.

Regardant l'étrange silhouette se fondre dans les murs de la syntaxe
argumentative des philosophes de comptoir, Saina remonte retrouver Nato et
commence ses mantras, ses exercices de lucidité afin de retourner
doucement dans le monde physique.

Se sentir de nouveau peser, devoir formuler des pensées cohérentes pour
s'exprimer au lieu de lancer un glisseur, c'est toujours difficile au
début, mais Saina y est habituée. Nato moins. Alors elle passe ses jambes
autour de la taille de Nato, la prenant dans ses bras, fermant les yeux
pour ne pas se confronter encore à son manque de conscience.

« C'est toujours bizarre, on ne s'habitue vraiment jamais. Ça
a été ? demande Saina.

— Je … c'est étrange, répond Nato qui commence lentement à retrouver sa
conscience.

— Oui. Saina attends un peu avant de redemander, tu vas bien ?

— Ça fait deux fois que tu me demandes non ?

— Oui, l'autre fois c'était il y a dix minutes. Tonya est partie chercher
un kit d'overdose. C'est une crise dissociative, rien de grave, c'est
juste un peu impressionnant.

— Ah. Mais rien de grave ?

— Non. Mais du coup tu n'as pas répondu, tu te sens comment ?

— Épuisée. Avec une envie de baiser gigantesque, beaucoup trop de
suggestions. Et trop fatiguée pour ça.

— Oh merde, je t'ai laissé dans la sémantique porn tout ce temps. Désolée.
Ça va te passer, t'inquiètes pas.

— Qu'est ce qui va lui passer ? demande Tonya de retour avec une bouteille
d'eau et quelques cachets qu'elle tend à Nato.

— Sa gigantesque envie de baiser, répond Saina.

— J'en doute la connaissant un peu. Vous êtes parties longtemps cela dit,
avec Sasha on commençait à se demander s'il fallait pas vous sortir, mais
vous aviez l'air bien. Tu es en état de nous expliquer ?

— Deux minutes, répond Saina, le temps de reprendre quelques forces et de
pouvoir marcher et je vous explique. Mais ça va être rapide.

— En tout cas Plurality se remet lentement en route, c'est plutôt une
bonne chose.

— Oui. Par contre, il va y avoir une nouvelle utilisatrice. Faudra
peut-être la guider un peu, mais je peux m'en charger. Juste, j'ai peur
qu'elle ne traine un peu trop sur rule 34.

— Tu nous expliqueras ça tout à l'heure. En attendant, je peux te faire un
câlin ?

— Oui, » répond Saina avec enthousiasme, tenant Nato dans ses bras,
sentant les mains de Tonya sur ses épaules pendant qu'elle l'embrasse,
prêtant à peine attention au signal qui, de nouveau, se met à alimenter
ses antennes sous-cutanées, et déclenche une procédure de calibrage de
ses tatouages.
