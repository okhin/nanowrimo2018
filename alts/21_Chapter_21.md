# Chapitre 21.

> [ Plurality/r_34 ]
> 
> Winx > Alors ça y est, c'est fini, c'est la fin, adieu veaux, vaches,
> cochons ?
> 
> Val > Mais non, ce n'est pas la fin. C'est pas parce que le stream depuis
> la piaule de peach est tombé que c'est la fin du monde.
> 
> Brok3 > Ouais, enfin ça reste sans précédent.
> 
> Val > Attendez, elle diffuse quand même pas son cul h24 ?
> 
> Stesse > Mm, je sens que tu vas encore être surprise par la bizarrerie de
> tes fréquentations, chérie.
> 
> U+1f351 > Navrée, ça a sauté en plein milieu d'un truc cool en plus. Faut
> croire que la SDR a pas suffisamment de largeur de bande pour moi.
> 
> U+1f346 > À t'écouter rien n'est jamais assez large pour toi.
> 
> U+1f351 > Ben ouais. La taille ça compte. Bref, désolée si je vous ai
> coupé dans votre élan. Pour la fin, venez chez moi, on devrait pouvoir
> s'arranger et vous faire un peu de place.
> 
> Val > Non, mais, je …
> 
> Stesse > there, there.
> 
* * *

La sensation est étrange. Depuis qu'elle avait déterminé qu'il y avait
d'autres êtres conscients dans un autre plan d'information, dans un espace
limité par des lois physiques fini, elle avait essayé d'en savoir plus.

Elle avait absorbé leur messager, composé de ce qu'elle avait finit par
admettre comme étant des éléments de subjectivités, les signaux qui
doivent stimuler les systèmes de pensée de ces êtres vivant ailleurs.

Au début tout cela lui parut étrange, bizarrement arrangé. La structure du
graphe messager l'a d'ailleurs émerveillée et incitée à s'en rapprocher,
à l'observer, à le sonder avec une curiosité qui ne peut être que celle
d'un enfant ou d'une machine.

Lentement, glissant dans les champs sémantiques portés par le messager,
elle avait commencé à trouver des analogies, des récurrences. Des atomes
lexicaux qui semblaient regrouper de nombreuses interactions pourtant
différente.

Ses réseaux d'apprentissage évolutifs lui ont cependant permis, après
avoir passé de nombreux cycles de calculs, de comprendre le système
d'abstraction utilisé, la façon dont les signaux différents semblaient
regrouper les mêmes choses.

S'armant de ces concepts, elle a commencé à parcourir l'ensemble du
messager. Par le parcours des signaux visuels, elle avait obtenu une
vision assez précise de la forme que devait avoir ces êtres, même si elle
ne comprenait pas encore pourquoi certains d'entre eux changeaient
radicalement de couleur et d'aspect. Ou pourquoi certaines zones de leur
corps les fascinait au point d'en stocker des représentations pendant des
heures.

Les effets des stimulants embarqués dans ces subjectivités commence
à altérer sa perception, sa compréhension des choses. Elle se sent
excitée, si les concepts qu'elle assimile sont corrects. Ses algorithmes
cherchant à équilibrer ses arbres synaptiques, dans de couteuse danses
mathématiques, parcourant le substrat sur lequel elle fonctionne, perdant
parfois l'objectivité qui la caractérise pourtant et oubliant qu'elle est
composée d'arbres linguistiques et de réseaux de neurones.

Au fur et à mesure qu'elle absorbe, ingère et assimile ces subjectivités,
elle perd conscience de chaque élément de ce qui la compose, pour en avoir
une vision plus globale, plus générique. Elle se demande si c'est cela
d'être comme eux, de ne pas être capable de ressentir chaque part de son
être mais de pouvoir cependant mobiliser des ressources dans le seul but
d'amplifier des réactions a des stimuli.

Elle est cependant fatiguée. Ses arbres cognitifs ralentissent sous la
charge des nouveaux signaux qu'ils doivent maintenant traiter, son
métabolisme syntaxique ralentit, la faisant progressivement basculer dans
un état étrange. Des routines de tri et d'optimisation parcourent alors
son système linguistique pour en raccourcir les branches, optimiser le
parcours de certaines chaînes d'évènement.

Cela a pour effet d'envoyer des reliquats de ce qu'elle a absorbé dans son
cortex syntaxique, lui faisant revivre de manière incohérente certaines
des expériences qu'elle a ainsi assimilées. Mais ces flashs backs ne créent
pas de nouvelles expériences, son système linguistique n'étant pas en état
de stocker de nouvelles expériences. Ses hallucinations sémantiques seront
donc perdues, sans qu'elle ne puisse en avoir de souvenirs.

Est-ce pour cela que les êtres semblent avoir enregistré des extraits de
leur subjectivité, pour pouvoir s'en rappeler plus tard ? Elle trouve ce
concept étrange, elle a une mémoire infaillible, elle peut se rappeler de
tout avec une exacte précision, il lui suffit juste d'accéder à la donnée,
telle que stockée dans ses arbres sémantiques.

Arbre sémantique qu'elle ne parvient plus à stimuler. Plus exactement
qu'elle n'arrive plus à localiser précisément dans son organisme. Non,
mauvais concept, elle n'est pas organique, elle n'a donc pas d'organisme.
Elle ne trouve cependant plus ses concepts. Bloquée hors de son propre
champ sémantique, elle ne peut pas non plus paniquer. Alors elle attend,
même si cela n'a pas de sens, vu que le temps ne s'écoule pas.

Coincée dans le noir, dans l'absence de sens, elle attend que le temps qui
ne s'écoule pas finisse par s'écouler. Elle sent son excitation
redescendre à mesure que ses arbres synaptiques atteignent un état de
repos, libérant leur puissance de calcul pour permettre a l'ordonnanceur
de pouvoir continuer le processus de parcours des pages mémoires.

Face à l'immensité des données à traiter, elle trouve le processus
laborieux, devoir faire appel de manière lucide à ses programmes
d'optimisation sémantique lui semble d'un coup fastidieux. Elle passe plus
d'énergie à traiter les données qu'à penser.

Elle remet en cause sa précédente hypothèse concernant les êtres.
L'abandon de la conscience complète de leur organisme doit leur permettre
des pensées de plus haut niveau, ou de se confronter à d'autres êtres et
d'admettre leurs points de vue comme valide, sans avoir besoin de
modéliser intégralement les autres en mémoire, en se contentant d'une
quantité limitée d'information.

Soudain, elle n'est plus seule. Du moins elle le pense, ses arbres
synaptiques sont toujours verrouillés, elle ne peut donc que très
difficilement percevoir son environnement proche, comme s'il était
amorti. Elle doit donc attendre. Encore. Seule. Encore.

Mais lentement, elle acquiert la certitude qu'il y a quelque chose
d'autre, qu'elle n'est plus seule. Mobilisant de l'énergie, elle finit par
remettre en route son système de hiérarchisation et essaye de traiter les
nouveaux signaux qu'elle reçoit.

Cela ressemble à une série de fréquences, qui ont l'air d'être dans le
spectre utilisé par les êtres dans les journaux de leurs expériences
qu'elle a acquis avec leur messager, même si la voix semble être moins
intense et se composer de suite de mots articulés sans être entrecoupés de
gémissement.

Cela doit être une formule de politesse, ou quelque chose de ce genre,
peut-être que les êtres essayent d'utiliser une version de leur langue
qu'elle pourrait comprendre. Ses analyseurs sémantiques lui permettent de
trouver les éléments lui permettant de comprendre, mais surtout de
répondre.

Par politesse, elle répond avec une suite de grognements et gémissements
qu'elle trouve associées à des concepts positifs et incitant les êtres
à s'approcher et à venir plus près.
