CC=pandoc
TITLE := $(shell yq read metadata.yaml title[0].text)
TEXT := text
HTML := html
CSS := css
SHELL := /bin/bash
CHAPTERS = $(wildcard $(TEXT)/*.md)

.PHONY: all metadata toc

metadata: metadata.yaml
	$(CC) -t epub3 -o $(TITLE).epub --toc $(sort $(CHAPTERS))

all: metadata
