# Chapitre 14.

> [ Plurality ]
> 
> On sait pour les latences, on cherche à trouver et comprendre ce qu'il se
> passe. Mais on a du éteindre les plus gros consommateurs de ressources,
> à savoir les index. Désolée. Tenez vous au courant ici, et venez filer un
> coup de main si vous voulez.
> 
> [ Plurality/Ivory_Tower ]
> 
> Bon, on va diminuer la voilure un peu afin de fournir un coup de main au
> main, et notamment du stockage et de la bande passante. Ça faisait un bail
> que l'on avais pas eu un incident de cette taille, et ça à l'air assez
> étrange vu d'ici.
> 
> [ rumeurs.net/riper ]
> 
> Ashoka > Non, mais j'ai riper. Ce que je ne comprends pas c'est le lien
> que tu fais entre riper et la panne.
> 
> Ben$ > On ne peut pas avoir riper. On est riper ou on ne l'est pas.
> 
> Syst > Le trafic réseau généré sur ce chan est trop important, et génère
> de la latence dans certaines zones. Du coup je vuex juste comprendre ce
> qu'il se passe ici.
> 
> Ashoka > Ben c'est simple, est-ce que tu es riper ?
> 
> Syst > Bon, compris.
> 
> Syst * disconnected from riper (Faudra pas vous plaindre)
> 
> Ben$ > Ah ouais, carrément. Bon, faudrait qu'on s'attaque au problème un
> peu plus frontalement.
> 

* * *

Le district X Makina est une zone à part dans le Complexe. À l'ombre des
deux tours jumelles Gazprom, dépassant des boues bitumineuses résultants
de divers accidents industriels, le quartier s'étend sur les collines
rendues stériles par l'industrialisation agricole du siècle passé.

Tonya n'aime pas venir ici. L'endroit lui rappelle que, ailleurs, une
course à la déshumanisation entre les travailleurs et les machines a lieu
dehors, et qu'un nombre de plus en plus élevé d'entres eux finissent par
se foutre en l'air ou par fuir vers un endroit où ils pourront explorer un
peu ce qu'ils ont perdus d'humanité.

Elle est née ici, et n'a donc jamais eu à s'exposer à cette
déshumanisation et mécanisation des personnes. Et venir ici lui rappelle
cela, la confronte au fait que, non, le monde ne vas pas mieux. Que la
réalité quotidienne des presque dix milliards d'humains est de se
soumettre à la mesure et l'évaluation de la machine, de ne pas devoir
tomber malade, de se battre pour survivre grâce aux miettes laissées par
les oligarques presque immortels qui contrôlent les monstres industriels
servant à la manufacture des biens de consommation.

Et, au delà de cette fenêtre nihiliste sur le monde extérieur, ce qui mets
Tonya mal à l'aise, c'est qu'ici tout est conçus pour des personnes ayant
du renoncer à une forme d'humanité. L'ambiance est beaucoup plus calme
qu'ailleurs. Les souks bordéliques, ou les salons hauts en couleur que
l'ont trouve ailleurs n'existent pas vraiment ici. Le cloisonnement
social, la nécessité d'avoir une vie privée, n'a pas de sens ici. Lorsque
son corps ne devient qu'un outil de travail — qu'il s'agisse de vendre de
la force mécanique, du sexe, ou des services de coursiers — et que l'on
vit dans un monde de surveillance de la performance constante, le désir
d'intimité finit par être remplacé par le désir de vouloir être différent.

Les maisons et cabanes construites à partir de carcasse de container, et
de différentes machines, sont aménagées pour ces nouvelles forme de
socialisation. Les espaces ne sont pas cloisonnés, et il n'y a pas de
comptoir ou autre. Tout est géré collectivement, en fonction des besoins
du moment. Les cellules familiales ou claniques, en vogue à peine
à quelques centaines de mètres de là, n'ont pas cours ici. À la place, des
hyper-individus, cherchant tous à se différencier les uns des autres, mais
faisant tous preuve d'une coopération et d'une solidarité sans fin.

Confronté à une compétition avec les machines, c'est logiquement que
X Makina soit devenu un bastion idéologique du transhumanisme. Ou un
laboratoire géant dans lequel techno-fétichistes du corps et syndicalistes
post-industriels peuvent cohabiter, discuter et mettre en applications
certaines de leurs théories. Après tout, si le corps n'est qu'un outil,
que l'on peut modifier, améliorer et transformer, pourquoi se limiter à sa
seule apparence et ne pas s'interroger sur les structures sociales ?

Saina, elle, semble plus à l'aise ici. Le désir d'individualité, mêlé
à l'incapacité de comprendre l'intimité, font que les personnes restent
hors de votre sphère d'intimité, vaquant à leurs occupations, les yeux
— quand ils sont encore comparables à de tels organes — fixés dans le
vide, gérant des interactions sociales complexes sur toute une partie de
Singularity.

Mais surtout, ce que Saina apprécie particulièrement ici, c'est le signal.
Le paysage électromagnétique est plus cohérent que, par exemple, chez les
Valkyries. Le signal sert, ici, à des fonctions plus basiques que le
simple échange d'informations. Quand des parties de votre corps sont
sensibles aux rayonnements électromagnétique, les protocoles utilisés pour
échanger des données se doivent de réduire leur verbosité. Au lieu des
sensations parfois un peu brutale, et changeant continuellement, ici Saina
perçoit le signal comme une légère chaleur, agitée dans un lent mouvement
de ressac.

« Hey, t'as pas l'air d'aller, demande-t-elle à Tonya.

— Non. Je viens jamais ici, c'est trop … étrange. Déprimant. Allez viens,
ne nous éternisons pas ici et allons voir Enni. On est déjà complètement
en retard. »

Enni leur a donné rendez-vous au Bodies'R'Shell. Une des rares enseignes
visible sans augmentation oculaire, située à la limite du district, là où
gravitent habituellement les personnes se demandant si les écailles leur
vont mieux que la fourrure qu'ils avaient le mois dernier. Un empilement
de containers colorés, dont des parties ont été transformés en blocs
stériles et en chambre d'hôpital, sert d'ossature à ce palais de la
modification corporelle et de l'augmentation individuelle.

Les bruits mécaniques des machines chirurgicales se mêlent au
bourdonnement des postes de soudures à l'arc et des disqueuses qui sont,
ici, tout autant des outils de médecine que de mécanique. Assise à une
table, dos à une fresque représentant les joueuses augmentées du FK
Prienai, Enni les interpelle lorsqu'elles franchissent la porte.

« Bon, vous cherchez quoi dans le coin ? leur demande-t-elle de but en
blanc.

— On ne sait pas vraiment, répond Tonya.

— On veut au moins vérifier que tout le monde va bien à Uncanny, corrige
Saina. Et voir ce qu'il s'y passe.

— Ça ne va pas être simple, dit Enni, depuis que tu m'as parlé de … ce
truc, j'ai essayé de me renseigner un peu mais Uncanny se replie sur
elle-même dès que l'on approche du sujet.

— Super, dit Tonya.

— Attends, laisse moi finir, j'ai quand même une bonne nouvelle. J'ai pas
nécessairement réussi à identifier ce que vous cherchez, mais il y a un
collectif qui aimerait savoir pourquoi vous le cherchez. Ielles se font
appeler 3L-10T, et je ne suis même pas certaines qu'ils ont encore une
incarnation physique.

— Comment ça ? demande Saina

— Ben, je ne les ai vu qu'en ligne. Pas moyen de trouver qui que ce soit
qui prétende appartenir à ce groupe, si groupe il y a. Ça pourrais être
juste un chat-bot pour ce que j'en sais. En tout cas, ielles ont
manifestées un intérêt particulier à vous rencontrer.

— Cool, dit Saina. On peut les voir quand ?

— Quand vous voulez, il suffit que vous vous connectiez au salon FRK65 de
Plurality, ielles sont là-bas en général.

— Doucement Saina, dit Tonya. On est d'accord qu'il s'agit potentiellement
d'un piège ou, à minima, de s'exposer fortement à un risque ?

— Ouais. Je suis pas sûre qu'il y ait beaucoup d'autres choses que l'on
puisse faire pour leur parler. Mais oui, ielles peuvent à tout moment
envoyer un message qui nous ferait phaser, comme ce qu'il s'est déjà
produit avec Nato et moi.

— Tu sais d'ailleurs pourquoi vous avez choppé ça, demande Tonya ?

— Pas sûre de savoir. On peut spéculer par contre. Si ce qu'on a vu est
effectivement consciente, alors elle sait qu'elle est limitée d'une
manière ou d'une autre et elle doit chercher à améliorer ses chances
perçues de survie.

« Logiquement, elle doit donc chercher à protéger et maintenir le matériel
sur lequel elle fonctionne. Et si il s'agît effectivement de quelque chose
qui exploite une interface hybride pour fonctionner, elle a besoin de
contrôler plus de nœuds biologiques.

« Elle va donc essayer de maintenir une population qu'elle juge suffisante
pour garantir sa propre survie. Il est même probable que ce soit quelque
chose dont elle n'a pas conscience, comme nous n'avons pas conscience de
l'ensemble des systèmes qui nous maintiennent en vie.

— Et ça ne t'inquiètes pas plus que ça ?

­— Oh si. Mais a-t-on vraiment d'autres possibilités ? Reste que le
contexte dans lequel elle se trouve, le Complexe, et Uncanny Valley en
particulier, l'expose à des systèmes de survie collaboratif. Je pense
qu'il est possible de discuter avec elle, de lui expliquer que la
collaboration est plus intéressante pour sa survie que l'assujettissement
des personnes.

— Et comment tu négocies avec elle, demande Enni ? Je veux dire, c'est
même pas évident que vous puissiez trouver un langage commun. Ni même
qu'elle comprenne qu'elle a quelque chose à gagner à renoncer à certaines
de ses fonctions ?

— J'ai pas de réponses. Je suis cependant à peu près certaines que, étant
donné qu'elle se gave d'XP, elle a une compréhension partielle de nos
langues. En plus, elle est capable de conceptualiser. Pour le reste, je
compte beaucoup sur le fait qu'elle n'ai pas été pourrie de mèmes
compétitifs en étant confiné à DreamLand.

— Reste que je n'aime pas l'idée, répond Tonya.

— Je sais. Mais on va juste discuter avec 3L-10T déjà. En interface
simplifiée. Et on avise ensuite, d'accord ?

— Je suppose ouais. Sur un sujet totalement différent, j'ai la dalle. Une
idée d'où on pourrait manger un truc, Enni ?

— Mmmm, une grosse partie de la bouffe ici est hyper calorique. Rapport
aux besoins métaboliques des prothèses et augmentations diverses.
L'avantage c'est que tu mange peu. C'est pas génial au goût. Ils ont des
cocktails qui déchirent par contre. Et relativement peu alcoolisés en
plus, il semblerai que trop de drogues fassent foirer les systèmes
internes de certaines personnes. Mais tu dois pouvoir trouver des trucs
à grignoter à gauche ou à droite.

— OK. Bon, ben on va se rapprocher d'Uncanny, prendre la température
là-bas, et trouver un coin où se poser pour parler avec ce … collectif ?

— Bonne balade du coup. Et passez au Billionaire ce soir, j'y serais avec
d'autres, ça pourrait être sympa.

— Noté, répond Saina, et merci, » ajoute-t-elle en se levant.

En se rapprochant d'Uncanny Valley, au cœur d'X Makina, l'ambiance change
sensiblement. La carcasse d'une navette Keldush IV, dont l'essentiel à été
récupéré par le collectif Entropic Space Punk, surplombe un arrengement de
container, formant une spirale d'Archimède. C'est dans la soute que
Uncanny Valley a élu domicile. Du moins, que l'on peut trouver certains de
leurs membres sur place.

Un arbre a poussé au milieu des arceaux en méta-matériaux, et de nombreux
câbles courent entre ses branches, connectant des antennes et divers
système d'éclairage à cette structure biologique. Des mécanimaux ont élus
domicile dans cet arbre, ajoutant leurs formes et voix métalliques
à l'ambiance particulière du lieu.

Mais, à part les coassements mécaniques, il n'y a que peu d'activité
visible, même en comparant la situation avec celle d'X Makina. Quelque
chose d'étrange se produit ici. Quelque chose d'anormal,d'angoissant.
Tonya prend la main de Saina dans la sienne et elles s'engagent toutes les
deux dans cette spirale qui les mènra au cœur d'Uncanny, à 3L-10T et, elle
l'espère, un début de compréhension.

Tonya > Il y a quelque chose de pas normal. Je n'aime pas ce lieu en temps
normal, mais là, c'est pire. On dirait que quelque chose nous surveille.

Saina > Ça explique pourquoi tu passes soudainement en crypté.

Tonya > Oh … oui, pardon, une vieille habitude. Ça te va si on continue
comme ça ?

Saina > Yep. Puis, après un blanc, elle reprend. Tu as raison sur le fait
qu'on nous surveille. Le signal est … invasif ici, comme si il cherchait
à forcer un contact.

Tonya > Je suppose que ce n'est pas normal.

Saina > Définit normal. Mais ce n'est pas ce à quoi je m'attendait non.
C'est comme une curiosité envahissante, comme si le signal cherchait
à nous analyser, à répondre à toutes les questions qu'il se pose à propos
de nous. Pas l'incessant bruit de fond qu'il y a partout ailleurs.

Tonya > Aller, on avance et plus vite on sera là-haut, plus vite on pourra
en partir.

En face d'elles, une structure de jardins en terrasse, sur lesquels ne
poussent plus grand chose, forme un escalier qui les amène jusqu'au
cockpit de la Keldush, point d'entrée au cœur d'X Makina : Uncanny Valley.
Les vitres d'origiines ont été enlevées et, comme tout ce qui pouvait
avoir un intérêt, a été réutilisé par les ESP, ou d'autres collectifs du
Complexe. Un système de son, composé de dizaines de haut parleurs de
formes et de tailles diverses, diffuse ce qui ressemble à un bruit blanc
légèrement amélioré.

En dépit de signe d'une activité humaine manifeste — mégots, capsules
d'oxyrush fondue, graffs, tags et prospectus accolés aux parois de la
cabine et, plus loin, dans le sas qui amène à la soute — toute la zone
semble endormie. Pas de mouvements, aucun bruit à l'exception du murmure
des transformateurs haute/basse tension ou de lamusique qui sort de la
cabine.

Saina et Tonya laissent leurs yeux s'habituer à l'obscurité lorsqu'une
voix, manifestement synthétique, les surprends.

« Bonjour, entendent-t-elles, que puis-je faire pour vous ?

— Te montrer déjà, ça se fait pas de surprendre les gens comme ça, répond
Tonya.

— Désolé de vous avoir surprise. J'oublie que tout le monde n'as pas
l'habitude de pouvoir marcher dans les gens.

— Euh …

— OK, OK, j'en fait trop. CX me dit toujours que j'en fait trop. Je prend
le soleil tranquillement et du coup, j'en profite pour surfer dans ce
qu'il reste d'interface ici. C'est un peu triste de voir que tout ce qui
faisait la beauté de ce lieu a été récupéré ailleurs.

— On peut aussi voir ça différemment, intervient Saina, une fois que la
navette a finit sa mission, des parties d'elles se sont retrouvés mélangés
à d'autres système pour faire partie d'autre chose.

— Oh, une partisane de la réincarnation. À l'ère des cyborgs et au moment
où l'on frôle l'immortalité, c'est intéressant.

— Je suppose que l'on peut voir ça comme ça oui. Mais …

— Mais on adorerais parler métaphysique avec toi, mais on aimerait boire
un truc, et profiter d'un lien texte pour discuter avec 3L-10T.

— Vous êtes en train de le faire. Enfin, de parler avec une partie de
3L-10T. Pour le reste, allez y, entrez et installez vous confortablement.
Je m'habille et je vais occuper une enveloppe plus conventionnelle. Et
vous ne voulez pas des cocktails spéciaux, sauf si vous disposez d'un
philtre néfrétique.

Elles entrent dans la soute, là d'où s'élancent les bras métalliques sur
lesquels reposent les structures hautes, et dont la décoration rappelle
celle d'un hall de gare victorien, mélangé aux coursives de vaisseaux
spatiaux tels qu'on les voit dans les vieux films de SF des années 2010.

Une dizaine de personnes sont assemblées ici, autour d'un comptoir jonché
de cannettes de boisson hyper-énergétique, d'emballages de barres de
céréales hyper protéinées et de divers autres débris de verre et d'alumium
ayant contenus différentes formes de nourriture ou de boissons
hyper-calorique.

« Désolé de l'accueil un peu froid, dit une personne se laissant glisser
du tout de la navette vers l'intérieur de la soute. Mais nous avons pas
mal de visiteurs indésirables, leur dit elle, sur le ton fatigué
qu'utilisent les guides touristiques lors de leur sixième croisière
historique de la journée.

— Désolée de l'entendre. Je suis Tonya au fait.

— Nous savons qui tu es. Appelles-moi CX.

— Donc tu n'es pas la personne qui nous parlait tout à l'heure, demande
Saina ?

— Si. Et non. Mais je suppose que vous êtes venues pour ça non ? Ces
temps-ci plus personne ne viens jusque là. Mais au moins on profite un peu
de tranquillité, je suppose que c'est un plus. »

CX s'assied à une table en bakélite blanche, et leur fait signe de
s'asseoir sur la banquette. La moitié de son visage a été remplacé par des
appendices décoratifs et métallique, évoquant les mandibules et les yeux
des insectes. Mais en métal. De longues fibres optiques ont remplacés ses
cheveux, et une demi douzaine de ports de données de tous types ornent
sa tempe droite, là où se trouvait son oreille.

Sa façon de bouger et perturbante. Une démarche qui pourrait passer pour
naturelle si elle n'était pas autant parfaitement cyclique. Des
scarifications courent sur ses bras, laissant affleurer sous la peau des
inclusions de titanes, auxquelles sont attachées nombre de capteurs et de
diodes.

Son sternum est recouvert d'une batterie qui s'enfonce dans ses chairs, et
qui est connectée au nombreuses extensions nécessitant plus de puissance
que son métabolisme dopé ne peut en fournir. À en juger par la couleur de
sa peau, brûlée par le soleil, il doit la recharger avec un principe
proche de la photosynthèse.

« Bon, pourquoi est-ce que vous voulez entrer en contact avec 3L-10T,
demande CX de but en blanc.

Saina > Ok, quelque chose ne va pas, 3L-10T cherche à entrer en contact
avec nous.

Tonya > Si il n'y avait qu'une seule chose qui ne va pas, ça irait. Mais
je n'arrive pas à mettre la main-dessus.

— On cherche les carnets noirs, répond Saina.

Tonya > Mais … stop, non, t'es cinglée d'y aller comme ça, on obtiendra
jamais rien.

Saina > Mais si, laisse moi faire.

— Quels carnets noirs, répond CX, dont l'intonation a subtilement changée.

— Ceux qui ont été numérisés. On n'arrive pas à en trouver une version
valable en ligne, que des scans basse définition merdique, sans
reconnaissance de texte. »

Les réactions de CX ralentissent, ses mouvements deviennent plus lent. De
même que le reste des personnes présentes remarque Saina.

Tonya > Ils sont en boucle. Je savais qu'il y avait un truc foireux, ils
sont en boucle, ils reproduisent les mêmes choses encore et encore.

Saina > Ouais, et le signal vient de saturer. C'est … douloureux. Mais ça
devrait aller.

—  Mais vous voulez en faire quoi de ces carnets noirs, finit par demander
CX, d'une voix synthésée ayant perdu tout timbre.

— Les étudier, répond Tonya. Écoute, je pourrais t'expliquer exactement ce
dont il est question, mais je commence à en arriver à la conclusion que
soit tu ne sais pas ce dont on parle, et donc nous perdons notre temps,
soit tu n'as pas envie de nous y donner accès, auquel cas on perd aussi
notre temps.

Saina > Tu oublies une option, mais on en parlera plus tard. Je pense que
j'ai de quoi avancer un peu, mais j'ai une migraine monstrueuse. Il va
falloir que tu me porte, mais surtout que tu ne regarde ni mon link ni
aucun écran avant qu'on ne soit dehors.

Tonya > O K je suppose.

Un filet de sang s'échappe du nez de Saina avant que celle-ci ne
s'effondre sur la table, secouée de léger spasme. CX, ou plutôt, la
personne qui est dans le corps de CX, se remet à bouger. Ses mouvements
semblent désynchronisés les uns par rapport aux autres.

« Ne pas regarder d'écran, se dit Tonya. Elle en a de bonnes, il y en
a dans tous les angles ici.

— Merde, elle a du oublier de prendre ses médocs, dit Tonya. Désolée de
devoir partir si vite, on pourra reprendre cette discussion plus tard
d'accord ?

— On peut probablement la soigner ici, répond la personne qui n'est pas
CX.

— Non, vraiment, il lui faut surtout du repos. » Puis, attrapant Saina par
la taille, elle la soulève et se dirige lentement vers la sortie. Ces dix
mètres à parcourir lui semblent une éternité. Le reste des personnes
présente dans les restes de la navette la suivent des yeux et elle résiste
à l'impulsion de fuite et de partir en courant.

Arriver à l'extérieur et sentir le soleil et l'air a rarement été aussi
bon qu'au moment où elles laissent le Seljouk derrière elle. Tonya rentre
dans le Bodies'R'Shell et installe Saina sur une banquette, le temps
qu'elle retrouve connaissance.

« Il s'est passé quoi, demande-t-elle au moment de reprendre conscience ?

— Tu m'as fait flipper, un peu. Après ils m'ont fait flipper, beaucoup. Et
ensuite on est arrivée ici. Tu te sens comment ?

— Comme si le trans-sibérien m'était passé dessus. Mais ça va mieux. Il
vame falloir un peu de temps pour traiter ce que j'ai trouvé et confirmer
ma théorie.

» Ah, et il faut quand même que l'on contacte 3T-10L. Parce qu'on ne les
pas encore vu je pense.

— D'accord, mais demain. Là on va manger un truc, rentrer faire une sieste
et aller se changer les idées au Hanged Billionaire, d'accord ?

— Tu veux pas me prendre dans tes bras plutôt ? J'aime bien quand tu me
tiens comme ça, ça me détend. Et là j'en ai besoin. De ça et d'un peu de
calme.

— Si ça te faire plaisir, volontier. Et on pourra aller se poser dans ta
chambre quand ça ira mieux, on verra pour la suite du programme. »

Et le temps que le soleil disparaissent, elles restent toutes les deux là,
dans un silence relatif, au milieu des clients et des habitués qui
viennent se mettre à jour ici.
