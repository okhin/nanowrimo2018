# Chapitre 22

Depuis le début de cette histoire, il y a quelque chose qui échappe
à Nato. Le fait qu'il y ait un bout de logiciel qui soit capable de
provoquer des plantages cognitifs chez certains, c'est quelque chose
qu'elle peut admettre. Mais comment ce code a-t-il été généré ? Comment
a-t-il été testé ? Saina a bien réussi à en chopper un enregistrement,
mais elle s'est faites infectée. Pareil pour Tonya qui n'y aurait été
exposé que quelques instants. Et elle même est probablement infectée d'une
manière ou d'une autre.

Il manque le patient zéro, la première personne infectée. La machine sur
laquelle le code a été généré. Ça viens d'Uncanny, lui disent les autres.
Mais ce n'est pas une réponse suffisante. Où et comment cette machine
a état infinie a été créée ? Quel lien y a-t-il avec les carnets noirs
d'Elliot ? Et pourquoi avoir écarté aussi rapidement le collectif 3l107 de
l'équation ?

Elle a besoin d'y voir plus clair, et elle a donc faussé compagnie à la
bande qui est parti danser chez les Amazons. Trempée par la fine bruine
qui baigne le Complexe, elle arrive en vue de la Seljouk qui sert de base
au groupe Uncanny. Sa nuque la gratte, comme si quelqu'un la suivait, mais
c'est probablement l'hypervigilance induite par le data et la caféine. Le
manque de sommeil aussi. Mais elle ne parviens pas à éloigner cette pensée
de son esprit : il y a quelque chose de foireux dans le coin.

L'œil unique de la Seljouk projette une aura mauve sur son environnement
immédiat. Les longueurs d'ondes proche des ultraviolets font ressortir les
arabesques et les motif phosphorescent dessinés sur la coque de l'ancien
vaisseau spatial, donnant au tout l'impression de virtualité, de manière
un peu similaire aux projections de réalité augmentée qui étaient à la
mode il y a une dizaine d'année.

S'échappant de la carcasse du monstre lui parviens une lente pulsation,
comme un battement cardiaque. Ou une respiration. Le collectif sait mettre
les personnes mal à l'aise, note-t-elle mentalement alors qu'elle s'engage
dans les spirales qui mènent au centre névralgique d'Uncanny. Plus elle
s'approche de son objectif, et pire sont les migraines qui attaquent sans
relâche sa volonté d'avancer.

Un frisson la parcourt alors qu'elle pénètre dans le ventre du vaisseau.
Elle est déjà venue, ou du moins elle à l'impression d'être déjà venue
ici, un déjà vu, un flashback, une réminiscence mémorielle. Elle sait
qu'elle n'a jamais mis les pieds ici, mais pour autant ce lieu possède lui
procure une sensation de familiarité qui ne viens qu'avec la fréquentation
assidue des lieux.

Elle a l'intuition que, après avoir franchi le sas du cockpit qui la mène
à la soute, FM23 et Echo seront assis à une table, en train de jouer à un
jeu quelconque. Cette intuition lui parvient quelques microsecondes avant
qu'elle ne franchisse ce sas, mais elle sait que ce n'est pas une
perception de son environnement immédiat, c'est quelque chose d'autre. Une
intuition lucide qui perce sa migraine, qui émerge au-dessus du chaos
qu'est devenu le train de ses pensées.

« Hey, bienvenue ici. On peut faire quelque chose pour toi ? lui demande
FM23 dont une partie du visage a été remplacé par des prothèses visuelles
diverses, lui donnant un aspect insectoïde. Aspect renforcé par les
plaques de chitine sub-dermale qui décorent ses épaules et sa poitrine.

— Essaye pas de gagner du temps et joue, c'est ton tour, » lui répond
Echo, assise en tailleur sur une chaise métallique, des câbles et
appendices divers pendant dans son dos, la connectant à une valise sur
roulette de émettant un bourdonnement léger. « Mais effectivement, si tu
cherche quelque chose, on peut peut-être t'aider, ajoute-t-elle en fixant
Nato de ses yeux rétro éclairés, trahissant l'utilisation d'un afficheur
rétinien.

— Je suis pas sûre, commence Nato, s'installant sur un tabouret
métallique, j'essaye de comprendre ce qu'il se passe en ce moment.

— Bienvenue au club alors, répond Echo.

— Tu prétend que vous ne savez pas ce qu'il se passe ici ?

— Oui. Et non. Comme toi, j'ai des idés, des intuitions sur ce qu'il se
passe. Le principal souci, et je suppose que c'est pour ça que tu es ici,
est de savoir si tout ce bordel à un sens.

— Et bon, c'est pas comme si il était possible de faire confiance à notre
façon de penser, ajoute FN23.

— Comment ça ?

— C'est pas exactement nos pensées. Dans le sens où leur intentionnalité,
leur amorce, ne viens pas de nous, continue FN23. C'est … comme si quelque
chose aurait implanté ces idées toutes faites.

— Un genre de suggestion hypnotique ?

— Pas tout ça fait, répond Echo. Disons que le vecteur d'infection, le
machin que tu recherche, est une manière d'exécuter du code arbitraire
dans nos cerveaux. Du moins, de ce que j'en comprend. D'autres sont sur la
théorie de mèmes infectieux qui changeraient notre façon de penser, ou
d'autres pensent que ce sont des rayonnements lasers qui nous ont ciblé et
implantés certaines idées dans la tête.

» Le truc important c'est cette impression de déjà vu, ces intuitions qui
remplacent nos processus habituels de réflexion linéaire. C'est la charge
cognitive qui nous file des migraines, c'est …

— Je vois l'idée, l'interromps Nato. Mais comment ça a commencé ? Qui est
la patiente zéro ? Sur quel matos ça tourne ?

— Honnêtement ? On en a aucune idée. On a l'intuition que ça viens des
carnets noirs d'Elliot, mais pourquoi maintenant et pas il y a cinq ans
? On sait pas.

— Et pour le matos, complète FN23, on construit certes des trucs bizarres,
mais c'est pas chez nous que ça se passe. On comprend pas la théorie qui
serait nécessaire à faire fonctionner ça, comment veux-tu qu'on ai pu
construire une machine?

— Je sais pas. Un accident, un effet de bord imprévu ? Je veux dire, vous
avez publié un manifeste pour la liberté d'autodétermination de la
conscience juste avant, vous aviez forcément une idée derrière la tête ?

— Des manifestes, on en pond tous les mois, lui répond Echo. C'est sympa
que tu y accorde de l'importance, mais c'est plus un genre de blague
qu'autre chose.

— Admettons. Qu'est-ce qui a déclenché tout ça ?

— Réfléchit, lui dit FN23. Tu as toutes les cartes en main. Qu'est-ce qui
a changé dans le Complexe juste avant que tout ne parte en vrille ? Tu
sais que tout ça est lié à un calcul, tu sais que pour résoudre ce genre
de calcul, il faut un système d'abstraction conceptuelle. Tu sais que
personne n'a le matos pour faire ça. Tu en es donc arrivé au même point de
réflexion que nous, et que probablement l'ensemble du Complexe.

— C'est un code informatique qui s'exécute de manière distribuée sur nos
cerveaux.

— Ouais. La théorie d'Echo c'est que c'est cette langue syncrétique que
tout le monde commence à parler depuis quelques jours.

— D'accord. On sait ce que c'est. On ne sait toujours pas comment on en
est arrivé là.

— Et tu voudrais jeter un œil à ces carnets. Je vais te les chercher, mais
tu risque d'être déçue. C'est à toi de jouer Echo. »

FN23 se lève de la banquette ou ielle était installée, ses jambes
cliquetant doucement à mesure qu'ielle se dresse, et se dirige vers un des
placards métallique intégrés dans les cloisons de la soute.

Le temps qu'ielle face l'aller retour, Nato se sent envahir par une
panique grandissante, comme lors d'une descente de speed, ou quelque
chose du genre. Ses mains commencent à trembler et, pour gérer son
angoisse, elle commence à planter ses ongles dans ses poignets.

Quelques minutes plus tard, ou quelques heures, elle reprend ses esprits.
Elle est assise au pied d'un arbre, en bordure du canal. Une boite
métallique posée devant elle dans laquelle des petits carnets noirs tous
identiques sont entreposés.

Alors qu'elle s'apprête à en feuilleter un, elle perçoit un message
d'urgence. Le SOS envoyé par Ennie. En même temps que ce signal, des
centaines d'autres messages viennent remplir le vide informationnel qui
avait couvert le Complexe. Plurality n'est pas revenue, mais les systèmes
de communications basique commencent à percer le brouillard.

Le son du silence radio qui crachotait du bruit, est soudainement remplacé
par le brouhaha des connexion multiplexées et, l'espace d'un instant, Nato
est comme figée par le retour soudain du bruit. Elle attrape la boite de
métal qu'elle referme rapidement, et se met en route vers la position
d'Ennie, quelque part chez les Amazons.

Instinctivement, elle se connecte aux sondes qu'elle a laissé à Ivory
Tower, pour essayer de déterminer ce qu'il se passe. Leur processus de
cartographie est finalement en train de converger, de trouver une
solution. Et elle connaît maintenant la solution. Elle sait qu'elle est
écrite dans ces carnets qu'elle tiens sous le bras alors qu'elle court
à en perdre haleine, descendant le canal. Elle sait ce qui a changé et qui
a provoqué cette recherche de convergence. Elle sait quelle est
l'interface qui a permis à ce calculateur hybride de commencer son calcul.
