# Chapitre 3.

> [ Plurality/wiki/groups/Valkyries:last_edit ]
> 
> Un nouveau mix du groupe mémétique est annoncé prochainement. Composé de
> mix d'XP, l'ensemble serait disponible prochainement et une représentation
> du groupe devrait avor lieu pour le lancement au Tesla Plaza.
> 
> [ comments ]
> 
> +Nym+ > Pourquoi tant de conditionnels ? Elles sortent un album ou pas ?
> 
> Qwertz > Mais on ne sait même pas si ce groupe est actif. Il est en mode
> restreint, personne n'a jamais prétendu faire partie de ce groupe, et
> personne n'arrive à déterminer si elles ont déjà fait quelque chose.
> 
> Rach_L > Tu oublies un peu vite leur deux derniers albums non ?
> 
> Qwertz > Ceux qui ne sont plus trouvables sur le Complexe ou ailleurs ?
> 
***

S'ajuster à la vie du Complexe s'avère être une expérience particulière et
qui exige un investissement émotionnel auquel Saina ne s'attendait pas.
L'intégration des non négociables est, au final, la partie simple de ce
parcours. Ils existent afin de permettre aux visiteurs et nouveaux
arrivants d'avoir un fil d'Ariane lors de leur immersion souvent brutale
dans le chaos culturel qu'est le Complexe.

Le Guide de Survie de Plurality, édité, révisé et mis à jour par une
communauté de personnes qui s'est donné pour mission de faciliter cette
transition, a permis à Saina de ne pas être complètement larguée les
premiers jours, mais elle s'est vite aperçue que ce guide la limite
à certains quartiers du Complexe.

Les conseils de ce guide lui permettent cependant de pouvoir manger et de
trouver une place dans une de ces auberges ouvertes, au confort spartiate,
mais lui permettant de stocker les quelques affaires qu'elle a amenés avec
elle dans une chambre qu'elle partage avec Enny, une danseuse qui, après
en avoir eu marre de se foutre à poil pour des connards, a décidé de venir
se foutre à poil pour des personnes qui la respecte elle et son art.

Comme Saina, Enny est un peu perdue. Mais elle s'est facilement intégrée
à une petite communauté de camgirl, camboy, danseurs et danseuses qui
travaillent ensemble, produise du contenu explorant leurs corps de façon
parfois étrange, dans des spectacles quelque part entre le cabaret, le
strip et la danse classique.

Alors qu'Enny est donc en train de quitter cette zone tampon, d'accueil,
pour aller vivre avec la troupe, Saina elle reste perdue. La socialisation
constante l'épuise. Elle pensait pouvoir facilement gérer la situation
mais ses habitudes de chamane qui la font vivre en retrait des communautés
humaines, sont mises à mal par la proximité sociale et physique qui semble
être la norme ici.

Elle s'est bien inscrite à quelques groupes, afin de pouvoir commencer
à contribuer, mais il s'avère que le seul fait de se connecter à leurs
salons n'est pas suffisante pour savoir quoi faire. Tous sont dotés de
guides de participations, de règles de vies implicite ou non, et Saina
à l'impression de passer ses journées à lire des règlements, des
ressources pour comprendre comment marche le Complexe et, au final, la
seule chose qu'elle comprend, c'est qu'il n'est pas possible de comprendre
le Complexe.

Les chats, chiens, rats, pigeons, bernaches, perroquets et autres animaux
sociaux sont partout. Soit comme compagnons arrivé en même temps que leur
accompagnateur, soit tout simplement, parce que le Complexe leur fournit
un abri et un accès à de la nourriture. La plupart des habitants semblent,
au minimum, faire attention à eux, mais personne n'essaye de les
domestiquer, les laissant vivre leurs vies, leur donnant des friandises ou
câlinant ceux qui se laissent approcher.

Mais ce qui étonne le plus Saina, c'est le signal. Un bruit de fond
permanent, calme, lent, comme si en sourdine ou atténué, mais présent
à tout moment. Les circuits logiques tatoués sur son corps la stimulent en
permanence, changeant de manière à peine perceptible en fonction des
endroits qu'elle traverse. Elle s'attendait plus à une explosion de
signaux, quelque chose d'inconfortable, de presque douloureux, comme
lorsqu'une dizaine de personnes essayent de tenir cinq conversations dans
un bar et de couvrir la musique trop forte de leur voix.

C'est une sensation qu'elle connaît, et qui donne une forme au signal dans
toutes les villes qu'elle a traversées. Les téraoctets de données qui
s'échangent dans les airs l'épuisent, le prix à payer quand son épiderme
est devenu un processeur, et la force à une réclusion seulement
partiellement choisie. Mais ici, le signal est doux, presque constant,
comme la sensation de sentir le soleil sur sa peau. Ce signal est utilisé
de manière ad-hoc par les habitants, quand ils en ont besoin.

Pas de réseau social global qui vous pousse à tout partager. Pas de
centrale médiatique délivrant ses flux digitaux de fictions formatée afin
de faciliter la délivrance d'un message publicitaire. Pas de système
central de gestion de l'identité. Juste les informations que les personnes
ont envies de partager.

Les jacks et autres interface corporelle semblent être une norme,
notamment pour planer sur les rêveuses, mais il n'y a pas ici de data
junkies, de prophètes néo-luddites ou de conspirationnistes fêlés rejetant
toute technologie par principe. Ou du moins, s'ils existent, ils ne sont
que peu visible dans l'espace public. Mais même avec ces moyens de
connexion, le partage global et total de son intimité n'est pas une
exigence sociale.

Saina se laisse porter par ce signal. Par habitude elle a commencé un
journal public, mais cela va faire deux jours qu'elle l'a laissé tomber.
Elle se balade le long des canaux et dans Plurality. La densité
culturelle lui donne le tournis. Des mèmes naissent et meurent tellement
vite qu'il est difficile de saisir le sens ou l'ambiance générale. Son
masque lui signale qu'il y a à minima trois concerts et deux performances
artistiques dans les deux cents mètres autour d'elle, mais elle ne sait
pas comment y aller, personne ne semble avoir de carte ou de plan.

Elle finit par débouler sur un plazza, s'avançant au-dessus de l'eau,
soutenu par des cariatides, à mi-hauteur d'un petit immeuble de bureau.
Les lières et les bougainvilliers enserrent dans leurs lianes les
cariatides, et s'élancent vers le sol et vers l'eau.

Le plaza est, semble-t-il, géré par Gurd, un groupe de botanistes et de
biologistes qui essayent de générer des substances psychoactives en
travaillant sur les relations de symbioses ou de parasitage entre
différentes plantes et champignons.

Parler de gestion est cependant exagéré. Gurd fournit un accès à sa
production, et s'assure que des volontaires maintiennent le lieu en état
et tiennent à disposition de quoi intervenir en cas d'overdose ou d'autres
incidents. Le reste est à la charge des personnes présentes sur le plazza.
Des pictogrammes indiquent certains points d'intérêt, composant un langage
compréhensible par de nombreuses personnes et qui, de plus, sont connectés
à un lexique sur Plurality pour pouvoir traduire leur sens dans une langue
au choix du lecteur.

Elle s'est assise à une table, et a posé devant elle un thé relaxant qui
finit d'infusé, libérant une odeur marquée d'humus due à la
fermentation des feuilles de thé et de valériane, fermentation stimulée
par le milieu micellaire dans lequel poussent ces plantes. Les effets
relaxant de l'infusion, combinés au calme relatif environnant l'aide à se
détendre et à lui permettre d'établir un début de synchronicité avec le
signal.

Son masque l'aide à modifier sa perception et sa compréhension du
bruit ambiant. Elle commence à percevoir plus distinctement les motifs
émergeant du signal. Elle ne trouve plus l'appel qui l'a amenée ici, mais
cela ne l'inquiète pas outre mesure, après tout, il n'y a aucune raison
que ce signal soit émis en boucle. Elle cherche à établir une carte
mentale des échanges de données, à voir quels sont les principaux aspects
du signal, afin de pouvoir adapter ses rituels en cas de nécessité.

« L'adaptation est la clef. Et c'est pour cela que je t'apprends comment
marchent nos rituels et non pas à les ânonner stupidement sans en
comprendre le sens. » lui a dit Ylsen alors qu'elle travaillait sur ses
runes d'amplification et qu'elle butait sur la structure étrange d'un
système inconnu.

C'est ce qu'elle fait en ce moment. Observer pour comprendre, comprendre
pour agir. Il lui faudra probablement plusieurs semaines encore pour
peaufiner ses runes, mais elle n'est pas pressée et elle se laisse
imprégner par ce bruit calme. Les rayonnements électromagnétiques lui
caressent la peau là où ses tatouages correspondent à la bonne longueur
d'onde, et le spectre ici est globalement chargé. Elle est caressée, en
permanence, par le lent ressac des transmissions.

Elle profite de cette synchronisation quelques minutes avant de se décider
à chercher quelque chose à faire. Enny, quand elle lui a dit au-revoir,
lui a dit qu'elle devrait passer la voir au Hanged Billionaire ce soir. La
convocation d'une rapide recherche lui donne beaucoup trop d'entrées et de
contenus lié au Hanged Billionaire. Il semble s'agir d'un des gros lieux
dédié à la fête au sein du Complexe, et elle a du mal à comprendre la
planification ou l'ordre dans lequel les choses se passent, ou même de
savoir s'il y a quelque chose de prévu spécifiquement ce soir.

Une des excroissances mémétique de la convocation l'attire cependant,
quelque chose qui génère de nombreuses discussions, un mème suffisamment
solide et stable pour être remarqué au milieu des milliers de résultats.
Quelque chose qui suscite beaucoup de questions, mélange d'anticipation
euphorique teintée de l'amertume des conspirations. Amertume atténuée par
la relative innocuité d'un mème qui semble être essentiellement lié à des
concerts et des soirées. Le mot qui le caractérise, issus des mythologies
nordiques, rapproprié et transformé par la mémoire culturelle moderne, est
Valkyries.

Ce qui intrigue en particulier Seina, c'est que, à part une petite page
sur Plurality, personne n'est capable de se mettre d'accord sur ce
qu'elles sont réellement, si le groupe existe toujours, si c'est un
groupe, le lieu de leur prochain show, ou si ce n'est qu'un gigantesque
hoax.

Personne n'a été capable de trouver qui aurait pu lancer un tel hoax, le
consensus s'est finalement établi sur le fait que ce groupe a, à un moment
donné et pour une durée déterminée, existé et produit des albums qui sont
devenus introuvables en ligne. La rumeur autour d'un concert
prochainement, voire ce soir, ne cesse cependant de croître et a été
lancée par une édition anonyme de leur page.

Un point sur lequel cependant tout le monde est d´accord est que ce groupe
dispose d´un sigle, une épée ailée stylisée, empruntant aussi bien
à l'esthétique néo païenne, qu'à celle des souks saturés de leds du
Complexe. Une si forte charge symbolique dans un seul symbole,
permettant de toucher un public extrêmement large, relève d'une maîtrise
technique rare. Et ce symbole, apparu à priori sur des fresques murales il
y a quelques années, est un générateur de discussions sémiologique sans
fin.

Les effets relaxant de son thé de valériane commençant à se dissiper, et
réalisant qu'elle n'a pas mangé depuis une dizaine d'heure, Saina sort de
sa transe, éteint son masque et se dirige vers le comptoir au centre du
plazza sur lesquels sont disposés quelques fruits et légumes, mais surtout
des tachareks, makrouds et baklavas fourrés à la pistache ou aux dates. 

Elle glisse une poignée de ces pâtisseries dans un sac, avant de les
fourrer dans une des besaces qu'elle porte à la ceinture. Elle avale
rapidement un baklava et se met en route en direction du souk quatre,
quelque part à mi-hauteur de la tour, là où, suite à un effondrement d'une
partie des planchers, un patio a été organisé, transformant les médinas
de ces étages en espace ouvert vers l'intérieur. Des terrasses ont été
installés sur la façade sud-est, cannibalisant les baies vitrées typique
de ces immeubles, pour en faire une structure translucide.

De l'autre côté du vide, presque à la même hauteur, un pont est en cours
de construction, connectant la tour SeAz à la tour quatre. En attendant
que le pont ne connecte définitivement les deux tours, les base jumpers
se servent de cette avancée comme point de saut, le canal étant à une
centaine de mètres en contrebas, ils ont le temps d'accumuler suffisament
de vitesse pour leurs combinaisons de vols.

Saina passe devant un warung duquel s'échappe l'odeur caractéristique du
saté. L'échoppe n'est indiquée que par un signe lumineux, un caractère qui
semble impossible à traduire, la plupart des systèmes de traductions
plantant lorsqu'ils sont confrontés à ce symbole, fournissant, de fait,
une zone depuis laquelle peu d'images sont créées.

Et même avec les syntaxeurs du créole utilisés par les habitants, mélange
de langues slaves, indiennes et européenne, alimenté par un argot vibrant
et complexe, il reste impossible d'extraire du sens de ce symbole à l'aide
de machine de Turing.

Écartant le rideau en PVC qui isole à peine le comptoir du souk,
Saina est prise d'assaut par les subtiles odeurs d'épices composant le
saté. Le comptoir est étroit et deux personnes y sont déjà appuyée,
dégustant un burger de tempe appétissant.

La cuisinière lui tend un menu listant la demi-douzaine de plats
qu'elle prépare, tout en surveillant une marmite dont le contenu
bouillonne tranquillement. Saina demande un bol de lotek et s'installe au
comptoir. Quelques secondes après, la cuisinière lui tend avec un grand
sourire un bol rempli de légumes, de tofu et de tempé mariné et frit et
une grande feuille de bananier pliée dans laquelle se trouve le riz.

Saina mange rapidement son lotek dans l'ambiance saturée d'une humidité
presque tropicale du warung. La nourriture est à l'avenant des langues ou
des cultures ici : un joyeux mélange de nombreuses influences, produisant
quelque chose de réconfortant mais également dépaysant et toujours
surprenant.

Après s'être servi un café à l'indonésienne, elle salue les personnes
encore présente, ramasse ses affaires et retourne se plonger dans le Souk.
Elle se met en route vers le Hanged Billionaire, il lui faut traverser une
bonne partie du Complexe avant d'y arriver.

À vol d'oiseau le trajet est cours, mais contourner les médinas en restant
dans les souks et le long des canaux rallonge le trajet, d'autant qu'elle
est fréquemment interpellée pour participer à des discussions, fournir un
coup de main pour déplacer du matériel, ou proposer des services divers et
variés.

Elle traverse un marché dans lequel beaucoup de monde proposent leurs
services contre rémunération, parfois en baltes ou en euros, souvent
contre d'autres services. Elle est surprise par le fait qu'il s'agisse
d'un lieu de vente, de négociation, un lien dans lequel les échanges
semblent régit par des contrats explicites auxquels s'associent deux
parties ou plus.

Rien dans les non-négociable n'empêchent la vente, ou l'échange de
services contre de la monnaie ou des biens, tant qu'il n'y a pas
d'intermédiaire. Certaines personnes, assise un peu partout autour de la
place sont identifiés comme juge, ou quelque chose d'équivalent, plus
proche d'un arbitre ou d'un médiateur lui signale une note sur Plurality,
servent de témoin, apportant parfois leur soutien, aux contrats négociés
et signés ici, en échange d'une part, parfois substantielle, de ce qui est
négocié.

En observant les personnes, entre autre par la consultation de leurs
profils en ligne, Saina déduit que ce genre de lieu est surtout fréquenté
par les nouveaux arrivants et différents groupes qui ont une présence en
dehors du Complexe. Gangs, trafiquants divers et personnes paumées
constituent donc une grande part des passants. Et même si les situations
peuvent être tendues, les situations amenant à de la violence physique
restent rare, et la présence d'une paire d'Amazons, assises de manière
à avoir une bonne vision de la place, semble être une dissuasion
suffisante pour la plupart.

Saina profite des discussions auxquelles elle participe dans son trajet
pour s'ajouter quelques contacts, elle s'arrête danser au son d'un concert
impromptus d'un groupe dont les instruments électroniques sont bricolés
à partir d'électroménager abandonnés, elle grignote une part de gâteau
expérimental contenant une dose intéressante de produits stupéfiants. Elle
se retrouve, planant, sur les rives des canaux, au milieu de parcs et
jardins mélangeant ornements, réserves naturelles et potagers. Des
lampadaires rouillés sortent du canal, derniers témoins que la rue se
trouve quelques mètres sous l'eau.

Les reflets du soleil couchant teignent les vitres miroirs de la tour
quatre en fushia. L'ambiance lumineuse saturée pulse doucement au rythme
du ressac numérique qui lui caresse la peau. Elle perçoit pleinement cette
pulsation douce qui la berce, la fait dériver dans ses pensées et mêle sa
peau aux hallucinations numériques qui commencent à apparaître dans son
champ de vision. Les signaux émis par les relais ou les appareils se
traduisent par une synesthésie visuelle, les ondes rebondissant sur les
façades teintant le paysage de leur longueur d'onde.

La projection hallucinatoire de l'espace informationnelle dans l'espace
contextuel, stimulée par les stupéfiants légers amène Saina à s'asseoir au
bord de l'eau, ses pieds affleurant la surface du canal, envoyant des
ronds dans l'eau qui se mélangent aux champs électromagnétiques qui
continue de courir sur la peau processeur de Saina.

Elle se surprend à murmurer, laissant échapper des mots dépourvus de sens,
les centres du langage de son cerveau étant soumis à une forte charge
cognitive. Le signal radioélectrique converge lentement le long de ses
marques chamaniques, descendant le long de sa colonne vertébrale pour lui
effleurer les fesses, glissant depuis ses clavicules vers ses seins pour
enserrer ses tétons. Une caresse légère juste ce qu'il faut de perceptible
pour être remarquée et amplifié par les drogues contenues dans ce qu'elle
a mangé aujourd'hui.

Elle s'allonge sur le béton chauffé par le soleil couchant,
l'hallucination numérique perturbant son sens de l'équilibre, et
s'abandonne aux caresses du signal, laissant échapper des gémissements
qu'elle associe, quelque part dans ses centres cognitifs, aux
bourdonnements des transformateurs parcourus par des courants électriques
intenses. Le bourdonnement s'intensifie à mesure qu'elle perçoit les
stimulations du signal radio électrique sur sa peau processeur, à mesure
qu'elle le perçoit descendre doucement entre ses cuisses, au bout de ses
doigts ou encore le long de sa langue bifide.

Les pupilles dilatées comme rarement, elle voit les nuages onduler en
rythme avec les communications qui la caressent, le rythme évoluant avec
la taille des échanges de données. Ralentissant, diminuant en intensité,
avant de ne devenir plus qu'un léger picotement le long de ses mains, une
sensation diffuse sur sa peau transpirante. L'effet des drogues se
dissipant, l'hallucination redevenant diffuse, cachée quelque part à la
limite de son champ de perception.

Elle reste allongée sur le béton de nombreuses minutes, le temps que le
soleil finisse de se coucher, le temps que l'ocytocine ne se dissipe, le
temps qu'elle profite des sensations qu'elle vient de ressentir, d'essayer
de les archiver, de les stocker quelque part. Puis elle se remet en route.
Le Hanged Billionaire ne devrait plus être loin.
