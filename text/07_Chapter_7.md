# Chapitre 7.

> [ rumeurs.net/user:Saina/inbox ]
> 
> Tonya > Passe me retrouver au Jardin 0 quand t'as deux minutes, j'ai un truc
> à te montrer. On ira au Hanged Billionaire après. Et je te mets un petit aperçu
> de ce soir 
> 
> * Tonya has attached the following files, they'll be deleted after you download
>   them according to their privacy settings.
> 
> [ top_off.pic] [ tease_1.mov ] [ tease_2.mov ] [ undies.pic ] [ boobs.pic
> ] [ smack.pic ]
> 
* * *

La journée est bien entamée au moment où Tonya rejoint Park, en bas du
Mfundo, là oú des gerbilles génétiquement modifiée se sont installées, au
milieu d'une jonction électrique. Pendant qu'elle démonte une des cloisons
qui permettent l'accès à la jonction, Park, adossée à un mur, enfile un
harnais doté d'une mini caméra LIDAR à un des rats qui jouent avec elle.

La rêveuse de Tonya, configurée en mode blast diffuse de la néo soul. Les
derniers écrous, un peu rouillés note mentalement Tonya, se détachent et
libèrent le panneau d'acier, donnant un accès à un fourreau emplit de
cables et de fibres diverses. Le boitier de distribution est un peu plus
loin, mais il n'y a pas d'accès simple, c;est pour ça que Park lui
a demandé un coup de main.

« Tu envoies Bisbille jetter un œil là-dedans ? Qu'on y voit plus clair,
avant que je ne me fasse chier à découper la cloison là-bas, je voudrais
être sûre qu'on ne peut pas faire autrement.

— Yep, laisse moi juste finir de lui attacher son harnais. » répond Park,
avant de pousser le gros rat gris et blanc qui joue entre ses jambes dans
l'ouverture. « Plus qu'á attendre un peu, le temps qu'il aille dans le bon
sens. Puis après un silence, elle reprend.

— Donc, on a une nouvelle habitante ?

— Ouais. Ça te gène pas ? T'as pas beaucoup été là, et les autres étaient
toutes d'accord.

— Oh, tu sais moi, tant qu'on ne m'embete pas, je m'adapte hein. Et du
coup, tu la sens comment ?

— Je saispas trop. C'est une chouette personne, je l'aime beaucoup, mais
il y a des trucs, pas simple.

— Rien n'est jamais simple tu sais, tu saurais détailler ?

— Je pense que c'est son chamanisme, sa façon de voir le monde. Ça et le
fait qu'elle ait vécu un certains temps en ermite. Elle n'a pas l'habitude
de la proximité physique. Du moins, ça la perturbe pas mal.

— J'irai pas la blamer pour ça hein, tout le monde n'est pas aussi
à l'aise que toi avec les rapports tactiles et intime. Surtout si elle
a passé beaucoup de temps seule tu sais.

— Je me doute. Ça viendra avec le temps je suppose.

— Ou pas, et il te faudra bien l'accepter.

— Ouais. »

Après un silence entre elle qui grandit, les pings LIDAR de Bisbille
arrivent sur le link de Park, se signalant par un bipbip démodé. Le
système d'imagerie permet d'afficher une vue tridimensionnelle de la
galerie devenue une des annexes du terrier d'une communauté de gerbilles.

Tonya ne remarque pas de dégat significatifs, si ce n'est les habituelles
marques de dents et cables sectionnés. En revanche, Park finit par
comprendre rapidement ce qui a attiré les rongeurs ici : un tas de déchets
organiques combinés à une relative aridité, un endroit parfait à l'abri
des prédateurs, même Bisbille a du mal à parcourir les tunnels
minuscules.

« Les faire partir ne sera pas simple. Si on peut isoler un peu le nid de
l'infrastructure ça sera mieux pour tout le monde.

— Ouais, les transfos ont l'air intact, quelques câbles à remplacer, mais
avec un fourreau renforcé ça devrait le faire, j'en profiterais bien pour
changer un ou deux adaptateurs, ils commencent `être vieux. Par contre,
d'où vient cette merde ? Bisbille peut essayer d'aller voir au-dessus ?

— Ça ne marche pas comme ça. Il va falloir attendre que Bisbille y aille
de lui-même. Je peux l'encourager quand il est dans la bonne direction,
mais il va falloir le laisser faire un peu. » Elle marque une pause. « Tu
ne veux vraiment pasen parler ?

— De quoi ?

— De ce qui te gènes chez Saina, de son chamanisme.

— J'ai pas dit ça. J'ai dit que j'étais pas à l'aise.

— Et tu es suffisament peu à l'aise, que en parler avec ta partenaire et
vermine préférée t'effraie ?

— Mais … » Tonya abandonne l'idée de répondre encore à côté de la plaque.
Park comprend son langage corporel à un niveau instinctif, et elle n'a
jamais pu vraiment lui cacher de choses qui la tracasse. Une des raisons
pour laquelle elles ont une relation. « Ok, ok, j'abandonne. Tu as raison,
je ne suis pas une grande fan des délires mystico-religieux.

— Je sais. Mais alors, pourquoi tu te poses des questions, c'est
relativement simple, non ?

— J'ai besoin d'ordonner mes pensées. Je vais chercher du matos pour
réparer et je nous ramènes à bouffer. Et on en parle pendant que je
bricole. Ça te va ?

— OK. Prends quelques trucs pour Bisbille et ses nouvelles copines aussi,
si tu trouve. »

Tonya l'embrasse avant d'aller dans le souk Mayakovski, le plafond nuageux bas
donne aux rues actives du souk une ambiance étrange, étouffée. De légers
sifflements précèdent les personnes sur leurs engins électriques,
projetant des halos colorés se reflétant dans le brouillard ; les
décorations, enseignes et portes séparent l'espace et le temps en entité sémantique
différenciées à défaut de pouvoir distinguer les limites physiques de ces
objets.

Tonya arrive à l'un des comptoirs de recycleurs. L'espace autour est
recouvert d'objets divers, en tas ayant plus ou moins de logique, d'étal
et de bacs contenant une grosse quantité de petites pièces. Ces tas
grossissent à mesure que l'on se rapproche du seuil, avant de former des
colonnes qui semblent soutenir la structure. Au fond les bacs d'algues
digèrent les plastiques et les transforment en composé plus simple
à traiter.

« Hey, Tonya, contente de te voir, lance une voix depuis l'intérieur du
comptoir, je peux faire quelque chose pour toi ?

— Salut Io. Ouais je pense, on a des bestioles qui se sont installées dans un
découpleur de puissance, du coup j'ai besoin de protéger ça et de remplacer
quelques pièces.

— Laisse-moi voir ce qu'il y a dans l'arrière-boutique, et hésite pas
à fouiller. » ajoute Io, habillée d'un bleu de travail teinté
de manière permanente de taches de graisse et de rouilles avant de
rabattre ses lunettes sur son nez alors que leurs verres commencent
à luire, signe de l'activité d'un écran, et de se diriger vers le fond du
comptoir, laissant Tonya parcourir et essayer de cataloguer mentalement
les composants à portée de vue, tâche épuisante, mais on ne sait jamais,
il y a toujours des choses intéressantes à trouver dans le coin, et il va
bien falloir finir la piaule de Saina.

Quelques minutes plus tard, Io surgit de derrière Tonya, les bras encombrés
d'une caisse en polyuréthane bleu, débordant de câbles, des gaines roulées
autour de ses épaules, et, entre les dents, un boîtier d'adaptation Scalectric.

« 'e cr'is 'e 'ai 'e 'i te 'aut, essaye d'articuler Io pendant que Tonya l'aide
à se délester.

— Je vois ça ouais. Pour ce boîtier il va me falloir les …

— … adaptateurs oui, ils sont là-dedans. Tiens, tu devrais avoir suffisamment
de câbles et de fibres pour remettre le tout en état. Je t'ai mis divers
couvercles et boitiers aussi, pour cloisonner ça. Tu feras gaffe le Sca …

— …lectric chauffe plus que le modèle Tata, je sais, tu m'as pris pour une
débutante ?

— Mais non. Bon, il te fallait autre chose ?

— Je sais pas. On a une nouvelle chez nous, et on lui prépare une piaule, je me
demandais si t'avais pas un truc pour décorer un peu. Mais tu as forcément
quelque chose, du coup je cherche.

— Bah, à part dans les gravats, l'inventaire ici est un peu faible, faut croire
que vous êtes pas les seuls à accueillir des nouveaux. Mais si tu veux, je dois
avoir quelques systèmes de lumière, faudra que tu remplaces certaines LED par
contre, mais c'est particulier. Attends, je vais te montrer.

Posant au sol ce qu'elle a dans les bras, Io se dirige vers des étagères
métalliques pliant sous le poids des caisses de composants électriques et
commence à parcourir rapidement l'inventaire projeté sur ses lunettes avant
d'envoyer une commande mentale à une araignée mécanique qui se dirige dans les
hauteurs des étagères extraire la pièce voulue par Io.

« Tiens, attrape, Io lance vers Tonya le boitier sphérique en bakélite que
l'araignée vient de lui donner.

— Merci. C'est quoi ?

— Tu vois ces systèmes de lumière qui bougent en fonction du son ? Ben ça
c'est pareil, mais avec le spectre électromagnétique. La lumière varie en
fonction des fréquences et de l'intensité, ou ce genre de chose. Ça
t'irais ?

— Ouais, je pense. Merci. Oh, tu sais s'il reste des choses à manger chez
Joe ?

— Aucune idée, mais j'ai vu des gens en sortir il y a quoi, un quart d'heure ?
Je pense que c'est ouvert.

— OK, parfait. Et hésite pas à passer à la maison quand tu as deux minutes.

— Ouais, dès que j'ai fini de trier ici, pas de soucis.

Et sur ces mots, Tonya, les bras chargés d'une caisse de matériel, s'éloigne
du comptoir et laisse derrière elle ce cimetière technologique qui ronge
doucement le Souk, avant de passer faire un détour par l'étal de Joe.

Deux cafés à la sauge, une plaquette de bra1n — un stimulant cognitif,
déclenchant une hypersensibilité tactile — hypersensibilité tactile —
quelques pâtisseries et des fruits à coques sont ajoutés à son
chargement pendant qu'elle prend des nouvelles de Joe et de sa marmaille.

Elle retrouve Park, qui joue avec Bisbille et une paire de petits
rongeurs, probablement quelques-unes des gerbilles du nid. Sa faculté
d'établir des liens avec des animaux super rapidement continue d'étonner
Tonya.

« Me revoilà, dit-elle, posant son chargement au sol, des nouveaux amis ?

— Ouais. Et je crois que j'ai trouvé pourquoi ils se sont installés dans le
coin. C'est pas uniquement en rapport avec les déchets qui sont arrivés là.
T'as pris quoi ?

— Je suis passé chez Joe. Du coup, cafés, brochts et muffins. Et des noix pour
Bisbille et les gerbilles.

— Oh, cool. Joe va bien ?

— Il avait l'air oui, d'après ses gosses du moins. »

Tonya commence à préparer le matériel nécessaire pour réparer le
convertisseur Tata défoncé par les gerbilles. En ayant marre d'attendre
que Tonya aborde le sujet, Park lance :

« Bon. Je vais pas attendre que tu ais envie de parler, parce que sinon
t'auras recabler trois fois le Complexe avant que tu ne te décides. Je
résume, tu me dit si je me plante, ok ? Il y a cette personne, Saina, qui
débarques apr hasard dans ta vie. Tu la trouve sympa, voire tu crushe un
peu sur elle. Elle est un peu paumée, donc tu lui propose de venir habiter
avec nous. Probablement parce que tu as envie de coucher avec elle, mais
aussi parce que tu ne peut pas t'empêcher d'aider les gens.

» Ça ne se passe pas tout à fait comme tu l'avais anticipé, notamment car
elle n'a pas encore digéré son arrivée ici et que, comme d'habitude, tu va
trop vite en besogne. Tu as envie de mieux la connaître, mais il y a ce
gros trucs qui te bloque et, plutôt que d'en parler avec elle directement,
tu cherche un avis extérieur pour savoir si c'est toi le problème. J'ai
bon ?

— Pour l'essentiel. Le ton narquois en moins, et t'as bon.

— Hey, si tu ne veux pas être narguée, me donne pas d'occasions de le
faire hein. Alors, tu veux parler ou tu préfères attendre que la situation
te stresse trop pour pouvoir le faire ? »

Tonya démonte machinalement la cloison qui la sépare du nœud de jonction
tout en essayant de formuler sa pensée.

« C'est quand même un système religieux le chamanisme, obscurantiste donc.
Tu es OK avec ça ?

— Que je soit OK avec ça n'est pas la question. Est-ce que toi, Tonya, tu
es OK avec ça ?

— Non. Je ne pense pas. D'accord, ce n'est pas une religion organisée, ou
autre système sectaire, mais ça reste une méthode obscurantiste de
description du monde.

— Pas nécessairement. Regarde comment moi je m'intègre à mon
environnement.

— Ben, ça n'a rien à voir.

— Au contraire. Par exmple, qu'est-ce que je dit toujours quand on me
demande ce que je fais ?

— Que tu n'extermines pas, mais que tu contrôle les bestioles, et que si
elles sont là, c'est qu'il y a une raison.

— C'est surtout que je dois analyser la situation trouver le déséquilibre
et voir comment je peux ramener la situation à un status quo. Si je trouve
pourquoi ces gerbilles sont là, je peux aussi trouver comment faire
ensorte qu'elles s'installent ailleurs.

— Ok, mais quel rapport avec Saina ?

— Jamais tu changeras hein, répond Park laissant échapper un soupir
d'agacement. De ce que j'ai compris de l'approche de Saina, et je n'ai pas
vraiment eu l'occasion de détailler, elle a une approche similaire. Elle
regarde la globalité du système, essaye d'en décoder les mouvements et
de comprendre comment les informations se positionnent les unes aux
autres. Elle approche l'espace informationnel comme un botaniste approche
un jardin, un écologiste un biotopes ou une vermine un déséquuilibre local
du milieu.

» Et une fois ces causes identifiées, elle agît dessus. Elle n'utilise
effectivement pas les outils mathématiques qu'utilisent la plupart des
nerds dans le monde, essayant d'explorer cet espace avec des outils
sémantiques plutôt que logique, en utilisant des métaphores hallucinées au
lieu de codes informatique. Mais ce n'est pas pour ça que sa façon de
procéder est invalide.

» Quand au côté obscurantiste, il vient souvent des systèmes de croyances
organisés. Et je ne vois pas ça dans ce qu'elle dit ou fait. Et, encore
une fois, je ne base ces informations que sur le peu que j'ai pu en voir
d'elle.

— On peut voir les choses commeça je suppose oui. Reste que c'est étrange
non ?

— Par rapport à quoi ? Au monde dehors ? Oui, probablement. Mais deux
meufs qui sortent ensemble dans des relatiosn non exclusives, c'est
bizarre pour eux déjà. Alors quelqu'un qui exploite le côté linguistique
et sémantique de l'information, c'est sûr, ça doit leur paraître étrange.

» Mais ici ? Regarde le nombre de personnes qui se sont volontairement
amputés pour pouvoir modifier lourdement leur corps, regarde la quantité
d'expérience sociale qui ont lieu, de personne qui explore les limites de
l'humanité, ou qui prétendent aller au-delà. Non, elle n'as pas une
approche étrange. Juste une approche que tu ne comprends pas. Et ça
t'emmerde, parce que ça ne t'arrives pas souvent. C'est probablement ça
qui t'attire chez elle d'ailleurs.

— Clairement. 

— Hey, souris, je charges la mule, mais jet'aime quand même. Et promis,
j'arrête avec le ton condescendant, mais seulement si tu me parle,
d'accord ?

— Yep. On dépose ce panneau d'abord, d'accord ? »

Elles se lèvent et saisissent fermement le panneau de plastbéton qui ferme
la cloison avant de le poser de côté, permettat à Tonya de pouvoir accéder
plus facilement à la zone de réparation.

« Bon. Admettons que tu ais raison.

 Il n'y a rien à admettre, j'ai raison, c'est tout.

— D'accord, tu as raison. Mais comment je fais moi, pour partager des
choses avec elle si on ne partage pas la même approche du monde ?

— C'est pour ça qu'on a inventé le language, que l'on est capable de
pensée abstraite. Et tu trouveras un moyen de comprendre. Je suis bien
placée pour le savoir.

— Le fait qu'on ait quasiment grandies ensemble y est pour quelque chose.

— Sans doute. Il y a aussi beaucoup de choses avec lesquelles on est en
désaccord. Mais c'est pas grave, on arrive à se parler et à se comprendre.
Même si je nevois pas le monde de la même façon que toi.

— D'accord, d'accord, » répond Tonya. Puis, après un silence, elle
ajoute : « Merci de la discussion, je suis larguée et je me sent un peu
conne. Je suis pas certaines d'être tout à fait d'accord, mais j'y vois un
peu plus clair. Et tu as raison, je vais en parler avec elle.

— De rien. Tu veux savoir d'oú viennent les gerbilles au fait ?

— Ouais. Du coup, tu m'expliques ce que tu as trouvé ?

Et Park explique, détaillant par le menu les escapades de Bisbille, d'où
viennent les gerbilles. Elle s'est aperçue que certaines d'entre elles ont un
pelage partiellement phosphorescent. Elles ont probablement été contaminé par
un virus qui se seraient encore échappé d'un labo.

Et en parcourant Plurality, et avec une estimation de l'âge des rongeurs,
la proportion de pelage phosphorescent ou de la présence d'autres parasites
génétiques, elle a localisé leur point de départ de manière plus précise.

« C'est à GlowTopia. Ça a une certaine logique tu me diras. Et ça explique que
ce soit ces gerbilles qui sont ici.

— Pourquoi ?

— J'ai mis un peu de temps a es trouver, on a pas assez de capteurs là-bas.
Du moins, on a plus assez de capteur là-bas. Une partie de l'infrastructure
est immergée depuis quelque temps déjà. Il faudrait aller voir, mais je vais
surtout laisser une note aux autres, j'ai pas envie de traverser le Complexe
pour ça.

— Bon. Ben plus qu'à patcher et remettre en état. Tu me files un coup de main ?

— Pas de soucis.

Tonya et Park s'attèlent donc à la réparation et au recensement de cette
médina de gerbilles phosphorescente. Le brouillard commence à se dissiper
alors que le soleil se couche. L'ambiance fantomatique de la fin d'après midi laisse
place à celle, plus nette, plus lumineuses, des débuts de soirée du Complexe.

Elles se séparent une fois la galerie technique refermée, Tonya, du cambouis
jusqu'au coude, se dirige vers les Valkyries, afin de se rincer, de se changer
et de se mettre en route pour le Jardin 0, après avoir invité Saina à la retrouver,
et Park escalade les containers et autres habitations temporaires du coin
pour grimper sur les toits et aller voir si les piafs vont bien.
