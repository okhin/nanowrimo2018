# Chapitre 11.

[ Plurality/Call_to_actions/Updates ]

Saina > Hey, je cherche à savoir quel est le langage minimum que l'on
peut utiliser ici. Pour cela j'essaye de rationaliser et de réduire le
nombre de tags dans [DreamLand]. Du coup, allez faire un tour là bas, mettez
à jours vos fav', etc.

[ comments ] 

WuT > WuT ?1?

__R1der > Ok, la blague était pas drôle il y a cinq ans WuT, elle ne l'est
toujours pas.

Anomnomnymous > Oh, cool, un prétexte pour reclassifier toutes ces XP de
bouffe qui s'entassent.

U+1f351 > J'étais plutôt partante pour remettre le [r_34] à jour moi.

U+1f346 > Oh oui tiens, je vais passer te donner un coup de main.

Vox > Oubliez pas de retaguer ce que vous allez produire vous deux.

U+1f351 > Vox: Fait pas comme si tu pensais qu'on ne serait que deux. Si
quelqu'un qui traine ici veut passer, ma piaule est comme mes DM : en
accès libre.

Stelle > Bon, ben je vais dire à Saina qu'on va surtout avoir du porn.
Great.

__R1der > Vous vous attendiez à quoi d'autre ?

Stelle > Je sais pas.

* * *

« Pourquoi vous êtes toutes parties du principe que c'était une mauvaise
chose, ou qu'il fallait y faire quelque chose, » questionne Saina qui se
sent particulièrement usée, d'autant que ses maux de têtes ne faiblissent
que peu.

Ça fait deux jours maintenant que la médina discute de ce qu'elle
a trouvé, et se pose la question de quoi faire. Personne ne semble
envisager la possibilité qu'il faudrait peut-être ne rien faire.

« Ben, on ne sait pas ce que c'est, t'es pas curieuse ? lui répond Tonya.

— Tu sais que ce n'est pas ce que je veux dire Tonya. Bien sûr que si je
suis curieuse, et oui, il faut faire gaffe. Mais j'ai l'impression que
votre approche est de commencer par limiter des dégâts qui ne sont, pour
l'instant, que théorique.

— Il y a ce hack basilisque dont tu nous as parlé, c'est pas complètement
inoffensif hein.

— Et soit il est déjà installé massivement partout pour faire je ne sais
quoi, soit je suis la seule a pouvoir être exposée. Dans tous les cas,
considérer que c'est nécessairement hostile est un pré-supposé de merde.

— OK, OK, j'entends tes inquiétudes. Mais reste que l'on ne peut pas
laisser le truc évoluer ?

— Ben pourquoi pas ? Nakato tu confirmes que tu ne mesure toujours rien,
pas de dangers pour les infrastructures ?

— Toujours pas non, mais …

— Donc il n'y a aucune urgence à agir. Du moins, on peut se poser quelques
questions avant, vous ne croyez pas ? Je veux dire, on ne sait même pas ce
que c'est, et je sait qu'y réfléchir trop me file des migraines pire que
les vodkas frelatées de contrebandes, mais je préfère une solution
positive au reste. Un équilibre.

— Mais c'est ça qu'on essaye de te faire comprendre, lui dit Tonya excédée
mais essayant de garder son calme, on ne veut rien de plus que ça. On veut
juste être sûres que quoi que ce soit, ça ne décimes pas le reste.

— Tu pars du principe que c'est une mauvaise chose. » Saina sert les
poings et, le souffle court, essaye de ne pas se laisser complètement
submergée par les émotions contradictoires qui balayent ses digues
mentales, les érodant les unes après les autres, la précipitant dans une
boucle émotionnelle de panique.

« J'ai … besoin de sortir et de souffler un peu là, reprend-elle. J'ai du
mal à formuler des idées, à réfléchir. Je … Rah, je perds mes mots. Je
vais prendre l'air, ça va pas, mais j'ai besoin de sortir d'ici, de
changer d'air.

— OK, va prendre l'air, fait gaffe à toi, répond Nakato avant que Tonya
n'ouvre la bouche. On en reparle plus tard, quand tout le monde sera un
peu plus reposé d'accord ?

— Je serais pas forcément d'accord avec vous en ayant dormi plus, mais oui
tu as raison, je vais prendre l'air. »

Et, franchissant le portail de la médina, Saina, furibonde, se retrouve
d'un coup plongée dans l'ambiance hyper-active du souk. Les poings
enfoncés dans les poches de son blouson, Saina marche rapidement jouant
des épaules pour avancer, bousculant quelques personnes et répondant
à leur insultes d'un majeur levé.

Elle couvre ainsi plusieurs centaines de mètres, avançant au hasard et
ruminant ses pensées, essayant de comprendre pourquoi elle est dans cet
état là, ce qui l'a amenée au bord de l'explosion. Elle se calme et
ralentis son pas à mesure que la colère, la frustration, l'angoisse et la
tristesse finissent par la quitter, la laissant seule avec son mal de
crâne qui repart.

Nato ne répond pas à ses pings, elle doit probablement être en train de
dormir, ou quelque chose du genre. L'attente vaine d'une réponse génère
chez Saina une énorme baisse d'estime d'elle même, une envie de
disparaître, de ne plus avoir à se confronter au monde extérieur et aux
autres. Elle parcourt machinalement la liste de ses contacts en ligne et
voit qu'Enni est connectée.

Saina > Hey, ça va ?

Enni > Doucement, la journée commence à peine, et je profite des derniers
jours de vrais soleil en terrasse. Et toi ?

Saina > Bof. On peut se voir pour discuter ?

Enni > Oui bien sûr, qu'est-ce qu'il se passe ?

Saina > Je sais pas trop, on peut en parler de visu ?

Enni > Bien sûr. Je suis à la terrasse de Dolly, tu vois où c'est ?

Saina > Mmm, c'est bien dans Ciało ?

Enni > Yep, c'est là. La vue est imprenable, je dis pas que ça pour le
p'tit cul du serveur. Mais tarde pas trop, j'ai des exclus à préparer ce
soir.

Saina > Si je me paume pas complètement, je serai là dans vingt minutes
max, c'est bon pour toi ?

Enni > Parfait, à tout de suite.

D'une commande mentale, Saina ferme la fenêtre de conversation et se mets
en route vers Ciało, pour y retrouver Enni. Le soleil tape fort
aujourd'hui, en un contraste net avec le temps pourri des jours
précédents, et les façades miroir d'Ivory Tower reflètent sur le Ciało
l'intense lumière.

La terrasse du Dolly est légèrement excentrée et se situe là où un ancien
point de vue touristique avait été installé, offrant à Saina une vue sur
l'ancien port et l'estuaire. Ce n'est pas un panorama de carte postale,
mais cette vue dégagée, encadré par des piles de containers transformés en
habitation d'un côté et les façades retravaillées des salons du Ciało,
apporte une certaine sérénité à Saina.

Le Dolly est situé juste de l'autre côté de la terrasse. Le salon joue la
carte d'une certaine sobriété, à l'exception notable de l'installation
lumineuse qui projette en monochrome rose des scènes de baise au ralenti
sur la façade. Avec le code du link d'un ou plusieurs participants à la
scène. Le tout passé au ralenti, au rythme de la space beat diffusée
depuis une petite estrade. Estrade sur laquelle une paire de danseurs,
à moitié nus et à la musculature extrêmement détaillée se préparent pour
leur numéro de pole dance.

« Hey, tiens, je t'ai préparé un truc qui devrait t'apaiser un peu. C'est
légèrement euphorisant, chaud juste ce qu'il faut et il y a, bien entendu,
une légère dose d'un rhum qui vient de la dernière réquisition, parce
qu'il faudrait pas déconner. » lui dit Enni, sans prendre le temps de
respirer avant de s'asseoir juste à côté de Saina.

Vétue d'un simple boxer et d'une brassière, elle a cependant beaucoup
changé depuis la dernière fois que Saina l'a vu. En plus de sa nouvelle
coupe de cheveux, arrangés en un carré bleu duquel dépassent quelques
fibres optiques, Saina remarque des écailles iridescente incrustées sur sa
nuque et qui descendent le long de sa colonne vertébrale, des tatouages
dynamiques qui n'était pas là la dernière fois qu'elle les as vu. La liste
des différences est encore longue, et Saina se demande comment son
organisme fait pour supporter ça.

« Merci, dit Saina en prenant le verre dans ses mains. T'as un poil changé
depuis la derrière fois, non ?

— Ouais. Un poil comme tu dis. Et l'incrustation de ces écailles sous la
peau n'as pas vraiment été une partie de plaisir. Ça te plaît ?

— Je ne suis pas sûre de savoir. Ça m'intrigue en tout cas.

— Si tu veux, je te passe le contact du collectif qui bosse là-dessus,
peut-être que tu en as entendu parler. Bodies'R'Shell.

— Jamais entendu parler.

— Ouais, normal, si le bodmod c'est pas ta came, c'est normal que t'en ai
pas entendu parler. Il sont à Uncanny Valley si jamais ça te dis un jour.

— Eux, j'en ai déjà entendu parler en revanche. Mais passons. J'en déduit
que tu t'habitue bien à la faune locale ? Tu bosses ici ?

— Ouais. Et ouais, je fait du freelance ici. La connexion vers
l'extérieure est bonne, ils ont vraiment des salons cools et déconnent pas
avec la santé, il y a un petit bloc opératoire au sous-sol pour
l'ambulatoire.

» Ils prennent dix pour cents de tout ce que je touche quand je fais des
shows ici, mais ça couvre largement les frais que je peux avoir, et je
montre mon cul dans suffisamment de soirée pour avoir le capital social
nécessaire pour que j'ai pas vraiment à me prendre la tête.

» Je suis pas au niveau de Peach, hein, mais elle, elle bat des records.
Bref, je me sers de ce joli cul, de ces super seins et de cette bouche
pour avoir la paix et à m'envoyer en l'air autant que je veux. Et j'ai
même le temps d'aller à certains concerts, pas en danseuse hein, dans le
public.

— C'est de là d'où viennent ces bleus ? demande Saina pointant les
hématome sur la cuisse d'Enni.

— Ça ? Oh, c'est rien. C'est juste le dernier concert de Sexplosive.
C'était violent, j'ai dégusté, mais c'était cool. Tu devrais venir
à l'occasion, il y a plein de concerts ici.

— Tu sais, moi, les foules …

— J'oubliai, désolée. »

Elles boivent tranquillement leurs verres avant qu'Enni ne rompe le
silence.

« Bon, qu'est-ce qui ne va pas ?

— Des broutilles, j'ai des migraines monstrueuses en ce moment, et du coup
je suis irascible, et je me suis pris la tête avec Tonya.

— Oh merde. Rien de grave ?

— Rien de rédhibitoire je pense. J'espère. C'est … J'ai pas envie de
t'embêter avec ça.

— Tu m'embêtes pas tu sais ?

— C'est en rapport avec un … projet ? Quelque chose sur lequel je bosse
avec IT. Et on est pas d'accord sur la suite à donner aux évènements. Et
en ce moment, je suis un peu une loque émotionnelle. Trop de monde, trop
de contacts, trop de … trop.

— T'arrives pas à trouver ton rythme ?

— Un truc du genre. Disons que ça à l'air cool, mais c'est compliqué pour
moi. J'ai globaement pas eu de vie sentimentale avant, à part les trucs
à l'école et en fac, et j'ai passé les cinq dernières années dans une des
zones les plus inhospitalière au monde, pour ainsi dire sans contact
social ou physique.

» Bref, je gère pas très bien la proximité physique que tout le monde
semble apprécier, et ça me rend dingue. Parce que j'aime bienêtre avec
Tonya, mais elle me touche tout le temps, sans forcément s'en rendre
compte, et elle est comme ça avec tout le monde, et je sais pas si je suis
yet another girlfriend ou si il y a quelque chose d'autres.

» Et ça v pas. Et il y a ces maux de têtes constants depuis deux jours, on
s'est prise la tête pour un désaccord mineur, qui en plus n'en est même
pas un. »

Saina craque. La tension des derniers jours a raison d'elle et elle ne
parvient plus à contenir ses larmes.

« Viens là, lui dit Enni, la prenant doucement par les épaules, viens
pleurer sur mon épaule. Ça règle pas le problème, mais ça réduit la
tension.

» Tu lui en a parlé de tout ça ? ajoute-t-elle après un long silence

— Oui. Et je sais qu'elle fait attention en plus, mais ça ne m'empêche pas
de tourner paranoïaque, et ça me fatigue.

— T'as quelque chose de prévu ce soir ?

— Pas vraiment, pourquoi ?

— Je te propose un truc. Tu dors ici cette nuit, on partage ma piaule. Si
tu veux dormir toute seule, tu dors toute seule, si tu veux autrement, tu
me dis. Dans l'intervalle on va aller se mater des vieux films d'actions
avec des mecs body buildés et des meufs badass en bouffant du pop corn,
des trucs qui débordent de testostérone et d'œstrogènes.

» Moi ça m'aide à mettre ma tête en pause, ça laisse le temps aux
questions de trouver leurs réponses toute seule. On peut aussi se faire
les reprises porn des classiques, mais je suis moins fan, autant se faire
l'original. On demandera à quelqu'un de venir nous déposer la bouffe qu'on
veut, t'inquiètes, j'ai suffisamment de clients qui cuisinent pour que ce
ne soit pas un problème.

— Et ton rendez-vous ?

— Oh ben elle peut toujours s'arranger avec sa main. Ou alors tu viens
regarder, mais tu reste hors-champs. Ou je te laisse au bon soin des deux
beaux gosses là-bas, comme tu veux.

— T'es sérieuse pour le coup de regarder ?

— Meuf, on a partagé une piaule une semaine, tu as probablement vu tout ce
qu'il y avait à voir chez moi. Certes, il y a eu des mises à jours, mais
je n'ai aucun problème à ce que tu me vois à poil ou en train de baiser.

— Voilà, c'est ça le truc, Ça ne te pose aucun problème, ça ne pose de
problème à personne, mais j'ai pas l'habitude moi. Il faut que je
m'habitue.

— D'accord, d'accord, viens pas regarder. » Enni réfléchit quelques
secondes avant d'ajouter. « Sinon, tu vois ma cliente à ma place.

— Mais je saurais pas quoi faire et je …

— T'inquiètes, elle elle sait quoi faire, tu as essentiellement juste
à suivre ses instructions. Elle n'as pas ton nom, et elle ne l'aura pas.
Voire même elle me payera un bonus. Et toi ça te fera peut-être du bien,
non ?

— Je sais pas, ça à l'air un peu trop pour moi.

— T'arrêtes quand tu veux. Et bon, c'est un peu comme se caresser toute
seule, sauf qu'il y a quelqu'un qui mate. Et aucune obligation, tu ne veux
pas, pas de problème.

— Une prochaine fois peut-être, là je suis pas suffisamment bien pour ça.

— Et t'as pas envie de perdre le reste d'estime de toi que tu as. Pas de
soucis, je comprends. Mais tu peux venir mater quand même hein.

— Ouais, je sais pas.

— Tiens, studio 402, ça c'est le contrôle d'accès. Si tu as besoin de
quelque chose, il y a le comptoir là-bas, ils faisaient des gaufres tout
à l'heure, il doit leur en rester. Tu montes quand tu veux, je te pingue
quand j'ai finit. Tu choisis, d'accord ?

— Ouais, ça me va mieux comme ça.

— Et envoie un message à Tonya, ou passe lui un coup de téléphone. Et
excuse toi, d'accord ? Moi il faut que j'aille me préparer un peu, et que
je mange un truc avant cette session. Hésites pas à me ping si ça va pas.
Je suis sérieuse, c'est juste une session, je peux l'arrêter au milieu
sans réelles conséquences. Allez, bises, à tout à l'heure. »

Et juste comme ça, après avoir donné toutes ces informations en une seule
tirade, Enni termine son café et se dirige vers le bâtiment, sur lequel
passe à ce moment un clip de ses propres ébats et, après l'avoir montré,
fière d'elle, à Saina, elle passe les portes automatiques du salon pour se
diriger dans sa piaule.

Saina profites de la terrasse et du soleil encore quelques instants.
Différents groupes de personnes se sont installés à la terrasse pendant
son échange avec Enni, donnant un peu plus de vie à cette partie excentrée
du Ciało, au moment où les danseurs se lancent dans leur numéro qui tiens
plus de l'acrobatie, de la gym aux agrès et de la danse moderne qu'au
strip. Même si Saina est forcée de reconnaître que, effectivement, la vue
est intéressante.
