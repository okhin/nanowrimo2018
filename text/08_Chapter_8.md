# Chapitre 8.

> [ rumeurs.net/lost_and_found ]
> 
> Joral > J'ai perdu le titre d'une chanson, vous pouvez m'aider à le
> retrouver ?
> 
> iXs > Chante un peu pour voir ?
> 
> Joral > Ouais, le refrain ça fait un truc genre : “Naaaaa nana, Tsk Tsk,
> mmmmMMMMmMmmmm Hey, Hey”
> 
> s0ng2 > Mmmm, attends, ça me dit quelque chose. C'est un truc récent ?
> 
> Joral > Non, je pense pas.
> 
> s0ng2 > Alors je penche pour “nie bez tańca” de Sasha Gantz. Un genre de
> soupe électro pop ?
> 
> Joral > Ouais, c'est ça. Merci
> 
* * *

Saina est portée par la musique de la war-room. Les boucles électroniques,
les syncopes des boîtes à rythme, les routines cycliques des arpégiateurs,
modulés par des amplis exagérant le déséquilibre entre les aigus et les
basses et crachés par des haut-parleurs saturés, créent une ambiance plus
festive que studieuse qui ne semble pour autant pas empêcher les memakers
de vaquer à leurs occupations.

Les tablettes de data finissent de fondre sous sa langue, libérant leurs
neurotransmetteurs dans le système circulatoire de Saina, ciblant les amas
neuronaux sont installés autour de sa broche de connexion. Une prise en
titane connecte son cerveau à son masque chamanique et à sa peau
processeur.

Nato est assise dos à elle. L'hyperactive spécialiste en calcul
distribuée, douée d'une compréhension intuitive des systèmes de données
complexe, s'est portée volontaire avec enthousiasme pour aider Saina dès
que celle-ci a expliquer ce qu'elle voulait faire à Ivory Tower.

Un câble la reliant à sa rêveuse “bricolée” sort de sa longue chevelure
verte. Les entrailles de la machine de haute technologie sont répandues
à même le sol lui permettant d'implanter un transducteur à la machine.
Cette greffe la transformera en émettrice / réceptrice de données, et non
comme simple réceptrice des signaux XP issus de Dreamland.

Sa chemise largement ouverte laisse entrevoir, tatouée sur son sein droit,
une spirale d'Ulam, dont les cofacteurs premiers sont mis en évidence par
un pigments phosphorescent est tatoué sur son sein gauche. Ayant terminé
la greffe de son transcodeur elle sort d'une de ses poches un inhalateur
coloré et chargé de freeq, un nootropique exacerbant son hyperactivité et,
quelques instants après, son esprit commence à papillonner, errant sans
but de pensées parasites en pensées parasite. Une cohérence intuitive se
dégage néanmoins de cette exploration chaotique en apparence.

« C'est bon pour moi, dit-elle à Saina, assise dans son dos, je devrais
pouvoir te suivre.

— D'accord. Ça va être étrange et … well, tu verras bien. Si quelque chose
ne va pas, débranche OK ?

— OK. »

Nato referme sa rêveuse et ajuste le gaffeur noir qui permet au tout de
rester assemblé. Elle verrouille la connexion avec sa broche de données et
file à Saina le diviseur de signal bricolé par Tonya qui leur permettra de
partager les signaux et de rester en lien l'une avec l'autre.

Saina abaisse la visière de son masque alors que les acides s'activent
à altérer sa perception de son environnement immédiat. Les murs deviennent
flou, se mélangeant peu à peu avec le signal, pulsant. Un léger vertige
s'empare d'elle alors que ses centres de l'équilibre essayent de
comprendre ce qu'il leur arrive avant de se résigner.

Le booster sensoriel contenu dans le data lui permet de décomposer son
corps, et sa peau processeur, en unité logique distincte. Les portes
logiques composants les motifs tatoués, basculent les unes après les
autres, structurant le spectre électromagnétique en protocoles de
communication ad-hoc. Ce qui en temps normal n'est qu'une sensation
tactile, voire une gêne, devient maintenant une machine intuitive bien
huilée.

Intégrant ces perceptions mathématiques, avec la précision d'un
ordinateur, à ses propres capacités d'abstraction et de conceptualisation,
surchargées de psychotropes, Saina commence à assembler une construction
cohérente. Elle atteint un état de transe, proche d'un rêve lucide,
déconnectant sa perception de la réalité concrète pour explorer ses
hallucinations virtuelles.

Autour d'elle, et de Nato qui est juste là, oscillant entre différents
états de conscience, papillonnant dans le signal, sautant de paquets en
paquets, de liens en liens, une projection du paysage de donnée se
construit par consensus entre elles. Deux subconscients explorant l'espace
virtuel, travaillant en coopération et en compétition pour trouver des
références conceptuelles sur lesquelles elles peuvent construire une
compréhension.

Nato est un peu perdue. Elle oscille entre des signaux qui n'ont pas de
sens et les échecs répété de comprendre ce qu'elle vit augmente son niveau
de stress, la poussant aux limites de sa résistance à la panique. Son
subconscient, mis au premier rang par l'hyper stimulation cognitive et les
différents neurotransmetteurs étranger, n'est plus bridé par la logique
rationnelle nécessaires aux interactions et explore donc, sans limite, les
concepts affleurant dans l'espace virtuel dans lequel il existe
maintenant.

« Vas-y doucement, lui dit Saina, essaye de te regrouper.

— Et je fais ça comment ?» répond Nato.

— Il va falloir que tu trouves ça seule. Mais j'essaye généralement de
projeter un concept ou un contexte sur ce que je ressent. Par exemple, à,
dans le bruit des protocoles, encore en surface, j'imagine une toundra. Je
donne un truc auquel se raccrocher à mon subconscient, pour lui permettre
de rendre cohérent cet ensemble de données.

— Et comment je fais ça ? Se concentrer sur un seul truc,c'est pas
vraiment ce que je sais faire. »

Saina cherche une réponse. Le subconscient de Nato est instable, change
d'aspect, de forme, à mesure que son train de pensée ne déraille,
empêchant toute capacité de concentration.

« Comment tu décris un système toi ? demande-t-elle

— C'est à dire ?

— Tu as bien une analogie, un concept auquel tu pense quand tu modélises
tes systèmes, non ? Ou que tu dois expliquer ça à quelqu'un ? Là où on
est, tout est métaphore et analogie.

— Ça dépend. Du contexte en général. Classiquement, on utilise beaucoup
les classiques analogies avec d'autres réseaux, mais je ne …

— Pas les autres. Toi. Quand tu réfléchit à un système, tu penses
comment ? Comment il se modélise ?

— Mmmm … j'y ai jamais vraiment réfléchit en fait. C'est … une machine
organique ? Ou un genre de jardin je suppose.

— Voilà, concentre toi là-dessus. Le signal, c'est ton sol, ton herbe. Les
constructs qui flottent là-bas c'est des arbres, etc … »

À mesure que Saina verbalise les métaphores, et que le subconscient de
Nato s'y accroche, sa compréhension de son environnement change. Elle
cesse d'osciller entre les signaux pour en avoir une vision plus globale.
Autour d'elle, une plaine ennuyeuse de données, dans laquelle certains
insectes sautent d'un point à un autre, algorithme devenus sauvages, bots
oubliés se nourrissant du bruit et reproduisant toujours les mêmes motifs.

Elle gagne en consistance aussi. Même si elle ne perçoit pas son corps,
elle commence à développer un fort sens d'elle-même, des pensées qui lui
sont propres. Son train de pensée continue de dérailler, mais elle est
présente dans cette virtualité étrange. Au loin, des bosquets d'arbres
éthérée émerge lentement du bruit, à mesure qu'elle traite les signaux
qu'elle partage avec Saina.

« Voilà. On reste là quelques instants, le temps de se stabiliser.

— Et après ?

— On fait comme prévu, on cherche Uncanny et on essaye de voir ce qu'il
s'y passe. Tranquillement, sans tout faire sauter.

— Et on y va comment ? 

— J'ai une carte. Tu dois en avoir une aussi. Je veux dire, l'analyse
réseau ça doit être simple pour toi. J'ai quelques routines mantras pour
aider, mais elles ne marcheront pas pour toi je pense, tu dois trouver les
tiennes.

» Mais reste dans le coin, essaye de me suivre. Tu ne peux pas tout faire,
c'est normal, il te manque une partie des outils pour faire ça. J'ai
surtout besoin de toi pour comprendre ce qu'on va voir. J'ai besoin de ton
imagination. On verra la suite plus tard.

» Désolée de passer directement à la pratique de cette façon, ça ne peut
pas être agréable pour toi ,alors que c'est ce que je préfère.

— Juste une question, tu peux pas lire mes pensées ?

— Nan, sauf si tu veux que je le fasse. Et encore. Je pourrais avoir accès
à une version partielle de tes pensées, telles que toi tu arriverais à les
formuler. Un peu comme si tu me parlais en fait. Donc ne crains rien de
ce point de vue, je ne peux pas lire ce à quoi tu penses.

» Bon, il faut qu'on bouge de là, et qu'on se dirige vers, Saina affiche
rapidement une sorte de carte, là bas. Je conjure un ping, ça nous fera
gagner du temps. »

Le temps pour Saina de tracer un shellcode et Nato commence à scanner leur
environnement. Rien que de la cartographie passive, mais le brouillard
présent aux limites de son champ de perception se dissipe et prend forme.

Elle aperçoit, à une centaine de millisecondes, les enclaves
corporatistes. Des drains jaillissent des murs délimitant ces enclaves,
asséchant la toundra, aspirant les faibles flux d'informations, détruisant
la structure même de la virtualité, laissant autour d'elles un désert dans
lequel le bruit de fond protocolaire est inexistant.

Et, à l'intérieur des enclaves, cernées par des murs de feu et de verre,
protégées par des psychopompes filtrant les donnéeoducs établis entre les
diverses enclaves, Nato voit des bosquets d'arbres aux couleurs vives. Ils
sont le contenu culturel protégé et défendus par les corporations. Des
raffineries de données extraient le contenu profitable et l'indexe avant
de l'injecter dans différents bosquets communautaire, chacun avec sa
forme, ses fruits mèmes et ses feuilles utilisateurs.

La stérilisation des cultures par la publicité, au nom de la sécurité et
de l'intérêt du public, est très nettement visible de ce point de vue
global et halluciné.

Du bruit les entourant, Saina finit de conjurer son ping, qui se
matérialise en une créature du froid, évoquant les rennes des plaines
sibériennes. De la tête du ping, sortent d'étranges organes de perception
s'agitant en avant, un peu comme les yeux des escargots.

« Voilà, on va pouvoir aller un peu plus vite maintenant et couvrir les
quelques millisecondes qui nous séparent d'Uncanny. En revanche, ça risque
d'être bizarre, j'ai pas encore eu le temps de regarder en détail à quoi
ressemble le Complexe. »

Embarquant dans le ping, elles plongent au travers de la plaine de bruit,
pour changer de couche protocolaires et donner du sens à ce qui n'est que
bruit. Autour d'elles, l'herbe gelée de la toundra change au fur et à mesure
que les données sont traitées. Les herbes, balayées par le vent, deviennent des
pierres, puis des bâtiments. Certains flottent au-dessus d'elles, à une
distance d'à peine quelques millisecondes.

La richesse du Complexe, la diversité de ses habitants, inondent de stimulis
les cerveaux de Saina et de Nato. Ces stimulations, combinées aux nootropiques
et psychotropes qu'elles ont avalées, donnent une impression de vertige
tourbillonnant, toujours changeant. Des arbres se créent à mesure que les
personnes se connectent au signal. Des constellations végétales apparaissent et
disparaissent en quelques instants, reflets halluciné des salons de discussions
de rumeurs.net.

Des signaux audio font apparaître des grosses bulles de sons, se baladant au
fur et à mesure des écoutes. De gigantesques enseigne néons attirent différents
flux de données, les guidant d'un immeuble victorien à une gare brutaliste. Les
connexions et déconnexions fusent telles des étoiles filantes.

Au milieu de ce capharnaüm, reflets des habitants du Complexe, Nato perds
pieds. Papillonnant d'un élément à un autre, son esprit est incapable de se
fixer et elle commence à sombrer dans une frénésie compulsive la forçant
à parcourir et à analyser tout ce que ses sens perçoivent.

Saina la prend par la main. Est-ce vraiment sa main, se demande Nato ? D'où lui
viens cette sensation si ce n'est pas Saina qui la prends par la main ? Son
esprit fugue doucement mais certainement, et elle commence à être saisie de
vertiges, ce qui est surprenant, se prend-elle à penser, vu qu'il n'y a pas ici
ni haut, ni bas, ni hauteur, ni profondeur.

Saina sent, plus qu'elle ne voit, Nato partir. Elle n'a pas anticipé la
quantité d'informations disponible ici, et même si elle a une discipline lui
permettant d'ignorer le syndrome d'ivresse des données, Nato elle, avec son
esprit hyperactif, est en pleine crise d'ivresse.

Elle renvoit le ping, elle a besoin de bande passante et il n'est de toutes
façon plus nécessaire. Elle doit agir vite. A partir de ses mantras d'analyse
de donnée, elle forme un rapide shell-code. Pas subtil, pas beau, mais elle
n'as pas le temps de peaufiner, il faudra faire avec.

Elle ajoute au shell-code une routine de saturation et elle exécute le tout
juste à côté de Nato. D'un coup, une pluie orageuses s'abat sur elles, noyant
le signal perceptible dans un bruit blanc assourdissant. Nato, coupée dans son
élan prend conscience de son environnement immédiat. Les quelques millisecondes
autour d'elles sont devenues inertes. Seul le bourdonnement de la routine lui
parviens.

Elle entend Saina lui parler. Plus exactement, la bulle autour d'elle vibre, et
elle interprète ces vibrations comme étant celles du timbre de Saina. Elle
s'aperçoit qu'elle a oublié de respirer, absorbée et fascinée par cette
débauche d'information.

« Désolée, entend elle. J'aurai du y penser plus tôt. Tu vas bien ?

— Oui, je crois. J'ai phasé longtemps ?

— Pas tant que ça. Tu nous as fait un syndrome d'ivresse de données. Ça arrive
les premières fois, le temps de s'habituer à cette façon de représenter les
choses.

— C'était … J'étais bien pourtant.

— Oui, mais tu aurais peut-être fini par te perdre et fragmenter ton esprit,
incapable de différencier le réel et le virtuel. Je te parlerais une autre fois
des légendes sur celles qui ne sont jamais revenues.

» À part ça, tu saurais me dire ce que tu as vu ? Ton intuition de là où nous
sommes ? » demande Saina, plus pour permettre à Nato de se recentrer que parce
qu'elle est perdue.

— Vu la tronche des connexions/déconnexions, on est dans un système
transitoire, quelque chose comme rumeurs.net ou un autre type de chat. »

Elle tend la main pour essayer d'attraper un des fils lumineux qui passe à côté
d'elle. Au contact de sa main, le fil explose, projetant des débris de
métadonnées un peu partout, avant qu'ils ne disparaissent, absorbé par le
bruit.

« Euh, il s'est passé quoi là ?

— Connexion chiffrée probablement. En voulant le voir de plus près, tu l'as
altéré, ses sommes de contrôles sont devenus incohérentes et le routeur
a terminé la connexion. Rien de bien grave, il y a juste quelqu'un qui doit se
demander ce que foutent les admins.

» Bon, mais je pense pas que l'on soit dans rumeurs.net. Les paquets sont trop
gros, même pour du chat vidéos. Du coup, on doit être sur DreamLand ou quelque
chose d'approchant.

— Ça se tiens. La structure est … Disont que je ne savait pas à quoi m'attendre
et que, de fait, je ne suis pas déçue. Bon, on peut y retourner ?

— T'inquiètes le shell code va rapidement être bouffé par les anti virus qui
trainent dans le coin. »

Et quelques longues secondes plus tard, le shell code avait effectivement été
supprimé, ses traces archivées et son code neutralisé par une paire de poissons
fushia et vert pomme. Le brouilleur disparu immédiatement après, et le signal
se remit à tambouriner sur les synapses de Nato et Saina, leur permettant de se
repérer dans cette zone.

Elles sont face à un bâtiment flottant au-dessus d'elles, et qui ne semble être
composé que de sas d'entrées, l'intérieur semblant être coupée de l'extérieur
par des protocoles d'anonymisation et de chiffrement lourd. De nombreuses
connexions viennent de l'extérieur du Complexe, créant une gigantesque tresse
grimpant vers le ciel et sortant du champ de perception de Nato et Saina au
bout de quelques dizaines de millisecondes.

Le trafic généré par ce seul bâtiment au sein de DreamLand éclipse presque tous
les autres nœuds par sa taille.

« Laisses-moi deviner, dit Saina, c'est du porn ou un truc du genre ?

— Surement r_34 oui. Peach a dit qu'elle allait mettre quelque chose en ligne
aujourd'hui, je suppose que ça explique tout ce trafic oui.

» Bon, je commence à trouver des repères moi. Je suis pas sûre de pouvoir être
d'une grande aide, mais au moins je comprends mieux l'environnement. On cherche
quoi au juste ?

— Aucune idée. Je pense qu'on le saura quand on le verra. Pas le choix, il va
falloir fouiller à l'ancienne.

— Il doit y avoir mieux que de se mater tout le porn du Complexe pour ça, non ?
Pas que j'ai pas envie, mais … c'est laborieux.

— Et je suis à peu près sûre que tu ne vivra pas les signaux XP de la même
façon. Je crois, je n'ai jamais vraiment été confrontée à autant d'XP.

— D'acc. Mais reste qu'il nous faut quelque chose pour commencer à trouver ça.
Je suis pas sûre de savoir comment m'y prendre, mais un genre d'index devrait
aider non ?

— J'ai pas ce qu'il faut pour charger un index. Il y a bien l'interface de
recherche, mais ça va nous prendre des heures.

— Sauf si tu connais quelqu'un qui sait se débrouiller en calcul parallèle.
Mais je suis pas sûr de savoir faire venir des machins ici.

— Explique-moi ce que tu veux faire, je peu peut-être t'aider.

— Ben, rapidement, si on est deux à lancer des requêtes, on pourra avoir un
index. Un jour. On pourrait accélérer le processus en s'y mettant à plusieurs,
mais ça nous laisserait aussi bien trop de temps. En plus, à un certain moment,
on va avoir une congestion du réseau sous le nombre de requêtes, et tu ne veux
pas ça. Personne ne le veux.

» L'autre option, serait de rentrer dans DreamLand et d'attaquer directement la
bibliothèques. Mais ce serait se compliquer la vie pour un résultat mitigé.
Déjà parce qu'il n'y a pas vraiment de base de données centrale. Ensuite c'est
chiant comme la mort.

» Reste la voie marrante. Pas besoin d'un index. On a juste besoin d'écouter
les requêtes. On aura la liste de questions, on aura aussi la liste des
réponses. Et on saura ainsi ce qu'il y a sur DreamLand. Du moins, ce qui
ressemble au trafic normal. Les trucs bizarres devraient être visibles plus
facilement.

— Mmm, je vois ce que tu veux dire. Il faut bricoler un mitm'notaure ou
équivalent. Faire sauter la crypto du flux ne devrait pas être trop difficile,
le faire sans se faire remarquer sera un peu plus difficile. Et il nous faut un
stockage quelque part, mais c'est trivial.

— Un mitm'notaure ?

— Ouais, c'est leur petit nom. Une attaque d'homme du milieu classique, chargé
aux amphétamines, à peu près aussi subtil qu'un taureau de deux tonnes, mais
tout aussi efficace quand il s'attaque à la crypto par flux.

» C'est pas non plus le bidule le plus intelligent, mais du coup il vit bien la
parallélisation. Bref, un mitm'notaure. »

Et Saina commence à assembler le mitm'notaure. Utilisant exploits et shellcode,
elle crée d'abord une ossature solide, connectée à un centre de contrôle
qu'elle range dans sa poche. Elle recouvre l'ossature de systèmes de
cryptanalyse et branche un algorithme simple en guise de tête.

Le mitm'notaure est rapidement prêt. Saina configure ensuite le Command Center
pour cloner des mitm'notaures pour chaque connexion et le passe ensuite à Nato.
Dès que Nato se saisit de l'étrange machine couverte de runes, elle change et
se transforme en une sorte de panneau de contrôle, permettant de contrôler la
horde de mitm'notaures, et de commencer à indexer le contenu.

« Nickel. Bon, il faudrait filtrer ce qu'on voit pour vriament aller plus vite,
mais ça devrait aller. Saina ? » s'interroge Nato devant le visage figé de
Saina en une expression de peur.

Et, se retournant, pour regarder dans la même direction que Saina, elle
comprend que quelque chose ne va pas. Elle n'est pas sûre de bien comprendre ce
qu'elle voit, ou pluôt, elle a l'intuition qu'elle devrait y avoir quelque
chose là, mais elle est incapable de le visualiser.

De ce trou noir s'échappe une mince tresse de connexions, qui plonge vers le
bas, à des centaines de millisecondes de là et qui semble aspirer peu à peu cet
espace non perceptible.

« Désolée, j'ai lagué, dit Saina qui reprend vie. Mais ça, là, c'est pas
normal. Je pense qu'on a trouvé ce qu'on cherche, et j'avoue  ne pas bien
comprendre.

— C'est normal que ça me fasse flipper comme ça ?

— Oui. Ton cerveau est soumis à quelque chose qu'il ne comprend pas, et ça le
fait régresser à des états de panique. J'ai peut-être une idée de ce que c'est,
mais je ne peux pas réfléchir. Il faut qu'on se barre.

— Et on fait comment ?

— Moi, c'est facile. Je rompt ma transe méditative. Et après je te débranche.
Tu va probablement être désorientée, et tu auras une sacrée gueule de bois
ensuite, mais pas trop le choix là. A tout de suite. »

Se concentrant sur sa respiration, Saina se met en route vers un état de
conscience active. Elle a le souffle court, à la limite de l'hyperventilation.
Ses capacités de méditations sont mise à rude épreuve, mais elle arrive
à fermer son esprit, à le replier dans son subconscient, à l'abri des
informations parasitaires du signal. Elle ouvre les yeux, une horrible douleur
abdominale la saisi, ainsi qu'une envie de vomir.

Elle tâtonne et trouve la rêveuse de Nato et arrache le câble du boîtier
défoncé. Elle ravale ses nausées, et allonge Nato qui commence à revenir
à elle. Elle attrapes une ampoule de naloxone qu'elle garde toujours, au cas
où, et en verse le contenu dans la bouche de Nato.

Brutalement, Nato reviens à elle, dans cet espace de perception limitée,
où les distances s'expriment en secondes et non en millisecondes, où son
champ de vision est arrêté par des murs.

« C'était quoi ça, » demande-t-elle après s'être réhabituée à ne pas
pouvoir obtenir toute l'information d'une simple action mentale.

— Aucune idée. C'est bien ce qui m'inquiète, j'ai besoin de réfléchir
à tout ça avant de formuler une hypothèse. Tu feras gaffe, il y a des
chances que tu subisse quelques échos dans les heures à venir, vertiges,
hallucinations visuelle ou auditive. Rien qui ne devrait durer plus de
quelques secondes.

— D'accord. On y retourne quand ?

— J'en déduit que ça t'as plu comme expérience ?

— Clairement, c'est mieux que le replay d'XP, ou que pas mal des trucs que
j'ai testé avant. C'est comme ça que tu vois le monde ? Et il me faut
aussi des tatouages comme les tiens, pas envie de dépendre de matos pour
prendre un pied comme ça.

— Doucement, j'apprécie ton enthousiasme, mais allons y doucement
d'accord ? Tu as déjà failli te diluer dans le signal une fois, on va
y aller plus calmement d'accord ?

— Ça à l'air moins drôle comme ça, mais je suppose que je n'ai pas
vraiment le choix ?

— Si, tu as le choix. Moi aussi. Et là, on va attendre un jour ou deux
avant d'y retourner.

— OK, OK. » conclut Nato sans conviction.

Elle se lève et se dirige vers la sortie de la pièce dans laquelle elles
sont assises. Le soleil couchant l'aveugle et, soudainement, le monde
tourne autour d'elle. Elle se rattrape à l'embrasure de la porte le temps
que les vertiges se dissipent avant de se remettre en route pour trouver
de quoi manger.

Saina jette un œil à l'horloge du mur. Elles sont parties bien plus
longtemps qu'elle ne le pensait. La plongée à duré quatre heures. Et, même
si le temps perçu en ligne est toujours très différent du temps du monde
physique, il lui manque au moins une heure, voire deux.

Pas moyen de se souvenir de quoi que ce soit entre le moment où elle
a lagué et le moment où elle a réussit à se déconnecter. Elle attend
quelques minutes assise et à pratiquer quelques exercices mentaux pour
dissiper les inévitables échos, avant de se lever pour essayer de trouver
Nakato ou T0n pour faire un point avec elles.

Elle finit par se lever, boire un verre d'eau et, après avoir dit
au-revoir à Nakato et T0n, se rend au Jardin 0, là où Tonya lui a dit de
la retrouver. Encore un peu sonnée, mais reprenant rapidement conscience
de son environnement, elle quitte l'Ivory Tower et sent la pluie tomber
sur sa peau et, à mesure que les acides se dissipent, elle parvient
à ramener le signal à un simple bruit de fond.
