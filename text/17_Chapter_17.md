# Chapitre 17.

> [ Plurality/r_34 ]
> 
> b0ï > Du coup, plus personne ne parle du riper ?
> 
> b0ï * has been muted - Memetic quarantined for 12h
> 
> Val > Ah ouais, c'est … vous plaisantez pas.
> 
> __Rid3r > Il y a à minima une corrélation entre l'apparition de ce mème et
> les premiers problèmes de Plurality, donc ouais, on plaisante pas.
> 
> Snow > Vous confondez encore corrélation et causalité là.
> 
> __Rid3r > Peut-être, on essaye justement de le déterminer. En attendant ce
> mème est bloqué jusqu'à ce qu'on en sache plus.
> 
> Snow > Et par décision de qui ?
>  
> \_\_Rid3r > De moi, unilatéralement et à l'unanimité de moi-même, il ne
> nous reste que ce canal pour essayer de se coordonner vaguement
> efficacement, on a pas vraiment envie de vérifier si c'est une causalité
> ou une corrélation sur ce chan vois-tu.
> 
> Br0ke > Hey, pour détendre l'ambiance un peu, il y a une soirée au Hanged
> Billionaire ce soir. Enfin, tout à l'heure. Bref, ramenez vos culs.
> 
> Nato > Il y a encore des DJs qui peuvent bosser ?
> 
> Br0ke > T'inquiètes, il y a toujours moyen de faire la teuf. Au pire les
> Valkyries viendront pour un jam. Ou Anna Logic nous sortira ses platines
> à l'ancienne.
> 
> Br0ke > Ah si, si vous avez des sons et images sur support numériques et
> hors-ligne, amenez-les, on en fera quelque chose. Et les portes sont
> ouvertes à partir de il y a 10 minutes.
> 
* * *

Saina est revenue au Hanged Billionaire, là oú son aventure dans le
Complexe a commencé ; là oú elle a rencontré Tonya, puis les autres
Valkyries ; là oú elle s'est habituée au signal local. Elle essaye de
faire le point, de trouver le moment où tout à basculer, oú tout est parti
en vrille.

Tonya n'est pas là. Depuis leur visite à Uncanny, elle a de monstrueuses
migraines et Park, après avoir esayé deparler avec elle, a décidé de la
mettre en isolation sociale. Saina se rappelle l'avoir laissée sous la
pergola, sur le toit de sa chambre, emmitouflée dans des couvertures et
coussins, avec une réserve de chocolat chaud lui permettant de tenir un
siège.

En une énième et vaine tentative de faire dérailler le train de ses
pensées, elle s'enfile le verre de ce que Broke appelle un screwdriver. Le
jus d'orange est tellement dilué dans son verre qu'elle se demande si il
n'aurait pas été plus efficace de se contenter de la vodka locale.

Elle se repasse le film des dernières heures dans sa tête. Ou des derniers
jours, elle ne sait plus trop. Après le passage chez Uncanny, et après
qu'elle ai établi un premier contact, les choses sont parties en vrille.
Il lui a fallu plusieurs heures pour se remettre de l'attaque cognitive
dont elle a été victime, mais elle avait quand même réussi à enregistrer
et isoler le signal. Ou ce qu'elle pensait être un simple signal
sensoriel.

Ça a été sa première erreur. Supposer que c'était un stimulus sensoriel
qui exploiterait une faille cognitive. Le temps qu'elle comprenne ce dont
il s'agissait, elle avait déjà infecté Tonya qui, quelqus heures après,
convulsait sous l'effet de ce qui ressemblait à une overdose de psilo. En
essayant de comprendre ce qui l'affectait Saina avait alors compris que le
problème n'était pas un signal, mais que c'était une idée. Une langue en
quelque sorte. Et que essayer d'en parler, ou d'éviter d'en parler, amène
à cette contamination. Simple et efficace.

Pas besoin de machine à état infinis, de trucs quantique ou autre.
L'interface entre l'humain et la machine, est ce langage, cette culture.
De ce qu'elle en a compris, plus il y a de personnes recrutées dans le
réseau, plus la capacité de conscience de cette intelligence augmente.

Ce qui l'a amené à sa deuxième erreur. Elle a essayé d'établir un contact
avec 3L10T, pensant avoir à faire à une faction dissidente, ou quelque
chose du genre. Pourtant Park l'avait prévenue, ce n'était pas une bonne
idée d'y aller sans préparation. Mais Saina a paniqué et, face à l'état
dans lequel se trouvait alors Tonya, elle a voulu agir vite. Trop vite. En
établissant ainsi un contact elle a permis à 3L10T, ou quel que soit son
nom, de tomber sur DreamLand.

Lancer une machine syntaxique pure dans un océan sémantique afin de
générer une intelligence, une conscience. C'est probablement ce qui était
dans les carnets noirs d'Elliot, c'est probablement ce qu'à compris
Uncanny et qui les as poussé à expérimenter en premier lieu. C'est ce
phénomène qui a fait émerger une conscience de cette accumulation
d'expériences personnelle. En établissant le contact avec 3L10T, et en lui
permettant de tomber sur DreamLand, elle n'a réussi qu'à renforcer et
accélérer le processus.

Exposé à l'entropie infinie de cette intelligence, elle a essayé d'adapter
ses processus descriptifs dans le but, vain, d'essayer de traiter ce
signal, consommant les ressources de Plurality, à mesure que 3L10T, motivé
par la curiosité, s'étendait dans DreamLand.

Peu de temps après, Plurality a commencé à faiblir et à déconnecter les
systèmes les uns des autres, certains dûs à une surchauffe des
processeurs, d'autres dans un but de protection, certains systèmes
préférant se déconnecter pour se préserver, d'autres prenant des actions
pro actives pour déconnecter des systèmes perçus comme une menace.
Rapidement la mémoire collective que constitue Plurality s'est amputée de
la plupart de ces systèmes, dans un but de se protéger d'elle-même, dans
un délire paranoïaque dont elle a oublié l'origine.

Seul r_34 et des bouts de Dreamand sont toujours en ligne, suppose Saina,
mais elle ne perçoit plus le signal. Pour la première fois depuis
plusieurs années, ses antennes sous cutanées ont cessé d'alimenter ses
tatouages runiques qui lui permettent habituellement de se connecter au
signal, de le lire, de le comprendre.

Le Complexe est sens dessus dessous, Tonya est incapable de communiquer et
consacre probablement une partie de ses processus cognitifs encore actifs
à lutter contre une infection mémétique, et Saina s'est déconnectée du
signal et est maintenant, pour la première fois, véritablement seule. Elle
est donc allée là où tout a commencé, et à demandé à Broke une bouteille
d'un truc fort, qui est maintenant à moitié vide. Ou à moitié pleine, lui
murmure une petite voix dans sa tête.

« Allez, à la santé du premier chamane qui a littéralement détruit tout ce
dont elle avait la charge, dit-elle de sa voix alcoolisée, trinquant avec
son fantôme.

— D'accord, et c'est qui, lui demande Nato qui viens s'asseoir en face
d'elle.

— T'es assise en face d'elle. Vas-y, rejoint la foule misérabiliste et
sert toi un verre. Ou alors fait comme les autres, comme si tout est
normal, et va danser avec leux. Je suis pas d'humeur.

— Moi non plus. Nakato m'a dit que je pourrais te trouver là. Je pensais
pas que je viendrais pour un concours d'auto apitoiement, mais plus pour
me vider la tête et trouver d'autres façon de raisonner.

— Cette bouteille me dit que je suis en état ni de faire l'un, ni de faire
l'autre. Mais n'hésite pas à aller te vider la tête avec les autres,
derrière », répond Saina, agitant sa main en direction de la nef de la
cathédrale, là où, dans une obscurité contrastant avec les éclairages
pulsés habituel, de nombreuses personnes dansent, rient, baisent comme si
le monde s'était arrêté et que plus rien n'importait.

Ce qui est le cas, dans une certaine mesure. Sans Plurality, Saina suppose
que le Complexe risque de s'effondrer, mettant ainsi un terme à la
promiscuité des cultures, aux expérimentations sociales, à l'existence
même d'un réseau et d'une infrastructure non corporatiste. Et tout ça
parce qu'elle a foncé tête baissée parce que quelqu'un a qui elle tiens
est attaqué. Avant de se cramer les ailes en beauté en pensant pouvoir
dominer les forces en présence et d'entraîner avec elle ses potes et ce
qu'ils ont construit depuis tout ce temps.

« Je ne vais nulle part sans toi, lui répond Nato, et si t'es trop bourrée
pour parler, alors tu l'es suffisamment pour danser, et c'est ce qu'on va
faire. Nakato pense, et moi aussi, que tu es probablement la seule
personne à pouvoir nous sortir de là, du coup, je suis venu voir ce que tu
faisait.

— Elle a tort, c'est à cause de moi si vous en êtes là.

— C'est ce que j'ai tendance à penser. Mais Nakato ne se trompe pas, elle
n'accorde pas facilement sa confiance, donc elle a dû voir quelque chose
sous toute cette couche de dépréciation et de mauvaise foi. Et j'ai de
toutes façon rien de mieux à faire, donc fini ton verre et viens.

— Tout est de ma faute de toutes façon.

— Je persiste, t'es pas en état de parler. Viens, on danse. On baise si tu
veux. Mais on doit sortir ton esprit de là où il est, et l'alcool ne
t'aide pas, tu as déjà dû arriver à cette conclusion » et sur ces mots,
Nato attrapes Saina par la main et la tire pour la forcer à se relever.

Saina résiste par principe, mais sans beaucoup de conviction, et, bientôt
elles courent dans le dédale d'alcôves baignée de lumière noire, faisant
apparaître sur Nato les formules mathématiques dont elle est recouverte,
et qui apparaissent sous sa chemise grande ouverte, sur ses jambes
habillées d'une paire de bottes coquées et lacées jusque sous ses genoux,
et coiffées d'un micro short en jean.

La musique, beaucoup de cuivres, un peu de chip tunes, rugit dans ses
oreilles et fait résonner ses sens à mesure qu'elles se rapprochent de la
nef, et elles se jettent dans la marée humaine. Peaux, tissus, prothèses
et encre constituent la masse dansantes des habitants du Complexe. Saina
résiste encore un peu, mais l'énergie de Nato, est communicative. L'alcool
n'aidant pas à sa concentration, il lui est bientôt trop difficile de
continuer dans la morosité et elle se laisse aller, elle se laisse
gagner par la danse.

Elle sent sur sa peau des mains, des bras, des cheveux qui la caresse,
involontairement ou non. Des sensations étranges, elles sont
habituellement signes d'activité électromagnétique ; bien qu'étant
particulièrement différentes, dans leur forme, leur rythme et leur
intensité.

Déterminer si c'est l'alcool, la fatigue ou l'absence de signal qui font
qu'elle prend conscience de ces sensations n'est pas quelque chose
à laquelle Saina veut répondre, elle veut juste profiter d'être là, au
milieu de toutes ces personnes, à ne plus trop savoir où commence son
corps et où commence celui des autres. Elle fait partie d'une conscience
collective, agitée par les rythmes cuivrés projettés à pleine puissance
par une formation impromptue de musiciennes analogiques. Après avoir
écrémé les classiques de l'eurodance, elles se sont lancéesdans des
détournement K-Pop du répertoire de l'armée rouge. La voix de la
chanteuse, modulée par une antique version d'une vocaloïde hors-ligne, est
très haut perché et égrène les syllabes de ces hymnes communistes,
résonnant étrangement avec les rythmes électroniques générés par Anna
Logic sur ses consoles hors d'âges.

Au bout d'une heure de dans effrénée, après avoir flirté avec Nato, après
que celle-ci l'ai embrassée, les jambes de Saina commencent à ne plus
répondre. En sueur, elle signe un temps mort à destination de Nato et
s'extrait lentement de la foule et se dirige vers une des chapelles
latérales, remplies de coussins et de tentures, les statues des saints
ayant été transformés en porte gobelets obscène, ou en distributeurs de
capotes et de kits d'injection en stuc baroque.

Nato la rejoint quelque secondes après, luisante de sueur. Les flashs d'UV
des stroboscopes font clignoter les cofacteurs premiers au milieu de la
spirale première d'Ulamde tatouée sur sa poitrine, maintenant à peine
couverte par sa chemise détrempée.

Elle prend Saina par la main et l'arrache du sol. Saina tire dans l'autre
sens et elles tombent toutes les deux dans les coussins dans lesquels
Saina a commencé à s'enfoncer.

« Reste là deux minutes ok, et laisse-moi reprendre mon souffle, lui
demande Saina

— Ben alors, on se fait vieille ?

— J'ai surtout pas vraiment dormi depuis trois jours.

— Oui, comme tout le monde, mais OK, on peut attendre deux minutes. »

Nato roule pour caler sa tête sur la poitrine de Saina, les recouvrants
toutes deux d'une tenture mauve en guise de couverture. Elle s'endort
quelques secondes plus tard, au milieu de la musique. Saina est trop
fatiguée pour réfléchir et laisse son esprit errer, sentant le corps de
Nato serré contre elle, elle ferme les yeux et sombre dans un sommeil
semi-lucide, bercée par la respiration de Nato, filtrant les bruits
extérieurs, et dérivant dans un univers onirique de sensations tactiles,
de peaux, de bouches, de Tonya et de Nato, et d'autres.
