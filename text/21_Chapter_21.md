Chapitre 21

L'odeur de cannelle sort Saina de son sommeil. Enny est accroupie à côté
d'elle, une tasse de chocolat chaud dans chaque main. Autour d'elle, les
mêmes arbres cybernétiques, les mêmes murs en tôle ondulée et les mêmes
couvertures. Mais la lumière n'est pas la bonne.

« Hey, lui dit Enny, bien dormi ? »

Des artefacts de compression à peine audible déforment sa voix, comme si
elle parlait dans une canette métallique. Mauvais échantillonnage, note
mentalement Saina.

« Je crois. J'ai dormi longtemps ?

— Aucune idée. Mais tu avais l'air de t'agiter pas mal sur la fin.

— Ouais, probablement un rêve étrange. »

Enny reste silencieuse pendant que Saina se redresse et s'étire. La
lumière est vraiment étrange. Possible que le soleil soit déjà très haut,
mais c'est étrange. Comme si la lumière pulsait au rythme des battements
de son cœur.

Elle a fait un rêve étrange en effet. Un rêve lucide, dont elle n'a
cependant aucun souvenir autre qu'un bruit blanc. C'est étrange
d'ailleurs, ce n'est pas tout à fait du bruit blanc, elle détecte des
signaux faibles, une intuition, mais rien ne lui permet de se rappeler de
son rêve.

« Tu vas rire, dit-elle, je n'arrive pas à me remémorer mon rêve.

— Et, c'est grave ? Je m'en souviens jamais moi, de mes rêves, lui dit
Enny de sa voix mal compressée.

— Ben, pour un shaman, c'est pas idéal. Et c'est pas tellement que je ne
m'en rappelle pas, c'est plus comme si le souvenir de ce rêve aurait été
… corrompu. Comme un défaut de disque dur.

— C'est pas la redescente de data ? Vu ce que tu en as pris ces derniers
temps, ça serait logique.

— Ça explique la pulsation de la lumière, où les artefacts de compression
dans ta voix oui. Pas le reste. Mais il faut que j'y réfléchisse à tête
reposée. C'est peut-être juste une redescente de quelque chose, mais mon
intuition me dit que c'est autre chose.

» Où sont les autres au fait ?

— Aucune idée. Elles ont préféré te laisser dormir quand elles sont
rentrées d'après Sacha, qui m'as dit de passer te voir quand je me suis
pointée tout à l'heure. »

Saina rassemble lentement ses esprits. Les pulsations induites par le
manque de data faiblissent lentement à mesure que ses neuro-transmetteurs
retournent à un état homéostatique. Elle et Enny se mettent ensuite en
quête d'un déjeuner dans le quartier général des Amazons avant de quitter
l'assemblage de containers aux couleurs saturées.

Une pluie battante leur tombe dessus alors qu'elles se dirigent vers le
studio d'Enny. Elle a promis à Sasha de garder un œil sur Saina, elle
entends bien le faire. Et le meilleur endroit d'où elle peut le faire
c'est là où elle crèche. Saina, elle, est absorbée par la contemplation
des gouttes d'eau tombant dans le canal, créant un motif chaotique à la
surface de celui-ci. Instinctivement, elle cherche un modèle mathématique
permettant de déterminer avec une précision suffisante où la prochaine
goutte va tomber.

Les systèmes logique qu'elle mobilise changent sa façon de percevoir et
d'analyser son environnement. Tout n'est plus que paramètre probabiliste
et loi statistique. Ses graphes de décisions s'enrichissent de paramètres
de plus en plus nombreux et des échos mémoriels sont déclenchés lorsque
ceux-ci commencent à déborder de leurs tampons cognitifs.

Des odeurs, des flashs visuels, mais surtout des sensations tactiles se
réveillent dans son cortex. Les yeux de Tonya, la peau de Nato sur la
sienne, l'odeur de Park quand elle est dans les bras de Tonya. Le goût
acidulé de cette pâtisserie que Jora est en train d'enfourner et qu'elle
n'a jamais mangé. La sensation de sa main tenant celle d'Enny, la
sensation de la main d'Enny tenant la sienne.

Bientôt elle ne vois plus son environnement immédiat qui devient
probabilités et statistiques. Elle s'effondre sur elle même dans un chaos
cognitif mêlant ses expériences passées, et celles d'autres. Elle perd la
capacité formuler et structurer ses idées alors que les calculs deviennent
de plus en plus clairs et précis.

Des flashbacks l'assaillent, stimulant des échos mémoriels dans lesquels
elle est sous la pluie en train de porter Saina dans ses bras tout en lui
parlant. Et puis elle danse. Elle danse et elle envoie les images quelque
part. Son corps semble changer à chaque cycle d'horloge. Des tatouages
disparaissent et apparaissent sur ses bras, sur sa poitrine. Elle ne sent
plus l'eau sur elle, mais elle sent les mains de quelqu'un d'autre sur ses
jambes. Elle sent ses mains sur les jambes d'une autre. Ce ne sont pas ses
mains. Ni ses jambes. Ni ses bras. Ni sa langue. Elle le sait, elle ne les
reconnait pas. Mais elle en reçoit bien les signaux sensoriels.

L'entropie joue son rôle et, alors que le calcul progresse, sa pensée se
structure. Son système de différentiation lui permet, petit à petit, de
recréer un périmètre autour de sa conscience et de son identité. Ses
réseaux de neurones, connectés au signal par sa peau processeur,
commencent à restructurer sa personnalité autour de cette frontière. Elle
n'est pas l'ensemble des signaux d'informations qu'elle perçoit. Elle
n'est pas Enny, Tony, Nato ou les autres. Elle n'existe plus uniquement
dans le présent, mais dans une cohérence spatiotemporelle.

Alors que ses centres nerveux se synchronisent, trient la donnée, la
compresse et la structure pour permettre des abstractions et
approximations rapide, elle perd peu à peu la conscience de l'ensemble des
informations de son environnement immédiat.

Le calcul arrive à sa fin, elle le sent. Elle sent aussi la pluie lui
tomber sur la peau, et la peur d'Enny qui la maintient en position
latérale de sécurité. Les fourmis dans ses doigts lui indiquent la
présence caractéristique d'un émetteur d'urgence à large bande. Émetteur
qu'Enny utilise pour joindre les Valkyries.

Sa proprioception s'affine, elle perçoit de mieux en mieux les limites
entre le signal et les données externes, et son identité et son corps.
Elle sait que le calcul touche à sa fin et que quand celui-ci sera
terminé, elle saura alors ce qu'il se passe. Une étrange sensation
cognitive la saisit, comme si ses pensées arrivaient dans le désordre. Une
concurrence critique entre plusieurs systèmes de pensées. Comme si ses
sensations étaient réparties sur un système distribué hétérogène.

L'émetteur essaye désespérément de percer le brouillard magnétique qui
baigne son enveloppe corporelle. Elle saisit une des antennes d'un des
nombreux émetteurs/récepteurs de la zone et commence à la réorienter. Il
lui faut un peu de temps pour comprendre que cette antenne n'est pas dans
sa main, mais qu'elle la déplace en modifiant ses paramètres de
fonctionnement, en formulant du code machine qu'elle embarque dans le
signal. Et petit à petit, elle s'attaque à réduire les échos et le bruit
ambiant réduit.

Ce qui était une cohue de stimulation étouffante, et douloureuse, devient
quelque chose de plus léger, de presque agréable. Une vibration du bruit
de fond, quelque part entre le champ des baleines et le murmure des
transformateurs haute tension / basse tension. Le signal à large bande
finit par sortir de ce bruit et diffuser son message de demande d'aide.

« Hey, dit-elle, ça va aller, je vais bien.

Ennie ne répond pas. Pas d'une manière qu'elle arrive à percevoir. Autour
d'elle cependant, des messages de réponses convergent en un point, et le
signal d'alerte est répété, altéré, relayé, à la recherche de système
pouvant apporter une réponse adéquate à la situation. L'influx nerveux qui
parcoure le Complexe mets en branle ses différents organes. Une partie
essaye d'établir un diagnostique, une autre à dépêché des systèmes
d'anticorps pour isoler et combattre l'infection. D'autres déclenchent des
réactions de panique et d'isolation de certaines cellules, les coupants
des boucle de rétroaction du système cybernétique qu'est le Complexe.

« Hey, si tu m'entends, dit quelque chose, dit-elle, une pointe de panique
commençant à la saisir.

— Je … Saina ? répond la voix mal échantillonnée d'Ennie. C'est toi ?

— Ben oui, qui d'autre ?

— Tu sais que tu me parle par un système de chat là ?

— Non …

— Tu es inconsciente sur le sol devant moi, et tu es agitée de spasme.

— Pas inconsciente, je perçoit les signaux autour de moi, je sent ta main
sur ma gorge qui mesure mon pouls. C'est étrange j'ai … des sensations qui
ne sont pas les miennes. J'ai un peu peur.

— C'est flippant. Puis, après un silence, Ennie reprend : Sasha nous
envoie une équipe de réponse d'urgence, mais ton poul est vraiment trop
élevé. Ta température aussi. Tu nous fait une petite overdose. »

Saina ne répond pas. Elle ne sait plus où elle est. Le calcul touche à sa
fin et elle est en train d'intégrer les dernières expériences à son
système cognitifs. Ses souvenirs. Ceux d'autres personnes. Des
enregistrements radioactifs mesurée par le satellite d'ESP qui écoute le
vide spatial. Son individualité émerge doucement. Sa proprioception
s'améliore et son système cognitif est bombardé de neurotransmetteurs
d'urgence. Son corps essaye désespérément à rétablir le contact avec sa
conscience et elle se sent, lentement, aspirée vers l'enveloppe corporelle
qui se trouve à ses pieds.

Le calcul est terminé, et elle, elle sombre dans le confort de
l'inconscience, dans le silence et le calme. Les endorphines et
l'adrénaline ayant enfin réglé leur course. Elle sent ses pulsations
cardiaques ralentir, et sa sueur quitter son corps à mesure que sa
température redescend. Et elle ferme ses yeux, du moins elle croit, et ne
cherche plus à surnager dans le signal. Elle sombre, un léger sourire
dessinée sur ses lèvres.
