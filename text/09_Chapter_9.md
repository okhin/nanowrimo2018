# Chapitre 9.

> [ Plurality/wiki/ESP ]
> 
> Entropic Space Punk est le nom dont s'est doté le collectif qui s'est mis
> en tête d'installer un réseau de satellite depuis la plateforme
> [[Ashoka]]. Après de premières étapes prometteuse, le groupe est
> actuellement à la recherche de nouveaux carburants de synthèse permettant
> d'atteindre les orbites terrestres basses.
> 
> Le lancement d'un nouveau lanceur devrait se produire prochainement, lors
> d'une soirée “To the moon” qui sera annoncée en temps voulus.
> 
> [ comments ]
> 
> Felssk > Ils sont sérieux, ils veulent aller sur la lune ?
> 
> Scully > Tu verrais jusqu'à quelle hauteur le sucre nous as emmené, tu
> penserais toi aussi que c'est faisable.
> 
> Felssk > Non, mais envoyer des fusées vers l'espace, je veux bien, mais
> aller sur la lune ?
> 
> Scully > Ben tu sais, le plus dur c'est de se barrer d'ici. Une fois
> là-haut, t'as juste à te laisser tomber suffisamment vite pour rater le
> sol.
> 
> Felssk > Vu comme ça.
> 
* * *

Les jambes dans le vide, passée au travers d'un balcon en fer forgé, Tonya
guette l'arrivée de Saina. Le balcon sur lequel elle est assise est
accroché à la façade baroque d'un hôtel de passe désaffecté. Les enseignes
de néons roses et mauves grésillent et projettent sur la façade les formes
évocatrices qui ont attirés pendant des années la clientèle polonaise ou
russe du bordel.

Les formes baroques tranchent avec le reste des bâtiments ici. Le quartier
a subit de nombreuses restructurations dans un vain but de dynamiser
l'économie locale, avant que le quartier, un peu à l'écart de la ville,
à la limite de zones industrielles, ne soit abandonné par les autorités.

Quand le Complexe a commencé à prendre forme, il s'est essentiellement
développé autour du port et de l'ancien centre d'affaires. Ces quartiers
ont été abandonnés, jugés trop éloigné du Complexe ou trop abîmé pour
pouvoir être facilement intégré.

Après des années de jachères, les plantes ont bien entamé leur rôle de
dépollution des sols, et un collectif de jardiviste s'est mis en tête de
comprendre ces phénomènes pour pouvoir s'en servir ailleurs. L'ancienne
zone industrielle polluée s'est transformée en jardin expérimental
partagé, fournissant une partie des vivres nécessaire au Complexe, mais
servant également de moyen d'expression pour les jardivistes.

Cet ancien hôtel, rongé par la rouille et l'humidité, est le coin de
jardin sur lequel travaille Tonya depuis une dizaine d'année. Le balcon
auquel elle est s'est installée, déborde de glycines et de plantes
grimpantes palissées dont les fleurs se ferment doucement à mesure que la
pluie ne tombe, encadrant Tonya d'un tapis de feuilles et de tiges
verdoyantes, s'agrippant aux murs, se mêlant au fer forgé du balcon, se
mélangeant aux motifs floraux défraichies des moulures et papiers peint
des chambres.

Sous la dalle de béton du balcon, projetant des lueurs verdâtres, des
champignons phosphorescents colonisent la façade, évitant les structures
de fer forgées qui ont été rajoutées afin de pousser le mycélium
à former des motifs colorés.

Outre leur aspect esthétique, ces party shrooms, comme ils sont appelés au
Complexe, sont cultivés pour leur propriété euphorisantes et légèrement
hallucinogènes. Toya en a récolté quelques uns, et les as mis à l'abri
dans une poche plastique qu'elle a glissé dans sa robe polka. Des motifs
géométriques et phosphorescents ont été peints sur le motif à pois, et
Tonya a également modifé légèrement la coupe de la robe classique.

Des hauts parleurs dissimulés au pieds de consoude à large feuilles
diffusent la musique qu'elle pilote depuis son link. Elle écoute donc de
l'électro-pop du début du siècle, réarrangé pour correspondre aux attentes
d'un public qui, pour l'essentiel, n'était pas né au moment où cette
musique étaient en tête des ventes et des téléchargements. Les mélodies
sont simples à appréhender, et les chansons parlent de danser, de baiser,
de se défoncer, mais aussi de féminisme, d'anti-racisme ou de la
difficulté des queers de vivre dans le monde au moment où ces chansons ont
été composées.

Elle fredonne Borrn this way lorsque son link s'active et lui signale un
message entrant.

Saina > Hey, désolée, ça m'a pris plus de temps que prévu à l'Ivory Tower.
Mais je suis en route.

Tonya > Pas de soucis, mais tarde pas trop ou on va rater le spectacle. Tu
vois où je suis ?

Saina > L'enseigne néon rose qui brille dans le noir en face de moi ?
Ouais, je devrais y être dans deux cents mètres, à toute.

Tonya fait glisser la conversation dans son champ visuel, pour la mettre
en arrière plan de son champ de vision. Quelques instants après, Saina
déboule sur son vélo, debout sur les pédales et commence à gravir la lǵère
pente qui mène à la façade en faux marbre du bordel.

Le souffle court et transpirante, elle grimpe les deux étages par
l'escalier monumental enserré de lierre et arrive sur le balcon. Elle
enlève son blouson, révélant les semis conducteur dessinés sur sa peau, et
s'assoit à côté de Tonya, passant elle aussi ses jambes à travers la
rambarde en fer forgé.

« Tiens, je nous ai pris ça, sur les recommandations de Stelle, dit Saina,
commençant à ouvrir la bouteille.

— Oh, cool, répond Tonya, commençant à glisser ses mains sous le débardeur
de Saina.

— Hey, stop. Je, laisse moi finir d'ouvrir ça déjà, » essaye de répondre
Saina au moment où Tonya l'embrasse.

— J'ai dit stop Tonya, dit Saina en la repoussant d'une main.

— T'as pas envie ?

— C'est pas ça. Je sais pas. C'est pas simple pour moi, j'ai pas
l'habitude d'être touchée, je sors d'un truc un peu épuisant
émotionnellement parlant, j'ai pas besoin que tu me forces la main.

— Merde, ouais, chuis nulle. Désolée.

— Nan, t'es pas nulle. » puis, aprs une pause, ayant trouvé les mots,elle
ajoute « Hey, viens là. Mais juste un câlin d'accord ? » et Saina enserre
Tonya de ses bras, lui embrasse la nuque et fait courir ses doigts sur le
chrome froid du port de donnée installé dans sa nuque.

Leur étreinte est rapidement rompue par une notification qui annonce les
cinq dernières minutes avant le lancement depuis ESP de leur fusée drone
vers une orbite basse. D'ici elles ont une vue dégagée sur la baie et
l'ancienne plateforme pétrolière qui sert de pas de tir au collectifs de
space punks — leur définition.

Le ciel est dégagé et les ESP vont donc pouvoir faire voler leur lanceur
et mettre en orbite basse un premier satellite développé en collaboration
avec l'agence spatiale balte.

Depuis les docks, un projecteur gigantesque affiche un compte à rebours
sur les tours du Complexe, garantissant que tout le monde soit au courant.
EN plus des habitants, des chercheurs mais aussi des journalistes se sont
déplacés pour venir voir ce qui est le premier lanceur spatial financé
uniquement par des dons individuels, reprenant une partie des travaux et
de la philosophie d'Anuradha TK.

« Cinq minutes. Le timing parfait pour boire ça. T'en penses quoi », lance
Saina pour briser le silence grandissant entre elles et qui la mets mal
à l'aise.

— Yep. Puis, après un silence, Tonya reprend. Vraiment je suis désolée
hein. J'aurai pas dû supposer quoi que ce soit ne pas te sauter dessus
comme ça.

— J'ai entendu tes excuses, et ça va. Je ne t'en veux pas mais il faut
qu'on parle d'un truc ou deux d'abord. Ça ta va ? » Dans un plop sonore,
le bouchon sort de la bouteille et Saina approche la bouteille de ses
lèvres pour en boire une rasade.

— Oui, on peut parler.

Un silence s'installe entre elle, pendant que Saina boit le vin à la
bouteille. La dissonance cognitive entre le silence gêné et les impulsions
électronique de la musique qui leur donne envie de danser finit par avoir
raison de Saina qui, prise d'un fou rire incontrôlé, recrache le vin
qu'elle a dans la bouche, aspergeant Tonya de tâches rougeâtre.

« Ah ben super, je vais devoir l'enlever maintenant. Tu l'as fait exprès
pour me voir à poil, hein ? » et, joignant le geste à la parole, elle
défait les bretelles de sa robe et la laisse glisser le long de ses
jambes, la laissant tomber doucement sur les fleurs du jardin sous elles.

— Non, mais je vais pas me plaindre hein.

— Tu voulais discuter ? Tu voulais parler de quoi ?

— Je sais pas. J'ai besoin de parler, de comprendre un peu mieux le coin
je crois. J'ai vu des choses que je ne comprends pas à l'Ivory. Et ça m'as
mis un gros mal de tête. Et je sait pas trop où j'en suis personnellement.

— Ah oui, Nakato m'as dit que t'avat filé un coup de main sur un truc
étrange ?

— Tu saurais m'en dire plus sur l'Uncanny Valley ?

— Laisses moi deux secondes, j'ai une compilation de liens qui te
donneront quelques clefs de compréhension. Désolé, ça risque de faire un
peu scolaire, je suis pas la meilleure pour expliquer les choses.

» Pour comprendre ce que font les gens dans l'Uncanny Valley, il est
important, je pense, de comprendre que c'est l'émanation de tout ce qui se
passe au Complexe. Si on considère que nous sommes progressistes, en
avance sur la société extérieure, eux ont carrément des années lumière
d'avance.

» Ça a commencé comme une sorte de course à la modification corporelle.
T'as vu Stelle, elle a eu un accident et à perdu un bras, et elle s'est
bricolée une prothèse. On a tous ici beaucoup de choses implantées, ports
de données, tatouages, piercings, silicone, divers appendice en fonction
des préférences des unes et des autres.

» Mais les personnes qui sont dans Uncanny Valley cherchent délibérément
à questionner notre perception de l'humain, d'abord en modifiant leurs
corps, puis en ajoutant des organes sensoriels, d'autres appendices que
les bras et les jambes, imagine tous les délires corporels que tu puisses
avoir, du body porn en gros.

» Leurs idées ont commencées à se propager un peu partout dans le
Complexe, développant notamment un rapport à nos corps particuliers. Comme
dans toute exploration culturelle, il y a eu de tout, des excès et des
abus, mais on a finit par trouver un point d'équiibre et à passer à la
suite.

— D'accord, mais eux, ils ont fait quoi une fois que leur culture a été
intégré ? Il y a une limite à ce que l'on peut faire comme modification
corporelles, ils n'ont pas pu continuer bien loin ?

— Tu serais surprise. Mais, globalement, le rapport du Complexe au corps
humain a rendu leur tâche d'exploration de l'étrangeté plus difficile. Du
moins, jusqu'à l'arrivée d'Elliot.

» Elliot était en fuite. Il cherchait à échapper aux démocraties
corporatistes de ce côté du monde, notamment suite à la publication d'un
de ses textes questionnant l'essentiel des tabous de notre société. Je ne
pense pas qu'il voulait réellement les transgresser, mais il voulait que
la société se pose la question du pourquoi de ces tabous. Bref, ça n'a pas
plu, certaines de ses œuvres d'art ont été interdites car dépeignant la
bestialité ou la pédophilie, et il est arrivé ici, on a jamais trop su
pourquoi, mais comme out le monde, il était libre de venir et de partir
comme il le voulait.

» Son intégration au Complexe n'a pas été simple. De nombreux militants,
moi incluse, ne voulait pas que quelqu'un prônant le viol comme une forme
de critique de la société, fasse partie du Complexe. »

Tonya marque une pause le temps de se prendre de longues gorgées de vin.
Son regard se vide alors qu'elle ressasse des souvenirs douloureux, oudu
moins, désagréables.

« Bref, ça a été le bordel. Finalement, il a été admis que tant qu'Elliot
n'avait pas encore enfreint les non négociables, il était libre de rester,
personne n'étant obligé de lui parler, de le dépanner ou quoi que ce soit,
il finirait par être ostracisé. Ou par partir.

» Elliot s'est, assez étrangement au début je dois l'admettre, tenu
à carreaux. Il a compris assez vite qu'il n'était pas vraiment le
bienvenu, mais au fil des mois, puis des années, on s'est habitué à lui.
Il contribuait beaucoup, notamment dans la structuration de textes
politiques et, vu que nous avions partiellement répondu à ses attentes
vis-à-vis de la morale, il ne se sentait plus obligé de violer les tabous
de notre société.

» Peut-être qu'il avait peur que Sasha ne lui démonte la tête au
démonte-pneu aussi, va savoir. Bref, on ne le portait pas dans notre cœur,
mais il écrivait des choses intéressantes. Et il nous a permis de nous
confronter à des concepts sociaux pas évidents.

» Et ce con, nous as bien eu. Il s'est pendu dans son appart', on a mis
une semaine à s'en rendre compte, parce que durant tout ce temps, une
machine s'occupait de ses interactions sociales. Un bot qui imitait ses
textes, qui générait et émulait ses traits de personnalités. Une semaine
à s'en rendre compte collectivement.

» Quand on a dépendu son corps, on a rien trouvé sur lui, ou autour de lui
qui aurait pu ressembler à une lettre de suicide. À l'exception de ses
carnets. Deux boîtes à chaussure de petits carnets papier, du genre que
tout le monde avait il y a soixante ans. Tous strictement identiques les
uns des autres, avec une couverture noire, bien rangés dans leurs
boîtes.

» Et une phrase écrite sur les boîtes : “Je suis parti dans la vallée de
l'étrange, je reviens bientôt”. Bref, on a jamais vraiment localisé le
bot, donc il doit probablement encore tourner quelque part, et on
a demandé à Uncanny Valley si les carnets les intéressaient. Et bien
entendu, ça les intéressait.

» Des carnets recouverts de l'écriture d'une personne qui pense en
permanence qu'il va mourir, fuyante, les lettres à peine formées. Des
dessins, des ratures, des zones coloriées intégralement en noir. Dès fois
juste des nombres. L'Uncanny Valley à intégrer tout ça et, vu qu'ils
s'ennuyaient, ont essayé d'extraire le sens de cette pensée. Ce qui les a
amenés à questionner les structures mentales et sociales qui nous rappelle
l'humanité, par trouver quels comportements nous font tous grincer des
dents.

» Et ça peut être aussi bien trop écarquiller les yeux, que se planter
dans l'utilisation d'une forme interrogative, montrant une naïveté
faussement entretenue. Bref, ils ont commencé, en gros, à travailler sur
des façons de réécrire leurs personnalités, en se basant sur les livrets
écrit pas un abruti nihiliste.

— Nakato m'as rapidement parlé de ces incidents oui, c'était il y a quoi,
cinq ans si je me rappelle bien ?

— Quelque chose du genre oui. Bref, des incidents et la demande de
supervision de l'Uncanny Valley arrive. On ne sait pas trop quoi en faire,
du coup les memakers de l'IT se désignent volontaires.

» Et depuis, on ne sait pas trop ce qu'ils font. Ils restent entre eux,
ont à priori des contacts d'approvisionnement réguliers, mais quasiment
essentiellement en télé présence et, dernièrement, ils ont commencé
à publier de nouvelles langues, générées par ordinateur manifestement, ou
par un traitement évolutif quelconque. Et un manifeste dont on a encore du
mal à comprendre le sens.

» Une fois de temps en temps, ils ouvrent leur médina dans une sorte de
journées portes ouvertes. C'est particulier, de quoi faire le plein
d'étrangetés pour plusieurs vies. On sait qu'ils sont donc actifs, mais
cela va faire bientôt un an que plus personne ne peut prétendre avoir eu
un contact avec eux.

— Et comment le Complexe laisse ce genre de chose arriver ?

— En n'interférant pas. En séparant les espaces privés et publics, en
aidant les groupes à se tourner vers eux-mêmes plutôt que vers les autres.
Il y a de nombreux ermites, et de groupes anti-sociaux parmi nous. Il
y a même des groupes complètement opposés politiquement parlant, qui
habitent à moins de deux cents mètres les uns des autres. Mais comme ils
n'ont aucune raison d'interagir, ils ne le savent probablement pas.

— C'est pour ça que vous n'avez pas de cartes du coup.

— Aussi parce qu'essayer de cartographier le Complexe est une gageure.
Mais oui.

— D'accord. Je pense que je comprends un peu mieux ce que j'ai vu depuis
l'IT. Je vais avoir besoin de laisser tourner ça dans ma tête pour voir si
j'arrive à rendre ça cohérent.

— Cool. Dis, demande Tonya après une pause, je peux te poser une
question ?

— Shoote.

— Pourquoi tu veux pas baiser avec moi ?

— Je suis pas sûre de savoir répondre en vrai. C'est pas un problème
d'attirance. Ou d'envie. Quoique. J'ai envie d'être avec toi, dans tes
bras, de te sentir respirer dans mon dos, de sentir ta peau contre la
mienne. C'est plus que …

— T'as jamais couché avec une meuf ?

— Non, non. Enfin si. Euh Non. C'est pas le fait que tu sois une meuf.
C'est juste … J'ai envie d'être avec toi. Mais il faut que tu saches que
j'ai passé une bonne partie de ces quinze dernières années seule, ou
presque. Avant d'arriver ici, les contacts physiques que j'avais avec des
gens se comptaient sur les doigts d'une main. J'exagère un peu, mais pas
tant que ça. Et les fois où je baise, je décroche, mon esprit se barre
ailleurs. Pas que je m'ennuie ou quoi, mais juste par moment je me demande
où je suis et comment j'ai atterri ici.

» Et je suis pas sûre d'avoir envie de baiser avec toi si c'est pour ne
pas être là.

— Tu te compliques pas un peu la vie ? La baise c'est surtout pour le fun
hein, du moment qu'on prend tous notre pied, voire qu'on passe un bon
moment, ça suffit.

— Mais toi tu n'es pas en permanence stimulée par un champ
électromagnétique qui amplifie tout contact physique parfois aux limites
du supportable. Il y a une raison pour laquelle les chamanes sont souvent
ermites, c'est parce que le monde est trop intense pour eux. C'est pas que
je n'aime pas ça, c'est que ça me fait aller loin, ailleurs, très vite et
que du coup, c'est parfois étrange, je peux dissocier, et je ne suis plus
vraiment là dans mon corps.

— Un genre de transe quoi.

— Un genre oui, mais pas nécessairement voulue ou contrôlée. C'est pour
ça, les câlins, les caresses me vont très bien. Je suis pas sûre de savoir
gérer le reste avec autant de signaux partout.

— Mais c'est trash non, une vie sans contact physique, intimes, sans
s'envoyer en l'air ?

— Justement, non. Je suis caressée en permanence par le signal qui fait
résonner mes antennes corporelles, vibrer les semis conducteurs, basculer
les portes logiques. Je m'allonge et j'attends et je peux prendre mon pied
rien qu'en utilisant certains amplis et en laissant le signal courir sur
ma peau. Mais ça rend les choses compliquées s'il y a, en plus, une ou
plusieurs personnes physiquement présentent.

— Mais tu jouis tout le temps du coup ?

— Non, répond Saina un sourire aux lèvres. Je peux, si il y a une
excitation suffisante du signal, prendre mon pieds avec le champ
électromagnétique. Mais la plupart du temps, c'est juste un léger bruit de
fond, auquel je ne prête pas spécialement attention. Juste, les
stimulations émotionnelles et notamment tactiles réduisent cette
atténuation, me faisant ressentir chaque longueur d'onde, chaque doigt
posé sur ma peau, chaque souffle, chaque vent solaire de même niveau. Et
parfois c'est trop, et je décroches,

— Et si je te fais boire, tu sentiras moins les choses ?

—  Toi quand t'as une idée en tête, tu l'as pas ailleurs hein. Je sais
pas. Je suis pas sûre de vouloir baiser complètement torchée en tout cas.
J'ai besoin d'être en confiance, et d'être sûre que tu t'arrêteras si ça
devient bizarre, mais dans l'absolu, si je suis dans le bon état d'esprit,
j'ai nvie de partager des trucs cools, intenses, avec toi en tout cas.

ESP > @tous lancement dans une minute.

— Allez, on regarde cette fusée s'envoler, on finit cette bouteille, et on
va au Hanged Billionaire danser, conclut Tonya.

— On prend pas ta robe avant ?

— Pour quoi faire, tu vas me l'enlever plus tard non ?

— C'est toi qui vois. Elle est jolie cette robe, elle te va bien.

— Embrasse moi et tiens moi chaud au lieu de dire des bêtises. »

Tonya se blottis dans les bras de Saina. Au loin, la plateforme ESP
s'illumine des flammes générée par le démarrage et la mise à feu du
lanceur Entropy à mesure qu'il accumule de l'énergie cinétique avant de,
lentement, monter vers sa destination.

Le nuage de vapeur et de produits de la combustion englobe rapidement la
plateforme, la transformant en une nuée éclairée par les flammes qui
arrachent le monstre d'acier au sol, avant qu'Entropy ne finisse,
lentement, par prendre une trajectoire parabolique, plein Est.

Le Complexe s'illumine alors de feux de bengales, de feu d'artifices, plus
ou moins improvisés, échos du lancement de la fusée et teintants de
multiples couleurs les reflets de la ville sur les eaux vertes de
l'estuaire.

Saina reste là, à tenir Tonya qui grelotte légèrement de froid, dans ses
bras, ses cheveux crépus chatouillant son cou, pendant qu'elle termine la
bouteille qu'elles ont ouvertes tout à l'heure. Le bruit de fond du
signal, parfois entêtant, généralement dérangeant, qui ne laisse à Saina
que peu de répit, se dilue, s'estompe, à mesure que les l'alcool lui monte
à la tête et que les mains glacées de Tonya glissent sur sa peau.

ESP > @tous Orbite stable dans cinq minutes. Merci à toutes pour le
soutien, on se retrouve au Hanged Billionaire.

« Tu veux vraiment aller là-bas ? demande Saina.

— Oui, ça va être cool, tu vas voir. Mais on peut rester là encore un
peu ? À profiter du jardin, de la vue, de toi ?

— D'accord.
