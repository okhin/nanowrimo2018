# Chapitre 2.

> [ rumeurs.net ]
> Ambrose > Il y a vraiment des trucs étranges ces derniers temps les gens.
> 
> _Billie_ > Tu peux pas être encore moins précis ? C'est le Complexe, les
> trucs étranges c'est tout le temps.
> 
> \# Ã? -- joins
>
> Ã? > Hey, la nouvelle release de ????? est en ligne
>
> \# Ã? -- quit (too meany encoding errors)
>
> Lobster > mmm, il faut vraiment qu'on bosse sur la gestion des caractères
> mal supportés ici.
>
> _Billie_ > Mais du coup, tu pensais à un truc précis ?
>
> Ambrose > Ben, l'Uncanny Medina, c'est … bizarre ce qu'il s'y passe
>
> _Billie_ > Sans blague. C'est écris dessus.

***

Les jardins en terrasses s'avancent au-dessus de l'ancienne autoroute qui
contourne la zone, avant que l'eau ne monte et ne s'approprie les rues,
les transformants en canaux. Les racines des arbres ont trouvé le chemin
de l'eau verdie par les micros algues et forment une mangrove, connectant
les terrasses bétonnées, les jardins et l'eau.

Tonya s'est installée dans un de ses endroits préférés des jardins, le
jardin zéro. Elle est adossée contre un des premiers arbres plantés ici,
en défiance de tout ce que l'on pensait savoir sur la possibilité de faire
cohabiter béton et arbres. Le jardin est légèrement en retrait de la
cacophonie des souks qui occupent les premiers niveaux du Complexe et dans
lesquels s'échangent informations, biens de consommations, rumeurs, art et
drogues.

Ces souks, connectés entre eux par de nombreuses ruelles, sont le
principal lieu de socialisation du Complexe. Ils sont devenus avec le
temps des points de passage obligés, le réseau de ruelles s'y connectant
formant un arbre fractal dont les feuilles sont les médinas, les zones
d'habitation collective, dont la structure est centrée sur elle-même, le
seul accès à l'extérieur état généralement un portail, décoré par les
habitantes, représentant ce que l'on peut trouver ici.

Cette structure, inspirée des antiques cités arabes, dans laquelle
l'espace public sert à permettre aux espaces privés d'exister et de vivre
avec leurs propres règles, permet la cohabitation et la proximité de
groupes qui peuvent avoir des opinions sur la vie radicalement différente.

Le développement du Complexe s'est fait avec les ressources laissées
derrière par les précédents habitants de l'enclave maritime. Les nombreux
containers, dont les marquages douaniers nomment parfois les habitations,
s'étendent au pied des tours de l'ancien centre d'affaire, apportant des
touches colorées aux buildings brutalistes.

Sans réelle concertation, du moins a l'échelle du Complexe, des structures
similaires sont apparus, les idées des uns étant récupérées et améliorées
par d'autre, pour créer les habitats hybrides composés de tôle, de béton,
d'arbres et de jardins, de panneaux solaires et de terrasses fournissant
une relative autonomie à chaque petit groupe d'habitants, qui apportent
tous leur touche personnelle, leurs customisations, leurs modifications.

Certains ont embrassés l'aspect fonctionnel de ces blocs de container, les
disposant en carré autour d'un atrium, utilisant le rez-de-chaussée comme
espace collectif, et fournissant des chambrées dans les étages, les toits
étant partagés entre zone agricole et production d'énergie. D'autre, ont
pris une apparence plus ad-hoc, les habitants sont partis d'une conception
utilitaire classique, mais ont depuis très largement modifié l'habitat,
transformant l'espace en une architecture unique, résultat des itérations
successives de tout les habitants. D'autres encore, se sont appropriées
les tours abandonnées, souvent partagées par plusieurs collectifs et
organisé autour d'une sorte de souk vertical, dont les couloirs et
anciennes salles de réunions ont été investis par des groupes réduit. Il
y a celles et ceux qui choisissent une voix intermédiaire, connectant des
habitats temporaires, des containers, à certaines façades, donnant à ces
tours des excroissances étranges.

Il semble n'y avoir qu'une seule constante, les espaces d'habitations sont
tournées vers l'intérieur, vers les habitants et leur vie privée, alors
que les espaces extérieurs, les plazzas, les jardins, les souks, sont les
espaces de vie publique, de vie commune.

Et partout des plantes. Pour dépolluer les sols, pour fournir de la
nourriture, pour essayer de comprendre aussi les bouleversements
climatiques qui ne sont réellement visibles que par la mémoire que
constituent les jardins. Les jardivistes, comme ils sont appelés dans le
Complexe, cataloguent les plantes, établissant une sorte d'herbier vivant
à l'échelle du Complexe. Ils essayent aussi de briser les hybridations
stérilisantes qui ont envahi la flore, de comprendre les structures
modifiées de ces OGM qui se sont échappés des champs et qui ont remplacé
la plupart des espèces endémiques.

Les jardins, et en particulier le jardin zéro, sont des zones d'archivage
et d'expérimentation, une tentative de supprimer certains des dégâts
irrémédiables que l'homme a causé à l'environnement dans sa quête
permanente de l'efficacité

Les espaces verts ont donc envahi une partie des toits des
containers, les terrasses qui connectaient les tours entre elles, certains
toits, mais aussi une partie des zones marécageuses qui s'étaient
développées en périphérie. Et Tonya aime bien y passer du temps. Le temps
semble s'écouler à un rythme différent de celui des souks. Elle va donc
souvent dans les jardins quand elle a envie d'être seule, ou qu'elle
a besoin de ne pas être dérangée.

De nombreux outils, pièces mécaniques et composants électronique qu'elle
essaye de reconnecter sont dispersées tout autour d'elle. Un drone s'est
écrasé dans le coin et Tonya essaye de déterminer s'il est réparable ou
s'il va rejoindre l'impressionnant stock de pies détachés de Kassym.

La pauvre machine, dont le but est de faire des analyses atmosphériaues,
en a vu d'autres et ne fonctionne déjà plus que sur trois pattes, c'est
donc plus par acquit de conscience qu'elle essaye de remettre ce bot en
état. Elle réfléchit déjà à ce qu'elle pourras faire des pièces, d'autant
que les capteurs et les processeurs sont encore en état de marche.

Au bout d'un quart d'heure de démontage, ayant réduit le bot à un ensemble
de pièces distinctes et ne pouvant plus aucunement reproduire la fonction
d'origine de la machine, Tonya s'accorde une pause. De la poche ventrale
de son bleu de travail rouge elle sort une rêveuse, un convertisseur de
signal permettant aux utilisateurs de se passer des XP, et de vivre de
nouvelles expériences. Enfin, plus exactement, des expériences vécues par
quelqu'un d'autre.

Les rêveuses peuvent aussi bien être utilisées en lecture qu'en écriture
et leur principale utilisation dans le Complexe — après l'usage récréatif
— est la transmission de compétences. Il y a parfois des complications (ce
n'est pas parce que tu as démonté une fois un carburateur que tu comprends
comment il fonctionne), mais il est devenu plus simple d'apprendre au
moins les bases d'un domaine, fournissant une certaines autonomies de
compétences aux habitants.

En l'occurrence, ce qui intéresse Tonya maintenant, ce n'est pas tellement
l'usage didactique, mais l'usage récréatif. Elle a chargé les XP qu'elle
a échangé avec Br0k3 lors de la dernière soirée Amazons. D'une autre poche,
elle sort un petit sachet transparents, contenant quelques échantillons
phosphorescents de ses dernières expérimentations sur les psylos, qu'elle
fait poussé dans les caves humides de la tour SeAz,

Ce champignon est Un cousin éloigné de l'ergot, teinté d'une jolie couleur
mauve est, comme l'ergot ou les psylos, devrait lui permettre d'amplifier
les stimuli sensoriels auxquels elle va bientôt s'exposer. Pendant que la
rêveuse charge les XP dans sa mémoire, elle se lève et secoue rapidement
les jambes de sa salopette pour en faire tomber les débris de feuilles
mortes et d'herbes.

Elle ressert sa ceinture outils noire à sa taille et réajuste le harnais
qu'elle porte quasiment tout le temps — on ne sait jamais quand on peut
avoir besoin d'escalader quelque chose — et duquel pendent diverses
poches, mousquetons, chaines et outils.

Elle ramasse lson teddy noir et rose, griffé du logo des Valkyries, et
l'enfile. Aussitôt en contact avec sa peau, les capteurs du blouson
ajustent la température pour la maintenir à une température préréglée.

Après s'être assurée d'être relativement tranquille, elle s'installe sous
l'ombre d'un saule et écarte ses cheveux crépus pour dégager le jack
implanté dans sa nuque et duquel elle extrait un cable en fibre optique
qu'elle connecte à sa rêveuse.

Le menu s´affiche dans son champ de vision. D'abord flou, puis de plus en
plus net, au fur et à mesure que son cerveau accepte l'hallucination. Elle
s'allonge à l'ombre du saule, choisit la fonction Playback, décroche une
bretelle de sa salopette et fait glisser sa main vers son pubis, avale
un des champignons mauves, laisse la calibration se terminer et, sur une
impulsion mentale, démarre le playback.

Les premières secondes déstabilisent toujours, et seule l´habitude permet
de limiter les dégâts de ce mal de l'XP. Puis son système cortical, boosté
par les acides du champignon, finit par accepter les stimulations qu'il
reçoit et cesse de les combattre, intégrant ces informations comme
n'importe quelle autre expérience que Tonya a vécu jusqu'ici et continuera
de vivre.

Il y a toujours un signal fantôme, plus ou moins fort, émanant du système
sensoriel, une boucle de feedback permettant aux rêveurs de décrocher si
quelque chose de grave leur arrive. Les rêveuses sont, généralement,
dotées de capteurs de proximité afin de pouvoir réveiller les utilisateurs
si quelque chose s'approche de trop.

Tonya utilise le plus souvent un signal de retour assez élevé. Elle aime
bien sentir l'herbe sous sa peau, ses doigts jouer avec sa vulve ou ses
poils se dresser sous l'excitation alors qu'elle vit une expérience
différente. Elle aime cette dissonance sensorielle, plus encore que le
fait de (re) vivre des expériences enregistrée par elle ou par d'autre.

Les images, les sensations, s´assemblent avant de finalement retrouver du
contexte et avoir du sens. Quelqu'un lui tiens la main et la tire dans une
pièce obscure, elle est euphorique et légèrement défoncée. Elle rentre
dans la pièce en suivant la personne qui lui tiens la main, et en riant
doucement. La porte se ferme dans son dos, et elle est plaquée contre
elle-même alors que des lèvres viennent se coller contre les siennes,
qu'elle sent une langue jouer avec la sienne. Qu'elle sent une main,
froide, déboutonner son jean.

Ses pupilles dilatées lui permettent d'y voir un peu plus clair. La
personne qui est maintenant en train d'enlever son teddy noir et rose lui
est familière. Même plus que ça. C'est elle. Elle reconnait ses cheveux,
ses yeux. Ses mains pleines de micro coupures à force de manipuler outils
et appareils électronique.

Elle se voit enlever son T-shirt pendant qu'elle libère ses jambes du
pantalon qui est maintenant sur ses chevilles, avant de sauter dans ses
bras et de tomber en arrière, sur le lit. Elle se regarde dans les yeux,
intensément. Elle est bien. Elle sent sa peau sous ses doigts, elle
caresse ses seins puis elles roulent sur le côté et se retrouve en
dessous.

Elle est allongée sur le dos, elle sent l'herbe dans sa nuque, sa main
jouant sur son clitoris. Elle se voit de dessous, à califourchon sur
elle-même, la faible lumière rose se reflétant sur sa peau ambrée, faisant
apparaître les formes géométriques des tatouages phosphorescent sur sa
taille.

Elle sent sa main courir le long de ses seins et descendre doucement vers
ses cuisses s'attardant quelques secondes sur ses hanches. Elle se voit
enlever sa ceinture et se pencher vers elle, attrapant ses poignets avant
de les lui monter au-dessus de sa tête. Elle sent les barres de la tête de
lit contre ses poignets, et, bientôt, le serrement de sa peau par sa
ceinture pour l'attacher fermement à la tête de lit.

Elle se voit se pencher doucement sur elle, l'embrasser, lui lécher les
seins et les tétons et descendre plus bas, lentement, doucement, avant
qu'elle ne sente une langue se glisser entre les lèvres de sa vulve, et
tourner en rond, lentement, se rapprochant de son clito.

Pendant des heures de temps subjectifs, elle baise avec elle-même. Les
acides contenus dans les champignons lui donne une conscience exacerbée de
toutes les sensations passées, présentes ou rejouées. Elle est au-dessus
et au-dessous. Elle est dehors allongée dans l'herbe à se masturber. Elle
se rappelle de cette soirée avec les Valkyries, chez elle. Elle se
rappelle l'euphorie, les rires, les flirts, les caresses. Elle les vit de
tous les points de vue en même temps.

Elle se rappelle avoir baisé Br0k3, la MC du groupe, pendant qu'elle
enregistrait l'XP qu'elle revit. Elle se souvient avoir été là, baisé avec
Br0k3 pendant qu'elles baisaient avec elle. Elle se rappelle des câlins
ensuite, de la chaleur des bras de Br0k3 pendant que l'ocytocine est
métabolisée dans leurs deux corps pour trois.

Les drogues finissent par se dissiper. Un peu plus tôt que ce qu'elle
aurait voulu. Le playback aussi. Elle est trempée de sueur, et la lumière
du soleil lui fait mal aux yeux. Elle déconnecte la rêveuse, ce qui lui
fait tourner la tête, comme à chaque fois, et le câble se rétracte
automatiquement dans son logement, à la base de sa nuque.

Elle se relève, réajuste ses fringues, et range le matériel de réparation
qu'elle a sorti. D'une main, elle sort de sa poche son link, son lien au
réseau du Complexe, et parcourt distraitement Plurality, le système de
prises de décisions et d'échange du Complexe, à la recherche de quelque
chose à faire ce soir. Elle compose rapidement un message sur
rumeurs.net, à destination de Br0k3:

Tonya > @Br0k3 C'était super fun l'autre soir, je viens de me repasser le
playback et il faut qu'on remette ça. »

Puis elle charge un agent d'inventaire, elle va devoir ajouter les pièces.
Et marquer le drone comme détruit. C'est fastidieux, mais les agents
rendent la tâche de suivi vraiment simple. Et pour tout ce qui sort un peu
des biens de consommations, l'inventaire est important. Elle vérifie que
Kassym a toujours son atelier dans le coin et commence à scanner les
pièces. Aucune pièce n'est notée comme inconnue, elle ajoute quelque
commentaire sur leur état et verse le tout à l'inventaire de Kassym.

Elle jette un dernier coup d'œil à ce coin de jardin, les liserons se
développent toujours aussi bien, et elle ajoute mentalement à sa liste de
choses à faire de penser à ajouter un peu de hauteur à ce jardin. Cela
crée une demande sur Plurality qui lui permettra de plus facilement
trouver des choses intéressantes.

Elle jette les pièces dans son sac à dos et se mets en marche vers le
souk. Kassym est probablement chez lui, elle en profitera pour parler des
dernières trouvailles qu'il a stockées, il y aura forcément quelque chose
d'intéressant.

La chaleur moite la saisie au moment où elle rejoint le souk qui fait la
liaison entre la tour SeAz et Mark, la tour la plus avancée dans la baie.
Elle est immédiatement happée par le rythme de ces artères. Les enseignes
leds annonçant différents stocks de différent objets, projettent une
lumière chaude, variant des jaunes aux rouges les plus vifs, sur une
foule bigarrée.

On peut circuler facilement dans ces espaces, sans se toucher, et aller
directement à ce que l'on cherche, le chaos ambiant permet de toujours
arriver là où l'on veut, de vous faire passer devant des performances
artistiques étrange, ou de faire surgir devant vous un groupe de NéonPunk
en train de boire un thé chargé en mescaline tout en commentant l'état de
telle ou telle médina.

La porte de chez Kassym est ornée de restes de machines hors d'usage,
formant une arche de laquelle pendent, telles des perles sur un collier,
différentes breloques assemblées à partir de dissipateur thermique, micro
processeur, semi-conducteurs divers, batteries et leds plus ou moins
fonctionnels qu'il accumule depuis son arrivée ici il y a une dizaine
d'années.

Elle écarte ce rideau de produit low tech et rentre dans la boutique, pose
les pièces sur le comptoir avant d'actionner la sonnette qui fera venir
Kassym, ou la personne qui est de permanence en ce moment.
