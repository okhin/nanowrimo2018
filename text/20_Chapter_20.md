# Chapitre 20.

> [ 888GHz ]
> 
> Bien, les admins de rule 34 me signalent qu'eux aussi commencent à avoir
> des ratées. Probablement parce que vous vous êtes toutes, chères
> auditrices, découvertes un intérêt particulier pour le porn. Ou que c'est
> votre seul moyen de recevoir des nouvelles et des XPs en ce quatrième jour
> de panne de Plurality.
> 
> Nous tenions, avec ESP, et vous me pardonnerez cette entorse aux
> non-négociables, à féliciter touts les techos, archivistes, sysops,
> médiatrices et conservatrices des gigantesques collections de porn qui
> alimentent votre libido depuis que le Complexe existe pour leur travail
> acharné qui leur permet de toujours, indépendamment de la charge, des
> pannes et des plaintes, maintenir un service fonctionnel.
> 
> Et je vous invite toutes, lorsque vous en croiserez, faire de même,
> à leur payer une bière ou un verre. Parce que sans elles ont aurait sombré.
> Sur ce, j'arrête de vous embêter et, après un rapide sitrep par Br0k3, on
> diffusera en direct un petit jam d'Anna Logic et de Chip.
> 
* * *

L'ancien open space de bureau, au dixième étage de la tour quatre, a été
transformé en centre de gestion de crise par les Amazons. Des hamacs sont
tendus au milieu des armoires de serveur. Une connexion radio avec Ivory
Tower, de l'autre côté du canal, permet le transfert de données
à l'ancienne : les modems analogiques ajoutent aux ronflements des
ventilateurs leurs antiques chants vecteurs d'information. Pour compenser
la faible latence, le collectif sn34k3r-net s'est activé et transporteles
supports de stockage entre les différentes tours et étages. Un autre
groupe s'est s'est mis en tête d'établir un pont en fibre optique au
dessus du canal.

Saina et Nato se sont installées autour d'une tête de réseau. Un récepteur
FM branché sur le canal 888 diffuse des mash up d'électro swing et de rap
hardcore. Autour d'elles des moniteurs diffusent les données des
différentes sondes qu'elles ont installés dans Plurality, affichant un
flot ininterrompu de données brutes, baignant la pièce de la lumière
froide des écrans à cristaux liquides et LED.

Lorsque Tonya et Park les rejoignent, elles sont accroupies au-dessus d'un
schéma qu'elles complètent à même le sol. L'odeur du solvant des marqueurs
se mêle à celles, plus douce, du café local et celle, plus sucré, du data
qu'elles fument non-stop depuis plusieurs heures pour essayer de rester
éveillées.

« Bon, dit Park après avoir parcouru du regard les notes, schémas et
cartes griffonnées sur les murs, vous nous dites rapidement où vous en
êtes ? Je crois que je vois votre approche, mais je suis un peu larguée
là.

— Comment faire ça simplement, commence Saina. Vous vous rappellez que
l'on parlais d'un message en entropie infinie et de comment ce n'était pas
possible avec nos machines de Turing ?

— Ouais, c'est ce qui t'amenais à penser à un système conscient, répond
Tonya.

— Sauf que je pense que l'on s'est plantée. Enfin, disont qu'on a pas
approché les choses par le bon angle. Pas moyen d'entrer en contact avec
une éventuelle conscience, si elle existe, elle est trop différente de
nous, et on arrive pas à établir un contact. Un peu comme si tu essayais
d'établir une communication avec une bactérie précise au milieu des
milliards qui trainent dans ton estomac.

» Bref, après m'être grillée la tronche plusieurs fois, on a essayé
d'autres hypothèses. On a essayé de modéliser ça avec des attracteurs de
Lorenz, mais les modèles n'ont jamais convergés. On est donc repartie de
zéro, et on a pris une approche plus analytique. Ça ne donne pas grand
chose, mais on trouve des trucs un peu plus cohérents en explorant les
voies syntaxiques et linguistiques que celle purement mathématique.

— Pour faire simple, l'interromps Nato, on pense qu'il s'agit d'un langage
naturel plus que d'une conscience. Et on ne sait pas pourquoi, mais
à priori, Plurality a essayé de l'apprendre amenant à différents crash et
plantages.

— Je comprends pas, dit Park dubitativement, vous parliez pas
d'intentionnalité à un moment ?

— L'intentionnalité c'est du flan, répond catégoriquement Saina. C'est
juste un truc que l'on se raconte pour se croire plus intelligent que le
reste du monde, mais on ne sait pas le distinguer d'une programmation,
d'une destinée ou autre.

— L'illusion du libre arbitre est équivalente au libre arbitre, complète
Tonya. Je suis pas sûre d'être d'accord avec ça moi et …

— L'alternative c'est l'existence du divin et autres conneries du genre.
Mais ce n'est pas le débat ici, la coupe Nato. On essaye de trouver un
moyen de décrire ce qu'il se passe. Et ce qu'on a de plus proche, c'est le
concept de langue naturelle, de système symbolique complexe à entropie
infinie. Et bon, on a suffisamment de culture et de religion qui pensent
que leurs dieux sont en fait une langue pour supposer qu'on est pas
forcément trop à côté de la plaque.

— Je pense, reprend Nato, que l'on a essayé de résoudre le problème de
distinguer le signifiant du signifié. Ça nous a amené à de jolis
paradoxes.

» Par exemple, comment quelque chose aillant une entropie infinie
peut-elle être stockée sur un support contenant un nombre fini et
définitif de particules. C'est là que les maths nous ont mises sur la
voix. Après tout, il existe des nombres dont on ne peut prévoir quelle
sera la prochaine décimale en ne se basant que sur la liste des décimales
précédentes. Il s'est avéré que c'est une impasse, mais on s'est bien
marrées avec pi et autres joyeusetés.

— On a donc essayé une approche plus intuitive, continue Saina. Et c'est
là qu'on a commencé à écrire ce que l'on sait. Au bout d'un moment,
quelque part par là, Saina désigne une zone du mur recouverte de notes
écrite au marqueur derrière-elle, on s'est aperçues qu'on faisait une
carte descriptive du machin.

» Et depuis, on continue. On note les trucs nouveaux, on essaye de les
relier aux autres. C'est notre œuvre des dernières heures. On essaye de
donner du sens à quelque chose qui n'en a pas, et ça nécessite donc de
savoir un minimum à quel genre de système on a affaire. Bref, on commence
à voir émerger quelque chose de tout ça.

— Tant mieux, parce que moi, je suis larguée, dit Tonya. Je ne saurais
même pas par quel bout commencer.

— Rapidement, continue Saina, on essaye de déterminer ce qui est de la
structure et ce qui est de l'éventuelle sémantique. Réussir à séparer la
partie biologique, de son éventuelle conscience. Différencier signifié,
signifiant, et cætera, et cætera.

» Se concentrer sur la structure biologique de ce réseau électronique nous
permet de pouvoir plus facilement déterminer les symboles et les concepts
utilisés par une éventuelle conscience. Et si il y a des symboles, et donc
de l'abstraction, alors il y a une capacité d'intégrer de l'information et
donc de vivre des expériences. On pourra alors commencer le grand jeu des
quiproquos et des contre-sens, spécialités des traductrices.

— D'accord, répond Tonya, on en avait déjà parlé de ça je crois. Et
ensuite ?

— Ben il n'y a pas de ensuite, répond Nato. On est en train de
cartographier ce truc. On est plus probablement en train d'halluciner une
partie des liens que l'on remarque cependant, trop de caféine, trop de
data, pas assez de sommeil.

— On se doutait d'un truc du genre, c'est pour ça qu'on est là. Pour vous
faire prendre l'air deux minutes, et souffler un peu. Et on vous a ramené
des trucs à manger, je suis certaine que vous avez oublié de manger. »

Après un pique nique impromptu sur une des terrasses surplombant le canal,
Nato et Park repartent tester quelques idées de Park et voir si elles sont
applicables au modèle élaboré ces dernières heures, laissant Saina et
Tonya seules. Un silence s'est installé entre elles, à peine perturbé par
le Complexe, inhabituellement silencieux.

« Hey. Tonya brise le silence la première

— Hey, répond Saina. Tu vas comment ?

— Épuisée, mais ça va. Elle marque une pause, inspire et reprend. Dit, je
t'ai sentie un poil sur la défensive, tu es sûre que tout va bien ?

— Je sais pas. Après un blanc, Saina reprend. Tu penses que je vais pas
bien ?

— Je ne sais pas. Tu m'as dit que les relations personnelles n'étaient pas
simple pour toi. Et je ne voudrait pas qu'on se fasse du mal si on peut
l'éviter, du coup je me lance. J'ai l'impression qu'il y a quelque chose
entre Nato et toi. Ça me va hein. Mais je me demande si, du coup, ily
a aussi un truc entre nous. Ou pas.

— C'est surtout elle qui m'as sauté dessus.

— Je n'en doute pas, mais tu pouvais dire non, tu pouvais dire que ça te
mettais mal à l'aise, tu pouvais lui dire que les relations personnelles
sont compliquées pour toi. J'en déduis que ça te va bien qu'elle t'ai
sauté dessus. Et du coup je ne sais pas si tu attends quelque chose de
moi, si tu étais bien contente que je soit là à un moment et que
maintenant tu préfères être avec elle, si ce n'est pas important pour toi,
ou autre. Je peux comprendre que tu soit perdue, mais juste dit le. Moi je
m'adapte, j'aime bien être avec toi, j'ai aucun problème avec le fait que
tu aies des relations avec qui tu veux, j'ai un problème à ne pas le
savoir.

— Je ne suis pas sûre d'avoir les outils pour parler de ça.

— Embrayes pas avec ton discours linguistique s'il te plaît, fais pas
comme si t'étais la seule à ne pas savoir expliquer les choses que tu
ressent. C'est notre cas à toutes, mais au moins on essaye. Alors fait cet
effort là s'il te plaît. »

Saina prend du temps avant de répondre. Elle cherche des mots qu'elle peut
utiliser, une abstraction suffisante pour être comprise s'entend elle
penser.

« Je n'ai pas grandi avec autant de gens autour de moi qu'ici. Les
relations personnelles c'est assez nouveau chez moi. Je n'essaye pasde
m'en servir comme excuse, c'est un fait. »

Tonya laisse échapper un soupir d'agacement, mais elle laisse Saina
poursuivre.

« J'ai donc suivi de manière instinctive ce qu'il se passait ici entre moi
et les autres, dont tu fais parti. En supposant que je finirais bien par
trouver quelque chose qui convienne à tout le monde. Et je n'avais pas
compris que c'était si important pour toi.

— C'est surtout important pour moi de pas me sentir délaissée parce que tu
préfères quelqu'un d'autres et que tu ne te sens pas le courage de me le
dire.

— T'es injuste là. Tu peux pas me dire ça ! ». La fatigue, la redescente
paranoïaque de caféine et de data, mais surtout le fait d'être poussée
dans ses derniers retranchement par la personne qu'elle considérait comme
la plus proche d'elle, lui coupent sa la réalisation qu'elle est poussée
dans ses derniers retranchements par la personne qu'elle pensait être la
plus proche d'elle, lui donne l'envie de pleurer. « Tu peux pas dire ça,
je suis complètement perdue, je comprends plus rien. J'aime bien être avec
toi, mais je ne sais plus où j'en suis. Avec toi, Nato, ou le reste du
monde.

— Oh merde, non désolée, je voulais pas te faire mal. Hey, regarde-moi »,
Tonya dégage la mèche de cheveux qui tombe devant les yeux de Saina et la
fixe droit dans les yeux. « Je t'aime beaucoup, t'es une personne géniale,
pas toujours facile à suivre, mais j'aime bien quand on es dans la même
pie ou quand on danse. Je sais que t'es perdue, on l'es toutes. Juste, je
suis pas un plan de rechange ou une roue de secours, d'accord ?

— T'es pas mon plan B. Ou A. Ou Z, répond Saina sanglotant. Je … je sais
plus. J'ai pas envie qu'on ne se voit plus, je veux qu'on fasse des trucs
ensemble. J'ai aussi envie de faire des trucs avec Nato. Et je sais pas si
c'est parce que vous vous intéressez à moi et que du coup je m'accroche
à vous par peur de l'abandon, ou si c'est quelque chose dont j'ai envie.

» Je suis pas claire. J'ai du mal à me définir par moi même. Du coup les
relations que les autres ont avec moi tendent à me définir, je crois.
C'est pour ça que j'évite d'aller dans les endroits ou il y a trop de
monde, c'est plus facile pour moi. Du coup, j'aime bien comment tu me
vois, comment tu fais attention à moi. Mais aussi, j'imagine que tu n'es
pas avec moi, mais avec cette version de moi que tu imagine et que du coup
je vais te décevoir si j'arrête de me comporter comme je pense que cette
personne est censée le faire. Et du coup je t'en veux de t'attacher à moi,
même si c'est complètement stupide. Et ça me met dans des états pas cool
du tout, du coup je cherche à sortir de cette relation qui me fait du mal
tout en étant super cool et en me faisant du bien. Et ça m'épuise. Et ça
me conforte dans l'idée qu'il vaut mieux pour tout le monde que je me
barre en fait.

— Viens là toi, répond Tonya la prenant dans ses bras. Pleure ce dont tu
as besoin, frappe si tu veux, j'en ai vu d'autre. Je ne t'abandonne pas
d'accord ? Je suis là. Nato aussi. Juste, me mets pas sur la touche. Ou
Nato. Ou qui que ce soit d'autre, et dis-nous quand tu ressens un malaise,
ou que quelque chose ne va pas qu'on puisse aider. D'accord ? »

Saina pleure dans les bras de Tonya quand les crachotements
électrostatiques sortant d'un talkie à la ceinture de Sacha se font
entendre.

« Tout va bien les filles ? Saina ?

— Oui, ça va t'inquiètes pas, répond Tonya, on est toutes fatiguées et on
dit des conneries qu'on vient à regretter.

— Saina, ça va ?

— Mmmm-mmm » répond Saina, hochant doucement la tête.

— Ça fait longtemps que tu es là ? demande Tonya.

— Nan, rassures-toi. Quelques secondes tout au plus. Je suis venue prendre
l'air, on a une galère d'approvisionnement en eau pas évidente à gérer et
j'avais besoin de respirer. Mais je peux aller prendre l'air ailleurs si
vous préférez rester seules.

Cinq minutes plus tard, Saina a finit de sécher ses larmes et elle et
Tonya restent silencieuses, enlacées, lorsque Nato, surexcitée par la
caféine débarque en parlant très vite et très fort.

« On a un souci. Park a levé un lièvre. Et un gros. On a besoin de vos
lumières là, et … elle s'interrompt réalisant la situation avant de
demander : Tout va bien ?

— Oui, répond Saina, ça ira. J'avais besoin de mettre des choses au clair
avec Tonya, mais ça va maintenant, dit-elle en serrant la main de
Tonya dans la sienne. Laisse nous deux minutes et on arrive, OK ?

— D'accord, à toute », répond Nato avant de repartir.

— Pas si vite, Saina s'est levée d'un bond ou presque et la rattrape, je
suis contente qu'on se connaisse tu sais, et je crois que t'es importante
pour moi, et je crois qu'il fallait que je te le dise, alors c'est fait.

— Oh, c'est gentil de dire ça. Et d'accord, » répond Nato avant d'ajouter,
les joues rouges d'émotions, « je crois que tu es importante pour moi
aussi. » ajoute-t-elle dans un souffle, avant de disparaître en direction
de leur salle des cartes.

Saina et Tonya retrouvent Park, Nato, Sasha qui a l'air particulièrement
fatiguée et Bloke qui semble encore plus déphasé qu'à son habitude. Une
demi douzaine d'autres personnes les ont rejointes dans la salle des
cartes et l'odeur du café tiède mêlée à celle des corps non entretenus
depuis quelques jours et à la moiteur de l'air renforcent le sentiment de
fatigue que Saina ressent lorsqu'elle s'assied, adossée à la baie de
machines trônant au milieu de la pièce.

« Bien, commence Park, au-delà de ce qu'il se passe sur Plurality, je
pense qu'on a un sérieux problème. Ce qui occupe la puissance de calcul,
ce que Saina pense être une machine à entropie infinie, a trouvé le moyen
de sortir de là pour venir s'implanter chez les personnes exposées à une
faille sensorielle.

— Comment ça “s'implanter”? demande Tonya.

— Ben comme une idée qui apparaitrait d'elle même dans ton esprit, une
forme de suggestion, d'expérience ou de on sait pas trop quoi, reprend
Park. Ce que l'on sait c'est que ça semble se transmettre par des signaux
spécifiques, conçus pour exploiter certaines failles logiques et
biologiques de nos cerveaux.

— Tu penses qu'on a un problème. Pourquoi n'en es tu pas sûre ? lui
demande Saina.

— Ben parce que je n'ai aucune idée de ce que cela a comme effet. Pour
autant que je sache, toutes les personnes ici ont étés exposées et, à part
la migraine fracassante de Tonya, je ne perçoit pas de changement.

— Enfin, rien qu'on puisse attribuer spécifiquement à ça. Nato
l'interromp. On a pas assez de données. Ou trop. Pas assez de données
significative, intéressantes. On a ce que tu as ramené de chez Uncanny,
mais, Saina, tu semble la seule a pouvoir percevoir qu'il y a quelque
chose.

» Plus on cherche, plus on classifie, plus on essaye de comprendre la
structure du bordel, moins il devient compréhensible. Comme si la
structure de ce merdier était conçue pour ne pas être théorisable.

— Un peu comme ces langues conçues pour générer une charge cognitive
monstrueuse quand quelqu'un essaye de s'en servir ?

— Quelque chose de ce genre là. Plus on y réfléchit, moins il devient
possible de réfléchir. Mais on est dans une impasse là, il nous manque un
référentiel ou quelque chose de ce style. Une forme de pensée différente.

— Et c'est là que vous pensez que je peux aider ? demande Saina. Je veux
bien, mais je ne comprend pas plus que vous ce qu'il se passe ici.

— Personne ne veut que tu comprenne, parce que tu ne peux pas le
comprendre, dit Tonya. C'est quelque choe qui est fait pour ne pas être
compréhensible. Ce que Nato et Park veulent dire, et je ne saurait pas
dire pourquoi mais je suis d'accord avec elle, c'est qu'elles ont
l'intuition que tu peux trouver une voix de sortie, ou quelque chose de ce
genre. Un concept qui permettrait d'aborder le problème sans effondrer nos
ressources cognitives.

— Mmmmm … Je vois l'idée. Comme je suis la seule personne, à priori,
à avoir pu enregistrer cet exploit biologique, je dois pouvoir m'en servir
pour trouver un patch quelconque, ou une routine qui permettrai de pouvoir
contrer ou limiter les effets de cette infection. Ça pourrait marcher oui,
mais il faut que je retourne en transe. Et là, je suis pas en état.

— Personne ne l'est. Park, Nato, vous pensez que ça peut attendre quelques
heures ? Le temps de se détendre un peu, prendre une douche, dormir,
manger, tout ça ? »

— Ouais, on est plus à quelques heures prêt de toutes façon. Et si
épidémie il y a, elle frappe déjà le Complexe de plein fouet, on peut
effectivement prendre le temps de dormir un peu, répond Nato. Ou alors, je
crois que Sasha parlait d'une soirée posée pour les Amazons et qui veut
bien venir, afin de prendre le temps de souffler au milieu de la crise.
Moi jepensait allez y jetter un œil, je crois que Park aussi. Ça sera pas
la fête du siècle, mais ça nous fera du bien. Et comme ça, Saina, tu
pourras plonger en transe dans un meilleur état d'esprit. Deal ? »

Personne ne trouvant rien à y redire, elles se mettent donc en route vers
le quartier général des Amazons, traversant les souks au pied la tour
Quatre alors que le soleil se lève timidement, imergeant les passages dans
l'ombre des plus grande tours du Complexe.

L'ambiance est calme. Une nappe de brume amortissant les sons et les
lumières s'est déposées sur les canaux et la plupart des systèmes
d'ambiances, privées de connexions à Plurality, sont éteints. Quelques
rares personnes croisent le chemin du groupe, échangeant quelques
salutations et banalités dans cette langue syncrétique que personne ne
semblait parler il y a encore quelques jours.

Elles arrivent au pied d'un assemblage de containers colorés, dessinant
une version parodique, et visible depuis le ciel, du logo de l'entreprise
ayant amener les Amazons à se s'organiser et à installer leur quartier
général ici. Des sons étouffés, évoquant des mélodies slaves et réhaussés
de sons électroniques, parviennet à leurs oreilles au moment où elles
s'engouffre dans la structure de métal colorés.

Tonya émerge lentement de la torpeur qui la retenait depuis qu'elle
a émergé, ses migraines s'étant estomper à un stade lui permettant de
nouveau de prendre conscience du monde qui l'entoure, sans pour autant que
chaque stimulis ne ressemble à la douleur aigüe d'une aiguille plantée
dans son bras. Elle embrasse Saina avant d'attraper une canette de rhum
cola mélangée à des euphorisants, et se dirige vers le centre de la médina
métallique, là d'où provient la musique au rythme lent.

Park la suit. Deux de ses rats sortent de sa veste à capuche. L'un noir et
frêle se pose sur son épaulee, profitant de l'ambiance et de la vue,
tandis que le second, au pelage blanc mais légèrement fluorescent descend
le long de sa poitrine avant de sauter vers les cacahuètes laissées sans
surveillance par le groupe d'Amazons qui sont enlacées dans un câlin
collectif, tout en glissant lentement vers le sommeil.

Nato a disparue peu de temps après qu'elles soient arrivées ici et Saina
se retrouve donc seule, pour la première fois depuis qu'elle est arrivée
ici, ou presque. Pour la première fois depuis plusieurs jours au moins.
Elle déambule dans le labyrinthe architectural formé par le chevauchement
des passerelles et des containers, et se retrouve bientôt seule dans ce
qui pourrait être un petit jardin d'hiver si les plantes qui poussaient
ici n'étaient pas des sculptures évoquant des arbres de désolation, aux
branches illuminées par des milliers de diodes électroluminescente.

Le calme de l'endroit la saisi et elle s'installe au pied d'un de ces
arbres, tirant à elle couvertures et coussins présents en quantité dans la
pièce où elle se trouve, avant de se laisser bercer par les basses à peine
perceptibles et de sombrer rapidement dans un profond sommeil.
