# Chapitre 5.

> [ rumeurs.net/gen_pop ] 
> Ikon > /search Valkyries +topic music -topic myth +order date +rev
> * index > Ikon : 8900 occurences trouvées correspondant à ta recherche. 
> * search_bot > Ikon : Hey, 6700 réponses correspondant à Valkyries, 23450 correspondant à +topic [trunc]
> * search_plus > Ikon : Voici la liste des 10 premiers résultats correspondant à la recherche Valkyries +topic music -topic myth +order date +rev
> * search_plus > 1 - hey, check this out, le concert des Valkyries va bientôt commencer <files/upload/users/_noJay/765tgt666.png>
> * search_plus > Error: index out of range
> * search_plus has quit (too much queries, we need the bandwidth!)
> * indexImproved > /search Valkyries + topic music -topic myth +order date +rev : 4750 results
> * indexImproved has quit (kicked by turing)
> * index > searchImproved : 8900 occurences trouvées correspondant à ta recherche. 
> * chat room has been muted by turing
> * search_bot has quit (kicked by turing)
> * search_plus has quit (kicked by turing)
> * chat room has been unmuted by turing
> Ikon > Voilà. Quand je dis que, dès fois, vous faites n'importe quoi et que ce serait cool que vous vous parliez, on pourrait peut-être imaginer avoir un index cohérent
> * turings approves
> 
* * *

Saina est assise au bord du lit, essayant de se réveiller complètement.
Elle a enfilé le boxer et le T-shirt qu'elle portait encore hier. Ou ce
matin. À taton elle cherche un interrupteur pour avoir un peu de lumière
dans la chambre avant de renoncer, se rappelant qu'elle ne connaît pas
cette chambre. Elle reste donc assise au bord du lit, dans le noir,
à attendre que le sommeil la quitte avant de se mettre en route pour un
café.

Elle finit par chercher à sortir de cette chambre dans le noir. Essayant
d'accéder à ses souvenirs, elle cherche à se rappeler de la configuration
de la pièce, de l'endroit où sont placés les éventuels obstacles.

Ses fringues, son sac, son masque, le coin du lit. Le miroir en face
d'elle et sur sa gauche la porte. Facile, elle peut le faire les yeux
fermés. Elle a presque atteint la porte de sous laquelle passe un rai de
lumière, quand son gros orteil entre en collision avec une commode dont
elle a oublié l'existence jusqu'à présent.

Après avoir laissé échapper un cri de douleur, copieusement insulté en
russe la commode, elle est finalement dehors. Au soleil. Il fait trop
jour, la lumière lui fait mal aux yeux. Le temps pour elle de s'habituer
à la luminosité et elle entreprend sa quête matinale de café. L'odeur du
café fraîchement torréfié la titille à travers les rideaux naturels des
plantes grimpantes et des lianes qui recouvrent l'atrium, au centre duquel
une étrange sculpture fait office de fontaine.

En traversant l'atrium, elle sent l'herbe, les fleurs des champs et la
terre humide sous ses pieds. Les odeurs de sauge, de thym et d'autres
herbes médicinales se mélangent aux odeurs subtiles de café et de pain
chaud qu'elle distingue mieux à mesure que son éveil progresse.

Elle pousse la porte à hublot qui la sépare de son café et tombe nez à nez
sur un mec, plutôt petit mais solidement charpenté, qui est torse nus en
train de mélanger quelque chose qui bouillonne doucement. Le couinement de
la porte qui s'ouvre l'incite à se retourner pour faire face à la
nouvelle arrivante.

Des tatouages sur ses tempes reprenant une iconographie Baltique encadre
un visage fin et marqué par quelques petites cicatrices. Cicatrices qui
semblent faire écho à celle qu'il a au niveau des pectoraux et qui
semblent remonter vers ses aisselles.

« Tu dois être la Saina que Tonya a ramenée hier soir ? Bienvenue dans la
famille. Je suis Joral. Je te serrerai bien la main, mais je ne suis pas
sûr que le jus d'algue qu'on utilise comme teinture soit un premier
contact. Et bienvenue dans mon royaume, ajoute-t-il en faisant un arc de
cercle de son bras, pour désigner la cuisine. Si jamais tu as envie d'un
truc particulier, hésites pas à me demander. La cuisine est un peu le
kávu, là où tout le monde se croise et j'essaye de faire en sorte qu'il
y a toujours quelque chose à manger. C'est cool si tu peux amener des
trucs aussi.

— Laisse-lui deux minute pour qu'elle prenne son café, et laisse la
s'ajuster, elle débarque complètement », dit Tonya en rentrant dans la
pièce par une échelle qui semble donner sur le toit. « Bien dormi ? »
demande-t-elle à Saina.

Elle n'a enfilé qu'un short un peu ample et se promène pieds nus, une
tasse de café à la main, une sacoche d'outil à la taille et tenant encore
un arrosoir et des outils de jardinage. De deux mains uniquement.

« Très bien. J'ai dormi longtemps ? » Joral pose devant elle une tasse du
café dont elle sent l'odeur depuis tout à l´heure.

— Treize heures à peine. Prends ton café et va profiter de la vue sur le
toit, c'est génial pour le petit dèj, et en plus il fait beau aujourd'hui.
Passe devant, je te rattrape il faut que je retourne là-haut pour planter
encore quelques fraisiers de toutes façons, et après on ira visiter un peu
et on commencera à te faire une place si tu veux rester un peu de temps
ici, d'accord ?

— OK.

— Tiens, t'as des brocht aux fruits tout chaud, » dis Joral sortant une
plaque recouverte de pâtisseries de forme indéterminée, mais dont la
surface est tendue, prête à rompre sous la pression de ce qu'elles
contiennent, « et, normalement, tous les fruits qui pendent des arbres
sont comestibles ici. Le meilleur fast food du monde. »

— Et il y a quoi dedans ?

— C'est une sorte de spécialité locale. Certains disent que c'est polonais
d'autre que ça viendrait d'Allemagne, des Balkans ou du Pakistan — mais
sous un autre nom — ou d'à peu près n'importe quelle culture qui intègre
des beignets dans son petit dèj. C'est un peu comme un chou à la crème, ou
un beignet fourré, mais a l'envers. Du moins, c'est comme ça que je les
fais ici. C'est plus un terme générique équivalent à “machin brioché
à manger”, mais brocht sonne mieux non ? Attention, c'est très chaud. »

Et sur ces mots, il sort de la cuisine, laissant la grosse gamelle sur le
feu, jetant ses gants en plastique avant d'attraper une paire de brocht
et de se servir de café en un seul mouvement fluide, presque robotique.

N'étant pas encore pleinement réveillée, et un peu perdue dans ses
rêveries, Saina finit par réaliser deux choses. La première est le
tatouage reprenant le logo des Valkyries sur le dos de ses mains. La
seconde c'est qu'elle avait encore la bouche ouverte et qu'elle avait
oublié de dire merci.

« MERCI, balance-t-elle par la porte.

— De rien », parvient-il à articuler, un brocht dans la bouche.

Les tours de l'ancien centre des affaires, sur lesquelles se sont
développées différentes structures, surplombent le Complexe et la baie que
Saina peut voir. Tonya termine de planter dans un des carrés de terres des
fraisiers, avant qu'elle ne retaille quelques plantes en prévision de la
fin de l'automne. Le soleil plongeant de cette fin de journée se
réfléchit sur sa peau transpirante, la teintant de reflets rouges et
roses, contrastant avec les sangles noires de son baudrier.

Quelques-unes des habitantes de cette médina passent discuter un peu avec
Saina, dans le but de se présenter, de faire connaissance et par
curiosité, pour savoir à quoi ressemble la nouvelle copine de Tonya. Tonya
qui, sur un ton de plus en plus énervé, ne cesse de leur dire que non, ce
n'est pas ce qu'elles imaginent. Saina est un peu perturbé par ce jeu
social auquel elle semble participer sans avoir vraiment décidé si elle
voulait en faire partie.

Stelle est la première à se présenter, un bras mécatronique attaché à son
épaule remplace celui qui lui lui manque. Elle se présente comme étant une
plasteronne cybernétique. Elle imprime des prothèses en tout genre pour
elle, et pour celles et ceux qui en ont besoin. Et elle se lance dans
une description précise de son nouveau prototype de mains autonome,
pouvant être pilotées par les interfaces classiques, en y ajoutant un
petit module RF classique et une batterie qu'elle entreprend de détailler
dans un jargon technique dont Saina ne comprends pas le dixième.

Bloke la sauve de cette situation, confirmant le côté « intéressant » de
ce prototype, notamment de l'utilisation qu'elles en ont faites cette
nuit. Bloke est un logueur. Il enregistre tout avec sa rêveuse, il
classifie les séquences qu'il produit et les remixe, tantot pour en faire
des clips pour de l'XPlay sur DreamLand, tantôt pour documenter certains
aspects du Complexe. Mais ce qu'il préfère c'est mélanger ces expériences,
ces enregistrements sensoriels, et vivre des expériences que le corps
humain ne peut, selon lui, te faire ressentir, du moins à jeun.

Val complète ce trio. Après s'être assise sur le rebord du toit, les pieds
dans le vide de l'atrium, elle commence à tapoter sur son moniteur de
poignet, connecté par un câble jack à sa nuque, au milieu d'une
demi-douzaine d'autres connecteurs, certains occupés par des puces,
d'autres obturés car inactifs. Si elle est plus en retrait que Bloke et
Stelle, elle interagit avec eux dans une synchronicité, que l'on ne peut
avoir qu'avec des personnes dont on partage l'intimité et la confiance
depuis un moment. La jack-head est probablement en train de chercher des
accès à des serveurs corporatistes, ou de jouer avec les derniers outils
assemblés par les code-brokers du Complexe.

Saina a aperçu Park, plus qu'elle ne lui a parlé. Son groupe de vermine
ayant apparemment besoin d'elle pour juguler une invasion de rats.
Enfoncée dans un sweat noir à capuche trop grand pour elle, dissimulant
la forme de son corps, mais servant à priori de lieux de vie pour une
paire de rats. Iwata, actuellement en rappel sur la façade de la médina
est le spécialiste des circuits de distributions électrique. Vétu d'un
maillot de basket qui a l'air petit pour lui, le géant noir aux dreads
fluo procède à l'ajustement de panneaux solaires. La dernière personne
à venir voir Saina, avant que Tonya ne termine ses plantations, est
Nakato. Sans conteste possible, est la doyenne du groupe. C'est aussi,
apparemment, quelqu'un qui, contrairement à ce que Saina constate, a prit
le temps de réfléchir à sa tenue avant de quitter sa chambre.

« Dis-moi, je peux te poser quelques questions ? demande-t-ele à Saina

— Hein ? Euh, oui, oui.

— Si j'ai bien compris ce que Tonya m'as raconté, tu es une sorte de
chamane ?

— Plus ou moins. Pourquoi ? »

Nakato s'assoit sur une chaise qu'elle installe face à Saina.

« J'ai besoin d'un coup de main sur un projet. Et de ce que j'ai compris,
tu es probablement la personne dont j'ai besoin.

— Je ne vois pas bien comment je peux être utile, j'arrive juste et je
suis larguée.

— Ha, Nakato sourit. C'est normal que tu sois perdue, c'est conçu pour ça.
Dire que le Complexe est conçu est une simplification monstrueuse doublée
d'un mensonge. Mais si ça peut te rassurer, personne ici ne sait vraiment
ce qu'il fait. Ça fait partie de l'intérêt de vivre ici. Bref, comment tu
décrirais ton chamanisme à quelqu'un qui n'y connait rien ?

— Avec plus de café déjà. Mais c'est une façon d'approcher la théorie de
l'information, et les machines qui en découlent, en utilisant les outils
linguistique et culturels au lieu des mathématiques seules. C'est admettre
que nos identités, nos cultures et donc notre monde ne peuvent être régit
que par des problèmes mathématiques, qu'il est important d'intégrer notre
façon de penser l'information dans nos vies, que nos vies sont modifiées
par nos cultures, nos langues et donc notre rapport à l'information

» Du coup, je cherche à trouver du sens et de la sémantique, dans le
signal dans lequel nous baignons sans même nous en rendre compte. Les
hackers utilisent des outils syntaxiques pour extraire de l'information,
moi j'utilise mes outils sémantiques pour comprendre l'information. C'est
pour ça que je m'intéresse beaucoup à ces bots oubliés, devenus des
artefacts linguistiques et culturels. C'est un bon moyen de comprendre
le monde autour de nous.

— Mmm. Mais du coup, tu penses que les machines ont des esprits ?

— Pffff, laisse échapper Saina. Non. Pas plus qu'une crémaillère ou qu'un
marteau. C'est plus comme … considérer ces machines comme faisant partie
d'un système plus grand qu'elle, et essayer de comprendre cette relation
et agir dessus. De comprendre le fonctionnement du tout comme étant plus
que la somme des machines et données qui y sont stockées. »

» Prends le bras de Stelle par exemple. En soit c'est juste un mécanisme.
Des interfaces, des actionneurs et des capteurs, comme à peu près tout
système. Ça c'est pour la partie mathématique, on sait modéliser ce genre
de système, les reproduire, les modifier, etc.

» Moi, je pense qu'il faut considérer ce bras non pas comme une mécanique,
mais comme ce qu'il est : une part de Stelle. Son existence change notre
perception de Stelle, change la façon dont elle voit le monde, la modifie.
De ce point de vue, du point de vue sémantique, il devient plus facile
d'appréhender le monde abstrait et virtuel de l'information, de la culture.
De même que Stelle dispose d'une extension, d'une augmentation de son
corps, nous nous sommes étendus. Nous avons augmenté notre perception et
notre compréhension du monde en connectant de plus en plus de capteurs
à notre conscience, en facilitant l'échange de données entre nous.

» Traiter ces données comme un sous-produit de notre existence est
dangereux, comme une pure théorie mathématique, c'est ce qui a amené à la
création des grilles corporatistes que la plupart des gens utilisent
désormais. C'est ce qui a amené à l'élimination progressive des marges de
cette société. Notre conscience s'étend bien au-delà des limites physiques
de notre corps, elle s'enrichit d'expérience virtuelle, de replay de
souvenirs d'autres, de gigantesque archives de données. Tout ce que je
fais, moi, c'est utiliser des évocations pour travailler sur ces
relations, j'étends ma conscience dans ce signal, dans ce bruit, pour
accéder à un point de vue différent, pour comprendre des choses que je ne
pouvais pas comprendre autrement. »

— Je crois que je vois un peu ce que tu veux dire. » Nakato réfléchit un
peu avant de poursuivre. « Ce projet dont j'ai envie de te parler peut
paraître un peu, étrange. Je traîne pas mal avec Ivory Tower et …

— Ivory Tower ? Qu'est-ce que c'est ?

— Un groupe de mémakers. Des méméticiens. Répond Tonya arrivant derrière
elle, Nakato, je t'ai dit d'y aller doucement avec elle, elle arrive
juste, commence pas à l'embarquer dans tes projets, d'accord ? »

Nakato répond d'un grognement. Puis pianote quelque chose sur le link
qu'elle vient de sortir de sa poche avant de répondre.

« Tu as raison, ça peut attendre que tu sois installée, ça ne risque pas
de s'envoler. Je t'ai pingué mon contact, et quand tu veux jeter un œil
à ça, tu me dis.

— Euh, d'accord … Mais tu ne peux pas m'en dire deux mots, rapidement ?

— Juste que c'est à propos de mème, de langage et de la vallée de
l'étrange.

— Ils ont fait quoi cette fois ? demande Tonya.

— Rien pour le moment. Je crois. J'ai besoin d'une aide extérieure pour
être sûre, mais il n'y a rien d'urgent. Et bienvenue ici Saina, fait
vraiment comme chez toi. »

Nakato se lève doucement avant de descendre vers la cuisine. Et, pour la
première fois depuis qu'elle est réveillée, Saina est presque seule avec
Tonya. Elle profite en silence de la vue sur le patio pour essayer de
comprendre comment cette structure a émergé, comment les choses sont
agencées ici, comment sans système décisionnaire central, un quartier
comme celui-ci a pu voir le jour.

Elle est au troisième étage d'un ancien immeuble de bureaux, dont une
partie du toit s'est effondré, créant ainsi le patio situé deux niveaux en
dessous, fusionnant le lobby du premier niveau avec l'étage supérieur.

En face et légèrement en contrebas, les cultures hors sol auprès
desquelles Saina s'est affairée une bonne partie de la journée et qui sont
surveillées par différents capteurs posé stratégiquement par Awiti. Au
centre du patio, à l'emplacement de l'ancien comptoir d'accueil, à côté
d'une étrange sculpture, s'élève maintenant un bananier dont les racines
écartent les carreaux de béton et ouvre le bâtiment en deux.

Sous la terrasse — qui n'est, au final, que le plancher d'anciens
bureaux — l'espace est cloisonné, réparti entre différentes parties
communes et zones privées. Le rez-de-chaussée est consacré aux
installations plus lourde et nécessitant des machines outils par exemple.
C'est là que Stelle a installé sa plasto-forge dont les imprimantes sont
en train de créer, à partir d'amidon de maïs, des pièces pour améliorer
son prototype. Il y a aussi le bar, accolé à la cuisine, et de nombreux
canapés, coussins, poufs et bacs de fleurs divers jonchent le sol du
patio.

La terrasse où elle se trouve, est bordée des restes d'une verrière, dont
les vitres ont étés remplacées par différents panneaux de bétons que les
habitants ont passés de nombreuses journées à décorer, les fresques se
succédant les unes aux autres en fonction des envies et de la population.
Un auvent permet de mettre la terrasse à l'abri du soleil, mais aussi d'y
suspendre boules de tango et guirlande diverses.

« Avant que je te fasse faire le tour du coin, et de voir comment tu peux
t'installer, il faut qu'on parle toutes les deux.

— D'accord. »

Tonya s'assied en face de Saina, sur le sol. Elle a les cheveux humides de
la douche dont elle sort et a enfilé un T-shirt déchiré orné de multiples
taches de cambouis ou de produits chimiques divers et dont le dessin a été
bien trop lavé pour encore signifier quelque chose. Elle a gardé le
short et le harnais qu'elle semble ne jamais enlever.

« Déjà, bienvenue ici. Il est important que tu saches que, quel que soit
l'issue de cette conversation, si tu veux t'installer ici, avec nous, tu
es la bienvenue.

» La plupart de mes … colocataires … sont d'accord avec cette idée. On
peut te trouver une chambre, te faire un peu de place, bref t'aider
à t'installer. Sans aucune obligation de quoi que ce soit.

— D'accord, mais pourquoi tant de précautions, qu'est-ce que tu as à me
dire ?

— Rien de particulier, mais il faut qu'on parle de toi, moi et les autres.
Je ne sais pas ce que tu as compris des relations interpersonnelles ici,
ni ce dont tu as l'habitude, mais on est un genre de famille. Non, pas
famille. Un clan ? Une tribu ?

— Une clique ?

— Un groupe de personnes qui partagent beaucoup de choses, qui sont
impliquées émotionnellement à divers degrés et dans diverses relations
avec les autres ici. Park par exemple passe plus de temps avec ses rats
qu'avec nous, et cela nous va très bien. On couche ensemble aussi, pas
tout le monde d'un coup hein, mais pour s'amuser de temps en temps. Ça ne
veut pas dire que personne n'a de relations privilégiées avec les
autres. » Tonya marque une pause.

» Ce que je veux dire, avant que je ne m'emmêle complètement les pinceaux,
c'est que je veux bien sortir avec toi, ça me ferait plaisir. Mais je
viens avec un package. Ça ne veut pas dire que tu dois être ultra tactile
avec tout le monde, ni que tu dois aimer tout le monde, juste que les
relations ici peuvent avoir tendance à impliquer plus de deux personnes.

Après quelque seconde de silence, qui passe pour une éternité, Tonya
commence à paniquer.

« Tu ne dis rien, pourquoi tu ne dis rien ? Je t'ai blessée ?

— Non, non, du tout. Je réfléchissais à la façon dont j'envisage les
relations. Et une des raisons pour laquelle je suis chamane, c'est de
pouvoir vivre à l'intersection des mondes. De pouvoir observer les
signes des changements, les altérations infinitésimales qui amèneront plus
tard aux bouleversements. Et ce n'est pas quelque chose que je peux faire
au milieu du monde. Et ça me va bien, les relations avec les autres sont
quelque chose de fatiguant pour moi, ça me draine mon énergie, mon envie
de faire des choses. C'est pas tant que je n'en ai pas envie que … » Saina
cherche ses mots, hésite. Tonya attend qu'elle trouve ses mots.

» J'ai du mal à définir ce que je suis. C'est utile quand il s'agit de se
détacher du monde pour l'observer de loin. Mais ça devient difficile et
épuisant si j'y ajoute des relations. Je perds alors une partie de cette
clairvoyance, de cette capacité d'analyse, parce que je fais partie de ce
monde que j'observe. Et je ne me comprends pas.

» Depuis que je suis arrivée ici, je souffre un peu de ça. Non, je souffre
beaucoup de ça. Entre la rumeur permanente, les bruits, discussions, cris,
rires, bousculades, ici ou dans le signal et la fatigue et l'incertitude,
je me perds. Et j'ai besoin d'un ancrage quelque part je crois. J'ai pas
envie de fuir en hurlant comme à chaque fois que je vais en ville, mais
j'ai besoin d'un peu de stabilité de certitudes. Parce que ça m'aide à me
définir, à savoir qui je suis. Et peut-être que ici je peux y arriver. Les
choses sont différentes ici. Les gens aussi. Mais je suis larguée.

» Et oui, j'ai envie d'être proche de toi, physiquement, socialement,
émotionnellement, même si je ne comprends pas ce que cela implique. Mais
je n'arrive pas à savoir si j'ai envie de ça avec toi spécifiquement ou si
j'ai cette attirance pour n'importe qui prend cinq minutes de son
temps pour me parler.

— Je ne suis pas sûre de comprendre tout. Mais est-ce que tu me fais
confiance ?

— Oui, comme je le ferais avec n'importe qui en fait. C'est le problème je
crois.

— Ben écoute, tu as envie d'être proche de quelqu'un, moi j'aime bien
l'idée d'être proche de toi. Est-ce qu'on a besoin de plus ?

— Ça a une certaine logique. C'est pas simple, j'ai pas envie que tu sois
juste une personne random, car si c'est le cas, moi je ne me vois que
comme une personne random aussi, et j'ai pas envie de ça et …

— Hey, on est deux dans une relation. Au moins. J'ai aussi mon mot à dire
tu sais, dit Tonya en prenant la main de Saina, et moi j'ai envie d'être
avec toi. Et j'entends que ce n'est pas simple pour toi, mais on peut
essayer, à ton rythme, d'accord ? T'es pas obligée d'être hors du monde
pour le voir et le comprendre, je ne pense pas. Je suis persuadée du
contraire. Et je peux t'aider à t'isoler si tu en as besoin, à refermer le
monde autour de toi. Mais j'ai besoin que tu me donnes cette possibilité,
que tu nous laisses la place d'essayer. Et peut-être que l'on arrivera
à faire en sorte que tu te sentes bien ici, sans que tu aies besoin de
vivre en ermite. J'ai envie d'essayer ça si tu es d'accord.

— D'accord. » Saina se glisse contre Tonya et l'embrasse doucement. Tonya
la serre contre elle. Elles restent là en silence, à s'enlacer,
à s'embrasser et à se caresser.

« Allez viens, mon lit est plus confortable que le béton ici. Et on sera
tranquille », dit Tonya en se relevant.

Elles redescendent de la terrasse, traversent la cuisine et le patio puis
Tonya ouvre la porte de sa chambre et jette Saina sur le lit avant
d'enlever son T-shirt et de sauter sur son lit, juste à côté de Saina. Et
Saina se laisse faire, elle sent les mains de Tonya se glisser contre sa
peau, et soulever son T-shirt trop grand. Les doigts de Tonya courent le
long des pistes électroniques tatouées sur sa peau, déclenchant de petits
court-circuits et modifiant la configuration de son processeur dermique
connecté par la broche neurale à son cortex.

Elle sent ses poils se dresser sur sa peau sous la décharge homéostatique
qui essaye de corriger le surplus d'ocytocine et d'endorphines, qui
provoque une dilatation de ses pupilles et de ses vaisseaux sanguins, le
resserrement de la bande de fréquence à laquelle elle est sensible,
l'augmentation de sa résolution tactile, l'accélération son rythme
cardiaque pour mieux dissiper la chaleur dégagée par les bobines dans ses
avant-bras et ses cuisses et générant des décalages de registre provoquant
des signaux incohérents.

Et, sans trop savoir pourquoi, Saina n'est plus là. Ses pensées partent
dans tous les sens, elle se demande ce qu'est ce truc qu'elle a entendu,
si Tonya est là parce qu'elle en a envie, elle se demande comment les
habitants du Complexe font pour s'organiser, elle se trouve super
chanceuse d'être là, dans ce lit, avec cette personne, mais elle dérive
dans un océan de pensées parasites.

« Ça va ? lui demande Tonya, enlevant ses mains de Saina.

— Je … Non. Si. J'ai décroché je crois. C'est super cool, j'aime bien,
mais … tu peux juste me prendre dans tes bras, c'est trop pour moi là.

— Oui, bien sûr. »

Tonya glisse sur le côté et serre Saina fort dans ses bras qui, pour la
première fois depuis longtemps, se met à pleurer.

« Qu'est-ce qui se passe ?

— Non. Je décroche, c'est pas toi. Je comprends pas pourquoi, mais d'un
coup je ne suis plus là et ça devient bizarre, perturbant. »

Tonya serre Saina contre elle, l'embrasse dans la nuque, et lui murmure
des paroles rassurantes à l'oreille. Elles finissent par s'endormir toutes
les deux, enlacées ensemble, emmélées dans les draps.
