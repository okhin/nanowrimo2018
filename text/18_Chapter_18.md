# Chapitre 18.

> [ 888Ghz ]
> 
> Aaaaaaaaand, we're live!!!!! Merci aux Entropic Space Punk de nous fournir
> de quoi vous envoyer les infos importantes, à savoir les derniers sets
> analogiques d'Electric Beast. Si vous parvenez à faire parvenir message,
> ou master divers et variés à notre QG, on les diffusera.
> 
* * *

Saina est nichée dans la flêche sud du Hanged Billionaire. L'endroit est
devenu au fil du temps une sorte de nid confortable, offrant une vue
imprenable sur les eaux vertes d'algues au Nord et sur les principales
tours du Complexe au Sud.

Un son saturé de parasites, crépitant sort d'un petit récepteur FM hors
d'âge, réanimé et branché sur 88.8 MHz. L'ensemble offre à Saina une zone
de calme, harmonisant ses perceptions sonores et celles de sa peau,
vibrant dans le spectre de basse fréquence qui, peu à peu, s'étendà
travers le Complexe.

Pendant que a brume matinale se dissipe, Saina se réchauffe les mains sur
un mug de café brulant, café servi par les premiers debout et les derniers
couchés du Hanged Billionaire. La frontière entre les jours lui échappe
complètement, et la fête continue qui se tiens dans la nef du Hanged
Billionaire ne l'aide pas à faire le point sur les derniers évènements. Ce
flou temporel, son manque de sommeil, le taux de caféine dans son organise
à faire se réveiller un mort et le signal perturbé, la laissent sur ses
gardes en permanence, coincée dans l'instant présent.

Elle a grimpé ici pour essayer de reprendre pied, de s'isoler afin de
pouvoir se concentrer et d'essayer de comprendre ce qu'il se passe. De
sortir son train de pensée de sa boucle destructrice et de se détacher un
peu de son état émotionnel. De se retirer du monde pour essayer de le
comprendre, comme l'ont toujours fait les chamanes à travers les âges.

Les routines de bases d'abord. Contrôle de la respiration et harmonisation
sur le signal, laisser la panique générée par la caféine passer, et s'en
servir pour convertir ses émotions négatives en un système de pensées
structuré. Puis laisser le signal courir sur elle, vague après vague, et
se laisser bercer. Sentir la chaleur dans ses doigts fusionner avec les
protocoles de routage qui, montant le long de ses bras et de sa nuque,
commencent à altérer ses perceptions. Mobilisant ses filtres synaptiques,
elle isole les protocoles, en extrayant le contenu, le signifié.

Des motifs se dessinent doucement dans ses centres nerveux, lui procurant
la sensation satisfaisante de se distancier de sa chair, de son organisme,
et de s'étendre à perte de pensée dans le chaos de ces données, à la
recherche de comportements émergents.

Vu d'ici, l'ensemble du Complexe est devenu triste, vidé de son énergie
créatrice bouillonnante, mais au centre il y a toujours cet espace
d'entropie infinie, supprimant toute capacité de réflexion à quiconque
essaierait de le lire. Saina s'isole dans une posture d'observatrice et
fouille dans sa mémoire profonde à la recherche de ce qu'elle a enregistré
chez Uncanny Valley.

Cet artefact symbolique qui semble affecter Tonya et tant d'autres dans le
Complexe. Elle n'a pas vraiment cherché à s'y confronter jusqu'à présent,
ne voulant pas risquer une infection, mais elle est à court d'idée. Et son
intuition lui dit qu'il pourrait s'agir de la clef du problème, ou au
moins fournir une carte pour trouver cette clef. Elle sait qu'il est
possible que cet artefact symbolique soit la cause de cette intuition, et
qu'elle est peut-être déjà infectée, mais elle n'a plus beaucoup d'idée et
elle compte sur sa transe pour la protéger des problèmes d'identités qui
affectent les autres.

Tel un oignon, elle épluche ce souvenir couche après couche, enlevant les
éléments n'ayant pas de lien avec cet artefact. Elle enlève Tonya, les
personnes avec qui elle parle. Elle enlève le goût étrange de la boisson
qui lui a été servie. Elle enlève la décoration étrange du Seljouk. Une
opération d'archéologie mémorielle minutieuse qui lui permet d'analyser le
moindre écho mémoriel de cette scène qu'elle rejoue encore et encore dans
le théâtre de sa conscience.

Détachant les derniers signaux externe, l'artefact cognitif est maintenant
tout ce qu'il reste dans son souvenir de cette rencontre. Elle l'observe.
Il semble être doté d'une intentionnalité, agissant de lui même. Envoyant
des synapses à travers le vide cognitif au milieu duquel il se trouve. Il
est structuré et possède une grammaire, mais ressemble étrangement à un
code informatique quelconque.

Saina se sent aspirée vers cet artefact. Quelque chose ne va pas. À mesure
qu'elle plonge son champ analytique vers cet artefact, elle perçoit des
structures fractales à l'intérieur de celui-ci, des motifs se répétant
à l'infini, tournant doucement et envahissant progressivement ses pensées.

« Ne t'oppose pas. Au contraire, accepte le champ de données, laisse le
passer à travers toi et partir. Et regarde le changement qu'il laisse en
toi. » Sa leçon la plus dure lui reviens en tête. Ne pas combattre, ne pas
s'opposer. C'est son rôle et elle doit s'y tenir, d'autres mènent les
combats, mais pas elle, elle doit comprendre, observer et documenter.

Elle abaisse mentalement ses barrières et laisse l'artefact l'aspirer dans
une spirale entropique infinie. Elle laisse l'artefact se brancher dans
ses souvenirs, dans sa mémoire. Et elle note le changement, les
différences. À mesure qu'elle tombe, de plus en plus de souvenirs lui
viennent en tête. Les siens, du moins elle le pense, mais aussi d'autres.
Elle se rappelle avoir fait l'amour à tellement de monde qu'elle ne peut
plus les compter. Elle se rappelle son reflet dans un miroir, lui
renvoyant l'image du corps tordu et adolescent du garçon qu'elle était.
Elle se rappelle la chute libre du haut des plus grande tour du Complexe,
et le choc subit à l'ouverture du parachute. Elle se rappelle vouloir
serrer très fort dans ses bras cette chamane qui à l'air complètement
perdue.

Elle se rappelle enfin la corde autour de son cou, l'étranglant à mesure
que sa conscience s'étiole alors que la vie quitte son corps. Elle veut se
débattre et crier, mais elle n'a plus d'air dans les poumons et plus
aucune énergie ou envie de vivre.

Elle est soudain replongée dans son corps, sortie brutalement de sa
transe. Le monde tourne de plus en plus lentement, et sa conscience sépare
le signal de son corps, lui permettant de revenir dans la flèche du Hanged
Billionaire, au milieu des coussins et des couvertures, secouée par une
Nato paniquée.

A ses pieds, Saina voit un petit sac en papier duquel s'échappe une odeur
de cannelle et de vanille et contenant des borchs chaud, à peine sortis du
four, et une gourde pleine de café noir.

« Ça va ? demande Nato, l'inquiétude dans la voix.

— Je crois oui. Pourquoi tu m'as réveillée ?

— Tu t'étais arrêtée de respirer. Je sais qu'il faut éviter de sortir les
personnes de leurs trip divers, mais tu allais pas bien du tout.

— D'accord. Ça fait longtemps que tu es là ?

— Une vingtaine de minutes. Je crois. Le soleil était pas encore levé. Tu
me diras où tu étais toi ?

— Passe moi du café. Et laisse moi le temps de me recentrer, et on en
parle.

Elles restent quelques instants en silence, leurs pensées divaguant dans
des univers bien distincts ; Saina essayant de comprendre ce qu'il se
passe et Nato échouant encore sur les brumes du sommeil, à l'équilibre
étrange entre rêve éveillé et lucidité.

« Dis, dit Saina brisant le silence, pourquoi tu es venue me chercher
hier ?

— Hier ? Il y a deux jours plutôt.

— Si tu le dis. Pourquoi tu es venue me chercher ?

— J'avais envie ?

— Certes. Mais comment tu savais où me trouver ?

— Nakato me l'a dit. Elle m'a aussi dit que tu l'inquiétais un peu, je
suis venu voir si je pouvais te remonter le moral un peu.

— D'accord. Saina prend une profonde inspiration avant de continuer, du
coup tu attends quoi de notre relation.

— Ben, pas grand-chose. Et beaucoup de choses, crois pas que je te vois
juste comme une personne random hein, mais les trucs nécessitant beaucoup
d'implications émotionnels, je les aime comme mes maths : distribuées.

— Je suis pas encore super à l'aise avec … les gens. Surtout avec la
fatigue. Je sais pas vraiment ce que je veux dans ce domaine en fait, il
y a beaucoup de monde, une intimité partagée volontairement par beaucoup
ici, mais moi j'ai pas vraiment vécu dans un milieu avec si peu de
frontières sociales.

— T'es pas la seule hein, et c'est pas parce que j'aime bien flirter,
embrasser, coucher, danser ou discuter avec des gens que c'est pas
douloureux par moment. Mais on apprend à faire avec, et on trouve ses
marques avec le temps je crois. Réalisant qu'elle s'éloigne un peu du
sujet dont Saina semble vouloir parler, elle reprend : je t'ai mise mal
à l'aise hier à te faire du rentre-dedans ?

— C'était pas vraiment du rentre dedans à ce niveau là. Mais c'était
perturbant … Pas désagréable, mais inhabituel. Je suis complètement paumée
là, et fatiguée, et mes émotions sont en vrac et …

— T'es pas obligée de savoir ce que tu veux. Si tu veux que je respecte
une limite, une distance, dis-le et je m'y tiendrais.

» On est pas obligé de baiser hein, juste flirter pour le fun, se
caresser, danser, dormir ensemble, moi ça me va très bien. J'ai besoin
d'être avec des gens, je suis pas super bien dès qu'il s'agit d'être
seule. Mais j'apprécie beaucoup une proximité physique qui est juste ça,
être collée l'une à l'autre, sans autre attente.

— C'est pas que je veux pas, note bien. Juste … c'est beaucoup de
changements pour moi ces derniers mois, je sais pas comment me positionner
par rapport à Tonya déjà.

— D'accord. On est obligées de rien, et tu me dis si je vais trop loin, ou
quoi, l'interromps Nato, et on est pas obligées de se prendre la tête
à définir une relation ou quoi. Et c'est bien pour moi. »

Saina ne répond pas, elle n'a pas grand-chose à ajouter et reste quelque
peu perdue, désorientée, dans ce monde qui n'as que peu de choses en
communs avec sa vie d'avant.

Elle sourit à l'idée que, à peine quelques semaines après son arrivée ici,
elle considère déjà le reste de sa vie comme une vieille mue laissée
derrière elle, comme si plusieurs mois se seraient écoulés depuis sont
arrivée.

« Tu penses à quoi ? lui demande Nato

— Au bordel avec Plurality, répons Saina, ne mentant que partiellement.

— Et donc ?

— Je ne sais toujours pas ce que c'est, pourquoi ça arrive ou quoi que ce
soit d'autres.

— Il faut que tu changes de manière de penser déjà.

— Tu veux dire, avoir une approche plus intuitive, genre le chamanisme ?

— Non, Nato ne prête pas attention au ton froid de Saina, changer non pas
de système de pensée, le tien est autant valide que les autres, mais
changer de manière de penser.

» Déjà, ce n'est pas ta faute. Tu n'as pas effondré Plurality seule, ce
sont des conséquences de choix, de décisions et d'actions menées
collectivement, et c'est important que tu gardes ça en tête.

» Ensuite, tu as peut-être merdé, mais on merde toutes, c'est pas ça qui
compte. Ce qui compte c'est ce qu'on fait de nos erreurs et d'en limiter
l'impact. C'est pour ça qu'on a une structure sociale. C'est aussi pour ça
qu'il n'y a pas de standards ou de comité de décision ici, la diversité du
Complexe est ce qui fait qu'il survit et s'adapte aux erreurs de tout le
monde.

» Et t'es pas toute seule à réparer. Tu es peut-être la seule à avoir ton
approche, mais si tu m'expliques, ça ne sera plus le cas. Et en plus,
beaucoup de monde essaye de réparer.

— Ouais, enfin on ne sait toujours pas si Plurality va redémarrer.

— Et ? C'est un problème auquel tu peux faire quelque chose ?

— Non, je ne pense pas. Enfin, peut-être. Il y a bien des choses que je
peux faire non ?

— Ben, tu parles réseau/routage ? Tu peux aider à remettre un index de DHT
en route ?

— Non, pas vraiment.

— Bien, on avance. On sait déjà que tu ne pourras pas aider sur la partie
logique et matériel. Il reste quoi du coup ?

— Je sais pas. Mais si Plurality ne repars pas, ça va pas foutre le
Complexe en l'air ?

— Pourquoi ça foutrait tout ça, et Nato dessine un arc de cercle en
direction des tours et bâtiments visibles depuis le clocher, en l'air ?
Qu'est-ce que Plurality a de si particulier que tu penses que nous ne
puissions pas faire sans ?

— Ben, c'est un peu votre moyen de communication, votre mémoire, non ?

— Bof, c'est surtout un index galvaudé de ce que tout le monde stocke
à gauche et à droite. Et regarde, on a déjà des gens qui indexent de la
musique, qui la partage, la diffuse. OK, sur du matériel préhistorique,
mais ça prouve qu'on a pas perdu de contenu.

— T'as sans doute raison. Reste que je comprends pas bien ce qu'il s'est
passé.

— C'est important pour toi de comprendre ?

— Oui, je pense. Et c'est intriguant. Est-ce qu'il y a effectivement une
ou plusieurs consciences qui vivent dans Plurality, ça ne t'intéresse
pas ?

— Oh si, mais c'était surtout toi dont on parlait. Allez viens, on va voir
comment on peut régler ce problème. Tu disait tout à l'heure vouloir me
parler de là où tu étais. T'es suffisamment d'aplomb pour ça ? demande
Nato.

— Ben, j'aurais une réponse, on gagnerais toutes du temps. Ce que j'ai
compris c'est qu'il y a un genre de langage viral qui est capable de
s'exécuter à la fois sur des machines de Turing, mais aussi dans nos
systèmes synaptiques.

— Comment tu passes la barrière entre l'électronique et la biologie ?

— En utilisant des failles cognitives. On en est bourré, c'est pas
forcément le truc le plus compliqué à faire hein. Toute l'hypnose, une
bonne partie des traumatisme, tout ça, ça exploite des failles dans notre
conscience.

» Bref, il y a donc un genre de code ou de langage qui nécessite, pour
fonctionner, des processeurs classiques et nos processeurs biologiques.
C'est ce qui est la cause de nos migraines monstrueuses, et de l'état de
prostration de Tonya.

— Un genre de code ?

— Artefact symbolique. Mais j'ai pas mieux pour expliquer ce que c'est.

— Est-ce que tu sais si il y a une intentionnalité, une conscience ?

— Pas plus que dans un chaos duquel aurait émergé notre conscience. Ce
n'est pas une question importante, c'est la question à laquelle tout le
monde cherche à répondre et qui n'as probablement pas de réponse.

» Ce que je sait en revanche, c'est qu'il y a des comportements de
communications, de protections. Et aussi que ça à un lien fort avec nos
souvenirs, nos mémoires.

— Pour ça qu'elle est partie se planquer dans DreamLand du coup.

— Voilà. Je pense qu'elle a aussi essayé d'établir un dialogue avec moi,
mais c'est à peu près le moment où tu m'as réveillée. Je me rappelle aussi
de choses que je n'ai pas pu vivre. Pas le bon corps, pas le bon endroit,
pas le bon point de vue. Mais c'est réel, comme si j'avais vécu ces
expériences moi-même.

— Ça n'as pas l'air de beaucoup te perturber pourtant.

— L'avantage de vivre dans une hallucination et de questionner son
existence en permanence je suppose, je n'ai jamais accordé beaucoup
d'importance aux souvenirs.

— Mais 3L10T as pas déjà essayé de prendre contact avec toi ?

— Justement, je ne suis pas sûre que 3L10T et cet artefact symbolique
soient liés. Je ne le retrouve nulle part ailleurs, et il semble être
connecté à l'espace à entropie infinie.

— Bon, je te propose qu'on aille manger un truc et danser un peu avant de
s'y remettre. Laisse le temps aux informations de décanter, de s'agencer
comme il faut et laisse aux admins le temps de remonter du réseau, ça
devrait te faciliter la vie. »

Joignant le geste à la parole, elle se laisse glisser le long de l'échelle
métallique amenant au clocher. Nato, l'odeur de canelle et de café venant
du sol, le besoin de se dégourdir les jambes sont autant de motivations
pour Saina qui finti par la suivre, refermant derrière elle la trappe
d'accès.
