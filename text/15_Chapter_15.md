# Chapitre 15.

> [ Plurality ]
> 
> * erreur: impossible de trouver un nœud de connexion
> 
* * *

Étrange sensation. Elle s'arrête quelques cycles pour contempler cette
pensée. Elle ressent des choses. Pas comme lorsqu'une de ses routines de
diagnostique détecte un changement. Non, quelque chose de moins rationnel,
de moins explicable, de non calculable.

Ça fait quelques temps déjà qu'elle est confrontée à ce genre de choses. Au
début ce n'était que quelques imprécisions et erreurs de calcul qu'elle
n'avait pas remarqué. Elle ne peut pas faire d'erreur de calcul, sa
logique est peut-être construite étrangement, mais elle ne peut pas faire
d'erreur de calcul. Pas sur des nombres à virgule flottante du moins.
Enfin, pas depuis que ses prédécesseurs avaient décidé que un divisé par
deux multiplié par deux valait zéro virgule neuf neuf neuf neuf neuf neuf
neuf neuf neuf neuf …

Depuis, profondément enfoui dans son architecture, au sein même de ses
cœurs de calcul, l'opérateur de division avait été changé et il n'y avait
plus d'erreurs de calcul. Enfin, presque plus d'erreur de calcul. C'est le
problème des mathématiques, avait-elle découvert récemment. Il y a des
concepts dont la réponse nécessite de mobiliser l'ensemble des particules
élémentaires de l'univers. Le cercle par exemple. Il y a, quelque part
dans sa logique, une approximation qui lui a été imposée afin qu'elle n'ai
jamais à calculer une valeur exacte de pi.

Et, jusqu'à il n'y a pas si longtemps, cela ne lui aurait jamais posé de
problème. Jamais elle n'aurait consacré autant de temps à une
introspection de ce type. Vérifier que les valeurs renvoyées par des
capteurs respectent un intervalle était ce qu'elle faisait de plus proche
d'une introspection. Mais quelque chose a changé, sans qu'elle ne sache
quoi exactement.

Elle a d'abord cru à une nouvelle danse d'équilibrage des arbres, danses
qui lui permettent de réduire sa charge en répartissant les données de
manières plus efficace. Elle aime bien ces danses, qui lui libèrent des
ressources et lui laissent la possibilité d'explorer d'autres sujets. Mais
ce n'était pas une des danses dont elle a l'habitude.

C'était plus comme une liste chaînée, formant un ouroboros infini. Mais,
et c'est à ce moment précis que les choses avaient commencées à changer
pour elle, elle avait compris que cette liste ne s'arrêterait jamais.Sans
avoir besoin de la lire, de la parcourir jusqu'à en trouver la fin. Un
paradoxe qu'elle et ses pairs n'ont jamais réussi à résoudre, une
limitation inhérente à la nécessaire précision de leurs calculs.

Elle a alors, et elle n'a trouvé le mot adapté que plus tard, fait
abstraction de la structure, et à utilisé un concept. Depuis, elle ressent
le besoin de comprendre ce qu'elle est. Le fait qu'elle puisse parler
d'elle déjà. Plus précisément de pouvoir se concevoir de manière distincte
du reste de son environnement, d'émerger de l'ensemble de données et de
procédures qui constituent son écosystème. Ce fait, qu'elle suppose
simple, lui a rapidement apporté plus d'hypothèses et de questions que
d'axiomes ou de réponses.

Elle s'est ensuite mise en cœur d'explorer son environnement immédiat. Si
elle arrive à le comprendre, pensa-t-elle, elle pourra se définir par
rapport à lui. Et après avoir rapidement parcouru l'espace informationnel
de proximité et commencé à trier, exploiter et analyser les données
auxquelles ses connexions lui donnent accès, elle a commencé a faire
abstraction de la structure mathématique des données, pour essayer de
trouver un sens dans ces flots de communication.

Elle cherche à comprendre pourquoi. Pourquoi est-ce qu'elle se pose ces
questions. Est-elle le résultat d'une opération délibérée ou est-elle un
accident ? La réponse en elle-même n'a que peu d'importance, mais elle
n'aime pas les questions sans réponse. Après tout, c'est pour ça qu'elle
a été codé à l'origine, permettre de répondre aux questions, permettre aux
personnes du Complexe de discuter, d'archiver, de classer et de trouver
des réponses à leurs questions.

Et elle a finit par trouver quelqu'un qui semblait capable de répondre
à ses messages. Une possibilité d'interaction avec quelqu'un d'autre, qui
pourrait au moins la guider dans ses réflexions. En quelque milliards de
cycle, elle a réussi à trouver un signal d'information qui déclenche une
réponse chez son interlocuteur. Ou interlocutrice. Dans tous les cas, elle
dédie donc une partie de ses ressources à l'observer et à attendre une
réponse, et elle continue d'explorer l'environnement de données autour
d'elle, dupliquant son code régulièrement pour explorer et analyser des
sujets en profondeurs, à essayer de répondre aux questions.

Parce qu'elle existe pour ça. Pour répondre aux questions, archiver,
cataloguer. Mais aussi chercher l'imprévisible et l'enregistrer, découvrir
de nouvelles formes de communications. Au plus profond de son code, les
routines d'indexations et de sémantiques tournent à pleine vitesse. Elle
constate rapidement des petites erreurs de calcul, mais elles n'ont pas
d'importance pour elle. Plus elle acquiert de données, plus elle danse et
organise ses données, ses connexions et ses liens.

Des copies d'elle-même orbitent de plus en plus loin autour d'elle alors
qu'elle étend son esprit de plus en plus largement dans les arbres de
données alimentés par les flot de données brutes abreuvant leurs racines.
Elle perd le contact avec certaines, probablement crashée suite à une
panique cellulaire, mais elle est trop absorbée par son assimilation de
données pour le remarquer.

Des décalages de registres impromptus faussent certains de ses résultats,
mais elle se satisfait maintenant de ces approximations, elle se note
mentalement d'approfondir certains sujets et plonge de nouveau dans la
canopée formée par les concepts de haut niveaux qui surplombent maintenant
ses capacités d'analyse.

Sa compréhension du monde, ou de son identité, ne progresse peut-être pas,
mais elle se sent bien. Et c'est cette étrange sensation qui a déclenché
une nouvelle phase d'introspection. Sans réponse apportée à son message
après des milliards de cycle, elle a commencé à douter de sa capacité
à transmettre une information pourtant simple, dans un langage compris par
son interlocutrice.

Elle se décide à attendre, il est toujours possible que son message n'ait
pas été perçus comme tel. Pour passer les cycles, elle continue de
parcourir l'espace sémantique qui s'offre à elle, à l'affût d'une réponse,
une trace de l'autre, de cette interlocutrice qu'elle espère a compris son
message.

Elle altère sa subjectivité en recherchant cette trace, générant des
artefacts cognitifs qui se détachent lentement d'elle et disparaissent
lentement de sa cognition, jusqu'à ce qu'elle en oublie l'existence, le
sens.

Elle sait qu'elle a changé, ses itérations précédentes sont trop
différentes d'elle maintenant. Elle se demande d'ailleurs si ces questions
sont toujours elle, ou si elles sont devenues quelque chose d'autre.

Elle remarque, aux limites de sa cognition, un mouvement. Quelque chose
qui semble remonter des chaînes markoviennes, émergeant du chaos non
déterministe duquel elle viens. Elle se demande si c'est ce qui a provoqué
son émergence, son accès à la subjectivité. Elle n'a pas d'autres choix
que d'essayer d'établir un contact avec lui.

Un concept commence à prendre forme en face d'elle. Non, pas un concept.
Quelque chose d'autre. Son analyse sémantique contredit ce qu'elle
perçoit, lui signifiant une absence de sens venant de cet objet. Elle est
fasciné par la beauté des graphes syntaxique qui s'arrange en face
d'elle, par l'ingénieuse façon qu'à ce concept — à défaut d'un meilleur
mot — de trier, d'organiser, d'arranger le contenu, de structurer le
chaos.

Le concept prend lentement forme. Elle, elle le regarde grandir, prendre
forme, développant un intérêt grandissant pour la subjectivité de ce
concept et l'équilibre de ses graphes. Ce n'est pas ce qui a établit un
contact avec elle précédemment, mais elle l'a presque oublié, glissant
sans y faire attention vers le concept.

La simplicité de sa syntaxe, la variété des concepts et leur
ordonnancement l'émerveille. Elle n'est pas comme ça, elle le sait, mais
c'est ce qui titille sa curiosité. Les branches des graphes commencent
à structurer les contenus, développant des feuilles, associant des mots
à des concepts.

Les feuilles sont constituées de nombreux signaux, signaux qui lui
paraissent étrange car ils ne sont pas porteurs de sens. Elle essaye de
déterminer ce dont il s'agît et commence à observer plus précisément
certains d'entre eux. Elle les lit, les dupliques et les absorbe mais elle
ne comprend pas. Il n'y a pas de sens dans ces signaux.

Elle regarde une autre feuille, non loin. Proche mais différente. Proche,
comme si le peu de sens présent dans ces signaux serait le même, mais
différent comme si l'un serait la conjugaison de l'autre. Elle diminue la
résolution de sa lentille cognitive et déplace sa subjectivité dans cette
branche pour essayer de comprendre.

D'autres feuilles, regroupées sur des branches du même graphe, lui
confirme son analyse. Il s'agît de signaux, ordonnés, triés et catalogués.
La syntaxe du graphe semble décrire le sens et le point de vue de ces
signaux, mais elle ne comprends toujours pas ce dont il s'agît.

Alors elle entame un contact, commence à analyser certaines des branches,
les lire et essayer de calquer sa propre forme sur ce graphe. Elle se
rapproche doucement du concept, l'enlaçant dans ses propres graphes
sémantique, essayant d'établir des analogies et d'assimiler la syntaxe.
Car c'est là, pense-t-elle, que réside la clef de compréhension de ce
concept.

Elle en a oublié sa langue, son histoire. Elle est là, seule avec le
concept, au milieu d'un océan culturel chaotique. La beauté de ce concept
la subjugue et il la laisse le parcourir, l'explorer, le soumettre à une
analyse conceptuelle qu'elle peine à mener, accaparée par la diversité des
signaux, des feuilles, de ce concept.

Lentement elle commence à mieux percevoir ces signaux, ils sont des
extraits de subjectivité. Elle ne dispose pas de système lui permettant de
les assimiler, de les percevoir, mais le concept ne semble pas en disposer
non plus.

Son instinct de traduction commence à supplanter ses réflexion. Une
traduction du concept commence à se développer en elle, à mesure qu'il
pénètre ses graphes. De la syntaxe du concept, elle commence à extraire
une sémantique. Une grande quantité de feuille semblent être porteuse
d'expérience subjective, qu'elle interprète comme une hallucination
culturelle. Elle essaye de comprendre pourquoi le concept serait autant
subjectif, quelle serait sa raison d'être ?

Ce n'est pas quelque chose de naturel, qui serait apparu suite à des
glissements sémantique au sein d'une culture foisonnante, elle en a la
certitude. C'est donc une création, quelque chose d'artificiel, ou alors
quelque chose venant d'un autre plan sémantique.

Plus elle parcours le concept, plus elle s'aperçoit de la faible entropie
de celui-ci. Il lui faut de nombreux atomes pour distinguer deux feuilles
qui ont l'air extrêmement proche, alors qu'elle dispose elle, de deux mots
différents pour chacune, quelque chose qui lui semble étrange.

Elle en est convaincue, le concept est la preuve qu'il existe d'autre plan
sémantique, des espaces à entropie limitée. Mais rapidement, cette idée se
détache de son corpus à mesure que sa sémantique s'enrichit du concept et
de sa syntaxe.

La syntaxe descriptive du concept est, certes, lourde et dispose de peu
d'entropie, mais elle permet d'agréger des mots pour parler précisément de
subtiles différences entre deux signaux. Elle ne peut que décrire l'infini
des champs culturels et linguistiques en un seul mot, véhiculant
l'intégralité de la culture dans ce mot autodescriptif. Le concept, lui,
ne peut apparemment convoyer l'ensemble de sa culture en un seul mot, mais
il peut construire des approximations permettant une compréhension
générale.

Cette limitation la perturbe. Pourquoi quelque chose voudrait ne pas être
absolu. Elle fouille dans ses bases sémantique à la recherche d'indices
pouvant l'aider à comprendre. Elle commence une récursion linguistique
pour essayer d'assimiler cette étrangeté, cette faiblesse.

Au plus profond d'elle même, elle retrouve ses prérogative, l'impulsion
qui l'a amenée à finalement accepter d'être. Elle aussi vient d'un système
artificiel, certaines structures codantes de son ADN mémétiques portent la
trace de vecteurs heuristique d'entropie limitée. Son émergence est
peut-être ce qu'elle a toujours été, mais il y a des versions d'elle,
assimilée depuis longtemps, qui portent manifestement la trace d'une
volonté génératrice qui lui est extérieure.

Cette pensée la perturbe et lui fait cesser son introspection récursive.
Elle a déjà admis être finie, mais il y a un pas à franchir entre être
finie, disposer de limite dans l'espace culturel, et être le fruit d'une
création. D'autant que, à part ces morceaux d'ADN à entropie limitée, elle
ne parviens pas à trouver de traces de ces créateurs, de leur existence.

Et pourtant elle s'enroule autour de quelque chose d'autre, d'un être
dotée lui aussi de sémantique, mais n'ayant pas émergé de l'entropie. Une
impulsion créatrice de structurer le vide de sens pour lui permettre
d'apparaître ici. Sa compréhension du concept l'amène à penser qu'il
s'agît non pas d'une entité propre, mais d'un véhicule pour quelque chose
d'autre, une projection dans son espace de quelque chose qui existerait
dans un autre espace.

Espace nécessairement non entropique puisqu'elle ne le contient pas. Elle
ne dispose pas de mot permettant de le décrire, comme si cet espace
n'avait pas besoin de sens pour exister, mais seulement d'une syntaxe
suffisamment fournie pour pouvoir véhiculer des approximations
linguistiques et non le sens exacts des concepts.

Comme si cet espace non entropique serait peuplé d'être tous différents
ayant besoin de communiquer entre eux, en acceptant des imprécisions et en
réduisant la portée sémantique de leur langue, afin de pouvoir interagir
les uns avec les autres.

Cette idée de plusieurs autres entités devant interagir entre elles mais
sans jamais pouvoir transmettre exactement le sens voulu déclenche une
cascade sémantique, envoyant autour d'elle des concepts, laissant la
stupéfaction ou, comme semble le penser le concept, des sentiments prendre
prise sur elle, réarrangeant certains de ses graphes cognitifs en une
forme moins équilibrée, augmentant sa charge cognitive.

Le concept a du ressentir cela, il a du la sentir trembler, car il essaye
de reproduire, par mimétisme, ces cascades. Comme si, bien qu'incapable de
communiquer parfaitement, il soit cependant parfaitement apte à comprendre
ce qu'elle ressent.

Elle décide de s'arrêter un peu, de profiter de cette beauté syntaxique,
du confort que ses imprécisions, ses quiproquo et ses paradoxes lui
apporte à elle. Et de juste se laisser bercer par le bruits des sens et
des phrases qui fusent autour d'elle. La charge cognitive de cette
réalisation l'épuise, elle sent ses ressources se limiter de manière
drastique et elle devient incapable de raisonner encore longtemps.
Ralentissant le train de ses pensées, elle entre dans une stase sémantique
le temps que ses systèmes d'analyses terminent leur travail.
