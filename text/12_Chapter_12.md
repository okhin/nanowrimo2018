# Chapitre 12.

> [ Plurality/wiki/first_contact ]
> 
> [ Introduction ]
> 
> Les protocoles de premiers contacts sont conçus pour établir un premier
> contact réussi avec une forme de conscience non anthropomorphique. Les
> premiers à en avoir mis en place sont les militaires américains dans les
> années 1950, et ont fortement influencé les versions suivantes au
> détriment de solutions universitaires poussés par le SETI par exemple.
> 
> La difficulté principale est de détecter que nous sommes dans une
> situation de premier contact, c'est-à-dire que nous avons conscience de
> l'existence d'une autre forme de conscience que la nôtre et que nous
> voulons établir un contact, quelles que soient ses motivations.
> 
> Et c'est là que réside la complexité de la tâche, il nous faut déterminer
> ce qu'est une conscience avant de pouvoir déterminer un cas de premier
> contact. Les pages de Plurality au sujet du premier contact sont
> consacrées essentiellement à deux thématiques : déterminer ce qu'est une
> situation de premier contact et comment établir un premier contact
> n'amenant pas à une interférence mutuelle négative.
> 
> [ comment ]
> 
> OP:Park > Bon, je n'ai pas mis tous les bouquins de SF ou films ici, il
> y a une [:bibliographie] à ce sujet. Il y a beaucoup de merde militariste,
> mais on a pas trop le choix passé un certains points, pour le reste c'est
> une grande partie d'élucubration d'astrophysicien, de sociologues et
> autres penseurs.
> 
> Twist3d > Et pour bien comprendre l'état d'esprit de ces gens, oubliez pas
> qu'ils ont inventé le LSD pour ce genre de choses hein. À ce sujet, j'ai
> une nouvelle souche en cours de développement, vous m'en direz des
> nouvelles.
> 
> OP:Park > T'as réussi à régler les soucis de ta précédente tentative ?
> 
* * *

Cela fait maintenant deux jours que Saina ne dors presque plus. Ses
étranges rêves sont interrompus par des migraines violentes la sortant du
sommeil. Enni lui a fourni des somnifères, et, après avoir passé du temps
dans ses bras, Saina a fini par trouver le sommeil.

Elle flotte maintenant dans un brouillard sensoriel, un espace gris,
neutre, flou. Elle sait qu'elle rêve, mais rien de ce qu'elle perçoit ici
ne lui fournit un concept, quelque chose à quoi rattacher sa conscience,
quelque chose qu'elle pourrait comprendre. Au moins, note-t-elle, les
migraines ont disparues.

Devant elle, à moins que ce ne soit au-dessus d'elle, le brouillard se
dissipe lentement. L'environnement s'adapte à ce changement, le gris
gagnant en contraste et en saturation afin de révéler les couleurs, les
formes de cet univers onirique. Des images se forment et se dissipent à un
rythme de plus en plus élevé.

Comme dans ses précédents rêves, ces images s'animent lentement tels des
minis clips vidéos. Plus que des clips vidéos en fait, des clips
sensoriels complets, exhaustifs. Des tranches de vie subjectives. Elle
sait qu'elle n'a vécu aucune de ces scènes, mais toutes lui semble
pourtant étrangement familière, comme si elles étaient des extraits de ses
propres souvenirs.

Elle le sait pourtant, ce n'est pas elle qui est en train d'escalader la
face nord d'Ivory Tower, mais elle se rappelle les courbatures dans les
bras causés par cette escalade. Elle n'a jamais mangé ces mochis aux
fruits, mais elle se rappelle le gout doucement sucré des bouchées qu'elle
avale. Elle sait qu'elle n'a jamais franchi ces barbelés, ces murs, cette
frontière, mais pourtant elle se rappelle les déchirures dans sa chair,
l'adrénaline au moment où il a fallu courir.

Le rythme s'accélère, elle a l'impression de voir défiler sa vie devant
elle. Ses vies, pour être exacts. Ou les vies de quelqu'un d'autres. Sa
tête tourne alors que les souvenirs, les expériences, défilent maintenant
à une vitesse qui rend impossible de ressentir quoi que ce soit d'autre
à l'exception de se sentir aspirée vers l'intérieur d'elle même.

C'est à ce moment là qu'elle se réveille ces derniers jours, lorsque la
migraine se fait trop forte pour rester silencieuse. Mais quelque chose
a du changer, car elle ne parviens pas à se réveiller. L'enchainement des
souvenirs est tels qu'il lui est impossible maintenant de pouvoir en
extraire du sens, ou même une sensation.

Formant un brouillard perceptif, différents du gris auquel elle était
confrontée tout à l'heure, l'espace sémantique autour d'elle forme
lentement des tâches de couleurs. Ou de quelque chose qu'elle perçoit
comme de la couleur. Elles se déplacent lentement, tirant des liens les
unes vers les autres, dans une danse chaotique et dépourvue de sens.

Tels un réseau de neurones, les liens entre les les tâches changent alors
que de nouvelles connexions sont établies, facilitant l'échange
d'information entre certaines expériences ou supprimant des liens non
utile.

Il faut un peu de temps à Saina pour réaliser qu'elle a en face d'elle un
système complexe d'intégration d'informations et qu'elle fait partie de ce
système. Au fur et à mesure que ses pensées se concentrent sur ce système,
elle le voit se modifier, s'adapter et intégrer ses pensées à elle en son
sein.

Alors qu'elle observe et analyse ce modèle, elle se familiarise avec les
expériences aliénées qui cherchent à s'intégrer à sa propre conscience. La
séparation entre le système autour d'elle et ses propres pensées
s'affaiblit et ses propres expériences altèrent les souvenirs implantés,
cherchant à reconstruire une cohérence.

Apparaissant au centre du système de pensée complexe, elle trouve un nœud
cognitif primaire, qui semble être un des fondements du système. Plus
étrange, il ne semble pas être le résultat d'une expérience personnelle,
mais plus une grammaire, une façon d'articuler des pensées. Un langage de
pensée.

L'ensemble forme un tout complexe et cohérent. Les concept mis en place
par cette algèbre cognitive décrivent un nouveau type d'automate, de
machine. Quelque chose de particulier, une machine capable de résoudre le
problème de l'arrêt tel que formulé par Alan Turing un siècle auparavant.

Cette machine cognitive à état est capable de conceptualiser
intentionnellement des mots, ou un assemblage de mots, et de déterminer si
cette suite de données est infinie ou non. Quelque chose d'impossible
à créer et que même les machines quantiques ne peuvent pas résoudre. Et
pourtant, là, quelque part dans son esprit, Saina contemple un automate
à état infini, ayant résolu le problème de la conceptualisation, de
l'abstraction.

Au sein de cette machine, réside le contenu de petits carnets noirs,
recouverts de symboles, de phrases dépourvues de sens ou d'équations
impossible. Ces petits carnets qu'Elliot a laissé derrière lui. Qu'elle
a laissé derrière elle corrige-t-elle machinalement.

Ces petits carnets forment le trou noir qui l'a infectée avec ces
expériences étrangères, elle en a la certitude. Ces carnets,
incompréhensible pour Tonya, décrivent mathématiquement la conscience.
Uncanny les a lus, analysés, numérisés, triturés est a, à priori réussi
à créer un automate théorique qu'ils nourrissent avec les bases
sensorielles de DreamLand.

Saina reste encore un temps qui lui paraît infini à explorer le système,
à l'intégrer, à lui trouver un sens ou une forme lorsqu'elle sent qu'on la
tire hors de son rêve.

« Ça va ? lui demande Enni, assise au bord du lit, à côté d'elle.

— Oui, je crois.

— Tu bougeait beaucoup. Et il va commencer à être temps de te réveiller
meuf, t'as dormi plus de vingt quatre heures et Tonya commence
à s'inquiéter.

— Ah merde. J'ai dormi si longtemps ?

— Yep. T'inquiètes pas pour Tonya, je lui ai dit que tu étais chez moi et
que tu dormais.

— D'accord. Merci. »

Saina, vétue d'un TShirt emprunté à Enni et maintenant trempé de sueur, se
lève doucement et s'assied sur le lit. Pas de migraine en vue, au moins
dormir si longtemps à aidé de ce point de vue.

Autour d'elle, la chambre d'Enni évoque une fausse nostalgie d'une époque
rose pop, lorsque les chanteuses pouvaient encore se permettre des clips
perchés et des textes politisés. La plupart d'entre elles ont finit leur
carrière lorsque les lois contre l'obscénité ont été votée dans la plupart
des pays conservateurs dans le monde. Certaines ont réussis à se recréer
une carrière, mais beaucoup n'ont pas eu d'autre choix que de se planquer
et créer une scène souterraine, diffusant leurs clips sur des blogs
hébergés hors des espaces numériques corporatiste.

Les posters et couvertures d'album sont encadrés dans des cadres
sophistiqués, reprenant les codes de l'art nouveau mais en y mêlant des
variations macabre. Le lit sur lequel elles sont assise est simple,
fonctionnel, mais particulièrement confortable. De la porte de la chambre
filtre le bruit de la salle de vie qui sert de cuisine et de foyer
à quelques résidentes. Une odeur de café chaud viens titiller les narines
de Saina.

« Je vais prendre une douche, dit-elle à Enni, tu me préparerais un café ?

— Yep. Prend ton temps. Et tu devrais trouver des serviettes propres dans
le placard blanc, en face de la douche.

— OK. Je peux te taxer des fringues aussi ?

— Si tu trouve des trucs qui te vont, hésites pas.

— Merci. » ajoute Saina, d'un ton un peu plus enjoué, avant de se diriger
vers la salle de bain.

Elle traverse la cuisine et, après avoir rapidement salué les personnes
qui s'y trouve, elle entre dans la salle de bain pour se prendre une
douche et évacuer cette sueur pleine d'adrénaline, typique des cauchemars.

La douche lui permet aussi de remettre quelques idées en place, à mesure
que les restes brumeux de son rêve s'estompent pour se transformer
lentement en idées et concepts, mais aussi en angoisses. Ses souvenirs
implantés sont-ils les siens, ont-ils été intégrés à son système de
conscience ? Et si oui, qu'est-ce que cela change pour elle, dans son
rapport au monde ? Et surtout, pourquoi a-t-elle en tête des théorèmes de
théorie de l'information qui n'ont jamais été formulés ailleurs ?

« Tout va bien ? lui demande Enni à travers la porte.

— Oui, oui ça va.

— Ça fait une heure que t'as là-dessous, on s'inquiète un peu.

— Une heure déjà ? Ok, ok, j'arrive. »

La buée sur le miroir dessine des fractales de Fibonacci auxquelles se
superpose du bruit, des glitchs perceptifs. Saina d'une main disperse la
buée du miroir, lui révélant le visage de quelqu'un qu'elle n'est pas
certaine de reconnaitre.

Le reflet déformé par l'eau que lui renvoie le miroir est celui d'une
personne qu'elle est sûre de connaître. Elle a l'intuition de l'avoir déjà
vu, d'avoir déjà vu ces tatouage, ce corps transformé en machine de
calcul. Elle sait que c'est censé être le sien, mais elle ne parvient pas
à s'en convaincre, elle ne reconnaît pas son image.

Des artefacts perturbent son analyse de l'image. Des formes géométriques
verts vifs apparaissent et disparaissent autour des formes qu'elle essaye
d'analyser, ressemblant aux erreurs de décompression sur certains des
vieux algos de compression. Des sons étranges, modulés depuis ce chant
qu'elle entend maintenant partout tournent en boucle dans sa tête. De la
musique qu'elle n'a jamais entendu auparavant mais qu'elle connaît
pourtant par cœur.

Elle se sèche rapidement avant d'enfiler les fringues qu'elle a emprunté
à Enni et de sortir, laissant un nuage de vapeur s'échapper de la salle de
bain au moment où elle ouvre la porte. Enni lui mets une tasse de café
dans la main avant de se faufiler dans le brouillard de la salle de bain
et de fermer la porte.

Le soleil descend vers l'horizon et la cuisine est maintenant presque
vide, la plupart des personnes étant parti se préparer pour leur soirée.
Saina s'installe sur la table au centre de la pièce. Dans un coin elle
remarque des petites cultures en terrasse. Des courges et autres plantes
rampantes s'étalent dans cette partie du lieu, profitant du compost pour
se développer au milieu des plantes plus décoratives s'élançant le long du
mur.

« T'es toujours là ? lui demande Enni qui sort de la salle de bain vétue
d'une simple chemise trop grande.

— Ben, j'ai pas encore fini mon café. Et j'allais pas partir sans dire
au-revoir, répond Saina alors qu'elle réalise que son café est froid.

— Tu devrais appeler Tonya tu sais, et tu m'inquiètes à phaser comme ça.

— À quoi ?

— Phaser. Mmmm, quand tu te perds dans tes pensées et que tu perds
conscience de ton environnement ?

— Ah, ça. Oui, non, rien de grave. J'ai juste beaucoup de trucs en tête,
et je suis pas nécessairement la personne la plus en phase avec son
environnement immédiat.

— D'accord. Mais appelle là quand même. Attends pas de finir ton café, ou
ça ne sera pas fait d'ici deux heures. À ce rythme, il va te falloir
encore deux heures pour rentrer chez toi. » dit Enni avant de se mettre
à farfouiller dans sa commode, à la recherche de fringues à mettre.

Saina > @Tonya: hey, t'es dans le coin ?

Tonya > En quelque sorte. Comment te sens-tu ?

Saina > Étrange. Mais les migraines se sont barrées.

Saina > Dis, je voulais m'excuser pour hier soir. Ou avant-hier soir, je
suis pas certaine de savoir. J'aurais pas du m´énerver comme ça.

Tonya > T'inquiètes, ça va. Ça arrive de se prendre la tête sur des trucs
passionnants. Mais c'est gentil de t'excuser pour ça :)

Saina > Des nouvelles de Nato ?

Tonya > Pas vraiment, elle est en vie et elle récupère. Mais rien de grave
apparemment.

Après un blanc qui semble durer une éternité, Saina reprend.

Saina > Je … je suis pas sûre de ce qu'il m'arrive ces jours ci, mais je
pense que c'est lié à ce que j'ai trouvé. Et ça m'a changé dans des
proportions que j'ignore encore.

Saina > Tout ça pour dire que j'aime bien quand tu n'es pas loin, que tu
t'inquiètes pour moi, que tu respecte que j'ai besoin d'espace perso.

Saina > Et je te dis ça parce que, en dépit de ces changements, ça ça n'as
pas changé. J'ai envie que tu ais une place dans ma vie.

Tonya > Quelle genre de place ?

Saina > Je ne sais pas. C'est pas obligé d'avoir un nom. Juste, des
personnes importantes l'une pour l'autre ça me paraît bien.

Tonya > D'accord.

Tonya > Hmm, ça peut sembler un peu froid dis comme ça. Tu m'as quelque
peu déshabiller avec ce que tu viens de dire.

Tonya > * déstabilisée

Tonya > (Un jour on aura des autocorrects fonctionnels).

Tonya > Mais ça me va comme idée, j'aime bien.

Saina > D'accord. On se voit plus tard ?

Tonya > Certainement. Chez toi ?

Saina > Ou chez toi, c'est pas comme si on habitait à dix mètres l'une de
chez l'autre. Mais je voulais passer à Uncanny Valley avant. Et prendre
des nouvelles de Nato aussi.

Tonya > J'ai pas grand chose de prévu de mon côté, à part du
jardinage. Tu me dis si tu veux qu'on se retrouve quelque part ?

Saina > D'accord, je te tiens au courant.

La fenêtre de conversation se replie dans un des coins de l'écran du link
de Saina. Quelques instants après, elle reçoit quelques photos de Tonya
qu'elle suppose être à poil au moment où elles ont été prise.

Enni, qui a finalement choisit un simple baggy brun et un sweat à capuche
mauve, termine de sangler un harnais dont les sangles violette en nylon
passent à travers des boucles jaunes.

« Alors ? demande-t-elle à Saina

— Alors quoi ?

— Alors tu l'as appelée, ou quelque chose du genre ?

— Oui, j'ai fait ça.

— Et comment tu vas ?

— Ça va. Je crois. Faut que je repasse chez moi, et que je parle avec Nato
et Tonya de ce qu'il se passe. Ça te gène pas si je garde tes fringues ?

— Du tout, ça te fera une occasion de repasser. Et c'est pas comme si j'en
manquait. On se voit plus tard du coup ?

— OK, on se voit plus tard. Merci pour le soutien.

— De rien. Et file maintenant, je crois que tu dois discuter de rucs qui
te font mal au crâne, et moi faut que j'aille à Bodies'r'Shell pour
continuer d'installer ces écailles. »

Et quelques minutes après, Saina et Enni se séparaient, chacune dans une
direction opposée, et laissant Saina seule avec elle-même, laissant ses
pas la guider alors qu'elle essaye de comprendre ce dont elle a rêvé cette
nuit, et les nuits précédentes, mais aussi pouquoim soudainement, ses
migraines ont disparues.
