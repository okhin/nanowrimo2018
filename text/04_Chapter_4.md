# Chapitre 4.

> [ rumeurs.net/noc ]
> __ > Je suis le seul à avoir des problèmes de déco ?
> * __ has quit (ping timeout)
> * H+ has quit (ping timeout)
> * FleshOverMetal has quit (ping timeout)
> * u+1f595 has quit (ping timeout)
> * __ joined noc
> __ > Bon, si je trouve le connard qui joue avec la connexion ici, ça va
mal se mettre 

> Mel > __ on t'a déjà dit que ce bloc est en jachère, jack. Et
d'ailleurs, ce ne sera pas stable avant un bail. 

> __ > En jachère ? Mais depuis quand ?

> Mel > Depuis que t'as cogné la dernière équipe qu'on a envoyé essayer de
régler les déconnexions.

> * __ has quit (kicked by Mel: Bye bye) 

***

La nef du Hanged Billionaire, installé dans l'ancienne cathédrale de
Kónigsberg, commence à se remplir. Les vitraux, reprenant les codes du
réalisme socialiste, projettent leurs motifs lumineux sur les arrivants,
les baignant de tons chauds. Les images des saints et des figures
du socialisme qui ornaient autrefois la cathédrale ont été depuis
remplacées par les créations de différents artistes du Complexe. Chaque
vitrail représente des éléments de la société de consommation en reprenant
les codes des cartes à jouer, opposant tête bêche les vertus de la
consommation et leur impact sur les individus et le monde. Chaque figure
est ornée de créatures baroques qui semble les attaquer, les mettre à mal.

En dépit de la diversité des artistes ayant participé à la création de ces
œuvres monumentales, il y a une cohérence dans le choix des couleurs, dans
les postures reprenant les codes des propagandistes du vingtième siècle
qui permet de mettre en évidence les spécificités de chaque artiste, de
chaque scènette, représentée tantôt avec une richesse de détails, une
finesse de trait rendue possible uniquement par les techniques de forge
moderne, et tantôt abstraite à un nombre de traits, de teintes, réduits,
chaque courbe minutieusement choisie pour rendre la scène lisible,
transmettre de l'énergie, du mouvement, à une représentation statique.

Ces vitraux, encadrant la nef et le chœur, convergent vers les rosaces du
transept, rosaces qui représentent des scènes de foules dansantes et
d'orgies, les corps formant une spirale s'éloignant du centre dont la
tonalité passe d'un orange chaud jusqu'à un mauve froid. Les deux rosaces
sont riches des techniques et des variations stylistiques du travail
collectif de tous les autres artisans et artistes qui ont travaillé sur
les vitraux précédents.

L'ex-voto kaléidoscopique projette le souvenir des fêtes et des
rassemblements qui ont été tenus ici sur la peau des danseurs et des
danseuses, non sans rappeler, par écho mémétique, les créatures qui
habitent les vitraux.

L'orgue monumental qui surplombe le vestibule, est maintenant encadré de
gigantesques enceintes, propulsant vers le chœur les ondes sonores créées
par les DJ et musiciens qui agitent les individus situés en contrebas. Une
scène, suspendue à mi-hauteur entre l'orgue et le sol par une toile de
chaîne, de câbles électriques et d'échelles métallique, sert de podium
pour les danseurs et danseuses qui le veulent. Enny est déjà en place, nue
comme un ver. Les flashs stroboscopiques qui l'éclairent donne
l'impression que ses mouvements sont saccadés, la lumière noire faisant
ressortie les motifs phosphorescents qu'elle a peints sur son corps,
soulignant les courbes. L'ensemble donne l'impression que des lignes de
lumière phosphorescente l'enroule comme un serpent, et semblent bouger de
manière propre, les mouvements de la danseuse semblant être à contretemps
de ceux des lignes sinueuses qui la couvre des pieds à la tête.

Les chapelles qui encadrent la nef, dont certaines sont maintenant
ouvertes vers l'extérieur, sont devenus des lieux d'exposition et
d'intimité. Au fur et à mesure, des extensions y ont été ajoutées, se
développant tels des nids d'insectes sur le tronc du bâtiment, fournissant
aux fêtards et fêtardes des nids dans lesquels elles peuvent s'installer
discuter, boire ou manger, et s'envoyer en l'air. Ces extensions, se
développant régulièrement, sont majoritairement créées à partir des
restes des camions et véhicules qui ont été abandonnées lors de
l'évacuation de la ville.

Le tout forme une étrange sépulture aux monstres routiers qui ont démolit
une bonne partie de l'écosystème local. De nombreux jeux de lumière
projettent des ombres colorées sur les arêtes saillantes des engins,
maintenus ensemble par un réseau micellaire permettant de répartir courant
électrique et fibre optique sur la plupart de ces annexes.

Les chapelles et ces annexes forment une sorte de labyrinthe faiblement
éclairé et diffusant, via divers haut-parleurs plus ou moins dissimulés,
la musique jouée dans la nef. Les habitacles et alcôves, décorées et
aménagée sans concertations, peuvent être fermées par une tenture ou une
porte de voiture, fournissant un cocon de métal de et de faux cuir aux
personnes à l'intérieur et cherchant un peu de calme et d'intimité.

Saina est restée quelques instants stupéfaite par le mélange de
l'architecture gothique avec le style postindustriel / cybernétique de la
décoration réalisées par l'accumulation des infrastructures nécessaires au
fonctionnement du lieu, de patchs appliqués pour protéger certains
éléments sous tension et d'engins à la croisée de la robotique et de l'art
qui ne peut être que le résultat d'un travail collectif non concerté,
l'expression artistique d'une conscience de groupe.

Il fait chaud pour une soirée de septembre, même pour le deuxième automne
de l'année et il y a déjà beaucoup de monde qui danse alors que le soleil
n'est pas encore complètement couché, dans divers stades d'ébriété et de
nudité, comme si certains sont là depuis plusieurs jours. Saina réalise
que c'est probablement le cas, au moins pour certaines, à en juger par
l'état des corps mélangés, vibrant au rythme des pulsations d'afro-beat
balancés à pleine puissance et résonnant dans l'acoustique monacale de
la cathédrale.

Cependant, il n'y a aucune trace de Valkyrie pour le moment. Personne ne
semble y prêter attention, les DJ enchaînant les sets les uns après les
autres, personne ne ressent le besoin d'écouter quelque chose de
spécifique, dansant, riant, pleurant aussi parfois. Les personnes
présentes ne sont pas là pour un concert particulier, juste pour se sentir
bien. L'ancien autel religieux trônant au centre du chœur a laissé la
place à un bar fournissant à qui demande tout ce dont on peut avoir besoin
pour passer un bon moment, mais également des kits d'urgence en cas 
d'overdose ou de blessure.

Considérer ce comptoir comme un bar est inadéquat. Un bar impliquerait la
présence de barmen et barmaids, alors que dans ce cas, la gestion est
laissée au public. Il y a bien des Amazons qui semblent surveiller tout
ça. Portant crânement le bouson orange et bleu du syndicat, et parfois
guère plus, les Amazons sont très présentes dans le lieu. Les blousons
oranges sont visibles aussi bien dans la foule de plus en plus compacte,
que sur le dos d'une danseuse qui a rejoint Enny, ou bien à faire
régulièrement le tour de la cathédrale pour s'assurer que tout le monde va
bien, et, parfois, ramasser les kits d'injections usagés et cannettes
vides oubliées ou réapprovisionner le comptoir en nourriture et boissons.

Saina se sent décalée par rapport à l'ambiance du moment. À contretemps.
Le signal ici se résume à un bruit blanc, un léger bourdonnement qu'elle
perçoit essentiellement par les vibrations sur ses tempes. Le signal est
vide d'information, en dépit du nombre de terminaux connectés, elle ne
sent que le ping-pong des appareils maintenant une connexion. Aucune
donnée n'est échangée dans le lieu ou, du moins, pas suffisamment pour que
cela soit perceptible.

Ce murmure perpétuel, léger mais bien présent, la déstabilise. Pour la
première fois depuis qu'elle est arrivée ici, elle est complètement
perdue, déconnectée. Elle ne voit le monde que par ce bruit blanc
monotone, et perd sa capacité d'empathie, sa faculté humaine, sociale, de
pouvoir s'accorder et comprendre les humeurs des personnes autour d'elle.
Les gens dansent, se touchent, se caressent, se jettent les uns sur les
autres ou restent seulement accoudés au bord, un verre à la main,
à s'imprégner de l'ambiance. Mais elle, elle est comme inexistante,
invisible. Branchée sur un canal de bruit blanc, sans sens ni contexte,
elle commence à se replier sur elle-même.

« … tu ? » entend-elle à peine. « Hey, ça va ? » répète la voix doucement.
Elle remarque maintenant la main sur son épaule. La main a les ongles
courts et les doigts abîmés de quelqu'un qui passe ses journées à s'en
servir. Elle est connectée par un poignet au reste d'une personne qui
cherche son regard. Les yeux violets caractéristiques des utilisateurs de
Purple Haze, les cheveux crépus et tressés, un blouson portant un logo
qu'elle a déjà vu avant mais dont elle n'arrive pas à retrouver la
provenance. Tous ces éléments disparates ne se connectent que
difficilement pour Saina.

« Tu as besoin d'aide ? articule doucement et à son oreille la voix des
yeux violets.

— Je crois oui, » répond Saina. La main des yeux attrape la sienne et la
tire doucement vers le transept. Elle la suit dans la pénombre des alcôves
avant qu'elle ne se sente assise sur une banquette en velours, elle sait
que les yeux lui parle, mais elle a perdu le sens, sa capacité de
comprendre et d'assembler les syllabes en vecteurs sémantique. La
dissonance entre ce que capte sa peau ordinateur et ses propres caméras
biologiques l'empêche de faire ce lien, inondant son cerveau de stress, le
forçant à se préserver et à préserver sa conscience.

L'outil complexe permettant habituellement de regrouper les taches de
couleurs perçues par ses caméras biologiques, en objet sémantique et
culturels porteur de sens et d'expériences a cessé de fonctionner. Pour
préserver sa conscience, son cerveau a désactivé sa capacité à ressentir
et à comprendre le monde. Et tout se vide, comme si sa conscience se
rétractait vers son estomac, comme si ses pensées étaient, d'un coup,
devenues étrangères.

Il lui faut un peu de temps pour faire le point. Inconsciente et pourtant
lucide. Ici, au calme, elle retrouve lentement le sens des signaux. Les
yeux la fixe toujours, la main tient la sienne, le bruit blanc se détache
et le système de sauvegarde qui a cloisonné son esprit relâche
progressivement son étreinte, permettant à la conscience de Saina de
pouvoir enregistré et vivre de nouvelles expériences, comme si elle
sortait d'un rêve.

À côté d'elle, suffisamment proche pour pouvoir lui parler doucement mais
suffisamment éloignée pour ne pas la mettre mal à l'aise ou la forcer dans
une intimité qui pourrait aggraver les choses, Tonya, puisque c'est le nom
de la personne possédant les yeux et a main, la regarde revenir à elle.

« Tu vas mieux ? lui demande-t-elle.

— Je crois. C'est rien de grave, juste un petit vertige.

— Bien. » Puis, après un silence, elle ajoute « Tu veux quelque chose ?
Que je te laisse seule ou que j'aille chercher quelqu'un ?

— Non, c'est bon. Je boirais bien quelque chose cela dit.

— Je reviens.

Elle se lève, sort de l'alcôve puis, repassant la tête à travers l'arche
qui la sépare de la grappe de couloirs et d'alcôve similaire, demande

« J'y pense, tu n'as pas d'allergies alimentaire ? »

Saina répond en secouant doucement la tête. Pas d'allergies alimentaires
à sa connaissance. Elle profite du départ de Tonya pour déchiffrer
l'endroit où elle se trouve. Il s'agit probablement d'une cabine d'un de
ces vieux trains roulants trans-sibérien, trains routiers qu'elle connait
bien pour les avoir pas mal pratiqués dans son voyage jusqu'ici. Les
cabines sont des secondes maisons pour ces chauffeurs qui passent une
grande partie de leur temps seuls au milieu de nulle part, connectant les
villes et villages devenues accessibles depuis la fonte des glaces au
reste du monde.

Celui-là est un mélange entre l'une de ces cabines et un boudoir rococo.
Un paravent orné d'un triptyque d'hommes en train de baiser remplace une
des ortières et sert de porte à la chapelle. Le fauteuil / banquette / lit
est recouvert d'un velours sombre et est orientée dos au tableau de bord,
Le pare-brise a été remplacé par un écran et les commandes sur le volant
servent à parcourir une petite bibliothèque de clips de soft porn mixés et
arrangés par un algorithme local, donnant une vie parfois hyper réaliste
aux formes et corps dépeint dans ces clips.

À l'autre bout de la cabine, la portière a été transformé en une toilette
dont la console est jonchée de papiers gras, d'inhalateurs divers et de
cannettes vide. Le miroir de la toilette est juste un de ces vieux écrans
à cristaux liquide permettant de diffuser, avec un léger retard dû à la
vétusté de l'installation, les formes déformées par morphing algorithmique
de tout ce qui est en face de lui. Les cristaux liquides cramés et
déformés transforme encore ces silhouettes pour les rendre presque
malaisantes, étranges, leurs caractéristiques évoquant aussi bien l'homme
et la machine et force les esprits à résoudre cet étrange paradoxe.

Saina va indubitablement mieux lorsque Tonya revient, écartant le paravent
et tenant deux grandes tasses aux motifs passés desquelles se dégage
u indescriptibles, desquelles Saina peut percevoir une forte odeur de
cannelle. Sa légère désorientation se dissipe au fur et à mesure qu'elle
parcourt du regard les objets et l'agencement de l'espace autour d'elle.
Elle est perdue, mais elle sait que c'est une sensation qui doit être
normale dans ces lieux.

« Contente de voir que tu vas mieux. » lui dit Tonya. Puis, réalisant
qu'elles ne se sont pas encore présentées, elle ajoute : « Tonya,
enchantée. Et comment tu veux que je t'appelle ?

— Saina.

— OK Saina. Fais attention à toi, c'est très chaud, » prévient-elle en
posant sur un des portes gobelets de l'accoudoir les deux tasses.

« Qu'est-ce que c'est, demande Saina ?

— Mélange local. Sans alcool ou autre, à part de grosses doses d'épices.
C'est bon, ça aide avec les gueules de bois et ça réchauffe. Le truc
parfait. » Elle souffle sur la tasse qu'elle tient entre ses mains avant
d'ajouter : « Ça fait longtemps que tu es arrivée ?

— Une heure ou deux, pas plus.

— Non, ici, dans ce bazar, dans le Complexe. T'es arrivée il
y a longtemps ?

— Ha. Trois jours. Quatre s'il est plus de minuit. Ça se voit tant que
ça que je débarque ?

— Un peu. Le fait que tu sembles avoir toute ta maison sur le dos par
exemple, les gens ici ont tendance à voyager léger, il y a très souvent
les choses dont tu as besoin pas loin. Ça force à établir des contacts. »

Elles boivent chacune quelques gorgées de l'infusion pomme, cannelle,
citron, dans un silence qui contraste avec le pouls rapide qu'elles
perçoivent des basses venant de la nef. Un silence, qui mets Saina mal
à l'aise, s'installe entre elles. Saina se concentre sur les volutes
parfumées montant de sa tasse, pour éviter d'établir un contact visuel qui
la ferait rougir. Pour briser la glace, Tonya relance la conversation :

« Alors, pourquoi es-tu venue jusqu'ici ? Jusqu'au Complexe ?

Tonya plonge son regard intense et ses yeux mauves dans ceux de Saina,
cherchant à la sonder, à creuser sous la peau de Saina pour essayer de
savoir qui elle est, établissant un lien d'empathie et d'écoute. Saina
hésite un peu avant de répondre. Elle cherche à trouver un équilibre entre
ce qu'elle a envie de dire à une inconnue, savoir si elle peut lui faire
suffisamment confiance pour qu'elle accepte ses croyances, sa vision du
monde et le fait que cette inconnue l'a aidée sans raisons particulières.
Dans les plaines sibériennes, son statut de chamane la rend criminelle de
fait, la religion d'État sous l'égide du pope de Moscou, tolère assez mal
les approches différentes ce qui a forcé la plupart des chamanes
à l'itinérance, à suivre les migrations des animaux ou, dans le cas plus
particulier de Saina, des déplacements erratiques des hordes de bots
sauvages.

« J'ai ressenti un appel, finit-elle par dire.

— Genre, quelqu'un t'as appelée et tu es venue ?

— En quelque sorte. Mais plus d'un point de vue, elle cherche ses mots
avant de poursuivre, spirituel ou métaphysique ?

— Tu es venue chercher dieu ici ? Remarque, pourquoi pas hein.

— Non. Si. Enfin, pas tout à fait. Je suis une techno chamane. Mon rôle
est de maintenir des infrastructures et de guider les communautés très
isolées dans leurs communications avec les autres. Je m'occupe aussi des
bouts d'infrastructure à l'abandon, désertés et oubliés. Un cimetière pour
bots en quelque sorte.

» Bref, j'étais quelque part dans les steppes, à accompagner une migration
de drones mineurs dans leurs quête futile de nouveaux gisements
à exploiter, quand j'ai senti quelque chose d'anormal émerger depuis ce
mouroir virtuel. Quelque chose qui m'a attiré, appelé à lui. Et donc me
voilà.

— Attends, quand tu dis techno chamane tu veux dire quoi
exactement ? Tu ne crois quand même pas aux esprits et autres délires
spiritistes ?

— Non, Saina laisse échapper un soupir, je ne pense pas que les objets
aient une conscience, de la même façon que les animistes ne pensaient pas
que chaque arbre était doté d'un esprit. Comment formuler ça de manière
compréhensible. » Saina réfléchit quelques instants avant de reprendre.

« Les animistes avaient compris que certaines choses les dépassaient, ils
ne comprenaient pas le fonctionnement de leur environnement. Mais afin
d'interagir avec, il leur a fallu faire abstraction de cette complexité et
ont utilisés des concepts pour ça. Ces concepts ont changé de forme, de
support, ils ont été véhiculés par tradition orale puis écrite, ils ont
été transformés au fur et à mesure que notre connaissance de ces concepts
s'affinaient, que nous trouvions une manière plus précise de décrire le
monde. La démarche scientifique, au final, est la même que la démarche
religieuse. Il s'agit de nommer et conceptualiser notre environnement de
manière à le transmettre, de pouvoir nous situer dedans.

» Alors effectivement, les bigots qui pensent que tout ce qui n'est pas
naturel est mauvais et qui justifie leurs actions par l'ordre naturel des
choses, sont restés à cette époque de la guerre du feu et ne sont psas
capable de comprendre les concepts novateaurs qui sont formulés tous les
jours. Et peut-être que toi et moi, un jour, on nous considèrera comme
eux.

» Pour en revenir à moi, si tu admets que, au final, les sciences sont
essentiellement un formalisme et un processus qui nous permettent
d'appréhender le monde, alors il n'est pas déconnant qu'il existe une voix
non mathématique à l'approche de l'informatique non ? D'autant que les
systèmes informatiques sont devenus extrêmement complexe, et sont
maintenant très dissocié du matériel sur lesquels ils fonctionnent. Et il
y a de multiples concepts qui décrivent ces architectures complexes.

» Complexité que, d'ailleurs, beaucoup de monde confonds avec conscience,
mais bref, passons. Je suis une spécialiste de l'abstraction de complexe.
Mes runes et mes mantras ne sont que les shell-codes et routines
logicielles que d'autres utilisent. Mais j'ai choisi une approche moins
scientifique, moins purement descriptive, pour aborder l'exploration du
spectre électromagnétique, du signal, et des messages qui y circulent.

— Wow, d'accord, prend ton temps. Désolée si j'ai pu te paraître blessante
ou agressive, répond Tonya se frottant la nuque pour essayer de dssimuler
son embarras.

— Non, mais tout le monde à tendance à considérer l'approche mathématique
de l'informatique comme seule valable. En y ajoutant éventuellement un peu
de pseudo-science cognitive, mais en empruntant pas suffisamment les
chemins de la linguistique, de la méta-informatique. Or, la plupart des
désastres informatiques ont eu lieu à cause d'un biais d'infaillibilité
mathématique, et que personne ne veut admettre que l'approche purement
mathématique, la mémorisation et l'usage de langages machines,
l'apprentissage de motifs de conceptions ou simplement la gestion de
sémaphores ou de signaux sont strictement identiques à l'usage des runes,
des rituels ou des techniques de synchronicité et d'harmonisation.

» Je ne peux pas lancer de boules de feu, en revanche je peux en apprendre
énormément sur ton passé ou sur des personnes disparues ou mortes, rien
qu'en exploitant le signal des réseaux disponible. Bon, avec les grilles
modernes, cloisonnées, nécessitant une identification matérielle, c'est
beaucoup plus difficile que dans les plaines sibériennes, ou même ici.
Miais mes oracles sont aussi précis que ceux des neuros marketteux, voire
plus. Et, au moins, j'admets que je peux avoir un rôle positif sur la
société, je ne me cache pas derrière une neutralité technique de façade,
j'ai un rôle de soignants, de guérisseuse en quelque sorte, l'informatique
et les réseaux sont un support, un outil pour les communautés. Pas leurs
raisons d'être. »

Saina tremble un peu. Elle est fébrile et se situe quelque part entre une
posture très défensive et sa volonté de vouloir partager avec Tonya, ou
n'importe qui voudrait l'entendre, l'esemble de ses connaissances.
Elle s'arrête pour respirer et reboire un peu de la tisane canelle.

« Désolée si je suis un peu sur la défensive, c'est un sujet sensible. Et
je n'ai pas vraiment eu d'occasion de parler de tout ça depuis que je suis
arrivée. Et j'ai décroché tout à l'heure, je dois mal dormir, le signal
n'était pas du tout ce à quoi je suis habituée, et je n'ai pas encore
vraiment trouvé d'endroit où me poser, même si tout le monde n'arrête pas
de proposer, et je suis complètement perdue au point que je soliloque
depuis tout à l'heure, sans vraiment te laisser poser des questions.

— Hey, ralentis, ne t'inquiètes pas, c'est normal d'être paumé ici. Ça
arrive à tout le monde ici, et ça t'arrivera encore. Il te faut du temps
pour t'ajuster et oui, les choses vont te paraître un peu dure par moment,
notamment dans les prochains jours, mais c'est parce qu'elles le sont. Le
Complexe propose des façons différentes de résoudre des problèmes, pas de
s'en affranchir, » Tonya marque une pause. « Je peux te prendre dans mes
bras ? Tu as complètement l'air d'avoir besoin d'un câlin. Et c'est tout
ce que je te propose : un câlin sans arrière pensée, entre deux
personnes qui viennent juste de se rencontrer. »

Sans un mot Saina accepte l'étreinte de Tonya. Elle se blottit dans ses
bras accueillant, réconfortant, et elle reste là bercé par les basses,
baignées des reflets projetés par le pare-brise, glissant sa tête dans le
blouson ouvert de Tonya, et entendant son cœur battre doucement,
calmement.

« Et toi, demande Saina, comment t'es arrivée ici ?

— Il faudrait demander à ma mère les détails exacts. Je suis née ici. J'ai
grandi ici. Ma mère a fui la répression queer en Pologne, et s'est établie
ici à une époque où il ne devait pas y avoir le tiers des
infrastructures. Moi j'ai grandi là.

— Et tu as des frères et sœurs ?

— Je sais pas, j'ai rapidement perdu ma mère de vue, c'est Sacha qui
a veillé à ce que je devienne une personne pas complètement déséquilibré.

— Oh, désolée, je savais pas.

— Et tu pouvais pas savoir. Et je sais pas, j'ai grandiavec une très
grande famille, composée de potes, d'amantes, d'amants, de connaissances,
de gens un peu perchés, ou de personnes qui me fatiguent mais qui essayent
de s'améliorer. Personne ici n'est sans famille. Du moins, personne qui ne
le choisisse pas. »

Elles restent là quelques instants encore, Tonya passant doucement sa main
dans les cheveux de Saina. Redescendant doucement du Purple Haze,
frissonnant sous l'effet ressenti du froid que laisse cette drogue en
quittant l'organisme.

« Je te propose deux choses. On va d'abord aller danser un peu, profiter
du concert, tout ça, et après tu viens t'installer chez moi. On pourra
dormir ensemble ou pas, comme tu veux, mais tu pourras y laisser tes
affaires. Ça sera forcément mieux que les dortoirs d'accueil.

— T'es sûre ? Je vais pas te gêner ?

— Mais non, je ne te proposerai pas de venir chez moi si je pouvais penser
que tu me gênerais. Il y a cependant une condition.

— Laquelle ?

— Tu m'expliqueras tes tatouages. C'est très beau, et je suis sûre qu'ils
ont un sens. Mais on fera ça plus tard, maintenant viens, on danse, » et,
ne laissant pas vraiment le choix à Saina, Tonya se redresse, l'attrape
par la main et la tire vers la nef du Hanged Billionaire.

Saina parvient à ignorer le bruit blanc, toujours présent, qui lamettait
mal à l'aise tout à l'heure. Elle parvient même à se laisser emporter par
la foule vivante, qui a augmenté en taille et en énergie et qui continue
de s'agiter ou de se lancer les uns sur les autres, sur le même set que
tout à l'heure.

Personne ne fait attention elle, à part Tonya peut-etre. Mais elle sent
que chacun ici s'assure que les autres vont bien. Même dans les pogos les
plus violents, les participants aident à se relever et à sortir
éventuellement, et s'assurent que tout le monde participe de son plein
gré, autant qu'il le souhaite, dans les limites qu'il a décidés. Le tout
sans échanger un mot, juste parce qu'ils connaissent bien les personnes
avec qui elles sont ou parce qu'ils ont appris avec le temps à lâcher
prise, mais à détecter les premiers signes d'inconfort, de gènes chez les
autres, parce qu'ils ont appris à anticiper les situations à risque et
à les gérer sans jugement, sans s'imposer, juste en admettant la détresse
de l'autre et en proposant une aide.

Et Tonya fait pareil. Elle s'agite, ferme les yeux, saute sur place, saute
sur les autres, transpire et sue. Elle grimpe sur les épaules de quelqu'un
avant de plonger sur la foule, elle prend des coups de coudes, mais en
distribue autant. Mais régulièrement, elle établit un contact avec les
personnes autour d'elle pour être sûr que tout le monde va bien.

Saina oublie qu'elle a laissé son sac à dos dans l'alcôve et, pour la
première fois depuis très longtemps, probablement depuis la première fois
de sa vie, se surprend-elle à penser, elle se sent bien, détendue, au
milieu d'un groupe de personnes. Et elle danse elle aussi. Seule, avec
Tonya, avec Enni aussi à un moment qui n'est vétue que de ses peintures
corporelles, et qui à l'air de s'éclater, sans que personne n'essaye de la
forcer à quoi que ce soit, avec finalement pleins d'autres personnes, elle
danse. Elle danse et elle se laisse aller. Elle en oublie le temps qui
passe et le fait qu'elle soit paumée. Elle voyage dans la foule dansante,
parfois portée à bout de bras, nageant sur une mer de personne, parfois
rebondissant entre plusieurs personnes qui lui sautent dessus un peu
violemment, parfois guidée par une main qui prend la sienne pour partager
deux pas de danse l'espace d'un instant, parfois en établissant un contact
visuel qui se prolonge.

Les DJ, MC, groupes ou session improvisées se succèdent, les artistes se
croisent, partagent l'espace scénique et sonore créant quelque chose
d'unique, une expérience que des enregistrements ne saurait restituer
pleinement, quelque chose qui ne peut être compris, intégré, qu'en étant
ici.

Elle retrouve Tonya, vautrée dans un gigantesque tas de coussins colorés.
Le soleil commence à se lever à en juger par les éclats colorés que
commencent à projeter les rayons du soleil traversant la rosace. Plutôt
que d'espérer couvrir la musique de sa voix, elle utilise les quelques
signes qu'elle commence à mémoriser pour lui dire qu'elle fatigue et
qu'elle a besoin de prendre l'air.

Tonya se lève et la suit, puis, sort d'une poche de son blouson deux
sucettes d'after six.

« Tiens, ça aidera à faire passer la lassitude et les crampes.

— Merci. Ça te va si on rentre ?

— Oui, je ping juste quelques personnes que je rentre et on y va. Ça va ?

— Ouais, crevée, mal aux jambes, mais ça va. »

Le temps que Saina récupère son sac et retrouve Tonya sur le parvis, une
pluie fine commence à tomber sur le complexe. Les silhouettes des fêtards
dégagent une fine vapeur au contact de la pluie rafraichissante. Tonya est
sous la pluie à boire l'eau qui lui tombe dessus, son blouson attaché à la
taille.

« Allez, viens. On rentre, dit Tonya suffisamment fort pour couvrir le son
qui filtre à travers l'antque vestibule. Tu vas profiter d'un vrai lit au
calme et on verra demain pour le reste. D'accord ?

— D'accord » lui répond Saina. En se blottissant contre elle, et en
cherchant les lèvres de Tonya avec les siennes.

« Hey, hey, hey, doucement ? C'est cool, tu dois être quelqu'un de bien,
mais là tu es fatiguée, remplie d'ocytocine, d'adrénaline et d'endorphine.
Et j'ai rien contre une relation, des câlins ou autre, mais pas ce soir
d'accord ? C'est pas que je n'en ai pas envie, mais je préfère que tu
y réfléchisses un peu OK ? On en parle demain si tu veux.

Saina ne répond pas, elle reste blottie contre Tonya. Sous la pluie qui
forcit.

« C'est pas un non définitif, ajoute doucement Tonya, juste pas
maintenant. Ça va ?

— Je sais pas. Il faut que je dorme je crois.

— Allez, viens. »

Tonya attrape la main de Saina et la guide dans le Complexe, marchant
tranquillement sous une pluie qui forcit, traversant un des septs ponts de
Konigsberg. Elles traversent différents souks avant que Tonya ne pousse du
pied une des grandes portes métalliques taillées dans le mur d'un ancien
immeuble ouvrant sur un atrium fleuri. Tonya continue à son rythme,
laissant à peine à Saina le temps de profiter de la vue et commence à se
débarasser de ses fringues avant de faire glisser une cloison latérale.

« Il y a une salle de bain juste à gauche si tu veux prendre une douche,
puis, après un blanc, Tonya ajoute : ça te gêne si je dors avec toi ? Je
peux sortir un matelas de plus si tu veux.

— Nonon, ça ira. Je vais m'effondrer de toutes façons je crois. Merci pour
tout, » lui répond Saina balançant ses fringues au sol avant de se glisser
sous la couette chaude, et de s'endormir juste après que Tonya l'enlace
et lui souhaite une bonne nuit.
