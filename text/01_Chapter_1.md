# Chapitre 1

> [ Plurality/wiki/welcome ]
>
> [ author: Médée (member since: 2053) ]
>
> [ version: 2.0.15 - last edit yesterday by <WelcHomeBot> ]
>
> Bienvenue au Complexe.
> Je profite de ton arrivée ici pour te donner quelques liens qui
> devraient te permettre de mieux profiter de ton séjour ici.
> Mais d'abord, quelques un rapide point sur les non-négociables. C'est un
> ensemble de règles sur lesquelles l'ensemble des participants au
> Complexe se sont mis d'accord. De fait il en existe peu.
> 
> 1. Tu ne peux parler qu'en ton nom. Ni au nom de quelqu'un d'autre, ni
>    au nom d'un groupe.
> 2. Tu ne peux empêcher ni la modification, ni la duplication de quoi que
>    ce soit qui est fait au Complexe.
> 3. Tu dois participer au Complexe d'une manière ou d'une autre. Le plus
>    simple est d'intégrer un des groupes. Tu les trouveras dans l'annuaire.
>
> Une dernière chose, ce n'est pas un non négociable, mais plus un conseil.
> Ce qui se dit dans la rumeur n'est pas archivé, et ne doit pas en sortir.
> Il y a des raisons pour lesquelles les messages là-bas ne sont pas
> conservés.
> Bon séjour ici.
>
> [comments]
>
> Jack_56y > Hey, si tu lis ça, hésite pas à passer me voir pour avoir un
> kit de démarrage clef en main.
>
> S0nya > T'es encore là toi ? Fais pas attention à ce que dit Jack_56y,
> c'est une arnaque, et lui c'est un spammeur. Un insistant en plus.
>
> __Arthus > Ce n'est pas une arnaque, passes me voir et t'auras un package
> te permettant de survivre.
>
> S0nya > On avait dit pas de spam sur cette page les gens. Je verrouille
>
> [comments locked]

***

Le Complexe s'étend à ses pieds. Émergeant des eaux saturées d'algues
vertes, la ville s'est développée autour de ses tours de plast-béton
y ajoutant passerelles, annexes constituées de conteneurs de stockages ou
arches audacieuses connectant les étages et jardins en terrasse.

L'ensemble est loin d'être harmonieux, mais il apporte une énergie, une
vie, aux symboles postindustriels d'une économie en ruine, inondée par les
restes de la calotte polaire.

Appuyée sur la balustrade en fer forgé d'un balcon au troisième étage d'un
hôtel laissé à l'abandon, Saina contemple les aurores boréales dansant au-
dessus du complexe. La tempête solaire ajoute une aura éthérée à la
résonance informationnelle à laquelle ses tatouages électrosensibles la
connecte.

Elle perçoit un bourdonnement constant alors que sa peau est parcourue de
micros courants électrique, transformant les données transitant dans le
voisinage en sensation tactiles. De petites diodes électroluminescente
glissées sous sa peau brillent doucement alors que les informations se
mélangent à la plainte lancinante des aurores boréales.

« C'est comme le chant des baleines » lui a dit Ylsen lors de son
initiation dans les plaines Sibériennes. Et c'est vrai que les ondulations
lentes des aurores boréales lui font penser un peu à ce qu'il reste du
chant des baleines : des enregistrements numériques, les derniers cétacés
ont fini par disparaître bien avant la naissance de Saina.

En plus de ses tatouages, elle a reçu, au fur et à mesure de son parcours
initiatique, diverses marques. Elle a mémorisé les formules permettant
de calmer les bots agriculteurs devenus erratiques. Elle a appris
à lire les signaux qui traversent son corps pour en extraire le sens. Elle
a également appris à calmer ses migraines lorsqu'un signal dissonant était
présent, pour en trouver la source et essayer de s'y synchroniser. Elle
a appris la discipline de la synchronisation pour entrer en transe et
échanger avec les esprits contenus dans les machines. Elle a appris tout
cela mais elle sait que ce n'est, au final, qu'une abstraction lui
permettant d'appréhender le monde hyper informationnel.

Avec Ylsen, puis seule depuis sa disparition, elle s'occupe des bots et
agents abandonnés, laissés là à perpétuellement faire ce pour quoi ils ont
étés créés, ne prêtant aucune attention au monde qui les entoure. Juste
suffisamment inintéressant pour que personne ne s'en soucie, mais pas
suffisamment consommateurs de ressources pour que quelqu'un daigne les
arrêter.

Sa tâche est de s'assurer qu'ils restent là, qu'ils n'infectent pas
d'autres agents avec de vieilles souches virales qui aurait pénétré les
défenses de ces systèmes non mis à jour depuis des années. Une tâche
ingrate, mais qui lui permet d'analyser les couches basses des systèmes
mémétique définissant le monde et ses cultures. Elle a développé une
certaine sympathie à l'égard de ces abandonbots, elle les connaît, les
observe, les protège.

Et, il y a six mois, quelque chose a changé. Un changement difficilement
perceptible dans le bruit de fond de ce mouroir pour immortels, mais un
changement quand même. Une douce pulsation, apaisante, qui s'immisçe
lentement dans ses fonctions cognitives, comme si elle cherche activement
à établir le dialogue.

Les abandonbots n'initient pas de communication. Ils exécutent,
imperturbables, et peu importe leur état, la tâche pour laquelle ils ont
étés conçus, sacrifiant leurs faibles ressources pour continuer d'accéder
aux données dont ils ont besoin pour fonctionner à mesure que celles-ci
sont enfermées dans des jardins murés, les privant d'un accès direct et ne
pouvant être lues que par des couches d'abstraction ou d'autres bots. Une
certaine intelligence, mais qui tiens plus de la détermination
obsessionnelle d'une machine que de la complexité de la conscience.

Elle a mis du temps à comprendre ce dont il s'agissait. Les transes
induites par stimulation électromagnétique ne lui ont pas permis de
comprendre, le signal est trop lointain, distant et atténué par la
distance. Elle a cependant localisé l'origine de cet appel et, après avoir
traversé une bonne partie de la Fédération et du Pacte Baltique, elle est
finalement arrivée en bordure du Complexe.

Fondée sur les ruines de l'enclave russe, abandonnée suite aux désastres
mémétiques qui ont poussé ses habitants à fuir vers la Pologne ou le Pacte
Baltique, la ville inondée a été récupérée par une population éclectique.
Fuyant des persécutions, préférant vivre en marge d'une société qui les
rejette, une faune de queers, d'apatrides, de personnes fuyant la
désertification de la Méditerranée ou d'artistes non conventionnels, les
habitants ont fini par rebaptiser le lieu le Complexe, expérimentant des
formes de gouvernance et d'auto suffisance.

D'autres collectifs sont venus s'y installer au fil des ansi. Deux
plateformes pétrolière abandonnées en Mer du Nord ont été patiemment
ramenées dans la baie, fournissant des plateformes idéales pour les
projets les plus ambitieux de certaines, incluant un programme spatial.
Mais ce qui intrigue le plus Saina, une des raisons qui l'ont pousséeà
venir jusqu'ici, c'est l'infrastructure de communication.

Les échanges culturels au sein du Complexe se font par le partage de
nombreuses sources d'informations. Qu'il s'agisse des réseaux sociaux
endémiques, des négociations pour obtenir un accès limité aux principales
grilles privées ou de systèmes ad-hoc permettant de faire circuler
rapidement des informations, tout en s'assurant qu'elles ne restent pas
disponibles passé la nécessité de cette information, toutes ces sources,
et de nombreuses autres expérimentations, génèrent un bourdonnement
incessant, font vibrer l'épiderme de Saina, caresse sa peau, font luire
ses diodes sous-cutanées et, de manière générale, stimule son système
tactile et érogène.

Il lui faudra encore un peu de temps pour s'habituer à ce bruit de fond,
pour en extraire du sens. Mais elle a suffisamment attendu. Elle profite
de la vue encore quelques instants avant de retourner dans la chambre
qu'elle squatte depuis quelques jours. L'hôtel n'a probablement jamais été
terminé, en témoigne l'absence de peintures sur les murs, ou le parquet
encore en cours de pose, comme si le chantier avait été abandonné la
veille, de la poussière et des toiles d'araignées en plus. Elle regroupe
rapidement les quelques affaires qu'elle a sortis de son sac, attrape son
masque et, avant de le connecter à la prise jack sur sa nuque, reçue lors
de sa dernière initiation, elle croise son reflet dans le miroir de
plain-pied brisé, lui renvoyant son reflet comme vu à travers un
kaléidoscope.

La silhouette, brisée et répétée, tel un espace de Mandelbrot, porte les
marques du voyage. Ses tatouages chamaniques sont estompés par la
poussière et la crasse, mais elle distingue encore les bobines
sous-cutanées qui alimentent ses circuits par courant induit. Des lignes
géométriques parcourent son corps et le divise en secteurs. Chacune d'elle
fait partie d'un ensemble logique et transforme sa peau en
semi-conducteurs finements sculptés pour former un processeur. Cette
finesse de gravure n'a pu être atteinte que pendant les transes de son
maître pour guider les nano-machines qui ont alors creusé leurs sillons
microscopiques dans l'épiderme, ajoutant divers matériaux et encres au fur
et à mesure du processus afin d'obtenir l'effet désiré.

Elle a mis du temps avant d'accepter ce changement dans son corps, à se
reconnaître dans un miroir. Mais maintenant, les motifs noirs ou de
couleurs vivent s'entrecoupent, un peu comme sur les tableaux de cet
artiste dont elle oublie le nom, composés de lignes noires et de
rectangles de couleurs disposés su un fond blanc. Plus précisément comme
si un bot artiste aurait essayé de faire un tableau de ce style, mais se
serait trouvé coincé dans une récursion infinie, reproduisant des motifs
fractals, jusqu'à ce qu'ils ne soient plus distinguable par l'œil humain
non augmenté.

Elle enfile un débardeur et un jean large qu'elle ceint avec une sangle
à laquelle sont suspendus divers petits outils, des micros circuits, des
adaptateurs de ports et, globalement beaucoup trop de choses plus ou moins
inutiles et qui pendent à sa taille. Elle passe une main dans ses cheveux
dans une vaine tentative de mettre de l'ordre dans ce fouillis de
fibres optiques, de cheveux, et de différents ports de connexion qui
s'accrochent à son cuir chevelu. Les circuits sur ses tempes brillent
faiblement, enfoncés sous les cheveux qui les masquent désormais.

Un dernier coup d'œil par la fenêtre, juste à temps pour voir les aurores
boréales s'atténuer, avant d'enfiler son sac à dos et d'attraper son
masque accroché à des pitons devant probablement originellement servir
à installer une suspension quelconque. Elle connecte le masque à un des
ports jack implantés dans sa nuque, et le rabat sur son visage.

Le miroir lui renvoie maintenant une image étrange, une hybridation d'un
humain et d'une machine. Comme à chaque fois qu'elle connecte ou
déconnecte, elle est prise de légers vertiges, lié à l'ajustement
sensoriel nécessaireau traitement des données. Les antennes diverses, le
masque servant d'afficheurs et les différents hauts parleur et système
d'interface lui donne un aspect insectoïde. Aspect renforcé par les
peintures et adhésifs appliqués sur l'ensemble, fusionnant les composants
disparates en un ensemble étrange mais vaguement humain.

Et là où sont censées se trouver les mandibules de l'insecte, le masque
s'arrête, lui laissant une partie du nez ainsi que ses mâchoires de libre,
pour faciliter les transes un peu longues. Elle ajuste les sangles de
serrage, attrape et enfile son sac à dos et se mets en route vers le
Complexe.

Elle traverse la zone industrielle laissée en jachère, offrant le
spectacle étrange de gigantesque grues rouillées, penchées au-dessus
d'espace sur lesquelles la nature recommence à prendre ses droits, elles
restent là, mues seulement par les vents, vestiges d'une gloire passée,
restes d'une civilisation industrielle ayant mutée en complexe
postindustriel.

Son masque la guide vers le Complexe. Les anciens parcs sont
redevenus sauvage et des vignes rampantes ont commencé à s'accrocher aux
oléoducs aériens qui traversent cette zone de la ville, tordant et
compressant lentement cette artère autrefois centrale dans l'alimentation
en énergie de la ville.

Elle grimpe encore une colline et arrive sur une plaine désertique,
stérilisée par les pesticides utilisés massivement il y a encore
cinquante ans, dans le but de faire pousser sous ces latitudes des plantes
tropicales, pour satisfaire les caprices des oligarques contrôlant
autrefois la région. Enfin, elle arrive à la bordure du Complexe. La
rumeur, les bruits, de cette ruche bourdonnante finissent par l'atteindre,
par s'ajouter aux vibrations qu'elle perçoit de manières plus distinctes.

Son masque lui affiche les principales fréquences utilisées et, d'une
commande mentale, elle réorganise ses filtres et reconfigure ses antennes.
Elle sent différent panneaux de son masque bouger et coulisser à mesure
que sa nouvelle configuration s'adapte à ce qu'elle voit.

Elle est saturée de messages, d'informations, de bruits, d'images. Les
espaces qui l'entourent prennent des couleurs de plus en plus chamarrées
au fur et à mesure que les filtres agrègent et trient les données, elle
s'enfonce doucement dans le paysage numérique du Complexe, un sourire aux
lèvres.

Et suis le bruit, sous le chaos, là, quelque part, elle retrouve la
pulsation qui l'a amenée jusqu'ici. Légèrement différente, mais
indubitablement la même.
