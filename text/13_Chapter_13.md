# Chapitre 13.

> [ rumeurs.net/main ]
> 
> Nancy > Dites, quelqu'un saurait m'expliquer ce qu'est ce truc de ripper
> dont tout le monde semble parler ici ?
> 
> Drew > Pas vraiment. Ça ressemble encore à un truc qui s'est échappée d'IT
> et qui fait parler. C'est efficace en tout cas.
> 
> West > Si j'ai bien suivi, c'est plutôt une bande d'allumés qui essayent
> de retourner aux racines du Complexe en en virant tout ce qui s'en dévie.
> 
> Nancy > Euh … Moi j'avais plus compris quelque chose à propos de fringues,
> pas d'un truc assimilationiste.
> 
> West > Quand tu regardes de plus près, tout est toujours à propos
> d'assimiler les cultures
> 
> Drew > Certes. Mais on essayait plus de trouver des trucs concrets.
> 
> Anomonomnomnymous > C'est pas une nouvelle façon de faire des brioches de
> tempé ?
> 
> T0n > Au risque de te décevoir, tout n'est pas question de bouffe. Et
> sinon, non on y est pour rien à IT.
> 
> West > Tu n'y es pour rien tu veux dire. Ou alors tu admets que vous avez
> un groupe avec une ambition de contrôle.
> 
> T0n * has quit (Ping Timeout)
> 
> Sloath > Ah oui, il y a ça aussi. C'est moi ou Plurality rame ?
> 
* * *

Saina a retrouvé Nato et Tonya dans le jardin des Valkyries. Stesse et Val
sont aussi là et lézardent au soleil, avant que celui ci ne se couche.
Dans le patio, en dessous d'elles, Park termine l'impression d'un jeu
d'outils adaptés à la morphologie des rats. La fin d'après midi se passe
au ralenti.

« Bon, récapitulons ce que l'on sait, dit Saina. Et spéculons, au moins
qu'on ait toutes les éléments de réflexions nécessaires à déterminer la
suite des opérations.

— C'est quoi votre hypothèse de travail, demande Tonya ? Qu'est-ce qui aa
changé depuis la dernière fois qu'on en a parlé ?

— Des rêves étranges, une migraine monstrueuse, et l'impression d'entendre
des voix ? répond Nato.

— OK je suppose que c'est quelque chose.

— J'ai eu des symptômes similaires, j'ai des souvenirs de choses que je
n'ai pas pu faire. J'ai l'impression de connaître le contenu des carnets
noirs d'Elliot par exemple, et je sais ne les avoir jamais lu.

Nakato > C'est un rappel mnémonique ça, non ?

— Non, pas forcément. Le rappel c'est ce que tu as quand tu te passes des
XP par exemple, et que ton cerveau isole les signaux reçus parce qu'il ne
sait pas comment les traiter, du coup tu vas en rêver ou avoir des
flashbacks, mais ça finira par disparaître, et te laisser une trace de
cette expérience. Tu vas te souvenir t'être passé de l'XP, pas d'avoir
vécu ces choses.

» Là, et je suppose que pour Nato c'est pareil, j'ai des souvenirs précis,
intégrés à ma conscience de choses que je sait ne pas avoir vécue. Du
coup, inconsciemment, j'essaye de résoudre ce paradoxe. Je pense que la
sensation d'entendre des voix de Nato est une manifestation de ce
phénomène.

» Pour rappel, on a vu quelque chose avec Nato qui n'est pas censé pouvoir
exister. Un message d'entropie infinie. Après ça, on a perdu deux heures
de nos vies, on ne sait pas ce qu'il s'est passé et, subjectivement
parlant, ça ressemblait à un dixième de seconde.

» L'hypothèse de travail, c'est qu'Elliot aurait, dans ses carnets, laissé
quelque chose qui a permis à Uncanny Valley de créer une machine à état
infinis. Et que quelque part, il y a quelque chose capable d'altérer des
souvenirs, d'implanter des suggestions, ou quelque chose du genre.

— Sauf que, poursuit Nato, ce que l'on sait depuis que Turing nous a pondu
ses machines, c'est que le modèle physique utilisé par l'ensemble de nos
machines, quantiques ou non, n'est pas capable de générer ce genre de
chose. Elles ne sont, par exemple, pas capable de déterminer si un
programme peut se terminer sans l'exécuter, et se retrouver bloquée
dans une boucle infinie. Mais même cette boucle infinie a une entropie
réelle : nous sommes capables de prédire, au moins partiellement, les
résultats d'exécution des programmes.

— Or, Saina reprend la parole, nous avons là, quelque chose qui a une
entropie infinie. Quelque chose que nos machines de Turing ne peuvent pas
générer. Quelque chose que nos cerveaux, en revanche, sont capable de
faire grâce à deux mécanismes : la conceptualisation — ou l'abstraction si
vous voulez — et la mise en contexte.

Nakato > Je crois que je vois où tu veux en venir.

Nakato > Tu supposes que ce truc que vous ne pouvez pas voir, ne peut pas
exister sur une interface logique physique. Quantique ou non. Qu'il
y a nécessairement besoin de quelque chose en plus des machines de Turing.

Nakato > Tu dis aussi que nos cerveaux, par différents moyens, sont
capables, grâce aux concepts et aux contextes, de gérer un nombre d'état
infini.

— Oui, le bon vieux Entscheidungsproblem. Il n'existe pas de système
capable de pouvoir universellement résoudre tous les problèmes. Sans même
parler de problème physique, les machines de Turing ont en théorie une
mémoire infinie, mais le nombre d'états qu'elles peuvent avoir est limité
par la taille de leur registre, et cette limitation ne permet pas de
résoudre l'ensemble des problèmes mathématiques.

Nakato > De fait, si il existe quelque chose ayant une entropie infinie,
et donc un registre infini, ce quelque chose n'est pas une machine de
Turing, mais quelque chose d'autres, capable d'abstraction.

— Attendez, interromps Tonya, tu veux dire qu'il y a là-bas, quelque chose
capable de conceptualiser ?

— Oui. Ça ne veut pas dire que c'est intelligent ou conscient.

— Si, ça l'est nécessairement, répond sèchement Park qui vient de
s'installer avec elles. La conscience est définie par ces deux concepts.
La conceptualisation, et donc la capacité à la pensée abstraite, nous
permet de pouvoir utiliser des choses comme des langages naturels, et donc
de partager notre point de vue sur le monde avec d'autres et de se définir
dans un contexte.

» La contextualisation, c'est ce qui nous permet de nous différencier des
autres autour de nous. Si tu n'as pas ça, tu n'as pas un système
conscient.

— Elle a raison, dit Nato. C'est forcément quelque chose de conscient, et
donc doté d'une certaine intentionnalité, une volonté d'agir au lieu de
simplement réagir. Le souci c'est qu'il n'est pas possible de créer une
machine consciente, on a des limites physiques normalement.

— Du genre ? demande Park.

— Du genre que pour le moindre cerveau, même pas nécessairement humain, on
arrive rapidement sur des nombres de connexions potentielles supérieures
au nombre de particules élémentaires de l'univers, répond Nato. D'autant
que ces connexions changent régulièrement.

» Et donc, on ne peut pas créer une machine théorique pouvant
potentiellement cartographier l'ensemble des neurones. Mais on sait que
ces réseaux de neurones, par leur complexité, permettent la pensée
abstraite, et donc la conscience.

Nakato > Ce qui rend donc impossible le fait de numériser une conscience.

— Voilà. C'est pour ça que les XP ne sont pas, intrinsèuement, des
expériences, mais juste un playback te permettant de créer ta propre
expérience.

— J'ajouterais que réduire le cerveau aux simples connexions neurales est
déjà, en soi, une grosse simplification, précise Saina. Ces connexions
changent en fonction du liquide dans lequel elles baignent, et de sa
richesse en différents neurotransmetteurs, dont la métabolisation est
gérée par le reste de notre organisme.

— D'accord, d'accord. On s'éloigne un peu trop du sujet non ? demande
Tonya.

— Pas forcément, répond Saina. Je ne vais pas vous faire un cours
magistral de philosophie du langage mais, très rapidement, les machines de
Turing, et peu importe leur niveau de complexité, ne savent pas ce qu'est
le signifié, pour reprendre des termes techniques. Pour elles, le nom de
la rose et la rose sont la même chose. Elles ne savent pas faire de la
pensée abstraite, ou manier des concepts.

— On arrive à simuler des conversations dans beaucoup de langues,
interviens Park.  Sur des sujets précis, spécialisés. Mais une
intelligence générale ne peut pas exister.

» Les plantages phénoménaux des systèmes convolutifs au début du siècle
ont bien montré que cette capacité de spécialisation ne permet pas de
généraliser un système. Le contexte leur étant fourni par des jeux de
données limités et des fonctions d'évaluation, ils devenaient très
efficaces dans leur branche, faisant des suggestions permettant de
simplifier la vie de spécialistes du domaine.

» Et toute la difficulté vient bien de là. Ces machines ne font que des
suggestions. Elles regardent le problème qui leur est soumis, génèrent une
liste de réponses avec une classification et une pondération, mais elle ne
prend pas de décision. Même un ver plat dispose de plus d'intentionnalité
que ces systèmes.

— Bon, donc on ne peut pas avoir une machine capable d'intentionnalité
sans avoir une capacité de conceptualisation et de mise en contexte,
résume Tonya. Mais quel est le rapport avec ce que vous avez vu ?

— Pour essayer de comprendre ce qu'il s'est passé. Le black out, le trou
noir entropique et pourquoi je me souviens de choses que je n'ai pas pu
vivre. Il existe quelque chose d'impossible, un message ayant une entropie
infinie. Il faut qu'on trouve les outils pour le décrire.

» L'étape suivante, c'est de savoir si oui ou non cette anomalie est
capable de comprendre un contexte, et d'adapter son comportement en
fonction.

— Tu oublies pas un peu vite l'intentionnalité, demande Nato ?

— Non. J'ai reçu un signal — et toi aussi — sans avoir préétabli une
communication avec elle. Je ne suis pas certaine, mais il me semble que
l'on a vécu ces deux expériences différemment, ce qui est normal.

Nakato > Tu va un peu vite là, ça pourrais être un scan ou quelque chose
du genre.

— C'est ce que je pensait aussi au début, dit Nato. Mais il faudrait que
quelqu'un puisse faire tourner ce scan sur du matériel ayant une limite
physique. Ou capable de conceptualiser.

Nakato > OK, OK. Ça ne serait possible que si ils avaient un appareil
capable de pensée abstraite. Je vois l'idée.

— Juste une question, demande Stesse, pourquoi est-ce que c'est dans
DreamLand ?

— Parce que c'est ce qu'il y a de plus proche d'un jeu d'apprentissage
conceptuel, répond Nato. Réfléchis. Tu sais que, quelque part dans ces
carnets noirs, il y a la réponse à un problème de math et de linguistique
vieux comme le langage. Tu as un système capable d'intégrer des
informations sensorielles et d'en former des expériences. Tout ce qu'il te
manque ce sont ces stimuli sensoriels. Tu vas donc te connecter sur
DreamLand, c'est ce qu'il y a de plus proche d'une bibliothèque de
sensations et d'émotions. »

Un silence s'installe progressivement dans la discussion, le temps que
chacune en assimile le contenu et le constat qui doit être fait. Nato est
la première à briser le silence.

« Ce que je capte pas moi, c'est comment et pourquoi on s'est fait
implanter des souvenirs — ou autres, et ce que ça peut avoir comme impact
sur moi, sur nous ou même sur notre vision de notre identité.

— Bonne question, répond Saina. Ça nous ramène aux éternels questionnement
philosophique classique. Est-ce que je ne suis que la somme de mes
expériences, ou est-ce que je suis quelque chose de plus ? Sous entendu,
si l'on fait une copie parfaite de moi, est-ce qu'elle réagira exactement
comme moi dans les mêmes conditions ?

» On peut aussi jouer avec le raisonnement inverse, si on ajoute à ma
conscience des souvenirs artificiels, est-ce que je deviens quelqu'un qui
a vécu ces expériences, de manière non différentiable de quelqu'un qui les
aurais effectivement vécus, ou est-ce que j'intègre ces expériences à mon
continuum, mon identité, et je deviens autre chose encore.

» Je penche pour cette solution. On intègre ces expériences à notre
conscience, mais on ne devient pas nécessairement une personne
fondamentalement différente. Les souvenirs que nous gardons sont ceux qui
sont intégrables à notre identité. Le reste est évacué sous forme de rêve,
ou, parfois, de traumatisme.

» C'est pour ça qu'on s'est cogné des migraines. Ce sont les parties
rejetées par notre conscience, qu'elle essaye tant bien que mal
d'intégrer. Et l'intégration de ces expériences dépend de l'état
précédent. C'est pour cela que l'on ne réagit pas de la même façon dans
les mêmes situations et que l'on crée une différenciation avec les autres.

» Reste à déterminer quel est le siège de la conscience, est-ce qu'il
y a un organe dédiée à celle-ci ? La réponse est non, ça a été éliminé il
y a quelques temps déjà. De fait, la conscience ressemble de plus en plus
à une propriété émergeant d'un système chaotique et complexe.

» Bref, je pourrais continuer longtemps, et je suppose que Park aurait
aussi des choses à apporter vu son expérience en premiers contacts. Mais
évitons de dériver encore plus. La vraie question à laquelle répondre
c'est : comment est-ce qu'ils ont réussit à construire cette machine
à états infinis.

— C'est forcément quelque chose de distribué, dit Nato, réfléchissant
à voix haute. Si tu as raison sur le fait que la conscience émerge de la
complexité, alors il faut une machine complexe, capable de se redéfinir,
de se réorganiser. Pas forcément, d'ailleurs, de fournir une réponse
correcte à la question posée, mais capable de répondre avec le résultat
d'une décision.

— Quand tu dis distribué, demande Tonya, tu penses à quelque chose qui
aurait énormément de nœuds ?

— Aucune idée. Intuitivement j'aurais tendance à penser qu'il pourrait
s'agir d'une énorme quantité de système simpliste, la complexité venant de
la quantité des systèmes, mais je ne pense pas que ce soit cette piste là
qui a été choisie ?

Nakato > Pourquoi ?

» Parce que, si nos hypothèses sont correctes, il y a quelque chose dans
ces carnets noirs qui permet de créer une conscience. Il y a donc un
langage, ou un bout de langage, capable de décrire et de créer un système
conscient.

— Reste que c'est théorique, dit Nato. L'idée est vieille mais se heurte
à un problème de physique. Il faut une source d'entropie infinie, et
l'électronique moderne nous abandonne en cours de route.

— C'est pour ça que je pense que ce n'est pas électronique, répond Saina.
Mais qu'il y a aussi, quelque part, une part biologique, un réseau de
neurones — pas dans le sens mathématique, qui s'interface avec cette
machine.

» Quelque chose de plus complexe que les convertisseurs de signaux dont
vous disposez pour vous passer des XP.Ce qui nécessite de franchir
certains systèmes biologiques qui ont pour but de protéger notre
conscience des agressions.

Nakato > Donc tu penses qu'il y a un symbole, un stimulus quelconque, un
message qui, lorsque tu y es soumis, permets de passer outre ces systèmes
de protection ?

» Ça on le sait depuis longtemps, répond Saina. C'est là dessus que se
base le principe des transes, ou de l'état d'illumination du Bouddha par
exemple. Ou du chant des rêves aborigènes. Il y a un sens occulte aux
symboles, qui permettent de modifier le comportement d'une personne. Un
peu comme une suggestion hypnotique.

— Ce qui m'échappes encore Saina, c'est pourquoi sommes nous les seules
à avoir été impactées ?

— Ça tu ne le sais pas Nato. On peut raisonnablement penser que les
personnes d'Uncanny ont été impactées également. Et il y a peut-être
d'autres personnes du Complexe qui le sont également.

Nakato > Et qui ne veulent pas paniquer tout le monde, et donc essayent de
garder l'information pour eux.

T0n > Ou alors, ils ne savent pas qu'ils ont étés affectés. Ou alors leurs
suggestions les forces à rester discret.

— Quoi qu'il en  soit, reprend Saina, on ne peut pas penser que nous
sommes les seules concernées. Il faut aller voir Uncanny et leur parler,
on est encore beaucoup trop dans le spéculatif.

— Juste une remarque Saina, dit Tonya. On est d'accord que, même pour une
fusion complète homme / machine, il est peu probable que deux consciences
indépendantes partagent le même matériel ? Voire, il est impossible
d'avoir deux consciences strictement identique ?

— À partir du moment où le temps s'écoule, oui. Tu penses à quelque chose
de particulier ?

— Si vous avez été exposées à des souvenirs différents, venant pourtant de
la même conscience de base, vous n'hébergez pas — à défaut d'un meilleur
mot — la totalité de cette conscience.

— Probablement oui. Ou alors des fractions de celles-ci.

— Qui ne constituent pas des consciences, juste des expériences, rappelle
Park.

— Il est donc envisageable de se dire que ce langage, cette conscience,
fonctionne en fait de manière distribuée, qu'elle existe comme la somme de
ces fragments, non ?

— Je ne sais pas. Je pense, comme Park, que ces objets seuls ne forment
pas une conscience. Mais oui, il y a forcément plusieurs nœuds utilisés
pour former cette conscience. Si le système est trop simple, la complexité
s'effondre et l'émergence n'arrive pas.

— On a donc affaire à un genre de processeur hybride qui connecterai
plusieurs ressources entre elles. Ou quelque chose du genre ?

— Quelque chose du genre oui, lui répond Nato. Ça permettrai de faciliter
la pensée abstraite, de déléguer une partie du processus de réflexion
à des entités spécialisées. Cela dit, cette conscience aurait donc pour
but de préserver son existence, donc de recruter de nouveaux systèmes pour
garantir sa survie.

» Un peu comme un complexe mémétique qui chercherai à se développer en
contaminant les cultures et tropes qu'il touche. Cette implantation
mémorielle serait du coup, un mécanisme de recrutement, pas forcément
conscient d'ailleurs.

— Ça se tiens, poursuit Saina, ça expliquerai aussi pourquoi nous n'avons
pas conscience qu'elle existe partiellement dans nos têtes. Que ce n'est
pas une voix qui nous parle, juste des sensations et souvenirs
étrangers. Que nous ne percevons pas son existence au sein de nos systèmes
ce pensées complexe.

— Encore faudrait-il que nous puissions percevoir cette conscience si nous
y étions confrontées.

— Bon, interviens Tonya, avant que vous ne repartiez dans de longues
discussions métaphysiques, j'ai deux questions pour vous. Quelle est la
suite des opérations, et qu'est ce qu'on fait ce soir ?

— Il faut aller voir sur place, répond Saina. On est beaucoup trop dans le
spéculatif là, au moins s'assurer que les gens qui habitent là bas vont
bien. Pour le reste, je suppose que tu as des idées ?

