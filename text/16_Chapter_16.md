# Chapitre 16.

> [ Plurality/r_34 ]
> 
> Nox > Sérieusement, c'est le seul truc qui marche encore ? Mais vous
> faites quoi les sysops ?
> 
> T0n > On se paluche, parce qu'il n'y a plus que ça qui fonctionne ici.
> 
> Uncle > Plus sérieusement, on cherche et on essaye de remettre ce qu'on
> peut en route. Mais c'est … étrange. Même pour les standards du coin.
> 
> X-L > Merde, c'est tendu. Bon courage les gens.
> 
> U+1F351 > Je suis un peu déçue moi, je pensais que toute cette activité
> ici c'était pour les dernières XP que j'ai poussée ici.
> 
> Uncle > T'inquiètes peach, tout le monde aime bien mater ton cul, ça, ça
> ne change pas.
> 
> U+1F351 > Ouf. Bon, je range un peu ici, et je prépare un petit show pour
> les sysop, vous pourrez le voir avec votre clef, mais uniquement quand
> tout sera remis en route. Voyez ça comme une motivation supplémentaire.
> 
* * *

Le Complexe est pris dans une nappe nuageuse. Les lumières diffractées
par les microscopiques gouttes d'eau semblent atténuer le brouhaha qui
monte des plazza, des médinas et des souks. Plurality est saturée de
signaux, mais plus aucun message ne passe à l'exception de certains canaux
très spécifiques.

Les techies, sysop et admins discutent et échangent sur des fréquences
radios modulées, profitant de l'infrastructure radio d'ESP. Comme une
colonie de fourmis dont la fourmilière vient d'être inondée, les
habitants du Complexe s'agitent à préserver ce qu'ils peuvent, cherchant
les sources de pannes et les points de congestions, basculant certaines
routes sur des itinéraires moins saturés, éteignant certains systèmes non
nécessaires, écrivant sur les murs les configurations extraites des
équipements de routage pour que les voisins puissent s'y connecter plus
facilement.

Ce que personne ne comprend en revanche, c'est pourquoi Plurality est
tombée sous la charge. Sous la charge de quoi d'abord ? Quel groupe a pu
réunir suffisamment de ressources pour faire planter massivement cette
infrastructure ? La tempête de broadcast de l'an dernier n'avait pas fait
dix pour-cent des dégâts provoqué par cet incident, et c'était le plus
gros incident de Plurality dont les sysops avaient connaissance, jusqu'à
hier du moins.

La force de ce réseau auto organisé est, justement, de permettre aux
informations de circuler, même en cas de perte d'une partie de
l'infrastructure. Pas de conception centralisée, pas de cartes
exhaustives du réseau, pas de listes de bonnes pratiques, juste des bonnes
volontés mettant en commun des ressources, s'accordant sur quelques
protocoles afin de pouvoir discuter avec les voisins.

Les deux services ayant émergés de ce chaos, et disponibles à peu près
partout sont rumeurs.net et Plurality, le premier parce que personne ne
garde d'archive et que tout le monde aime bien pouvoir discuter dans un
contexte éphémère, le second parce qu'il représente la mémoire du
Complexe. Mais même ces services omniprésents sont gérés par des
volontaires, hébergés sur ce réseau grouillant de machines.

Le seul endroit pouvant ressembler à un des centres de traitements de
données omniprésent dans les infrastructures, c'est les baies de calcul
d'Ivory Tower, qui leur servent essentiellement à stocker les infos de
l'extérieur avant traitement ou à fournir de la puissance de calcul pour
certains projets dépassant ce que les collectifs peuvent fournir.

C'est donc ici que de nombreux sysop se sont installés pour discuter et
réfléchir avec les autres à ce qu'il se passe. Le second point de chute
est la section rule 34 de Plurality dont les admins, et même eux ne savent
pas vraiment comment, ont réussis à éviter une partie de la surcharge et
à maintenir des canaux de communications ouverts.

Les écrans de la war room d'Ivory Tower sont éteints, ils n'ont plus de
données à diffuser. Mais les salles de travail autour sont surchauffées
par l'activité humaine, ajoutant à la moiteur des machines saturées et des
climatisations en surcharge. Les stations de jeu et de détente ont été
reconverties en plan de travail, et une effervescence particulière secoue
cette partie du Complexe.

Les personnes travaillant habituellement ici ont laissées la place aux
diagnosticiens qui essayent de comprendre ce qu'il se passe, mais aussi
comment sortir de la crise. Les débats vont bon train et la tension
augmente proportionnellement au taux de caféine et de stimulants dans le
sang et au manque de sommeil, personne n'ayant vraiment pris de repos ces
trente-six dernières heures.

Nakato essaye de réfléchir, mais trop d'idée fusent et envahissent son
espace cognitif, parasite son train de pensée et, comme de nombreuses
autres personnes, elle n'est plus capable de quoi que ce soit nécessitant
une capacité d'analyse et de concentration. Son cerveau essaye simplement
de préserver son état mental. Baignant dans un bain d'hormones et de
neurotransmetteurs divers, il a désactivé les fonctions de haut niveau
privilégiant les routines instinctives de préservation.

Elle finit par abandonner sa lutte contre le sommeil et arrête les
discussions rapidement.

« Bon, moi j'arrive plus à réfléchir. Et je pense que je suis pas la
seule, il faut accepter qu'on ne remettra pas Plurality en route
aujourd'hui. Ni probablement demain. Je vais manger un truc et dormir
un peu, vous devriez faire pareil. Ou aller vous changer les idées.

— Mais du coup, la suite des opérations c'est quoi ? demande Nato

— Je n'en ai aucune idée. Je n'ai plus d'idées moi, je ne réfléchis plus.
Donc faites ce que vous voulez, moi je vais dormir dix ou douze heures,
manger un morceau et reprendre le taff là. Et vu que t'as probablement
moins dormi que moi, tu devrais en faire autant, ou au moins sortir d'ici.

— Oui maman, répond Nato.

— Des nouvelles de, rah, j'oublie les noms … Saina ! Des nouvelles
d'elle ?

— Non, mais elle m'a dit qu'elle nous tiendrait informé dès qu'elle aura
des news. Si tu veux, » et Nato détache le talkie grésillant de messages
à peine audible qu'elle a à la ceinture et le fait glisser sur la table en
direction de Nakato, « tu peux utiliser ça pour savoir oú elle en est.

— Nan, ça ira merci. Et faut essayer de garder les fréquences libres de
toutes façon. Et donc, je vous abandonne et je vais dormir, à plus tard.
Et va dormir.

— Tu sais oú elle est ? demande Nato avant que Nakato ne soit passée par
la porte.

— Si pas chez elle, ni chez Tonya, alors elle doit être au Hanged. T'as
qu´à passer là-bas si tu veux, ça te fera une rupture avec la merde d'ici.
Et moi je me casse, bonne nuit », et Nakato sort, fermant derrière elle la
porte de la salle de réunion en guise de fin de conversation, laissant
Nato et la demi-douzaine d'autres encore présents seules.
