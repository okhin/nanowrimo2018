# Chapitre 6.

> [ Plurality/wiki/ Amazons ]
> [ filter: summary ]
> 
> Amazons est un syndicat international centré autour de l'entraide et
> la dense des travailleurs en 0-hours. Au début principalement centré
> autour des services de livraisons, les Amazons ont ouvert des chapitres
> entiers dédiés aux travailleuses du sexe, aux modératrices ou aux
> chauffeuses privées.
> 
> Le Chapitre du Complexe est essentiellement dédié à la coordination de
> leurs actions internationales, mais également à la formation des
> habitantes à la gestion collective.
> 
> Leur mot d'ordre reste à ce jour: Nous ne t'abandonnerons jamais, Nous ne
> te trahirons jamais, Nous ne te fuirons pas, Nous ne te ferons jamais
> pleurer, Nous ne te dirons jamais adieu.
> 
> [ comments ]
> 
> Risk > Rah mais merde, jusqu'ici vous allez nous foutre ce truc en tête ?
> 
> Yva > Il faut croire que certains mèmes ont la peau dure. Ou que les dorks
> dans leur Tour sont réellement aussi efficaces qu'ils le prétendent.
> 
* * *

Le soleil atteint son zénith lorsque, sous une fine bruine, Saina arrive
dans le Ciało District. Elle ne sait pas trop comment elle est arrivée
ici, elle a simplement suivi le mouvement, comme Tonya lui a recommandé de
faire. À cette heure, quelque part entre la fin d'une soirée et le début
d'une autre, l'ancien centre commercial est relativement calme, l'activité
débordante des souks contraste ici avec la sérénité des gueules de bois
et des descentes des quelques personnes encore présentent ici.

Les hôtes et hôtesses des salons, les danseurs et danseuses, les dealers
et dealeuse émergent doucement de la courte nuit qu'ielles se sont
accordées, se regroupant autour des patios ouvert dans lesquels ont peut
trouver à cette heure des viennoiseries, brocht et autres pâtisseries,
diverses boissons chaudes et nootropique pour aider à faire passer les
maux de tête et baisse d'énergie.

Les urbanistes qui ont conçus ce lieu ont fait en sorte de forcer les
visiteurs à passer devant le plus grand nombre de vitrines, bloquant
l'horizon et zigzaguant entre les boutiques. Des enseignes criardes et
rivalisant d'outrance éclairent désormais ces passages dans une cascade de
couleurs vives. Elles annoncent les différents salons et services
disponible à qui veut bien se donner la peine d'entrer.

Saina sent le bruit de fond particulier généré par les chat-room et
cam-girls installées dans les nombreux parloirs, fournissant à des
milliers de personnes les peep-shows dont elles ont besoin. Étrangement,
le spam est quasi inexistant ici, tant que l'on ne considère ni le fait de
projeter des clips de danseuses sur des façade, ni le fait de faire
l'article pour vendre ses psychotropes comme étant du spam.

Une bande son relativement sobre, mélangeant boucles synthétique basique
et lancinantes mélopées distordues des saxophone,s est diffusée via le
système sonore présent dans tout Ciało. Cette bande son, Saina n'en doute
pas, est beaucoup plus énergique lorsque le lieu reprend vie. Elle
s'arrête quelques instants prendre un café, « sans rien d'autre que du
café, merci » pour essayer de se repérer et de trouver l'Ivory Tower

« Tu peux pas la rater, tu lèves les yeux et tu devrais la voir dépasser
dans ce quartier, » lui a dit Nakato quand elle lui a demandé comment
y aller. Sauf que là, en levant les yeux, passé les câbles, cordes,
enseignes, balcons et autre excroissances développées par les bâtiments,
il n'y a qu'un épais brouillard.

Et pas de carte ici. Enfin, pas de carte globale. Plus tôt dans la
journée, elle a demandé à Tonya si il y avait un plan du Complexe pour
qu'elle puisse se repérer. Tonya a laissé échappé un rire avant de
répondre.

« On a pas de carte globale du Complexe. Déjà parce que maintenir à jour
une carte obligerait soit à fixer plus ou moins définitivement la forme du
Complexe, et que cela nécessiterait en plus de devoir avoir une bonne
version de la carte, donc une sorte d'autorité.

» Et cartographier les lieux à une résolution suffisamment précise est une
tannée sans fin. Le temps que tu mette à jour la carte, une demi douzaine
de rues, passages et autres auront changé de forme ou de fonction. On
a pas besoin de carte.

— Mais comment vous trouvez des trucs ?

— Réseau social ma grande. En gros, pour la plupart de ce
dont j'ai besoin, je trouve généralement quelqu'un dans le coin qui peut
m'aider, soit en ayant ce que je cherche, soit en me mettant en contact
avec quelqu'un qui a ce que je cherche.

» Pour les trucs plus exotique, ou si j'ai vraiment besoin de quelque
chose de très précis, Plurality dépanne. On peut poster des demandes,
proposer des services ou du matériel, enregistrer dans un wiki un genre de
mémoire collective, publier nos schémas et idées de conception.

» Et après, chaque groupe maintiens une liste de points d'intérêts plus ou
moins publique, permettant en quelque sorte de mettre les réseaux sociaux
en commun.

— D'accord, mais vous ne vous perdez jamais ?

— Si, tout le temps. Mais c'est comme ça qu'on peut découvrir des perles.
Typiquement les personnes qui font le kim-chi que tu est en train
d'avaler, je suis tombé dessus il y a une semaine en cherchant où était la
fuite d'un circuit d'eau. »

Tonya clôtura la discussion en embrassant Saina puis elle enfila son
blouson et son harnais / caisse à outils avant de sauter sur son vélo.
Elle est parti aider Park à reloger une famille de néo-gerbille hors d'un
pipeline de données et, peu après, Saina s'était mise en route pour
retrouver Nakato dans Ivory Tower.

En tournant un angle, et après avoir esquivé un skateur descendant les
étages du Ciało à toute vitesse, slalomant entre les personnes à peine
éveillées, elle se retrouve face à Ivory Tower. La tour de miroir et de
plast-béton blanc se fond dans le ciel brumeux. Elle est placée en
éclaireur, première d'une douzaine d'autres qui constitue l'ancien centre
d'affaire.

S'élançant au-dessus du vide, des plateformes hétéroclites greffées sur la
tour abritent des zones d'habitation, des marchés, ou servent de point de
départ des ponts connectant Ivory Tower au reste de la tour. Un entrelacs
de câbles et de pipelines enserre la tour de miroir afin d'y distribuer
énergie et connexion au réseau.

Saina s'engage dans le lobby inondé de la tour, des échafaudage et des
plateformes permettant de passer au-dessus de l'eau verdâtre de laquelle
émerge le haut des anciens guichets d'accueil. Les ascenseurs inondés ne
fonctionnant pas, elle traverse le hall de faux marbre, dont le plafond
n'est plus qu'à une paire de mètres du sol, avant de prendre l'escalier de
service et de se rendre à la war room où elle doit retrouver Nakato.

Reprenant et détournant les codes de la culture start-up, la war-room est
une ruche bourdonnante d'activités. Les murs sont couverts de grand écrans
affichant l'activité détectée sur les différents réseaux sociaux
corporatistes, basculant entre les canaux toutes les secondes, dans un
brouhaha cognitif auquel personne ne prête attention.

L'open space, créé par la suppression d'une grande partie des cloisons
mobiles, est plus un espace de jeu que de travail. Bar, consoles de jeu,
musique et, avachi dans les coussins, à discuter entre eux ou plongés dans
leur rêveuse, les memakers, sappé dans un mélange des genres entre costume
corporatistes et punk anglais du siècle dernier, échangent, discutent,
jouent et, parfois, vont s'isoler dans les bureaux pour discuter d'un
projet, accoucher d'une analyse commandée par l'extérieur ou baiser.

Et partout, sur tous les murs, colonisant la surface comme un champignon
se développe sur un hôte, des affiches, des posters, des feuilles
reprenant des mèmes, des réponses à ceux-ci, des messages pour annoncer
diverses réunion ou projet, des plans griphonés rapidement et suivi d'un
point d'interrogation et d'un ID, des phrases n'ayant pas de sens, des
paradoxes, des fausses théories complotistes absurdes, des vraies théories
à propos d'elles …

Ce fourmillement culturel est mis à jour au fur et à mesure que les
personnes travaillant ici ajoutent leurs propre touche personnelles. Par
endroits les couches sédimentaires de papiers ont recouvrt une bonne
partie des vitres, alors qu'a d'autres elles ne s'installent que
timidement sur des fresques abstraites, peintes à même les murs et
incluant, parfois, le débordement chaotique qui les entourent.

Saina perd un peu pied dans cette cohue, d'autant que le signal est
particulièrement fort et que sa peau processeur la fait transpirer pour
dissiper l'excédent de chaleur. Nakato, depuis la balustrade donnant sur
la war-room, lui fait un signe lui demandant de monter, ce qu'elle
s'empresse de faire.

Nakato l'emmène dans une salle de réunion au centre de laquelle trône une
gigantesque table de bois verni. À l'autre bout de la table, vêtue d'un
tailleur rayé et d'un T-shirt sur lequel est imprimé une nébuleuse, et
laissant ses cheveux bouclés rouge tomber sur ses épaules, T0n se lève et
se présente.

« Et lui, qui est encore en train de rêver, c'est R1d3r, » dit-elle,
désignant du menton la silhouette avachie dans un fauteuil en simili cuir
et vêtue d'un veston noir sur un jean défoncé. « Bienvenue au cirque,
ajoute-t-elle.

— Bon, on en est où avec Uncanny, » demnade Nakato.

— Pas grand chose. Depuis que l'on a activé nos sondes, on a pas vraiment
vu une évolution. Ils continuent de parcourir arbitrairement DreamLand,
dans le but, à priori, de générer une nouvelle langue. Ou une culture
fucked up. Par rapport à nos standards.

— Excusez-moi, mais je suis larguée là. Vous faites quoi ici ? Pourquoi
vous surveillez certains groupes et surtout, qu'est-ce que je fais ici ?

— Pardon, rapidement donc. Ici, on s'amuse à créer, analyser et anticiper
les tendances, un peu comme ce que font les gros Think Tank dehors, mais
dans le but de faire passer certains messages.

» Du coup, on a des clients externes. Et on se connecte beaucoup aux
toiles commerciales. C'est probablement le seul endroit du Complexe qui se
connecte à ces réseaux. On profites de nos missions pour faire changer les
structures qui nous emploie. Et ça permet aussi de bien contribuer à notre
trésor de guerre. Voilà pour ce qu'on est, rapidement.

— On surveille certains groupe, poursuit R1d3r, généralement à leur
demande. Si on peut voir venir certaines crises, il est possible d'agir un
peu en avance. Et Uncanny Valley nous as demandé, suite à certains
incidents mémétique, que l'on garde un œil sur eux.

— Ce qui, ajoute Nakato, n'est pas forcément de tout repos, vu qu'ils
bricolent beaucoup autour des concepts de conscience et de culture, ce qui
peut les emmener sur des chemins destructifs si on ne fait pas gaffe. Vois
ça comme un watchdog si tu veux.

» Et tu es ici parce que l'on sèche. Il y a eu une activité anormale chez
eux, rien de grave pour le moment, mais juste assez pour que l'on commence
à s'inquiéter. Et je me suis dit que tu pourrais nous aider avec ton
approche particulière.

— D'accord. Je veux bien, mais j'ai besoin de plus de détails là.

— Basiquement répond T0n, c'est une tempête Tchevenkov d'amplitude 4.
Relativement stable et avec peu de radiations, il n'y a pas de …

— Tempête Tevenkov ?

— Tchevenkov, répond R1d3r. Tempêtes Tchevenkov, du nom du propagandiste
du FSB qui a théorisé les méthodes d'ingérence dans les états utilisant
des trolls capables de propager très vite des mèmes destructeurs.
Globalement, ces tempêtes sont alimentées par le désir d'individualité. Et
le fait d'appartenir à une caste particulière.

— D'accord, mais comment vous détectez ces tempêtes ? Je veux dire,
rumeurs.net n'a pas d'historique et, même si Plurality semble posséder un
historique, il me semble avoir vu que passé un certain temps, les
modifications sont archivée et fusionnée ?

— Bon, répond Nakato, on est parti pour une rapide histoire du Complexe et
de Plurality. Le Complexe s'est installé sur les ruines de
l'infrastructure abandonnée de la ville et, après quelques tentatives de
gestion communautaire de l'ensemble de l'infrastructure, les habitantes
ont opté pour un modèle différent. Les diversités culturelles étaient trop
importantes pour que l'on parvienne à un consensus. Mais le besoin
d'échange d'information, et de communication était un besoin réel.

» Et donc, sans concertation, sans demande unanime, et alors que tout le
monde s'écharpait pour déterminer une façon acceptable de prendre des
décisions collectives, les techos des différents collectifs ont
commencés à établir des passerelles protocolaires avec les autres. Elles
ont commencées à indexer leur contenus sur des wikis puis elles ont
fournit aux autres des interfaces d'accès. Un système commun finit par
émerger, et, au fur et à mesure que certains usages se généralisaient, ont
finit par devenir ce qui sera plus tard Plurality.

» Plurality est donc, en quelque sorte, un miroir de nos cultures. Ou
plutôt le creuset dans lequel se mélange les différentes cultures pour
pouvoir en créer d'autres, ou sublimer certaines. Et même sans historique
complet, le contenu de Plurality nous permet d'avoir une idée assez
précise de ce que les personnes font.

» Une fois que Plurality a commencè à devenir cohérente, il a été plus
simple pour chacune de communiquer et de dialoguer, d'échanger ses idées
ou d'en tester de nouvelles. Plurality fonctionne sur une base de
volontariat, si tu veux restreindre l'accès au contenu, tu peux le faire.
Tu es même encouragé à le faire. Cela a permis aux voix dissidentes de
s'organiser et de s'exprimer.

» C'est aussi Plurality qui a rendu possible la discussion sur les
non-négociables. Non-négociables qui sont en fait une transcription
formelle des règles implicites que les techos suivaient d'eux même.

» Rumeurs.net s'est créé un peu différemment, mais je vais laisser R1d3r
t'expliquer ça, rumeurs.net est un peu son bébé.

— Pas vraiment, j'étais pas né quand rumeurs.net a été créé. Mais le beau
rêve que te vend Nakato sur l'émergence de Plurality n'est qu'une part de
l'histoire.

» Plurality cause beaucoup de problèmes. C'est super d'avoir des
ressources sur à peu près tout ce que tu peux imaginer, mais qu'en est-il
des discussions entre personnes ? Est-ce qu'elle doivent être archivées,
ou est-ce qu'elles doivent être gardées hors-ligne ? De manière plus
générale, est-ce qu'avoir une base exhaustive de nos cultures n'est pas
une façon de les figer et d'en arrêter l'évolution, recréant les conflits
entre progressistes et conservateurs qui plombent le monde extérieur ?

» Les non-négociables fournissent un léger cadre pour limiter ça, mais on
sait que les réseaux sociaux de type corporatistes sont nocifs car ils
n'ont aucune notion de résolution, de distance. Tout y est mis à la même
valeur, qu'il s'agisse de discussions sur l'oreiller, de questions
pratiques pour créer telle ou telle trucs, ou de propos incitant à la
destruction d'une partie de la population.

» Plurality propose une certaine hiérarchisation des contenus. Mais ne
fournit pas d'outils permettant de bénéficier d'une certaine intimité,
d'une possibilité pour un contenu de n'exister que pendant un temps
réduits.

» Une interprétation naïve des non-négociables, notamment du premier,
serait que citer une personne est équivalent à parler en son nom. De fait,
citer ce que dit quelqu'un serait, théoriquement, interdit. Sauf que cette
interprétation est en contradiction avec le deuxième non-négociable qui
dit que tout ce que l'on fait doit pouvoir être repris ou copier.

» Je t'évites les nombreuses discussions autour de ce paradoxe. Il doit en
exister une archive quelque part si vraiment tu veux avoir une
connaissance précise des choses, mais les discussions autour de ces
interprétations se sont mal passée, certaines personnes commençant à se
déconnecter de Plurality pour créer un ou plusieurs réseaux éphémères et
privés. Ces petits réseaux orbitant dans une ambiance clandestine autour
de Plurality ont commencés à se structurer, par le besoin de maintenir le
lien entre certains groupes ou personnes par exemple.

» Au final, deux meufs ont écris le manifeste rumeurs.net. Manifeste qui
décrit également les spécifications d'un protocole social permettant de
préserver notre intimité, de garantir le caractère éphémère de ce qui se
dit sur ce réseau, et de fournir un arbitrage intéressant entre les deux
premiers non-négociables.

» Globalement, si ce que tu dis mérite d'être repris et/ou sauvegarder, il
finira par arriver sur Plurality. Cela implique que plusieurs personnes
aient accéder au message, et qu'au moins l'une d'entre elle le juge
pertinent.

» Rumeurs.net est donc un protocole, une organisation et un manifeste. Le
pendant bordélique et temporaire de l'exhaustivité de Plurality. Et les
deux cohabitent, dans une recherche d'équilibre, oscillant entre les
conflits, bénéficiant des évolutions de l'autre et s'adaptant aux nouveaux
usages et aux nouvelles arrivantes. Ce n'est pas parfait, mais cet
équilibre convient à l'essentiel des personnes ici.

— Deux faces d'une même pièce donc, conclut Saina. Ce que je ne comprend
pas, c'est comment vous pouvez détecter des patterns dans rumeurs.net sans
enregistrer l'ensemble des conversations.

— Analyse en temps réel, extrapolation et définition de tendance, lui
répond T0n. C'est aussi à ça que servent les personnes qui bossent ici.
Plut que d'enregistrer les données directement, on enregistre des
descripteurs de ces données. En y ajoutant suffisamment de bruit
différentiel, on obtiens un aperçu suffisamment précis pour pouvoir
l'étudier, et avec un échantillonnage suffisamment grand que désanonymiser
la base est coûteux.

» Voilà pour l'histoire. C'est un peu plus clair pour toi ?

— Ouais. Je ne suis pas sûre de tout bien comprendre, mais vos archives
aideront. Par contre, je ne voit pas vraiment comment aider avec vos
tempêtes de Tchevenkov moi.

— Uncanny Valley nous pose problème. Pas tellement à cause de la tempête,
dit Nakato, mais plus avec la forme de celle-ci. De son œil pour être
précise. Une tempête de cette taille, c'est rare. On en voit
essentiellement en période électorale et à l'extérieur. Une tempête stable
c'est encore plus rare. Je ne voudrait pas te donner trop de détails avant
que tu ne te fasse une idée, mais leurs derniers manifestes produits
parlent de passer au stade suivant de l'évolution, de transcender la
nature humaine. En arrosant le tout de nihilisme. Et juste après cette
tempête.

» De ce que l'on a compris, il y a un concept qui semble s'être installé
chez eux, et qui a commencé à contaminer les personnes aux idées proche
des leurs. Mais il semble que cette tempête ait trouvé un équilibre, ne
contaminant pas plus de concepts, tout en assimilant et digérant les mèmes
et concepts externes. Bref, nous n'avons pas les outils pour comprendre,
ce que l'on voit n'est pas cohérent avec nos modèles.

— Donc, on essaye de changer de modèle, l'interromps T0n. Et tu semble en
avoir un différemment du notre. On veut juste que tu regarde et que tu
nous dise ce que c'est, si ça représente un risque ou si on peut laisser
vivre.

— Je n'ai pas un autre modèle que vous. Je suis soumises aux mêmes
informations que vous et il est probable que j'en tire les mêmes
conclusions. Mais je comprends votre raisonnement et, effectivement, ça ne
coûte pas grand chose d'essayer, au pire on ne trouve rien de nouveau,
c'est ça ?

— Voilà. Il te faut quoi pour travailler ?

— Déjà des accès aux données. Et d'une assistante, quelqu'un qui,
idéalement, n'est pas encore trop influencée par vos modèles, et des
drogues. Mais ça, je sais qui peut m'aider. Ah, et j'aurais besoin d'un
adaptateur pour ma broche neurale, je ne suis pas sûre que les connexions
classiques que vous utilisez fonctionnent.

— D'accord. On trouvera quelqu'un, j'ai déjà une idée de qui. Pour le
reste, travaille à ton rythme. Si tu veux être tranquille, je te conseille
de grimper une dizaine d'étages, il n'y a globalement personne qui vit si
haut sans ascenseurs fonctionnels. »

Et la réunion se termine. Saina reste un peu dans la salle désertée par
les memakers, et se laisse porter par le bruit ambiant, pour essayer de se
faire une idée un peu plus précise. Elle bascule son masque sur ses yeux
et adapte sa respiration pour un contact de surface.

Maintenant qu'elle y regarde de plus près, elle voit ce que les autres
appellent une tempête de Tchevenkov. Quelque part dans le champ sémantique
identitaire, figée dans un calme apparent, la tempête est là, détachée des
autres concepts. Et elle sent la même pulsation que celle qui l'a amenée à
se mettre en route.
