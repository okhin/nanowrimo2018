# Chapitre 10.

> [ rumeurs.net ][ #check_this_out }
> 
> Jizx > J'ai réussi à mettre la main sur un des vieux enregistrements des
> Valkyries, c'est par là [checkthis.out/hash/dfgyuk4ghjk5678gh2345hik]
> 
> /dave/null > C'est probablement encore une arnaque. Les Valkyries n'ont
> jamais sorti d'album.
> 
> T0n > Ben, il y a des gens qui prétendent l'avoir écouté partout pourtant,
> il doit bien exister ?
> 
> /dave/null > Venant d'un memakers, j'aurais globalement tendance à ne pas
> te croire sur parole, tu m'excuseras.
> 
> Reid * downloading [checkthis.out/hash/dfgyuk4ghjk5678gh2345hik]
> 
> Absurd * downloading [checkthis.out/hash/dfgyuk4ghjk5678gh2345hik]
> 
> /dave/null > Venez pas pleurer si vous choppez encore une infection avec
> ça.
> 
> Jizx > Rabat-joie.
> 
* * *

Émergeant doucement de sa chambre, Saina se dirige vers la cuisine en vue
de se faire un café. Elle est fourbie de courbatures, son estomac essaye
de lui faire comprendre qu'il serait opportun de se débarasser de l'alcool
qu'elle a encore dans son organisme, et, en plus de la migraine, des
acouphènes perturbent ses processus cognitifs.

Dans ce piteux état, elle remarque à peine que Tonya est partie, emportant
presque toutes les fringues qu'elle avait laissées sur le sol hier soir. Ou
ce matin.

Dans le couloir, elle manque de percuter Bl0ke qui sort de la cuisin. Un
bol de soupe chaude entre les mains il lui adresse un vague bonjour, les
yeux perdus dans le vague, parcourant les rushs de son prochain XPlog

L'odeur du café chaud et des gâteaux fraichement sortis du four
l'accueille lorsqu'elle franchi le seuil de la cuisine.

« Pas trop dur le réveil ? demande Joral qui termine la préparation de
beignets de fleurs

— Si. C'est quoi ? répond Saina, désignant du menton les patisseries
étalées sur la table.

— Mon remède contre les matins difficiles. Banane, dattes, diverses noix,
et de la pomme. Tu farcis un genre de chou a la crème avec, et voilà, plus
qu'à cuire. Oh et de la cannelle bien entendu. Tu verras, ça fait passer
les migraines et les courbatures.

— Merchi, répond Saina un des gâteaux dans la bouche, chest chuper bon.

— Fait passer ça avec un grand verre de jus. Je sais, tu aimes ton café
matinal, mais bois un peu plus de truc avec des fruits, ça aide à remonter
la pente. Crois en ma longue, et douloureuse, expérience.

— C'est pour ça que je t'aime bien Joral », lui dis Tonya entrant dans la
cuisine, une tasse de thé brûlant entre les mains. Elle s'installe au
comptoir avant de s'adresser à Saina. « Tu vois, Joral est peut-être trop
curieux et fourre son nez partout où il ne met pas la langue ou les
doigts, mais il se plante jamais sur la bouffe. Ou alors uniquement de
façon spectaculaire.

— Venant de toi, je vais prendre ça pour un compliment.

— C'en est un, ingrat. Bien dormi Saina ?

— Oui. Tu es réveillée depuis longtemps ?

— Pas tant que ça, mais j'ai tout fait pour te laisser dormir, tu avais
l'air d'en avoir besoin.

— Merci. »

La conversation entre les trois s'installe doucement, partageant les
détails de la soirée, avec plus ou moins de détails ou de replay en
fonction des situations. Elles parlent doucement, lentement, au ralenti,
en accord avec les mélodies cuivrées que Val et Bloke bricolent sur leurs
consoles. Nakato, vétue d'une tunique noire et portant des mules en forme de lapins
roses, rejoint le groupe en se préparant un moka très serré.

Ne participant qu'à peine à la conversation, Saina laisse ses pensées la
porter, parcourant les souvenirs pus ou moins flous des derniers jours.
Ses oreilles sifflent toujours, les acouphènes entretenant sa migraine,
rendant presque impossible pour elle de suivre la conversation.

Le sifflement se module et se distord en de longues plaintes éthérées
suivi de silence pesant. Activant ses processus associatifs, ils se
transforment lentement et deviennent proches du signal qu'elle a entendu
en Sibérie et qui l'a incité à venir ici. Quelques parts entre vents
solaires et chants de baleines, le signal se reconstruit lentement dans sa
tête et elle se met, inconsciemment, à fredonner les modulations.

« Tout va bien Saina ? lui demande Tonya, la sortant de sa rêverie.

— Hein ? Ah … euh … oui oui, ça va. À part ce mal de crâne qui passe
pas.

— Dis, t'as pu réfléchir un peu à ce que tu as vu hier ? T0n a essayé de
me faire un résumé, mais je ne suis pas sûre qu'elle ait compris ce dont
tu parlais, lui demande Nakato.

— Si Joral me ressert un café, je vous explique. Je suis pas sûre du
tout de ce que j'ai vu, et c'est difficile d'y réfléchir en raison de la
nature même de ce … truc, à défaut de mieux.

» Habituellement, je me mets à l'écoute du bruit de fond puis, en
conjurant les filtres nécessaire, j'isole des particularités du paysage de
données. Souvent des choses que l'on ne peut pas vraiment voir sans faire
tourner une grosse base de donnée.

» Pour faire simple, j'ai un bon instinct pour détecter ce qui sort du
naturel, ce qui me permet d'agir vite dessus. Passer outre la syntaxe et
le formalisme, pour me concentrer sur le sens et la sémantique et donner
des formes à ce que je vois. Je suis pas sûre d'être claire. Votre niveau
de théories des langages est comment ?

— Expliques nous ça comme si on était des enfants de cinq ans extrêmement
intelligents, répond Tonya.

— Bon, un langage, une langue, est souvent perçue comme une structure du
sens permettant à différentes personnes parlant la même langue de
communiquer, d'échanger des informations. Un signifiant commun décrivant
un signifié, en gros.

» Mais je pense que c'est une version qui ne correspond pas à la réalité.
Après tout nous avons des systèmes non conscients qui savent nous parler,
nous donner des informations. Est-ce que pour autant ils communiquent avec
nous ? Non, je ne pense pas. La communication nécessite une intention, une
volonté d'expliquer à d'autres un état mental, une perception du monde.

» C'est pour cela que l'on utilise une langue. Le sens — le signifié
— n'est pas dans les mots — le signifiant, mais quelque part dans notre
conscience. Les mots, les verbes, les phrases, sont des projections d'un
espace sémantique personnel sur un autre, en utilisant une syntaxe
commune. Vous suivez toujours ?

» Si un mot nous évoque toutes les deux la même chose, nous avons vécu des
expériences différentes qui font que nous n'aurons pas exactement la même
représentation mentale d'un objet. Si je te parle d'une table, on va
toutes les deux imaginer des objets physiques remplissant les mêmes
fonctions, mais qui auront des formes différentes.

» Cette différence de compréhension est due à l'accumulation d'expériences
personnelles tout au long de notre vie. Confrontées à des stimuli
identique et dans des contextes identiques, nous vivrons pourtant des
expériences différentes. Et je ne peux pas transmettre ces expériences, ni
les partager pleinement, c'est d'ailleurs ce qui les définit. Je peux
les évoquer afin que tu puisse imaginer et comparer avec tes propres
expériences mon vécu, mais tu ne vivras jamais exactement la même
expérience que moi.

» Et la différence entre le signifiant et le signifié, l'écart entre les
deux, définit, en gros, une forme d'entropie. Vu que les expériences sont,
nécessairement unique, il ne peut exister une langue qui les transmette.
On aura donc des langues avec différents niveau d'entropie, tendant vers
l'infini mais ne l'atteignant jamais. Ces langues évoluent au fur et
à mesure que nous vivons différentes expériences, mais aucune ne pourra
jamais transmettre l'intégralité d'une expérience, peu importe le nombre
de signaux utilisés pour ça. Y parvenir amènerait à nécessiter une
entropie infinie, une imprévisibilité par rapport au contexte.

» Tu ne peux pas décrire une culture, de la même façon que décrire tes
expériences dans le but de permettre à quelqu'un d'autre de vivre
exactement cette même expérience, est impossible. Des artistes utilisant
divers moyens, pas uniquement des mots, essayent depuis des siècles et des
millénaires de le faire, et certains s'approchent parfois, ou arrive
à rendre la syntaxe suffisamment complexe que l'on peut la confondre
avec le sens, mais personne n'a réussi à trouver un langage à entropie
infinie. À cause de la subjectivité. »

Saina marque une pause pour manger un des choux post gueule de bois que
Joral continue de poser sur le comptoir au fur et à mesure qu'ils sortent
du four.

« Vous suivez toujours ? On arrive dans la partie rigolote. Si on regarde
la plupart des religions, monothéistes ou non, il y a nécessairement un
lien entre le divin et le langage. Les juifs n'écrivent jamais le nom de
leur dieu par exemple, les musulmans lui donnent des centaines de noms,
correspondant à autant d'aspect, les chrétiens ont également un rapport
intéressant avec la langue — que ce soit par la pentecôte ou le début de
l'évangile de Jean. Les runes et les oghams disposent d'un sens sacré et
d'un sens non-sacré, les occultistes de tout genre cherchent des symboles
tout autour d'eux, et les sikhs eux vénèrent une parole, un texte sacré.

» Même ici, les mots définissent les choses, les concepts. On peut aussi
faire des parallèles intéressants avec les maths qui permettent, par la
conceptualisation faites par les nombres, de décrire dans un espace fini
des concepts infinis. Tels que pi, au hasard.

» Bref, notre conscience est un processeur qui exécute une langue pour
faire un parallèle foireux avec l'informatique. Et en fonction de la
langue, des nuances importantes peuvent disparaître lors de son
utilisation. C'est pas pour rien que les traductrices font encore un
meilleur travail que nos ordinateurs quand il s'agît de transcrire le sens
et les nuances d'un texte d'une langue à l'autre.

» Mais quoi qu'il en soit, le signifiant et le signifié ne seront jamais
égaux. Notre conscience fait en sorte que ce ne soit pas le cas. Il y a eu
de nombreux débats et expériences de pensées à ce sujet, avec des
histoires de clones parfait, de transfert de conscience ou de capacité
à numériser sa conscience. Dans tous les cas, la conclusion est que passé
la première seconde d'existence, deux copies parfaites commencent
à diverger.

» Et donc, en théorie, il n'est pas possible d'avoir un message qui aurait
soit une entropie nulle, c'est à dire qui n'évoque aucun sens, soit une
entropie infinie, c'est à dire qu'il est lui-même conscient, n'en déplaise
aux fans de SF.

— Tu dis théoriquement, signale Tonya, il s'est passé quelque chose ?

» Je ne sais pas. C'est difficile d'y réfléchir, je suppose en partie
à cause de ce que j'ai vu. Ou plutôt de ce que je n'ai pas vu. On se
baladais avec Nato dans DreamLand, à essayer de trouver où Uncanny pompait
ses données. Et nous avons trouver un espace d'information nulle. Ou
infinie, ça revient au même dans ce cas, notre cerveau ne parvient pas
à le comprendre.

» Et ça a fait quelque chose, il me manque deux heures de plongée, Nato un
peu moins, elle a été exposée plus tard. Et même si le temps est subjectif
quand je me promène là bas, je suis certaine qu'il me manque deux heures.
Depuis, dès que j'essaye de comprendre ce que j'ai vu, j'ai une migraine
monstrueuse et des sifflements dans les oreilles.

» Mais ce dont je suis sûre, c'est qu'Uncanny a connecté énormément de ses
ressources pour aller à l'intérieur de cette anomalie. Et tout cela n'a
aucun sens.

— Tu dis donc qu'Uncanny Valley est liée, d'une manière ou d'une autre,
à quelque chose que nos visions du monde ne peuvent comprendre , demande
Nakato.

— Je ne dis rien. Je sais juste qu'ils travaillait avec quelque chose qui
ne peut se transcrire dans un espace mathématique ou linguistique. Et la
seule chose que je connaisse qui ait cette propriété là, c'est une ou
plusieurs expériences.

— Et donc, une conscience, si j'ai bien suivi.

— Exactement, Tonya. Mais ce n'est pas censé être possible. Pas avec ce
que l'on sait actuellement de la science.

— Et qu'en dit ta philosophie ?

— Ça me dit qu'il y a un esprit dans la machine. Mais ce n'est pas censé
arriver non plus. En général, je perçoit des avatars. Des projections
d'une conscience si tu veux. Et ces avatars, aussi complexes soient ils,
ne sont eux-mêmes que des projections.

» Bref, j'ai aucune idée de ce que ça pourrais être, je peux juste essayer
de deviner. Mais ça me file des migraines monstrueuses, pas uniquement
parce que c'est étrange. Comme si mes pensées étaient compressées et
auraient besoin de plus d'espace pour s'ouvrir.

— D'accord, la suite des opérations, c'est quoi ? demande Nakato

— J'irai bien voir là-bas. Eux ils arrivent à s'y connecter, ils ont donc
trouvé un moyen de contourner le problème sur lequel je butte. Il faudrait
aussi que je mette la main sur les carnets noirs d'Elliot. Je sais pas
pourquoi mais mon intuition me dit que c'est lié, d'autant que, récemment,
Uncanny jouait avec.

— Et tu penses qu'ils ont réussi à comprendre ce que ces foutus carnets
contiennent ? Je les ai parcourus à l'époque et ça n'avait aucun sens, dit
Tonya.

— Ça en a forcément, c'est pas parce que tu ne le vois pas qu'il n'y est
pas. Il y a peut-être un genre de clef de déchiffrement nécessaire pour le
comprendre, ou quelque chose du genre.

— D'accord, d'accord, il y a peut-être un truc lié à ça, je te le concède.
Reste que tu n'as pas expliqué ce qu'il s'est passé pendant ces deux
heures.

— Aucune idée. Nato m'a dit qu'elle m'avait vu me figer sur place, comme
si je laguait trop et que un dixième de seconde après j'étais revenue. Je
sais juste que ça a pris plus d'un dixième de seconde.

» Il y a bien des légendes à propos de suggestions implantées, ou de hack
qui permettrait de faire planter temporairement le cerveau humain en
l'exposant à certains stimuli, genre les hacks basilisques et autres
joyeuseté. La beauté du truc c'est qu'il n'est pas vraiment possible de
les enregistrer et de les étudier, car si ils existent, les étudier
signifie s'y exposer.

— Ça a l'air moche, dit Tonya

— Ça n'en as pas que l'air. Mais pour le moment je considère que ce n'est
pas ce que nous avons en face. Le fait que je puisse me rappeler l'avoir
vu sans pour autant tomber à terre me fait penser que même si les
mécaniques semblent similaires, il y a peu de chances que je soit tombée
sur ça.

» Bon, et je ne suis toujours pas persuadée qu'Uncanny Valey soit à la
source de cette idée hein, il est tout autant probable qu'ils soient aussi
tombé dessus et qu'ils essayent de comprendre. Et je suis catégorique,
c'est — au moins partiellement — le même signal que celui qui m'a fait
venir ici.

— Attends, ça veut dire que ça fait quoi, presque six mois que ce truc est
là et qu'on ne l'avait pas vu ? Nakato tu as une idée de pourquoi on ne
l'a pas vu ?

— Parce qu'on ne cherchait pas ce genre de chose. Je ne sais toujours pas
comment on pourrait le détecter d'ailleurs, répond l'opératrice.

— Qu'est-ce qui t'as mis sur la piste ? lui demande Saina.

— Pas grand chose. Un instinct, ça fait six mois qu'on a pas vu grand
chose venant d'Uncanny, puis ils publient leur manifeste et retourne en
silence radio. J'ai commencé à chercher, j'ai vu des logs bizarre, une
consommation de données qui ne correspond pas à une utilisation
habituelle selon leurs critères, mais je n'en air rien tiré de concret,
à part peut-être une intuition que quelque chose ne va pas.

— Et quand je t'ai parlé de Saina l'autre jour, tu t'es dit que son
approche permettrait de faire ressortir ce qui t'échappes et que dans le
pire des cas, une paire d'yeux supplémentaire ne pourrais pas faire de
mal ?

— En gros oui. »

Tonya, Nakato, Joral et les autres habitants de la médina qui passent par
là, poursuivent la discussion, se posant la question de savoir quoi faire.
Le ton monte parfois, à mesure que les points de vue s'articulent et se
confronte, principe de précaution contre interventionnisme. Laisser faire
contre consentement. Les points de vue tranchés stérilisent rapidement la
discussion, celle-ci nécessitant que les participantes prennent un peu de
temps pour réfléchir à ce qui a été dit.

Au fil de la discussion, qui se déroule aussi bien sur rumeurs.net que
dans la cuisine, une bibliographie de textes commence à se constituer
autour du thème du langage, sur les proto-langage ou les théories de de
communication inter espèces.

Saina se retrouve en retrait, un peu isolée, perdue dans ses pensées et au
milieu de ces personnes qui ont toutes l'air de se connaître très bien, de
partager une intimité en groupe et convenant à tout le monde. Elle se sent
mise à l'écart, abandonnée, oubliée, et se laisse petit à petit gagner par
l'angoisse. La migraine se réveille alors que les bruits de la
conversation sont étouffés par le chant des baleines qui résonne
maintenant dans sa tête à plein puissance.

Tonya la prend alors par la main et l'amène dehors, sous la fine pluie
d'automne qui tombe depuis la matinée.

« Tu n'as pas l'air bien. Je peux faire quelque chose ?

— Je sais pas. La migraine reviens, mais c'est plus … C'est pas facile
d'intégrer ce groupe, vous vous connaissez toutes très bien et là, j'ai un
peu l'impression d'être la cinquième roue du carrosse.

— Oh chérie, désolée. J'ai été absorbée par la discussion, et oui, il va
falloir du temps pour qu'on t'intègres bien. Désolée de t'avoir laissé un
peu à l'écart.

— C'est pas tellement ça. C'est juste, tout ça est très nouveau pour moi,
passer plus de cinq heures par jours avec d'autres personnes autour c'est
étrange, j'ai pas l'habitude, je sais pas ce que tu ressent pour moi, ou
comment les autres me voient ou si vous me prenez pas pour une illuminée,
et …

— Ralentis d'accord, j'aime beaucoup être avec toi, même si on baise pas
et qu'on ne fait que des câlins, c'est très cool aussi. Et j'aime aussi
beaucoup d'autres personnes. Et non, je ne te vois pas comme une
illuminée, d'accord ? »

Elle l'embrasse alors qu'elle reste toutes les deux sous un auvent qui les
protègent de la pluie.

« Viens, dit Tonya, on va s'occuper les mains, il faut qu'on ajoute un
petit jardin à ta piaule, probablement sur le toit, ça te dis ?

— Sous la pluie ?

— Justement, c'est le meilleur moment. Et si t'as peur de te salir, fait
comme moi, vire le haut, et enfile une de ces bleus de travail 
que tu dois bien avoir quelque part.

— Quand tu parle de jardiner tu parles bien de trucs qui impliquent de la
terre et des pantes, c'est pas un sous-entendu quelconque ?

— Possible, tu ne le sauras que si tu viens. » Dit elle en ayant déjà
enlevé son T-shirt pour le laisser au sec, avant d'aller attacher son
baudrier à l'une des cordes qui tombent dans la cour et qui permette
d'atteindre les carrés végétaux installés sur le toit.
