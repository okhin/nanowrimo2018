# Chapitre 19.

> [ Plurality/r_34 ]
> 
> S4cha > Dites, on essaye de s'assurer que tout le monde est couvert point
> de vue ressources minimales, du coup si vous avez des surplus ou s'il
> vous manque quelque chose, vous faites signe ?
> 
> S4cha > Et si vous pouviez laisser un peu de bande passante ici pour ça,
> ce serait pas mal. C'est beaucoup vous demander à vous, je sais, mais
> pensez aux autres.
> 
> U+1f351 > Mais oui, genre on passe notre journée à catégoriser, archiver,
> mater et faire du porn, du coup ça bouffe toute la bande passante.
> 
> U+1f346 > Nan, mais au moins, quand on fap, on parle assez peu ici. Du
> coup, ce que veut dire S4sha c'est qu'il faut qu'on se magne d'uploader
> blackout toi et moi. Et les autres aussi.
> 
> S4cha > Voilà. Quelque chose du genre. Je crois.
> 
* * *

Émergeant des rêves éveillés, des flashbacks et de l'infernale migraine
qui l'affecte depuis plusieurs jours, Tonya finit enfin par rejoindre un
état de conscience compatible avec une réflexion logique. Pendant que ses
souvenirs, mêlés à d'autres, se dissipent peu à peu, elle essaye de se
rappeler ce qu'il s'est passé ces derniers jours.

Son port de données temporal la brûle et les capteurs synaptiques associés
sont au rouge sous la surcharge de données qui semble y transiter. Elle se
rappelle des douleurs fantômes, du même type que ce qu'elle a ressenti
depuis que sa jambe a été amputée suite à une très mauvaise chute. Sauf
qu'elle a bien deux jambes. Et sensibles en plus, ajoute t elle
mentalement après s'être pincé fortement la jambe en question.

Son système cortical termine de combattre l'infection mémétique qu'il
a subit, revenant à un état proche de son état d'origine, même s'il ne
sait pas quoi faire de certains souvenirs présents qui continueront,
de réveiller Tonya la nuit pendant encore un long moment.

Mais maintenant elle se sent mieux. Elle profite de la vue depuis la
pergola où elle a réussi à se traîner encore quelques instants et, après
avoir terminé un bol de chocolat chaud que Br0k3 est passé lui déposer,
elle se dirige vers la cuisine. Faible, elle parvient néanmoins
à rejoindre le territoire de Joral, duquel s'échappe les effluves épicées
de ses dernières expérimentations culinaires.

Après s'être requinquée et avoir englouti une bonne part des pâtisseries
expérimentale de Joral, Tonya est de nouveaux prête à affronter le monde
extérieur. Son link, cependant, refuse de se connecter à quoi que ce soit
d'autre que rule 34.

« Il y a un problème avec Plurality ? demande-t-elle à Joral.

— On peut dire ça oui. Tout est à peu près down, on ne sait pas bien
pourquoi encore. Saina pense que c'est sa faute, Nakato est pas passée par
ici depuis deux jours, je suspecte qu'elle supervise les opérations
d'analyse depuis Ivory Tower. Et Sacha devrait pas tarder à revenir avec
Br0k3 on essaye de faire un point sur l'état du Complexe, afin de pouvoir
filer un coup de main.

— Ah ouais, je pars quelques heures, et vous foutez tout en l'air.

— Mais non, mais non, pas tout. La plupart des données existent encore,
c'est juste Plurality qui est HS. ESP a ouvert une fréquence que tout le
monde squatte en SDR afin d'avoir un remplacement à rumeurs.net, et il
y a une fête permanente au Billionaire. Je crois que Saina y est
d'ailleurs.

— Elle va bien ?

— Aucune idée. Elle avait l'air secouée par ton état en tout cas. Mais
comme tout le monde, elle essaye de comprendre ce qu'il se passe. Elle est
cependant persuadée que tout est de sa faute. Nato est cependant parti la
rejoindre depuis une douzaine d'heure, donc je pense que la situation est
sous contrôle.

» Tu te sens comment toi ? Tu nous as fais un peu flippé. Heureusement que
Br0k3 et Val sont un peu calés en overdose cognitive. Si on m'avais dit
que ça pourrais arriver à quelqu'un qui passe si peu de temps en X, je
l'aurais pas cru.

— Doucement, j'ai encore une bonne gueule de bois. J'ai la tête comme le
as de tir d'ESP après un de leurs lancement. En feu.

Val arrive dans la cuisine, et après avoir posé quelques questions un peu
étrange à Tonya, elle se détend et laisse partir l'inquiétude qui
l'affecte.

« Bon, tu feras gaffe, il est parfaitement possible que tu fasses des
rêves un peu étrange, ou que tu soit persuadée de savoir faire quelque
chose de particulier pendant les prochains jours. Les flashbacks vont
disparaître, le temps que les circuits de continuité de personnalité de
ton cerveau retrouvent leurs petits.

» Mais à part ça, c'est terminé. Je suppose que Joral t'as mis au courant
de la situation ?

— Rapidement. Je peux aider comment ?

— J'allais passer à la tour 4. Sasha et ses Amazons essayent de coordonner
le réseau de solidarité. Et j'ai vu Nato et Saina discuter ensemble
là-bas. Je suppose que cela veut dire qu'IT n'a pas vraiment besoin
d'aide. Ou n'a toujours aucune idée de ce à quoi ils font face.

» Le bon côté des choses c'est qu'il ne manque rien à personne, ou
presque. La plupart des collectifs sont en train de faire un inventaire de
ce qu'ils ont, en données ou en produit de première nécessité, mais l'un
dans l'autre, tout va bien. Moi j'allais là-bas pour filer un coup de main
à pest-control : il y a quelques corvidés qui se plaignent un peu de la
perte de leur réseau, et deux trois bestioles dont on a pas de nouvelles.

» Tu veux venir ?

— Laisse-moi deux minute le temps de prendre une douche et on y va. Ça
fait longtemps que j'ai pas essayé de négocier avec ces pies.

— OK, ben je t'attends ici avec Joral, prend ton temps, moi je vais
combattre mon hyperglycémie avec un peu de son excellent café.

Lorsque Tonya revient dans la cuisine, les cheveux encore humides, Sasha
a rejoint Park et Joral dans leur dégustation de café. Celle qui
a installé le chapitre local des Amazons avale en silence une tasse d'un
café torréfié et assemblé par Joral, adossée au comptoir, appuyant son
blouson renforcé délavé aux couleurs du syndicat, détournant le logo de la
méta-corp. Ses trente ans de vie ici, et ses années de combat
syndicalistes l'on marquée plus qu'elle ne voudrait l'admettre, mais n'ont
en aucun cas émoussé ses convictions ou sa détermination. Tout pour sa
famille a t elle l'habitude de dire. Et le Complexe est sa famille. Même
si parfois il faut prendre certaines libertés avec les non négociables, il
est indéniable que l'Amazon aux cheveux gris est une des figures
fondatrice du Complexe, même si elle aura toujorus tendance à réduire son
implication, mettant en avant les autres.

« Ravie de voir que tu vas mieux, dit-elle avec un sourire lorsque Tonya
rentre dans la cuisine, vêtue d'un de ses pantalons de travail et d'un
T-Shirt sous son teddy des Valkyries, les cuisses prises dans le harnais
multi-outil qui fait partie intégrale de son corps et de son identité.

— Je vais mieux oui. Je ne sais pas si je vais bien, mais c'est une
question pour plus tard. On y va et vous me brieffez en route ? »

Après avoir terminés leurs cafés, elles se mettent en route vers la tour
quatre, évoluant lentement dans les souks surchargés de monde. Une foule
de personnes cherchant à savoir ce qu'il s se passe et comment elles
peuvent aider de mêle à la faune habituelle.

Et en dépit de la fatigue manifeste, d'une certaine tension et de
l'impression de gueule de bois palpable dans l'atmosphère moite, les
personnes échangent, discutent, courent, rient, jouent et pleurent. Elles
continuent de partager leurs expériences et leurs points de vue sans
nécessiter un recours à Plurality.

Mais ce qui intrigue le plus Tonya, ce sont les langues parlés. Sans les
systèmes de traductions et de transposition culturels fournis par
Plurality pour faciliter les échanges entre personnes n'ayant que peu de
points communs culturels, une langue syncrétique a commencé à se
développer. Cet argot urbain post-globalisation n'est pas une langue
écrite, elle n'est pas une langue de symboles, mais de contexte.

Tonya a du mal à suivre, en un peu moins de trois jours, par la force de
la nécessité, le Complexe s'est doté de nouvelles langues. Ce slang manque
de finesse, mais le besoin n'est pas encore celui-ci. La finesse, la
subtilité, les nuances se véhiculent autrement, mais cette langue permet
à tout le monde de travailler ensemble. 

Et dans tout ce brouhaha, Sasha et Park essayent d'expliquer à Tonya ce
qu'elles ont compris de ce que Saina leur a dit un peu plus tôt, à l'issue
d'une longue discussion avec Nato. Elles y ont passé la nuit, mais Saina
commence à avoir une idée un peu plus précise de ce qu'il se passe.

« Je ne comprends toujours pas ce qu'elles disent toutes les deux,
notamment depuis qu'elles mélangent mysticisme et maths, dit Sasha, alors
qu'elles arrivent dans la tour 4, mais elles ont l'air relativement sûre
d'elle. Ce qui est une nette amélioration par rapport à l'état dans lequel
était Saina hier soir. »
